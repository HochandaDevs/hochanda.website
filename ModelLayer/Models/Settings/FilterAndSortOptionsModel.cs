﻿using Jil;
using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Settings
{
	public enum SortBy
	{
		Relevance = -999,
		Newest = 1,
		AZ,
		ZA,
		HighToLow,
		LowToHigh
	}

	public static class SortByHelper
	{
		public static string GetSortByText(SortBy sortBy)
		{
			switch (sortBy)
			{
				case SortBy.AZ:
					return "A - Z";
				case SortBy.HighToLow:
					return "High To Low";
				case SortBy.LowToHigh:
					return "Low To High";
				default:
				case SortBy.Newest:
					return "Newest";
				case SortBy.ZA:
					return "Z - A";
				case SortBy.Relevance:
					return "Relevance";
			}
		}
	}

	public class SortByValuesToDisplay
	{
		public string ValueToShow { get; set; }
		public int ValueToUse { get; set; }
	}

	public class FilterAndSortOptionsModel
	{
		public FilterAndSortOptionsModel(bool addSearchRelevance = false)
		{
			SortByDictionary = new Dictionary<string, int>();
			SortByValuesToDisplay = new List<Settings.SortByValuesToDisplay>();

			var allSortBy = Enum.GetValues(typeof(SortBy)).Cast<int>().OrderBy(i => i);
			foreach (var sortBy in allSortBy)
			{
				bool canAdd = true;

				if (!addSearchRelevance && sortBy == (int)SortBy.Relevance)
				{
					canAdd = false;
				}

				if (canAdd)
				{
					SortByDictionary.Add(SortByHelper.GetSortByText((SortBy)sortBy), sortBy);
					SortByValuesToDisplay.Add(new Settings.SortByValuesToDisplay() { ValueToShow = SortByHelper.GetSortByText((SortBy)sortBy), ValueToUse = sortBy });
				}
			}



			Categories = new List<CategoryFilterModel>();
			Brands = new List<BrandFilterModel>();
			//Default newest
			if (addSearchRelevance)
			{
				SortByValue = SortBy.Relevance;
			}
			else
			{
				SortByValue = SortBy.Newest;
			}

			FilteredCategories = new List<FilteredModel>();
			FilteredBrands = new List<FilteredModel>();
		}

		public List<SortByValuesToDisplay> SortByValuesToDisplay { get; set; }

		public List<FilteredModel> FilteredBrands { get; set; }
		public List<FilteredModel> FilteredCategories { get; set; }

		public Dictionary<string, int> SortByDictionary { get; private set; }

		[JilDirective(TreatEnumerationAs = typeof(int))]
		public SortBy SortByValue { get; set; }

		public bool? IsFreedom { get; set; }

		public bool? IsFlexibuy { get; set; }
		public bool? IsDownload { get; set; }
		public bool? IsPickAndMix { get; set; }

		public List<CategoryFilterModel> Categories { get; set; }

		public List<BrandFilterModel> Brands { get; set; }

		private int _PageNumber = 1;
		public int PageNumber
		{
			get
			{
				return _PageNumber;
			}
			set
			{
				if (value <= 0)
				{
					value = 1;
				}

				_PageNumber = value;
			}

		}
		private int _PageSize = 20;
		public int PageSize
		{
			get
			{

				return _PageSize;
			}
			set
			{
				if (value > 200)
				{
					value = 200;
				}
				else if (value < 20)
				{
					value = 20;
				}

				_PageSize = value;
			}

		}

		public int PageCount { get; set; }

	}
}
