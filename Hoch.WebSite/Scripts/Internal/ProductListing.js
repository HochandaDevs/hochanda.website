﻿app.controller("prodListHeaderCtlr", ['$scope', 'CommonCode', '$http', '$rootElement', '$rootScope', function ($scope, CommonCode, $http, $rootElement, $rootScope) {
    CommonCode.getBaseCookieSettings();
    $scope.commonCode = CommonCode;

    $scope.IsLoggedIn = 0;
    var path = window.location.pathname;
    var custRef;
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    var custEmail;

    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        custRef = keepMeSignedIn.CustomerLookupId;
        custEmail = keepMeSignedIn.Email;
        $scope.IsLoggedIn = 1;
    }

    $scope.BrandHeader = [];
    $scope.CategoryHeader = [];

    var favourites = JSON.parse(localStorage.getItem("userFavourites"));

    if (path.indexOf('brand') != -1) {
        //var brandid = 288;//for testing
        var splitStr = path.split('/brand/')[1];
        var brandid = splitStr.split('/')[0];

        $http({
            method: 'POST',
            url: baseUrl + "Brands/GetBrandHeader",
            contentType: 'application/json',
            data: JSON.stringify({ LanguageId: CommonCode.LanguageId, CurrencyId: CommonCode.CurrencyId, CountryId: CommonCode.CountryId, TryingAgain: false, BrandId: brandid })
        }).then(function (result) {

            $scope.BrandHeader = result.data;
            
            var url;
            var splitstr;
            $scope.brandIsFav = 0;
            if (favourites != null) {

                var favpages = favourites.pages;
                for (var i = 0; i < favpages.length; i++) {
                    url = favpages[i].pageurl;
                    if (url.indexOf('/brand/') != -1) {
                        splitstr = url.split('/brand/')[1];
                        splitstr = splitstr.split('/')[0];
                        if (brandid == splitstr) {
                            $scope.brandIsFav = 1;
                        }
                    }
                }
            }           

        });
    }
    if (path.indexOf('category') != -1) {
        //var categoryid = 57;// for testing
        var splitStr = path.split('/category/')[1];
        var categoryid = splitStr.split('/')[0];
        $http({
            method: 'GET',
            url: baseUrl + "CategoryMenu/GetCategoryHeader?categoryId=" + categoryid + "&languageId=" + CommonCode.LanguageId + "&currencyId=" + CommonCode.CurrencyId + "&countryId=" + CommonCode.CountryId,
            contentType: 'application/json',
            //data: 1
        }).then(function (result) {
            $scope.CategoryHeader = result.data;

            var url;
            var splitstr;
            $scope.catIsFav = 0;
            if (favourites != null) {
                var favpages = favourites.pages;

                for (var i = 0; i < favpages.length; i++) {
                    url = favpages[i].pageurl;

                    if (url.indexOf('/category/') != -1) {
                        splitstr = url.split('/category/')[1];
                        splitstr = splitstr.split('/')[0];

                        if (categoryid == splitstr) {
                            $scope.catIsFav = 1;
                        }
                    }
                }
            }
            
        });
    }

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }
}]);

app.controller("productListingController", ['$scope', 'CommonCode', '$http', '$rootScope', function ($scope, CommonCode, $http, $rootScope) {
    var urlSplit = '';
    var urlToUse = '';
    var isBrand = false;
    var isCategory = false;
    var path = window.location.pathname.toLowerCase();
    $scope.IsLoggedIn = 0;
    var isBrandAmbassador = null;

    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        custRef = keepMeSignedIn.CustomerLookupId;
        custEmail = keepMeSignedIn.Email;
        $scope.IsLoggedIn = 1;
    }

    if (path.includes('brand')) {
        urlSplit = path.split('/brand/')[1];
        urlToUse = "Products/ProductsForBrand";
        isBrand = true;

        var brandIdSplit = urlSplit.split('/')[0];
        isBrandAmbassador = brandIdSplit[brandIdSplit.length - 1].toLowerCase() == "b"
    }
    else if (path.includes('category')) {
        urlSplit = path.split('/category/')[1];
        urlToUse = "Products/ProductsForCategory";
        isCategory = true;

        if (urlSplit != '') {
            idToUse = urlSplit.split('/')[0];
        }
    }

    var idTouse = 0;

    if (urlSplit != '') {
        idToUse = urlSplit.split('/')[0];

        //The id will contain B. So we need to remove it.
        if (isBrandAmbassador) {
            idToUse = idToUse.substring(0, idToUse.length - 1)
        }
    }

    $rootScope.GetProducts(urlToUse, idToUse, isBrandAmbassador);
    $scope.setPartialName = function (name, partialCheck, ppid,ifFavLogin) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);
        }
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }
    $rootScope.GetMetadata(isBrand, isCategory, false, '', idToUse);
    /*if (window.location.href.toLowerCase().indexOf('brands') != -1 || window.location.href.toLowerCase().indexOf('category') != -1) {       
        $rootScope.GetMetadata(isBrand, isCategory, false, '', idToUse);
    }*/
}]);