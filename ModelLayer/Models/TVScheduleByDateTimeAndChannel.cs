﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class TVScheduleByDateTimeAndChannelModel
    {
        public int TvScheduleId { get; set; }
        public DateTime ShowDateTime { get; set; }
        public string ShowName { get; set; }
        public int EPGCategoryId { get; set; }
        public bool IsAuctionHour { get; set; }
    }
}
