﻿using Hoch.Website.Helpers;
using Mandrill;
using Mandrill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Controllers.Refer_A_Friend
{
    public class ReferAFriendController : BaseController
    {
        // GET: ReferAFriend
        [Route("refer-a-friend-welcome")]
		[MetadataActionFilter(MetaDataFriendlyName = "Refer a Friend - Welcome Landing Page")]
        public ActionResult Welcome(string voucher)
		{
            ViewBag.VoucherCode = voucher;
            return View("~/Views/Refer-A-Friend/ReferAFriendWelcome.cshtml");
        }

        [Route("refer-a-friend")]
		[MetadataActionFilter(MetaDataFriendlyName = "Refer a Friend - Share Page")]
        public ActionResult ReferAFriend()
        {
            return View("~/Views/Refer-A-Friend/ReferAFriend.cshtml");
        }

        public class RAFMOdel
        {
            public string vouchercode { get; set; }
            public string emailAddress { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string offer { get; set; }
            public string url { get; set; }
        }

        [HttpPost]
		[ExtendLoginCookie(DoNotExtend = true)]
        public async Task<JsonResult> SendShareFriendEmail(RAFMOdel RAFMOdel)
        {
            bool result = false;
            WebRequestHelper webHelper = new WebRequestHelper();
            if (RAFMOdel.vouchercode != "" || RAFMOdel.emailAddress != "")
            {
                try
                {
                    var api = new MandrillApi("mwikOWlc9HLroBSaEpQkhw");
                    var message = new MandrillMessage();

                    message.FromEmail = "customer.service@hochanda.com";
                    message.AddTo(RAFMOdel.emailAddress);

                    message.BccAddress = "customer.service@hochanda.com";
                    message.ReplyTo = "customer.service@hochanda.com";
                    message.Subject = "Here's" + " " + RAFMOdel.offer + " " + "thanks to" + " " + RAFMOdel.firstname;

                    message.AddGlobalMergeVars("FriendName", RAFMOdel.firstname);
                    message.AddGlobalMergeVars("Discount", RAFMOdel.offer);
                    message.AddGlobalMergeVars("VoucherCode", RAFMOdel.vouchercode);
                    message.AddGlobalMergeVars("Year", DateTime.Now.Year.ToString());
                    //message.AddGlobalMergeVars("LandingPageURL", "https://www.hochanda.com/ReferAFriend/Welcome?voucher=" + vouchercode);
                    message.AddGlobalMergeVars("LandingPageURL", RAFMOdel.url);


                    var rs = await api.Messages.SendTemplateAsync(message, "Refer A Friend - Invite - Blue", null, true);
                    result = true;
                }
                catch (Exception e)
                {
                    return Json(result);
                }
                return Json(result);
            }
            return Json(result);
        }
    }
}