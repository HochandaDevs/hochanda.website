﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class OrderStatusModel
    {
        public int OrderId { get; set; }
        public decimal ToPayFigure_Today { get; set; }
        public string ToPayFigure_TodayWithCurrency { get; set; }
        public decimal DeliveryCost { get; set; }
        public string DeliveryCostWithCurrency { get; set; }
        public int FlexiOrderSelected { get; set; }
        public int FlexiOrderInstallments { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string BuildingNumber { get; set; }
        public string City { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string b_StreetAddress1 { get; set; }
        public string b_StreetAddress2 { get; set; }
        public string b_BuildingNumber { get; set; }
        public string b_City { get; set; }
        public string b_Town { get; set; }
        public string b_County { get; set; }
        public string b_Country { get; set; }
        public string b_Postcode { get; set; }
        public int CurrencyId { get; set; }
        public List<OrderSummaryModel> OrderItems { get; set; }
		public List<Decimal> Installments { get; set; }
        public List<string> InstallmentsWithCurrency { get; set; }
    }

    public class OrderSummaryModel
    {
        public int ParentProductId { get; set; }
        public int VariationId { get; set; }
        public string VariationName { get; set; }
        public int Quantity { get; set; }
        public decimal ItemPrice_Paid { get; set; }
        public string ItemPriceWithCurrency { get; set; }
		public decimal ppidpandpsurcharge { get; set; }
        public int CategoryId { get; set; }
        public int FlexiBuyItem { get; set; }
        public string ParentProductSKU { get; set; }
        public string CategoryName { get; set; }
        public string TVDescription { get; set; }
        public decimal UsualPrice { get; set; }
        public string UsualPriceWithCurrency { get; set; }
        public string DefaultThumbnailFileName { get; set; }
        public int FlexibuyInstallments { get; set; }
    }
}
