﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class TVScheduleNowAndNextModel
    {

        public TVScheduleByDateTimeAndChannelModel OnNow { get; set; }

        public TVScheduleByDateTimeAndChannelModel OnNext { get; set; }

    }
}
