﻿using Hoch.Website.Models;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Xml;


namespace Hoch.Website.Controllers.SiteMap
{
    public class SiteMapController :  BaseController
    {
        [Route("sitemap")]
        public ActionResult SiteMap()
        {


            string sXmlPath = Server.MapPath("~/SiteMap/sitemapWeben.xml");

            ViewSiteMapModel oViewSiteMap = new ViewSiteMapModel();

            
            XmlTextReader oXmlRd = new XmlTextReader(sXmlPath);
            DataSet oXmlData = new DataSet();
            oXmlData.ReadXml(oXmlRd);
            oXmlRd.Close();
            List<SiteMapModel> colSiteMap = new List<SiteMapModel>();
            for (int i=0; i<oXmlData.Tables[0].Rows.Count;i++)
            {
                SiteMapModel oSiteMap = new SiteMapModel();
                
                oSiteMap.Id = int.Parse(oXmlData.Tables[0].Rows[i][0].ToString());
                oSiteMap.categoryId = int.Parse(oXmlData.Tables[0].Rows[i][1].ToString());
                oSiteMap.translation = oXmlData.Tables[0].Rows[i][2].ToString();
                oSiteMap.belongsTo = int.Parse(oXmlData.Tables[0].Rows[i][3].ToString());
                oSiteMap.catLevel = int.Parse(oXmlData.Tables[0].Rows[i][4].ToString());
                oSiteMap.catUrl = oXmlData.Tables[0].Rows[i][5].ToString();
                colSiteMap.Add(oSiteMap);
            }
            
            oViewSiteMap.siteMapData = colSiteMap;


            return View("~/Views/SiteMap/SiteMap.cshtml",oViewSiteMap);
        }
    }
}