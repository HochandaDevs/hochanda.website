﻿/// <reference path="../angular.js" />

//Pending changes::
//populate data on menu click 
//get third level
//get static content
app.controller('MenuSideController', ['$scope', '$http', '$filter', '$compile', '$sanitize', '$rootScope', '$httpParamSerializer', '$window', '$sce', '$cookies', 'CommonCode', function ($scope, $http, $filter, $compile, $sanitize, $rootScope, $httpParamSerializer, $window, $sce, $cookies, CommonCode) {
    $scope.isLoggedIn = false;
    $scope.firstname = '';
    var keepMeSignedIn = $cookies.get('keepMeSignedIn');
    if (keepMeSignedIn != null && keepMeSignedIn != undefined) {
        keepMeSignedIn = JSON.parse(keepMeSignedIn);
        $scope.isLoggedIn = 1;
        $scope.firstname = keepMeSignedIn.Firstname;
    }
    $scope.selectedCatID;
    $scope.offerResults = [];
    var levelID;
    var catID;
    var catName;
    var showDiv;

    PopulateMenu(catID);

    function PopulateMenu(catID, levelID, catName, showDiv) {
        if (catID != undefined && catID != null) {
            $scope.selectedCatID = catID;
        }
        else {
            $scope.selectedCatID = 0;
        }
        var menuParams = { languageId: CommonCode.LanguageId, categoryID: $scope.selectedCatID };//, 
        $http({
            method: 'GET',
            url: baseUrl + "CategoryMenu/GetMenu",
            contentType: 'application/json',
            params: menuParams,
        }).then(function (result) {
            if (result.data !== undefined) {
                var resultset = result.data;

                angular.forEach(resultset, function (value, key) {
                    value.CategoryURL = GetCategoryUrl(value.CategoryId, value.CategoryName);
                });

                if ($scope.selectedCatID == 0) {
                    $scope.topMenuData = [];

                    var paperCraft = resultset.find(function (d) { return d.CategoryId == 48 });
                    var sewing = resultset.find(function (d) { return d.CategoryId == 461 });
                    var jewellery = resultset.find(function (d) { return d.CategoryId == 506 });
                    var knittingNeedleCraft = resultset.find(function (d) { return d.CategoryId == 462 });
                    var artSuppliers = resultset.find(function (d) { return d.CategoryId == 463 });
                    var hobbies = resultset.find(function (d) { return d.CategoryId == 57 });
                    var homeCraft = resultset.find(function (d) { return d.CategoryId == 252 });

                    if (paperCraft != null) {
                        $scope.topMenuData.push(paperCraft);
                    }

                    if (sewing != null) {
                        $scope.topMenuData.push(sewing);
                    }

                    if (jewellery != null) {
                        $scope.topMenuData.push(jewellery);
                    }

                    if (knittingNeedleCraft != null) {
                        $scope.topMenuData.push(knittingNeedleCraft);
                    }

                    if (artSuppliers != null) {
                        $scope.topMenuData.push(artSuppliers);
                    }

                    if (hobbies != null) {
                        $scope.topMenuData.push(hobbies);
                    }

                    if (homeCraft != null) {
                        $scope.topMenuData.push(homeCraft);
                    }

                    var sideMenuUnordered = $filter('filter')(resultset, function (d) { return d.CategoryBelongsTo == 37 || d.CategoryBelongsTo == 38 || d.CategoryBelongsTo == 39 }, true);

                    //We need to order it in a particular way according to how Marketing Want it.
                    $scope.sideMenuData = [];

                    var paperCraft = sideMenuUnordered.find(function (d) { return d.CategoryId == 48 });
                    var homeCraft = sideMenuUnordered.find(function (d) { return d.CategoryId == 252 });
                    var eventTickets = sideMenuUnordered.find(function (d) { return d.CategoryId == 443 });
                    var sewing = sideMenuUnordered.find(function (d) { return d.CategoryId == 461 });
                    var knittingNeedleCraft = sideMenuUnordered.find(function (d) { return d.CategoryId == 462 });
                    var jewellery = sideMenuUnordered.find(function (d) { return d.CategoryId == 506 });
                    var digitalDownloads = sideMenuUnordered.find(function (d) { return d.CategoryId == 593 });
                    var hobbies = sideMenuUnordered.find(function (d) { return d.CategoryId == 57 });
                    var artSuppliers = sideMenuUnordered.find(function (d) { return d.CategoryId == 463 });

                    if (paperCraft != null) {
                        $scope.sideMenuData.push(paperCraft);
                    }

                    if (homeCraft != null) {
                        $scope.sideMenuData.push(homeCraft);
                    }

                    if (eventTickets != null) {
                        $scope.sideMenuData.push(eventTickets);
                    }

                    if (sewing != null) {
                        $scope.sideMenuData.push(sewing);
                    }

                    if (knittingNeedleCraft != null) {
                        $scope.sideMenuData.push(knittingNeedleCraft);
                    }

                    if (jewellery != null) {
                        $scope.sideMenuData.push(jewellery);
                    }

                    if (digitalDownloads != null) {
                        $scope.sideMenuData.push(digitalDownloads);
                    }

                    if (hobbies != null) {
                        $scope.sideMenuData.push(hobbies);
                    }

                    if (artSuppliers != null) {
                        $scope.sideMenuData.push(artSuppliers);
                    }

                    for (var i = 0; i < sideMenuUnordered.length; i++) {
                        if ($scope.sideMenuData.find(function (d) { return d.CategoryId == sideMenuUnordered[i].CategoryId }) == null) {
                            $scope.sideMenuData.push(sideMenuUnordered[i]);
                        }
                    }

                }
                else {
                    if ($scope.selectedCatID != undefined && $scope.selectedCatID != null) {

                        if (levelID == 1) {
                            $scope.firstLevelMenuData = $filter('filter')(resultset, function (d) { return d.CategoryBelongsTo == $scope.selectedCatID }, true);

                        }
                        else {
                            if (levelID == 2) {
                                $scope.secondLevelMenuData = $filter('filter')(resultset, function (d) { return d.CategoryBelongsTo == $scope.selectedCatID }, true);
                            }
                            if (levelID == 3) {
                                $scope.thirdLevelMenuData = $filter('filter')(resultset, function (d) { return d.CategoryBelongsTo == $scope.selectedCatID }, true);
                            }
                            if (levelID == 4) {
                                $scope.fourthLevelMenuData = $filter('filter')(resultset, function (d) { return d.CategoryBelongsTo == $scope.selectedCatID }, true);
                            }
                        }

                        var viewAllCategory = $filter('filter')(resultset, function (d) { return d.CategoryId == $scope.selectedCatID }, true);
                        $scope.viewAllCategoryURL = viewAllCategory.length != 0 ? viewAllCategory[0].CategoryURL : "";
                    }
                }

                if (catName != null && catName != undefined) {
                    $scope.CatName = catName;

                }
                if (showDiv != undefined && showDiv != null) {
                    $scope.ShowDiv = showDiv;

                }

                $scope.ShowOffers = 0;
                $scope.ShowBrands = 0;

            }
        });

    }
    $scope.nextLevel = function (catID, levelID, catName, showDiv, brandSelect) {//catName for view all of catName,showDiv to populate data
        if (catID != "" && catID != $scope.selectedCatID && brandSelect != 1) {//added this conditon to avoid populating data on close of menu,
            $scope.selectedCatID = catID;
            PopulateMenu(catID, levelID, catName, showDiv);
        }
        else {

            $scope.selectedCatID = 0;
            $scope.ShowDiv = 0;//added this for topmenu
            if (brandSelect != undefined) {
                $scope.ShowBrands = 1;
            }


        }


    }

    $scope.offerClick = function (showDiv) {

        if (showDiv == 1 && showDiv != $scope.ShowOffers) {
            PopulateOffers(showDiv);
        }
        else {
            $scope.ShowOffers = 0;
        }



    }

    function PopulateOffers(showDiv) {


        $http({
            method: 'GET',
            url: baseUrl + "CategoryMenu/GetTopMenuOffers",
            contentType: 'application/json',

        }).then(function (result) {
            if (result.data !== undefined) {
                $scope.offerResults = result.data;
                $scope.ShowOffers = showDiv;
                $scope.ShowDiv = 0;

            }
        });


    }

}]);

app.controller('SelectorsController', ['$scope', '$cookies', '$window', 'CommonCode', function ($scope, $cookies, $window, CommonCode) {
    $scope.selectors = null;
    $scope.countries = [];
    $scope.currencies = [];
    $scope.languages = [];
    $scope.defaultCountryId = null;
    $scope.init = function (selectors) {
        if (selectors != "null") {
            CommonCode.getBaseCookieSettings();
            $scope.selectors = JSON.parse(selectors);
            $scope.countries = $scope.selectors.Countries;
            $scope.currencies = $scope.selectors.Currencies;
            $scope.languages = $scope.selectors.Languages;
            $scope.defaultCountryId = CommonCode.CountryId;
            $scope.defaultCurrencyId = CommonCode.CurrencyId;
            $scope.defaultLanguageId = CommonCode.LanguageId;
            $scope.defaultChannelId = CommonCode.ChannelId;
            $scope.defaultCountryCode = CommonCode.CountryCode;
            $scope.defaultCurrencySymbol = CommonCode.CurrencySymbol;
            $scope.defaultStreamId = CommonCode.DefaultStreamingId;
        }
    }

    $scope.initCountry = function () {
        if ($scope.defaultCountryId == null) {
            $scope.countrySelected = $scope.countries[0].CountryId;
            $scope.countryPreference = $scope.countries[0].CountryName;
            $scope.defaultStreamId = $scope.countries[0].CountryName
        } else {
            $scope.countrySelected = parseInt($scope.defaultCountryId);

            var filtered = $scope.countries.filter(function (c) {
                return c.CountryId == $scope.defaultCountryId;
            });

            if (filtered.length != 0) {
                $scope.defaultStreamId = filtered[0].DefaultStreamId;
                $scope.countryPreference = filtered[0].CountryName;
            }
        }
        localStorage.setItem("countrySelected", $scope.countryPreference);
    }

    $scope.initCurrency = function () {
        //if default currency is null, get the data from cookies
        if ($scope.defaultCurrencyId == null) {
            $scope.currencySelected = $scope.currencies[0].CurrencyId;
            $scope.currencySymbolSelected = $scope.currencies[0].CurrencySymbol;
            $scope.currencyPreference = $scope.currencies[0].CurrencySymbol + ' ' + $scope.currencies[0].Currency3LetterCode;
        } else {
            $scope.currencySelected = parseInt($scope.defaultCurrencyId);
            var curr = $scope.currencies.filter(function (c) {
                return c.CurrencyId == $scope.defaultCurrencyId;
            });
            $scope.currencySymbolSelected = $scope.defaultCurrencySymbol;
            if (curr.length != 0) {
                $scope.currencyPreference = curr[0].CurrencySymbol + ' ' + curr[0].Currency3LetterCode;
            }
        }
    }

    $scope.initLanguage = function () {
        if ($scope.defaultCurrencyId == null) {
            $scope.languageSelected = $scope.languages[0].LanguageId;
            $scope.languagePreference = $scope.languages[0].LanguageName;
        } else {
            $scope.languageSelected = parseInt($scope.defaultLanguageId);

            var filtered = $scope.languages.filter(function (c) {
                return c.LanguageId == $scope.defaultLanguageId;
            });

            if (filtered.length != 0) {
                $scope.languagePreference = filtered[0].LanguageName;
            }
        }
    }

    $scope.updatePreference = function () {
        ClearBasket();

        var countryId = $scope.countrySelected;
        var currencyId = $scope.currencySelected;
        var languageId = $scope.languageSelected;
        var curr = $scope.currencies.filter(function (c) {
            return c.CurrencyId == currencyId;
        });
        var country = $scope.countries.filter(function (c) {
            return c.CountryId == countryId;
        });
        var currencySymbol = curr[0].CurrencySymbol;
        var simpleStreamRewindDataStream = curr[0].SimpleStreamRewindDataStream;
        var channelId = curr[0].DefaultChannelId;

        var countryCode = country[0].Country2DigitISOCode;
        var companyId = country[0].CompanyId;
        var defaultStreamId = curr[0].SimpleStreamLiveDataStream;

        var obj = "CountryIDCookie=" + countryId + "&CurrencyIDCookie=" + currencyId + "&LanguageIDCookie=" + languageId + "&ChannelIDCookie=" + channelId + "&CustomerCountryCodeCookie=" + countryCode + "&CurrencySymbolCookie=" + currencySymbol + "&DefaultStreamIdCookie=" + defaultStreamId + "&SimpleStreamRewindDataStreamCookie=" + simpleStreamRewindDataStream + "&BaseCompanyIdCookie=" + companyId;
        var date = new Date();
        date.setDate(date.getDate() + 1);
        document.cookie = "BaseCookieSettings=" + obj + ";expires=" + date + "; path=/;";
        CommonCode.getBaseCookieSettings();
        $window.location.reload();
    }
}]);