﻿namespace ModelLayer.Models
{
	public class ProductPriceModel
	{
		public int CurrencyId { get; set; }
		public int ActivePriceType { get; set; }
		public int PromoPriceType { get; set; }
		public float IntroPrice { get; set; }
		public float PromoPrice { get; set; }
		public float? FreedomPrice { get; set; }
		public float? FlexibuyPrice { get; set; }
		public float? FreedomFlexiPrice { get; set; }
		public float UsualPrice { get; set; }
		public float Price { get; set; }
	}

	public class ProductPriceModelTwo
	{
		public int CurrencyId { get; set; }
		public int ActivePriceType { get; set; }
		public decimal IntroPrice { get; set; }
		//public decimal PromoPrice { get; set; }
		public decimal? FreedomPrice { get; set; }
		//public decimal? FlexibuyPrice { get; set; }
		//public decimal? FreedomFlexiPrice { get; set; }
		public decimal UsualPrice { get; set; }
		public decimal Price { get; set; }
	}
}