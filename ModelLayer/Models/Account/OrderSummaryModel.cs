﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class OrderSummaryModel
    {
        public Nullable<int> orderId { get; set; }
        public Nullable<System.DateTime> orderDate { get; set; }
        public Nullable<decimal> orderValueTotal { get; set; }
        public Nullable<int> orderDispatchStatusId { get; set; }
        public string orderDispatchStatus { get; set; }
        public Nullable<int> containsFlexi { get; set; }
        public Nullable<int> containsDownload { get; set; }
        public Nullable<int> orderitems { get; set; }
        public Nullable<decimal> deliveryCost { get; set; }
        public Nullable<decimal> orderTotalCost { get; set; }
        public string shippingAddress { get; set; }
        public string billingAddress { get; set; }
        public string currency { get; set; }

        public string deliveryCostValue { get; set; }
        public string orderTotalCostValue { get; set; }
        public string orderValueTotaltValue { get; set; }

        public List<OrderSummaryItemsModel> items { get; set; }

        public string courierName { get; set; }
        public string typeOfDelivery { get; set; }
        public Nullable<int> mindays { get; set; }
        public Nullable<int> maxdays { get; set; }

        public List<FlexiOverView> flexiOverview { get; set; }
    }

    public class OrderSummaryItemsModel
    {
        public Nullable<int> itemid { get; set; }
        public Nullable<int> parentProdId { get; set; }
        public string itemstatus { get; set; }
        public Nullable<int> itemStatusId { get; set; }
        public string imgFile { get; set; }
        public string title { get; set; }
        public string sku { get; set; }
        public string variationname { get; set; }
        public string downloadUrl { get; set; }
        public Nullable<int> qty { get; set; }
        public Nullable<decimal> displayPrice { get; set; }
        public Nullable<decimal> actualPrice { get; set; }
        public Nullable<int> flexiItem { get; set; }
        public int currency { get; set; }
        public Nullable<int> supplierCourierId { get; set; }
        public List<OrderFlexiDetails> flexiDetails { get; set; }
        public string displayPriceValue { get; set; }
        public string actualPriceValue { get; set; }
    }

    public class OrderFlexiDetails
    {
        public int itemid { get; set; }
        public DateTime scheduledate { get; set; }
        public string amountdue { get; set; }
        public decimal amountdueDecimal { get; set; }
		public string notes { get; set; }
    }

    public class OrderInfo
    {
        public int orderId { get; set; }
        public Guid customerLookupId { get; set; }
        public string email { get; set; }
        public int languageid { get; set; }
    }

    public class FlexiOverView
    {       
        public string ScheduledDate { get; set; }
        public string Amount { get; set; }
        public int Status { get; set; }
    }
}

