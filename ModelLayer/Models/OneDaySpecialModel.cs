﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class OneDaySpecialModel
    {
        public int ParentProductId { get; set; }
        public string ParentProductSKU { get; set; }
        public string TVDescription { get; set; }
        public string WebText { get; set; }
        public int IsPickAndMixItem { get; set; }
        public string DefaultImageFileName { get; set; }
        public string DefaultThumbnailFileName { get; set; }
        public string DefaultImageAltText { get; set; }
        public string FreedomSashFilename { get; set; }
        public string FlexibuySashFilename { get; set; }
        public int IsMembersDiscount { get; set; }
        public int IsFlexibuy { get; set; }
        public int FlexibuyInstallments { get; set; }
        public int? FeefoAveragePercentage { get; set; }
        public int? FeefoTotalReviews { get; set; }
        public decimal? FeefoAverageRating { get; set; }
        public int? DefaultVariationId { get; set; }
        public int ActivePrice { get; set; }
        public float IntroPrice { get; set; }
        public string IntroPriceString { get; set; }
        public float PromoPrice { get; set; }
        public string PromoPriceString { get; set; }
        public float UsualPrice { get; set; }
        public string UsualPriceString { get; set; }
        public float Price { get; set; }
        public string PriceString { get; set; }

        public bool ODSInStock { get; set; }
        public int IsFav { get; set; }
    }
}
