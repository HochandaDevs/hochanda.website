﻿using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class CategoryModel
	{
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public int? CategoryBelongsTo { get; set; }
		public int CategoryLevel { get; set; }
		public string CategoryURL { get; set; }
		public int EventShowType { get; set; }
		public string CategoryDescription { get; set; }
        public string CategoryImage { get; set; }
        public int SubCatInfoCount { get; set; }
        //public List<ProductInformationCacheModel> productsInfo { get; set; }
        public List<CategoryCacheModel> subCatInfo { get; set; }
    }

    public class MenuOffersModel
    {
        public string CampaignName { get; set; }
        public string PageURL { get; set; }
      
    }
}
