﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoch.Website.Models
{
	public class PickAndMixVariationModel
	{
		public int VariationId { get; set; }
		public string ImageFilename { get; set; }
		public string VariationName { get; set; }
		public string ImageAltText { get; set; }
	}
}