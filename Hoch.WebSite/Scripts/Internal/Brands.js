﻿//pending css class and creating cache for 1 day 
app.controller("BrandsController", ['$scope', '$http', '$compile', '$filter', '$rootScope', function ($scope, $http, $compile, $filter, $rootScope) {

    $scope.alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ$";
    var selectedVal;
    $scope.filteredBrandsData;
    var int = 0;
    var filteredNonAlphaBrands = [];
    $scope.BrandAlphaCount = [];
    var unique_array = [];



    FilteredBrands(selectedVal)//get selectedval from url
    if (window.location.href.toLowerCase().indexOf('brands') != -1) {
        $rootScope.GetMetadata(false, false, false, 'Brands - Overview Page', 0);
    }

    $scope.filteredBrands = function (selectedVal) {
        if ($scope.isBrandActive(selectedVal)) {
            FilteredBrands(selectedVal);
        }
    }

    $scope.GetBrandLetterLink = function (letter) {
        var activeLetter = false;

        if (letter == "$") {
            activeLetter = true;
            letter = "0-9"
        }
        else if ($scope.isBrandActive(letter)) {
            activeLetter = true;
        }

        if (activeLetter) {
            return "/brands#" + letter
        }

        return "";
    }

    $scope.isBrandActive = function (letter) {
        if ($scope.BrandAlphaCount.length > 0) {
            var brand = $scope.BrandAlphaCount.filter(function (brd) {
                return brd.letter == letter
            });

            if (brand[0].count > 0) {
                return true;
            }
        }
        return false;
    }

    function FilteredBrands(selectedVal) {
        var filteredData = '';
        var brandAlphabets = 0;
        $http({
            method: 'GET',
            url: baseUrl + "Brands/GetBrands",
            contentType: 'application/json'

        }).then(function (result) {
            if (result.data !== undefined) {
                $scope.brandsData = result.data;
                //added this for css disabled

                angular.forEach($scope.brandsData, function (value, key) {
                    brandAlphabets += value.BrandName.charAt(0);


                });
                removeDuplicates(brandAlphabets)

                var filterByGenres = $filter('filter')(brandAlphabets, function (d) { return d.indexOf(brandAlphabets) !== -1 });
                $scope.BrandAlphaCount = [];
                angular.forEach($scope.alphabets, function (value, key) {
                    var test = value + ":" + $filter('filter')(brandAlphabets, { $: value }).length;
                    var brd = [];
                    if (value == "$") {
                        var c = 0;
                        for (var i = 0; i <= 9; i++) {
                            c += $filter('filter')(brandAlphabets, { $: i }).length;
                        }
                        brd = { letter: "$", count: c };
                        $scope.BrandAlphaCount.push(brd);
                    }
                    else {
                        brd = { letter: value, count: $filter('filter')(brandAlphabets, { $: value }).length };
                        $scope.BrandAlphaCount.push(brd);
                    }
                });
                //added this for css disabled
                $scope.selectedVal = selectedVal != null && selectedVal != undefined && selectedVal != "" ? selectedVal : window.location.hash != null && window.location.hash != undefined && window.location.hash != "" ? window.location.hash.substring(1) : "ALL";

                if ($scope.selectedVal == "ALL") {
                    filteredData = $scope.brandsData;
                }
                else {
                    filteredData = $filter('filter')($scope.brandsData, function (d) { return d.BrandName.charAt(0) === $scope.selectedVal });
                }

                if (filteredData.length != 0) {
                    $scope.filteredBrandsData = filteredData;
                }
                else {
                    if ($scope.selectedVal == "$" || $scope.selectedVal == "0-9") {//just in case

                        for (int = 0; int <= 9; int++) {
                            filteredData = $filter('filter')($scope.brandsData, function (d) { return d.BrandName.charAt(0) === "" + int + "" });

                            if (filteredData.length != 0) {
                                filteredNonAlphaBrands.push(filteredData);
                            }
                        }
                        $scope.filteredBrandsData = filteredNonAlphaBrands[0];
                    }


                }



            }
        });

    }

    function removeDuplicates(brandAlphabets) {

        for (var i = 0; i < brandAlphabets.length; i++) {
            if (unique_array.indexOf(brandAlphabets[i]) == -1) {
                unique_array.push(brandAlphabets[i])
            }
        }
        return unique_array
    }

}]);



