﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ModelLayer.Helpers
{
	public class GenericHelper
	{
		private GenericHelper()
		{

		}

		private static volatile GenericHelper _Instance = null;

		public static GenericHelper Instance
		{
			get
			{
				if (_Instance == null)
				{
					_Instance = new GenericHelper();
				}

				return _Instance;
			}
		}

		private const string _HttpContext = "MS_HttpContext";
		private const string _OwinContext = "MS_OwinContext";
		private const string _RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

		/// <summary>
		/// Taken from https://www.strathweb.com/2013/05/retrieving-the-clients-ip-address-in-asp-net-web-api/
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public string GetUsersIPAddress(HttpRequestMessage request)
		{
			try
			{
				if (request.Properties.ContainsKey(_HttpContext))
				{
					dynamic ctx = request.Properties[_HttpContext];
					if (ctx != null)
					{
						return ctx.Request.UserHostAddress;
					}
				}

				if (request.Properties.ContainsKey(_RemoteEndpointMessage))
				{
					dynamic remoteEndpoint = request.Properties[_RemoteEndpointMessage];
					if (remoteEndpoint != null)
					{
						return remoteEndpoint.Address;
					}
				}

				if (request.Properties.ContainsKey(_OwinContext))
				{
					dynamic owinContext = request.Properties[_OwinContext];
					return owinContext.Request.RemoteIpAddress;
				}
			}
			catch (Exception e)
			{
				//TODO: Logs to PostGress
			}

			return string.Empty;
		}


		private const string _Purpose = "BasketEncryption";

		public string ProtectBasketId(long basketId)
		{
			var unprotectedBytes = Encoding.UTF8.GetBytes(basketId.ToString());
			var protectedBytes = MachineKey.Protect(unprotectedBytes, _Purpose);
			var protectedText = Convert.ToBase64String(protectedBytes);
			return protectedText;
		}

		public long UnprotectBasketId(string basketId)
		{
			var protectedBytes = Convert.FromBase64String(basketId);
			var unprotectedBytes = MachineKey.Unprotect(protectedBytes, _Purpose);
			var unprotectedText = Encoding.UTF8.GetString(unprotectedBytes);
			return long.Parse(unprotectedText);
		}

		public string ConvertDecimalValueToCurrency(string currencySymbol, double valueToConvert)
		{
			var culture = CultureInfo.GetCultures(CultureTypes.SpecificCultures).FirstOrDefault(x => new RegionInfo(x.LCID).CurrencySymbol == currencySymbol);

			if (culture == null)
			{
				//Default to UK so we have something
				CultureInfo.GetCultures(CultureTypes.SpecificCultures).FirstOrDefault(x => new RegionInfo(x.LCID).CurrencySymbol == "£");
			}

			string outputCurrency = valueToConvert.ToString();

			if (culture != null)
			{
				outputCurrency = valueToConvert.ToString("C", culture);
			}

			return outputCurrency;
		}

        /// <summary>
        /// Does a HTTP Post with data. The data needs to be transformed into a list of KeyValuePair. new list ==> new KeyValuePair Key,Value = BasketId, 1
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<OutputClass> PostRequest<OutputClass>(Uri uri, IEnumerable<KeyValuePair<string, string>> postData)
        {
            OutputClass getResponse = default(OutputClass);
            //ServicePointManager.Expect100Continue = true;
            try
            {
                using (var client = new HttpClient())
                {
                    //HTTP Post
                    using (var response = await client.PostAsync(uri, new FormUrlEncodedContent(postData)))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = await response.Content.ReadAsStringAsync();

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                getResponse = JsonConvert.DeserializeObject<OutputClass>(responseData);
                            }
                        }
                    }
                }
            }
            catch
            {
				//Do nothing, handle the null output in the calling class.
            }
            return getResponse;
        }

        /// <summary>
        /// Convert the string passed in in UTF-8
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <param name="text"></param>
        /// <returns></returns>
        
        public string ConvertToUFT8(string text)
        {
            string result = string.Empty;
            byte[] bytes = Encoding.Default.GetBytes(text);
            result = Encoding.UTF8.GetString(bytes);
            return result;
        }
    }
}
