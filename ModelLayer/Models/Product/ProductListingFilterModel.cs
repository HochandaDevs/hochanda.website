﻿using Jil;
using ModelLayer.Models.Settings;
using System.Collections.Generic;

namespace ModelLayer.Models.Product
{
	public class ProductListingFilterModel
	{
		public ProductListingFilterModel()
		{
			FilteredCategories = new List<FilteredModel>();
			FilteredBrands = new List<FilteredModel>();
		}

		/// <summary>
		/// This can be categoryId, eventId, brandId. All handled in controller.
		/// </summary>
		public int IdToUse { get; set; }
		/// <summary>
		/// Terrible thing I had to put in for brands only.
		/// </summary>
		public bool? IsBrandAmbassador { get; set; }
		public string SearchTerm { get; set; }

		public int LanguageId { get; set; }
		public int CurrencyId { get; set; }
		public int CountryId { get; set; }

		public bool? ShowFreedomOnly { get; set; }
		public bool? ShowFlexibuyOnly { get; set; }
		public bool? ShowDownloadsOnly { get; set; }
		public bool? ShowPickAndMixOnly { get; set; }

		private int _PageNumber = 1;

		public int PageNumber
		{
			get
			{
				return _PageNumber;
			}
			set
			{
				if (value <= 0)
				{
					value = 1;
				}

				_PageNumber = value;
			}
		}

		private int _PageSize = 20;

		public int PageSize
		{
			get
			{
				return _PageSize;
			}
			set
			{
				if (value > 200)
				{
					value = 200;
				}
				else if (value < 20)
				{
					value = 20;
				}

				_PageSize = value;
			}
		}

		private SortBy _SortByValue;

		[JilDirective(TreatEnumerationAs = typeof(int))]
		public SortBy SortByValue
		{
			get
			{
				if (!string.IsNullOrEmpty(SearchTerm) && _SortByValue == 0)
				{
					_SortByValue = SortBy.Relevance;
				}
				else if (_SortByValue == 0)
				{
					_SortByValue = SortBy.Newest;
				}

				return _SortByValue;
			}
			set { _SortByValue = value; }
		}

		public List<FilteredModel> FilteredBrands { get; set; }
		public List<FilteredModel> FilteredCategories { get; set; }
	}
}