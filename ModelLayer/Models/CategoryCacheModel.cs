﻿using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class CategoryCacheModel
	{
		public int CategoryId { get; set; }
		public Dictionary<int, string> CategoryNameAndLanguage { get; set; }
		public int? CategoryBelongsTo { get; set; }
		public int CategoryLevel { get; set; }
		public string CategoryURL { get; set; }
		public Dictionary<int, string> CategoryDescriptionAndLanguage { get; set; }
        public string CategoryImage { get; set; }
    }
}
