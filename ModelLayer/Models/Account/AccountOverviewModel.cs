﻿using ModelLayer.Interfaces;
using ModelLayer.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class AccountOverviewModel: IAccountOverviewModel,IModel
    {

        public int customerid { get; set; }
        public string freedommember { get; set; }
        public string freedomholiday { get; set; }
        public string hascustomercredit { get; set; }
        public Nullable<decimal> creditamount { get; set; }
        public string creditamountwithcurrency { get; set; }
    }

    public class AccountInfoModel
    {
        public Guid CustomerLookupId { get; set; }
        public string Email { get; set; }
        public int CurrencyId { get; set; }
    }
}
