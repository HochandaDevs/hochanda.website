﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoch.Website.Models
{
    public class NewOrderResponseModel
    {
        public bool Result { get; set; }
        public string OrderId { get; set; }
		public string ErrorCode { get; set; }
										   //public string CreditApplied { get; set; }
	}
}