﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class ProductInformationModel
	{
		public ProductInformationModel()
		{
			//Default Constructor
		}

		public static ProductInformationModel CreateProductInformationModel(ProductInformationCacheModel cacheModel, int languageId, int currencyId, int countryId, IEnumerable<CategoryModel> allCategoriesForCacheModel)
		{
			if (cacheModel == null)
			{
				return null;
			}
			//First check if this product is available for this language, currency, and country.
			if (!cacheModel.AllLanguages.Contains(languageId))
			{
				return null;
			}

			if (!cacheModel.AllCountries.Contains(countryId))
			{
				return null;
			}

			if (!cacheModel.AllCurrenciesAndCultures.ContainsKey(currencyId))
			{
				return null;
			}
			//We have no prices for this currency
			if (cacheModel.Prices.Where(i => i.CurrencyId == currencyId).FirstOrDefault() == null)
			{
				return null;
			}

			if (!cacheModel.DefaultImageAltTextWithLangauges.ContainsKey(languageId))
			{
				return null;
			}

			if (!cacheModel.AllWebTextWithLanguages.ContainsKey(languageId))
			{
				return null;
			}

			if (!cacheModel.AllTVDescriptionsWithLanguages.ContainsKey(languageId))
			{
				return null;
			}

			if (cacheModel.ProductImages == null || cacheModel.ProductImages.Count == 0)
			{
				cacheModel.ProductImages = new List<ProductImagesModel>()
				{
					new ProductImagesModel()
					{
						DisplayOrder = 1,
						Filename = "https://www.hoch.media/products/main/no-image.jpg",
						ImageAltText = cacheModel.DefaultImageAltTextWithLangauges[languageId],
						VariationId = 0
					}
				};
			}

			ProductPriceModel prices = cacheModel.Prices.Where(i => i.CurrencyId == currencyId).FirstOrDefault();
			var culture = new CultureInfo(cacheModel.AllCurrenciesAndCultures[currencyId]);

			var category1 = allCategoriesForCacheModel.Where(i => i.CategoryId == cacheModel.CategoryLevel1Id).FirstOrDefault();
			var category2 = allCategoriesForCacheModel.Where(i => i.CategoryId == cacheModel.CategoryLevel2Id).FirstOrDefault();
			var category3 = allCategoriesForCacheModel.Where(i => i.CategoryId == cacheModel.CategoryLevel3Id).FirstOrDefault();
			var category4 = allCategoriesForCacheModel.Where(i => i.CategoryId == cacheModel.CategoryLevel4Id).FirstOrDefault();
			var category5 = allCategoriesForCacheModel.Where(i => i.CategoryId == cacheModel.CategoryLevel5Id).FirstOrDefault();

            var productInformation = new ProductInformationModel()
            {
                ActivePriceType = prices.ActivePriceType,
                BrandId = cacheModel.BrandId,
                BrandName = cacheModel.BrandName,
                CategoryLevel1Id = cacheModel.CategoryLevel1Id,
                CategoryLevel1Name = category1 != null ? category1.CategoryName : "No Category",
                CategoryLevel2Id = cacheModel.CategoryLevel2Id,
                CategoryLevel2Name = category2 != null ? category2.CategoryName : "No Category",
                CategoryLevel3Id = cacheModel.CategoryLevel3Id,
                CategoryLevel3Name = category3 != null ? category3.CategoryName : "No Category",
                CategoryLevel4Id = cacheModel.CategoryLevel4Id,
                CategoryLevel4Name = category4 != null ? category4.CategoryName : "No Category",
                CategoryLevel5Id = cacheModel.CategoryLevel5Id,
                CategoryLevel5Name = category5 != null ? category5.CategoryName : "No Category",
                DefaultImageAltText = cacheModel.DefaultImageAltTextWithLangauges[languageId],
                DefaultImageFilename = cacheModel.DefaultImageFilename,
                DefaultThumbnailFilename = cacheModel.DefaultThumbnailFilename,
                DefaultVariationId = cacheModel.DefaultVariationId,
                FeefoAveragePercentage = cacheModel.FeefoAveragePercentage,
                FeefoAverageRating = cacheModel.FeefoAverageRating,
                FeefoTotalReviews = cacheModel.FeefoTotalReviews,
                FlexibuyBannerFilename = cacheModel.FlexibuyBannerFilename,
                FlexibuyInstallments = cacheModel.FlexibuyInstallments,
                FlexibuyPrice = prices.FlexibuyPrice,
                FlexibuyPriceString = prices.FlexibuyPrice.HasValue ? prices.FlexibuyPrice.Value.ToString("C", culture) : string.Empty,
                FlexibuySashFilename = cacheModel.FlexibuySashFilename,
                FreedomBannerFilename = cacheModel.FreedomBannerFilename,
                FreedomFlexibuyPrice = prices.FreedomFlexiPrice,
                FreedomFlexibuyPriceString = prices.FreedomFlexiPrice.HasValue ? prices.FreedomFlexiPrice.Value.ToString("C", culture) : string.Empty,
                FreedomPrice = prices.FreedomPrice,
                FreedomPriceString = prices.FreedomPrice.HasValue ? prices.FreedomPrice.Value.ToString("C", culture) : string.Empty,
                FreedomSashFilename = cacheModel.FreedomSashFilename,
                IntroPrice = prices.IntroPrice,
                IntroPriceString = prices.IntroPrice.ToString("C", culture),
                IsBundle = cacheModel.IsBundle,
                IsFlexibuy = cacheModel.IsFlexibuy,
                IsFreedom = cacheModel.IsFreedom,
                IsPickAndMix = cacheModel.IsPickAndMix,
                IsVirtualProduct = cacheModel.IsVirtualProduct,
                LargeFlexibuySashFilename = cacheModel.LargeFlexibuySashFilename,
                LargeFreedomSashFilename = cacheModel.LargeFreedomSashFilename,
                ODSBannerFilename = cacheModel.ODSBannerFilename,
                ParentProductId = cacheModel.ParentProductId,
                ParentProductSKU = cacheModel.ParentProductSKU,
                PickAndMixQuantity = cacheModel.PickAndMixQuantity,
                Price = prices.Price,
                PriceString = prices.Price.ToString("C", culture),
                ProductImages = cacheModel.ProductImages,
                ProductVariations = cacheModel.ProductVariations.Where(i => i.LanguageId == languageId).ToList(),
                PromoPrice = prices.PromoPrice,
                PromoPriceString = prices.PromoPrice.ToString("C", culture),
                PromoPriceType = prices.PromoPriceType,
                StockLevel = cacheModel.StockLevel,
                TVDescription = cacheModel.AllTVDescriptionsWithLanguages[languageId],
                UsualPrice = prices.UsualPrice,
                UsualPriceString = prices.UsualPrice.ToString("C", culture),
                VariationCount = cacheModel.VariationCount,
                WebText = cacheModel.AllWebTextWithLanguages[languageId],
                CreatedDate = cacheModel.CreatedDate,
                IsFav = 0,
				IsUpsellOnly = cacheModel.UpsellOnly,
                YoutubeVideo = cacheModel.YoutubeVideo
			};

			if (productInformation.IsPickAndMix)
			{
				var pickNMixVariation = productInformation.ProductVariations.Where(i => i.VariationName.ToLower() == "pick-n-mix").FirstOrDefault();

				if (pickNMixVariation != null)
				{
					productInformation.ProductVariations.Remove(pickNMixVariation);
				}
			}

			return productInformation;
		}


		public int ParentProductId { get; set; }
		public string ParentProductSKU { get; set; }
		public bool IsPickAndMix { get; set; }
		public int PickAndMixQuantity { get; set; }
		public bool IsFreedom { get; set; }
		public bool IsVirtualProduct { get; set; }
		public bool IsFlexibuy { get; set; }
		public int FlexibuyInstallments { get; set; }
		public bool IsBundle { get; set; }
		public int? FeefoAveragePercentage { get; set; }
		public int? FeefoTotalReviews { get; set; }
		public decimal? FeefoAverageRating { get; set; }
		public int BrandId { get; set; }
		public string BrandName { get; set; }
		public int ActivePriceType { get; set; }
		public int PromoPriceType { get; set; }
		public float IntroPrice { get; set; }
		public string IntroPriceString { get; set; }
		public float PromoPrice { get; set; }
		public string PromoPriceString { get; set; }
		public float? FreedomPrice { get; set; }
		public string FreedomPriceString { get; set; }
		public float? FlexibuyPrice { get; set; }
		public string FlexibuyPriceString { get; set; }
		public float? FreedomFlexibuyPrice { get; set; }
		public string FreedomFlexibuyPriceString { get; set; }
		public float UsualPrice { get; set; }
		public string UsualPriceString { get; set; }
		public float Price { get; set; }
		public string PriceString { get; set; }
		public string TVDescription { get; set; }
		public string WebText { get; set; }
		public string DefaultImageAltText { get; set; }
		public string DefaultImageFilename { get; set; }
		public string DefaultThumbnailFilename { get; set; }
		public string FreedomSashFilename { get; set; }
		public string FlexibuySashFilename { get; set; }
		public string LargeFreedomSashFilename { get; set; }
		public string LargeFlexibuySashFilename { get; set; }
		public string FreedomBannerFilename { get; set; }
		public string FlexibuyBannerFilename { get; set; }
		public string ODSBannerFilename { get; set; }
		public int DefaultVariationId { get; set; }
		public int VariationCount { get; set; }
		public List<ProductVariationModel> ProductVariations { get; set; }
		public List<ProductImagesModel> ProductImages { get; set; }
		public int StockLevel { get; set; }
		public int CategoryLevel1Id { get; set; }
		public string CategoryLevel1Name { get; set; }
		public int CategoryLevel2Id { get; set; }
		public string CategoryLevel2Name { get; set; }
		public int CategoryLevel3Id { get; set; }
		public string CategoryLevel3Name { get; set; }
		public int CategoryLevel4Id { get; set; }
		public string CategoryLevel4Name { get; set; }
		public int CategoryLevel5Id { get; set; }
		public string CategoryLevel5Name { get; set; }

		public DateTime CreatedDate { get; set; }
        public int IsFav { get; set; }
		public bool IsUpsellOnly { get; set; }
        public string YoutubeVideo { get; set; }
    }
}
