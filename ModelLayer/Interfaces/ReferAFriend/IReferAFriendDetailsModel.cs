﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.ReferAFriend
{
    public interface IReferAFriendDetailsModel
    {

        string voucherCode { get; set; }
        Nullable<int> CreditCurrency { get; set; }
        Nullable<int> PendingCount { get; set; }
        Nullable<int> SuccessfulCount { get; set; }
        Nullable<int> TotalReferralsCount { get; set; }
        Nullable<decimal> ActiveCredit { get; set; }
        string youReceiveOffer { get; set; }
        string youReceivePercentOrAmount { get; set; }
        Nullable<decimal> theyReceiveOffer { get; set; }
        string theyReceivePercentOrAmount { get; set; }
        Nullable<int> VoucherId { get; set; }

    }
}
