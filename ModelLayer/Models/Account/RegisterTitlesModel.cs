﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class RegisterTitlesModel
    {
        public string title { get; set; }
        public int languageid { get; set; }
        public int sortby { get; set; }
    }

    public class Phones
    {
        public int pid { get; set; }
        public string pdescription { get; set; }
    }
}
