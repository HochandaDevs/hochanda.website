﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.ParametersModel
{
   public class paramsGetBasket
    {
		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse
		{
			get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}
		public bool PerformCalculation { get; set; }
        public int CountryID { get; set; }
        public int CurrencyID { get; set; }
        public int LanguageID { get; set; }

        [System.ComponentModel.DefaultValue(-1)]
        public int IsFlexi { get; set; }

        [System.ComponentModel.DefaultValue(0)]
        public int ReservItemID { get; set; }

        [System.ComponentModel.DefaultValue(-1)]
        public int IsItemFlexi { get; set; }
    }
}
