﻿/// <reference path="../angular.min.js" />

app.controller('SearchController', ['$scope', '$http', '$window', '$location', 'CommonCode', 'loginService', function ($scope, $http, $window, $location, CommonCode, loginService) {
    $scope.inputSelect = ["All", "Brands", "Categories", "Products"];
    $scope.inputSelected = '';
    $scope.searchText = '';
    $scope.isTextEmpty = 0;
    $scope.products = [];
    $scope.categories = [];
    $scope.brands = [];
    $scope.displayLimit = 15;
    $scope.favouritesProducts = [];
    $scope.favouritesPages = null;
    CommonCode.getBaseCookieSettings();
    var languageId = CommonCode.LanguageId;
    var currencyId = CommonCode.CurrencyId;
    var countryId = CommonCode.CountryId;
    var history = JSON.parse(localStorage.getItem('recentlySearched'));
    $scope.loginDetails = null;
    $scope.history = null;
    if (history != null) {
        $scope.history = history;
    }
    $scope.tab = 3;

    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };

    $scope.textClick = function () {
        $scope.history = JSON.parse(localStorage.getItem('recentlySearched'));
        if ($scope.searchText == '') {
            $scope.isTextEmpty = 1;
        } else {
            $scope.isTextEmpty = 2;
        }
    }

    $scope.getFavourites = function () {
        var loginServiceResult = loginService.validateLogin();
        if (loginServiceResult != null) {
            loginServiceResult.then(function (loginDetailsResult) {
                $scope.loginDetails = loginDetailsResult;
                if ($scope.loginDetails != null && $scope.loginDetails.CustomerLookupId != null) {
                    var favourites = JSON.parse(localStorage.getItem("userFavourites"));

                    if (favourites != null) {
                        $scope.favouritesProducts = favourites.products;
                        $scope.favouritesPages = favourites.pages != null && favourites.pages != "" ? favourites.pages : null;
                    }
                }
            });
        }
    }
    $scope.getFavourites();

    $scope.submit = function () {
        $scope.Search($scope.searchText, 1, 0);
    }

    $scope.$watch('searchText', function (tmpStr) {
        if (!tmpStr || tmpStr.length == 0) {
            $scope.isTextEmpty = 0;
            return 0;
        }
        if (tmpStr === $scope.searchText && tmpStr.length > 2) {
            $scope.isTextEmpty = 2;
            var searchParam = {
                IdToUse: 0,
                SearchTerm: $scope.searchText,
                LanguageId: languageId,
                CurrencyId: currencyId,
                CountryId: countryId,
                ShowFreedomOnly: false,
                ShowFlexibuyOnly: false,
                ShowDownloadsOnly: false,
                ShowPickAndMixOnly: false,
                PageNumber: 1,
                PageSize: 20
            };
            $http({
                method: 'POST',
                url: baseUrl + "Products/Search",
                contentType: 'application/json',
                data: searchParam
            }).then(function (result) {
                    if (result.data !== undefined) {
                    $scope.products = result.data.Products;
                    $scope.categories = result.data.Filter.Categories;
                    $scope.brands = result.data.Filter.Brands;
                }
            });
        } else {
            $scope.isTextEmpty = 0;
            $scope.products = [];
            $scope.categories = [];
            $scope.brands = [];
        }
    });

    $scope.GetData = function () {
        if ($scope.searchText.length > 2) {
            $scope.isTextEmpty = 2;
            var searchParam = {
                IdToUse: 0,
                SearchTerm: $scope.searchText,
                LanguageId: languageId,
                CurrencyId: currencyId,
                CountryId: countryId,
                ShowFreedomOnly: false,
                ShowFlexibuyOnly: false,
                ShowDownloadsOnly: false,
                ShowPickAndMixOnly: false,
                PageNumber: 1,
                PageSize: 20
            };
            $http({
                method: 'POST',
                url: baseUrl + "Products/Search",
                contentType: 'application/json',
                data: searchParam
            }).then(function (result) {
                if (result.data !== undefined) {
                    $scope.products = result.data.Products;
                    $scope.categories = result.data.Filter.Categories;
                    $scope.brands = result.data.Filter.Brands;
                }
            });
        } else {
            $scope.isTextEmpty = 0;
            $scope.products = [];
            $scope.categories = [];
            $scope.brands = [];
        }
    }

    $scope.getUrl = function (name, type) {
        var url = '/';
        if (type == 'brand') {
            var brand = $scope.brands.filter(function (brand) {
                return brand.BrandName == name
            });

            if (brand != null && brand.length > 0) {
                url = '/brand/' + brand[0].BrandId + '/' + GetWebSafeURL(brand[0].BrandName);
            }
        } else if (type == 'category') {
            var cat = $scope.categories.filter(function (cat) {
                return cat.CategoryName == name;
            });
            if (cat != null && cat.length > 0) {
                url = '/category/' + cat[0].CategoryId + '/' + GetWebSafeURL(cat[0].CategoryName);
            }
        } else {
            var prd = $scope.products.filter(function (prd) {
                return prd.ParentProductId == name
            });
            if (prd != null && prd.length > 0) {
                url = '/product/' + prd[0].ParentProductId + '/' + GetWebSafeURL(prd[0].TVDescription) + '/' + prd[0].ParentProductSKU;
            }
        }
        return url;
    }

    $scope.Search = function (searchText, selectedType, productId) {
        $scope.StoreHistory(searchText, selectedType, productId);
        var url = '';
        if (selectedType == 1) { //pressed search redirect to resultPage
            searchText = encodeURIComponent(searchText);
            var searchType = 0;
            if ($scope.inputSelected == "All") {
                searchType = 1;
            } else if ($scope.inputSelected == "Brands") {
                searchType = 2;
            } else if ($scope.inputSelected == "Categories") {
                searchType = 3;
            } else if ($scope.inputSelected == "Products") {
                searchType = 4;
            }
            url = '/Search/SearchResult?keyword=' + searchText + '&type=' + searchType;
        } else if (selectedType == 2) { //selected a brand redirect to brand page
            var brand = $scope.brands.filter(function (brand) {
                return brand.BrandName == searchText
            });

            if (brand != null && brand.length > 0) {
                url = '/brand/' + brand[0].BrandId + '/' + GetWebSafeURL(brand[0].BrandName);
            }
        } else if (selectedType == 3) { //selected a category, redirect to category page
            var category = $scope.categories.filter(function (cat) {
                return cat.CategoryName == searchText
            });

            if (category != null && category.length > 0) {
                url = '/category/' + category[0].CategoryId + '/' + GetWebSafeURL(category[0].CategoryName);
            }
        } else if (selectedType == 4) { //selected a product redirect to product page
            url = '/Product/ProductPage/' + productId;
        }

        $window.location.href = url;
    }

    $scope.InHistory = function (text) {
        var found = false;
        for (var i = 0; i < $scope.history.length; i++) {
            if ($scope.history[i].Text == text) {
                found = true;
                break;
            }
        }
        return found;
    }

    $scope.StoreHistory = function (searchText, type, productId) {
        if (searchText != '') {
            if ($scope.history != null) {
                if (!$scope.InHistory(searchText)) {
                    if ($scope.history.length > 9) {
                        $scope.history.pop();
                    }
                }
            } else {
                $scope.history = [];
            }
            if (!$scope.InHistory(searchText)) {
                if (type == 4) {
                    $scope.history.unshift({ Text: searchText, Type: type, ProductId: productId });
                } else {
                    $scope.history.unshift({ Text: searchText, Type: type });
                }
            }
            $scope.isTextEmpty = 0;
            localStorage.setItem('recentlySearched', JSON.stringify($scope.history));
            
        }
    }

    $scope.InputChange = function () {
        if ($scope.inputSelected == "All" || $scope.inputSelected == "Brands") {
            $scope.tab = 1;
        } else if ($scope.inputSelected == "Categories") {
            $scope.tab = 2;
        } else if ($scope.inputSelected == "Products") {
            $scope.tab = 3;
        }
    }
}]);

app.controller('SearchResultController', ['$scope', '$http', 'CommonCode', '$compile', '$rootScope', function ($scope, $http, CommonCode, $compile, $rootScope) {
    $scope.commonCode = CommonCode;

    var searchKeywordElement = document.getElementById("search-keyword");
    var searchTypeElement = document.getElementById("search-type");
    $scope.productCount = 0;
    $scope.keyword = searchKeywordElement.value;
    $scope.type = searchTypeElement.value;
    $scope.products = [];
    $scope.categories = [];
    $scope.filteredBrandsData = [];
    $scope.displayAllBrands = true;

    $scope.getCategoryUrl = function (catid, catname) {
        return "/category/" + catid + "/" + GetWebSafeURL(catname);
    }

    $rootScope.GetProducts("Products/Search");
}]);

function ToggleSearchView() {
    var x = document.getElementById("search-container");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById('searchText').focus();
        $("html").addClass("frozen");
    } else {
        x.style.display = "none";
        $("html").removeClass("frozen");
    }
}