﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class BasketCheckoutModel
    {
        
        public DisplayCheckout displayCheckout { get; set; }

        public BasketCheckoutModel()
        {
            displayCheckout = new DisplayCheckout();
        }
    }
    public class ReservationsModel
    {
        public bool ReservationActive { get; set; }
        public bool ReservationLocked { get; set; }
        public bool ReservationValid { get; set; }

        public bool ProductRemoved { get; set; }

        public bool HasCountryCurrencyOrLanguageChanged { get; set; }//country currency language changed

        public bool FlexiBasketAvailable { get; set; }

        public bool FlexiBasketSelected { get; set; }


        public int FlexiBasketInstallments { get; set; }

        public decimal FlexiBasketPreview { get; set; }

        public decimal ToPayFlexiBuy { get; set; }
        public decimal ToPayFlexiBuySingle { get; set; }

		public decimal ToPayToday { get; set; }

        public int? MessageCode { get; set; }

        public string MessageTitle { get; set; }

        public string MessageToDisplay { get; set; }

        public string OkButtonText { get; set; }

        public string CancelButtonText { get; set; }

        public string CurrencySymbol { get; set; }
        public decimal SubTotal { get; set; }

        public decimal Delivery { get; set; }

        public string SubTotalWithCurrency { get; set; }

        public string DeliveryWithCurrency { get; set; }

        public string ToPayTodayWithCurrency { get; set; }

        public string FBPreviewWithCurrency { get; set; }

        public string ToPayFlexiBuyWithCurrency { get; set; }
        public string ToPayFlexiBuySingleWithCurrency { get; set; }
		public bool VoucherUsed { get; set; }
		public string VoucherCode { get; set; }
		public decimal? VoucherDiscount { get; set; }
		public string VoucherDiscountString { get; set; }
	}

    public class ReservationItemsModel
    {
        public int ReservationItemId { get; set; }

        public int ParentProductID { get; set; }

        public int VariationID { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }

        public int FlexiBuyItem { get; set; }

        public decimal ItemTotal { get; set; }

        public decimal PreviousPrice { get; set; }
        public string TvDescription { get; set; }

      
        public string VariationName { get; set; }

        public string ParentProductSKU { get; set; }

        public string VariationProductSku { get; set; }

        public string ImageName { get; set; }

        public int FlexiBuy { get; set; }

        public int FlexiBuyInstallments { get; set; }

        public decimal FlexiLeftToPay { get; set; }

        public int ServiceID { get; set; }

        public int ServiceID_Default { get; set; }

        public int PandpruleApplied { get; set; }

        public int FreeItem { get; set; }

        public int PickNMixItem { get; set; }

        public int AuctionItem { get; set; }

        public int PendingStatus { get; set; }

       
        public string ItemPriceWithCurrency { get; set; }

        public string WasPriceWithCurrency { get; set; }

        public string FlexiLeftToPayWithCurrency { get; set; }
		public bool IsUpsellOnly { get; set; }


	}
    public class DisplayCheckout
    {
        public DisplayCheckout()
        {
            reservationModel = new List<ReservationsModel>();
            reservationItemsModel = new List<ReservationItemsModel>();
           
        }
        public List<ReservationsModel> reservationModel { get; set; }
        public List<ReservationItemsModel> reservationItemsModel { get; set; }
      
    }
}
