﻿using Hoch.Website.Helpers;
using ModelLayer.Models.PageBuilder;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Hoch.Website.Controllers.PageBuilder
{
	public class PageBuilderController : BaseController
	{
		[Route("about/{urlToUse}/{isPreview?}")]
		public async Task<ActionResult> About(string urlToUse, bool isPreview = false)
		{
			PageBuilderModel model = await GetPageBuilderStructure(urlToUse, "about", isPreview);

			if (model == null)
			{
				return Redirect("/");
			}

			if (!string.IsNullOrEmpty(model.RedirectURL))
			{
				return RedirectPermanent(model.RedirectURL);
			}

			SetMetaData(model);

			return View("~/Views/PageBuilder/PageBuilder.cshtml", model);
		}

		[Route("event/{urlToUse}/{isPreview?}")]
		public async Task<ActionResult> Event(string urlToUse, bool isPreview = false)
		{
			PageBuilderModel model = await GetPageBuilderStructure(urlToUse, "event", isPreview);

			if (model == null)
			{
				return Redirect("/");
			}

			if (!string.IsNullOrEmpty(model.RedirectURL))
			{
				return RedirectPermanent(model.RedirectURL);
			}

			SetMetaData(model);

			return View("~/Views/PageBuilder/PageBuilder.cshtml", model);
		}

		[Route("sale/{urlToUse}/{isPreview?}")]
		public async Task<ActionResult> Sales(string urlToUse, bool isPreview = false)
		{
			PageBuilderModel model = await GetPageBuilderStructure(urlToUse, "sale", isPreview);

			if (model == null)
			{
				return Redirect("/");
			}

			if (!string.IsNullOrEmpty(model.RedirectURL))
			{
				return RedirectPermanent(model.RedirectURL);
			}

			SetMetaData(model);

			return View("~/Views/PageBuilder/PageBuilder.cshtml", model);
		}

		/// <summary>
		/// Sets the metadata for a page builder.
		/// </summary>
		/// <param name="model"></param>
		private void SetMetaData(PageBuilderModel model)
		{
			ViewBag.MetaKeywords = model.MetaKeywords;
			ViewBag.MetaDescription = model.MetaDescription;
			ViewBag.PageTitle = model.PageTitle;
		}

		/// <summary>
		/// Gets the structure from the API.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="pagePrefix"></param>
		/// <param name="isPreview"></param>
		/// <returns></returns>
		private async Task<PageBuilderModel> GetPageBuilderStructure(string url, string pagePrefix, bool isPreview)
		{
			string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");

			string queryString = string.Format($"pageBuilderUrl={url}&pagePrefix={pagePrefix}&isPreview={isPreview}&languageId={BaseLanguageID}");

			return await new WebRequestHelper().GetItem<PageBuilderModel>(apiUrl + "pagebuilder/GetPageBuilderStructure?" + queryString);
		}
	}
}