﻿using ModelLayer.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class AccountMessageModel: IAccountMessageModel
    {

        public int MessageCode { get; set; }
        public int ObjectId { get; set; }
        public string TitleMessage { get; set; }
        public string MessageDescription { get; set; }
        public string MessageToDisplay { get; set; }
        public string OKMessage { get; set; }
        public string CancelMessage { get; set; }
       
    }
}
