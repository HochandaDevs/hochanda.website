﻿using Hoch.Website.Helpers;
using ModelLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.IO;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
//using Hoch.Website.PayPalSvc;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using Mandrill;
using Mandrill.Model;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.Routing;
using ModelLayer.Helpers;
using Hoch.Website.Models;
using PayPalCheckoutSdk.Orders;
using PayPalCheckoutSdk.Core;
using System.Net.Http;
using Hoch.Website.Models.PayPal;
using System.Dynamic;
using ModelLayer.Models.Reservation;

namespace Hoch.Website.Controllers.Checkout
{
	public class CheckoutController : BaseController
	{
		public string statusCode { set; get; }
		public bool transStatus { set; get; }
		public string resId { set; get; }
		public string avsResult { set; get; }
		public string chaseOrderId { set; get; }
		public string dateStamp { set; get; }
		public string lastFourDigits { set; get; }
		public string cardBrand { set; get; }
		public string cardExpiryDate { set; get; }
		public string customerRefNum { set; get; }
		public string textRefNum { set; get; }
		public string mitTxId { set; get; }
		public string returnCode { set; get; }
		public string cvvResponseCode { set; get; }
		public string currencyForAvs { set; get; }
		public bool inserted = false;
		public int? CustomerId { set; get; }

		[Route("checkout")]
		public async Task<ActionResult> Checkout(bool newAccount = false, string registered = "0")
		{
			try
			{
				var message = $"Checkout Test";

				Utility.WriteLogMonitor(message);
			}
			catch
			{
				//Do nothing
			}

			int? cid = await GetCustomerId();
			if (cid == null)
			{
				Session["CustomerId"] = 0;
			}
			else
			{
				Session["CustomerId"] = cid;
			}

			ViewBag.ForNewCustomer = "";
			ViewBag.RegisteredNewAccount = registered;
			if (newAccount == true)
			{
				ViewBag.ForNewCustomer = "Account created successfully. Please login to your account.";

			}

			//return RedirectToAction("Checkout", "Checkout");
			return View("~/Views/Checkout/Checkout.cshtml");
		}

		/*public async Task<ActionResult> PaypalCancel(bool isexternal = true)
		{
			Session["isPaypal"] = true;
			string basketId = Session["BasketId"] != null ? Session["BasketId"].ToString() : string.Empty;
			if (!string.IsNullOrEmpty(basketId))
			{
				WebRequestHelper wh = new WebRequestHelper();

				Uri uri = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/ReservationsManageLock");

				var postData = new List<KeyValuePair<string, string>>();
				postData.Add(new KeyValuePair<string, string>("Lock", "0"));
				postData.Add(new KeyValuePair<string, string>("BasketId", basketId));

				await wh.PostRequestString(uri, postData);
			}
			return RedirectToAction("Checkout", "Checkout");
		}*/

		public ActionResult OrderStatus(bool isExternal = false, string errorCode = "")
		{
			NewOrderResponseModel res = new NewOrderResponseModel();
			res = (NewOrderResponseModel)Session["OrderStatusResult"];
			//res.CreditApplied = Session["CreditApplied"].ToString();
			return View(res);
		}

		public ActionResult OrderStatusTemp(string result, bool paypal = false, bool isExternal = false)
		{
			NewOrderResponseModel res = new NewOrderResponseModel();
			string errCode = string.Empty;
			if (result.Contains("failure"))
			{
				res.Result = false;
				errCode = result.Split('-')[1];
			}
			else
			{
				res.Result = true;
				res.OrderId = result;
				//EcommerceTracking(res.OrderId);
			}

			//return View("/Views/Checkout/OrderStatus.cshtml");
			Session["OrderStatusResult"] = res;
			Session["isPaypal"] = paypal;
			//TODO: find a way to redirect from paypal popup
			return RedirectToAction("OrderStatus", new { isExternal = isExternal, errorCode = errCode });
		}

		public async Task<bool> SendConfirmationOrder(string basketId, int orderId)
		{
			WebRequestHelper webHelper = new WebRequestHelper();
			HttpBrowserCapabilitiesBase browser = Request.Browser;
			string browserInfo = browser != null ? browser.Type : "";
			//int orderId = 0;
			try
			{
				var api = new MandrillApi("mwikOWlc9HLroBSaEpQkhw");
				var message = new MandrillMessage();
				int currencyId = BaseCurrencyID;
				string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");

				Uri uri = new Uri(apiurl + "Order/GetOrderStatus");

				var postData = new List<KeyValuePair<string, string>>();

				postData.Add(new KeyValuePair<string, string>("BasketId", basketId));
				postData.Add(new KeyValuePair<string, string>("LanguageId", BaseLanguageID.ToString()));
				OrderStatusModel orderStatus = await webHelper.PostRequest<OrderStatusModel>(uri, postData);

				if (orderStatus != null)
				{
					string billingAddress = !string.IsNullOrEmpty(orderStatus.b_StreetAddress1) ? orderStatus.b_StreetAddress1 : string.Empty;
					string billingTown = !string.IsNullOrEmpty(orderStatus.b_City) ? orderStatus.b_City : string.Empty;
					string billingPostcode = !string.IsNullOrEmpty(orderStatus.b_Postcode) ? orderStatus.b_Postcode : string.Empty;
					string billingCounty = !string.IsNullOrEmpty(orderStatus.b_County) ? orderStatus.b_County : string.Empty;
					string ShippingBuildingNo = !string.IsNullOrEmpty(orderStatus.BuildingNumber) ? orderStatus.BuildingNumber : string.Empty;
					string ShippingAddress = !string.IsNullOrEmpty(orderStatus.StreetAddress1) ? orderStatus.StreetAddress1 : string.Empty;
					string ShippingAddress2 = !string.IsNullOrEmpty(orderStatus.StreetAddress2) ? orderStatus.StreetAddress2 : string.Empty;
					string ShippingTown = !string.IsNullOrEmpty(orderStatus.Town) ? orderStatus.Town : string.Empty;
					string ShippingPostcode = !string.IsNullOrEmpty(orderStatus.Postcode) ? orderStatus.Postcode : string.Empty;
					string ShippingCounty = !string.IsNullOrEmpty(orderStatus.County) ? orderStatus.County : string.Empty;
					string paymentType = Session["PaymentType"].ToString();
					decimal deliveryPrice = orderStatus.DeliveryCost;

					int orderDay = DateTime.Today.Day;
					string orderTime = DateTime.Now.ToString("hh:mm tt");
					//string orderDateTime = orderDay.ToString() + " " + DateTime.Now.ToString("MMMM") + " " + DateTime.Today.Year + " at " + orderTime;
					string orderDateTime = ToStringWithOrdinal(DateTime.Today) + " at " + orderTime;

					CalculateDispatchDatesModel dispatchDates = await webHelper.GetItem<CalculateDispatchDatesModel>(apiurl + "Order/CalculateDispatchDates?orderId=" + orderId);
					int minimumDays = dispatchDates.MinimumDays ?? 1;
					int maximumDays = dispatchDates.MaximumDays ?? 1;
					//string totalFromDeliveryDate = DateTime.Now.AddDays(minimumDays).ToString("dd/MM/yyyy");
					//string totalDeliveryDate = DateTime.Now.AddDays(maximumDays).ToString("dd/MM/yyyy");

					string itemQty = "";
					string orders = "";
					string image = "";
					//removing freedomproduct
					foreach (var item in orderStatus.OrderItems)
					{
						if (item.Quantity > 0)
						{
							string itemname = item.TVDescription;
							string itemnumber = item.ParentProductSKU;
							int ppid = item.ParentProductId;
							decimal untiPrice = item.ItemPrice_Paid;
							//decimal unitFullPrice = untiPrice * item.FlexiBasketInstallments;
							int qty = item.Quantity;
							string img = item.DefaultThumbnailFileName;
							itemQty = qty.ToString();
							orders += "<tr><td><img src=\"" + img + "\" height=\"50\" width=\"50\" alt=\"" + item.TVDescription + "\" onerror=\"this.onerror=null; this.src='https://www.hoch.media/products/master/no-image.jpg';\"></td>";
							orders += "<td style=\"color:#000000; font-family:Verdana, Geneva, sans-serif; font-size:12px;text-transform:uppercase;\">" + itemname + " (" + itemnumber + ")<br/>";
							if (item.VariationId != 0 && item.VariationName != string.Empty && item.VariationName != ".")
							{
								orders += "<span style=\"font-size:10px; text-transform:none; font-style:italic;\">" + item.VariationName + "</span>";
							}
							orders += "</td>";
							orders += "<td style=\"color:#000000; font-family:Verdana, Geneva, sans-serif; font-size:12px;\">QTY:&nbsp;" + qty + "</td>";
							orders += "<td style=\"color:#000000; font-family:Verdana, Geneva, sans-serif; font-size:12px;\">&nbsp;" + item.ItemPriceWithCurrency + "</td>";
							decimal flexibuyTotal = item.ItemPrice_Paid * item.FlexibuyInstallments;
							string flexibuyTotalWithCurrency = GenericHelper.Instance.ConvertDecimalValueToCurrency(CurrencySymbol, Convert.ToDouble(flexibuyTotal));
							orders += "</tr>";
							if (item.FlexiBuyItem == 1)
							{
								orders += "<tr><td bgcolor=\"#ce3234\" colspan=\"3\" style=\"color: #ffffff; font-size: 10px;\">";
								orders += "<span>The total cost of this will be <strong>" + flexibuyTotalWithCurrency + "</strong> per item once all instalments are completed.</span></td>";
								orders += "<td bgcolor=\"#ce3234\" colspan=\"1\" align=\"center\"><img src=\"https://hoch.media/web-assets/hoch-logos/flexi-buy-web-main.png\" height=\"21\" width=\"80\"></td>";
								orders += "</tr>";
							}
							orders += "<tr><td colspan=\"4\"><hr style=\"border-top:1px dashed #000000; border-bottom:none;\"></td></tr>";
						}
					}
					//orders += "<tr><td height=\"20\"><img src=\"https://www.hoch.media/notification-emails/spacer.gif\" border=\"0\" height=\"5\" width=\"10\"></td></tr>";
					string flexiorder = string.Empty;
					if (orderStatus.FlexiOrderSelected == 1)
					{
						flexiorder += "<tr><td><table cellpadding=\"5\" style=\"width:100%; background-color: #ce3234;\">";
						flexiorder += "<tr><td style=\"background-color: #ce3234; width:100%\"><img src=\"https://hoch.media/web-assets/hoch-logos/flexi-order-basket.png\" height=\"42\" width=\"200\"></td>";
						flexiorder += "<td align=\"center\" style=\"background-color:#ce3234; line-height:30px;\"></td></tr>";
						flexiorder += "<tr><td style=\"background-color: #ffffff; line-height: 30px;\">Number of Instalments</td>";
						flexiorder += "<td align=\"center\" style=\"background-color:#ffffff; line-height:30px;\">" + orderStatus.FlexiOrderInstallments + "</td></tr>";
						//flexiorder += "<tr><td style=\"background-color: #ffffff; line-height: 30px;\">Paid Today</td>";
						//flexiorder += "<td align=\"center\" style=\"background-color:#ffffff; line-height:30px;\">" + orderStatus.ToPayFigure_TodayWithCurrency + "</td></tr>";
						DateTime today = DateTime.Today;
						for (int i = 0; i < orderStatus.FlexiOrderInstallments; i++)
						{
							if (i > 0)
							{
								today = today.AddMonths(i);
								flexiorder += "<tr><td style=\"padding:5px; background-color: #ffffff; line-height: 30px;\">" + today.ToString("dd/MM/yyyy") + "</td><td align=\"center\" style=\"padding:5px; background-color:#ffffff; line-height:30px;\">" + orderStatus.InstallmentsWithCurrency[i] + "</td></tr>";
							}
							else
							{
								flexiorder += "<tr><td style=\"padding:5px; background-color: #ffffff; line-height: 30px;\">Today (including Delivery)</td><td align=\"center\" style=\"padding:5px; background-color:#ffffff; line-height:30px;\">" + orderStatus.InstallmentsWithCurrency[i] + "</td></tr>";
							}
						}
						flexiorder += "</table></td></tr>";
					}

					/*string currSymb = reservationModel.CurrencySymbol;
                    tPrice = FormatPrice(currencyId, currSymb, totalAmount);
                    dPrice = FormatPrice(currencyId, currSymb, deliveryPrice);*/
					decimal totalAmount = Session["TotalAmount"] != null ? Convert.ToDecimal(Session["TotalAmount"]) : 0;
					string tPrice = orderStatus.ToPayFigure_TodayWithCurrency;
					if (orderStatus.ToPayFigure_Today != totalAmount)
					{
						//tPrice = totalAmount.ToString();
						tPrice = GenericHelper.Instance.ConvertDecimalValueToCurrency(CurrencySymbol, (double)totalAmount);
					}
					string dPrice = orderStatus.DeliveryCostWithCurrency;

					//------populating template email------------//
					message.FromEmail = "customer.service@hochanda.com";
					string email = orderStatus.Email;
					//string email = "marco.ribaudo@hochanda.com";
					message.AddTo(email);

					string isDev = ConfigurationManager.AppSettings.Get("IsDev") != null ? ConfigurationManager.AppSettings.Get("IsDev") : "1";
					if (isDev == "1")
					{
						message.BccAddress = "marco.ribaudo@hochanda.com";
					}
					else
					{
						message.BccAddress = "customer.service@hochanda.com";
					}
					message.ReplyTo = "customer.service@hochanda.com";
					message.Subject = "Hochanda Order Confirmation: " + "#" + orderId;
					message.AddGlobalMergeVars("customer-invoice", DateTime.Now.ToShortDateString());

					string address = "<p style=\"margin-bottom: 0;\">";
					if (!string.IsNullOrEmpty(ShippingAddress))
					{
						if (!string.IsNullOrEmpty(ShippingBuildingNo))
						{
							ShippingAddress = ShippingBuildingNo + " " + ShippingAddress;
						}
						address += "<span>" + ShippingAddress + "</span><br>";
					}
					if (!string.IsNullOrEmpty(ShippingAddress2))
					{
						address += "<span>" + ShippingAddress2 + "</span><br>";
					}
					if (!string.IsNullOrEmpty(ShippingTown))
					{
						address += "<span>" + ShippingTown + "</span><br>";
					}
					if (!string.IsNullOrEmpty(ShippingCounty))
					{
						address += "<span>" + ShippingCounty + "</span><br>";
					}
					if (!string.IsNullOrEmpty(ShippingPostcode))
					{
						address += "<span>" + ShippingPostcode + "</span><br>";
					}
					address += "</p>";

					//-------populate email for sending-----------//
					string accountlink = "http://www.hochanda.com/myaccount";
					//string creditApplied = Session["CreditApplied"] != null ? Session["CreditApplied"].ToString() : string.Empty;
					message.AddGlobalMergeVars("TITLE", orderStatus.Title);
					message.AddGlobalMergeVars("FIRSTNAME", orderStatus.Firstname);
					message.AddGlobalMergeVars("ORDERNUMBER", orderId);
					message.AddGlobalMergeVars("TOTALFROMWORKINGDAYS", minimumDays);
					message.AddGlobalMergeVars("TOTALTOWORKINGDAYS", maximumDays);
					/*message.AddGlobalMergeVars("ADDRESSNUMBER", ShippingAddress);
                    message.AddGlobalMergeVars("ADDRESSLINE2", ShippingAddress2);
                    message.AddGlobalMergeVars("TOWNCITY", ShippingTown);
                    message.AddGlobalMergeVars("COUNTY", ShippingCounty);
                    message.AddGlobalMergeVars("POSTCODE", ShippingPostcode);*/
					message.AddGlobalMergeVars("ADDRESS", address);
					message.AddGlobalMergeVars("ACCOUNTLINK", accountlink);
					message.AddGlobalMergeVars("ORDERITEMS", orders);
					message.AddGlobalMergeVars("ORDERTIMESTAMP", orderDateTime);
					message.AddGlobalMergeVars("PAYMENTTYPE", paymentType);
					message.AddGlobalMergeVars("TOTALPRICE", tPrice);
					message.AddGlobalMergeVars("DELIVERYPRICE", dPrice);
					//message.AddGlobalMergeVars("CREDITAPPLIED", creditApplied);
					message.AddGlobalMergeVars("FLEXIORDER", flexiorder);
					//string add2display = "block;";
					//if (string.IsNullOrEmpty(ShippingAddress2))
					//{
					//    add2display = "none;";
					//}
					//message.AddGlobalMergeVars("ADD2DISPLAY", add2display);

					try
					{
						var rs = await api.Messages.SendTemplateAsync(message, "order-confirmation-mvc-angular-build", null, true);
						if (rs.Count > 0)
						{
							if (rs[0].Status.ToString() == "Rejected")
							{
								await webHelper.InsertErrorToPostgres("SendConfirmationOrder orderId: " + orderId + " - email: " + email + " - Browser: " + browserInfo + " - Mandrill response: rejected.", "1", rs[0].RejectReason, 1);
								SendGrid(orderId, email);
							}
						}
					}
					catch (MandrillException mandrillEx)
					{
						await webHelper.InsertErrorToPostgres("Mandrill Exception orderId: " + orderId + " - email: " + email + " - Browser: " + browserInfo + ": " + mandrillEx.Message, "1", mandrillEx.ToString(), 1);
						SendGrid(orderId, email);
					}
				}
				else
				{
					await webHelper.InsertErrorToPostgres("SendConfirmationOrder Failed for order id: " + orderId + " - Browser: " + browserInfo + "! The orderStatus is null so data cannot be retrieved.", "1", "", 1);
				}


			}
			catch (Exception e)
			{
				await webHelper.InsertErrorToPostgres("SendConfirmationOrder orderId: " + orderId + " - Browser: " + browserInfo + "Exception: " + e.Message, "1", e.ToString(), 1);
			}
			return true;
		}

		public string ToStringWithOrdinal(DateTime d)
		{
			switch (d.Day)
			{
				case 1:
				case 21:
				case 31:
					return d.ToString("dd'st' MMMM yyyy");
				case 2:
				case 22:
					return d.ToString("dd'nd' MMMM yyyy");
				case 3:
				case 23:
					return d.ToString("dd'rd' MMMM yyyy");
				default:
					return d.ToString("dd'th' MMMM yyyy");
			}
		}

		public void SendGrid(int orderId, string email)
		{
			var myMessage = new SendGrid.SendGridMessage();

			myMessage.AddTo(email);
			myMessage.EnableTemplateEngine("87ab3b00-7aa4-4dcf-8c72-c8e3464d2fe5");
			MailAddress bccEmails = new MailAddress("customer.service@hochanda.com");
			var bccAddresses = new[] { bccEmails };
			myMessage.From = new MailAddress("customer.service@hochanda.com", "Hochanda");
			myMessage.Bcc = bccAddresses;
			myMessage.Subject = "Thank you for your Order";
			myMessage.Html = "Your purchase was successful." + "<br /><br />" + "Your OrderNumber is :" + orderId.ToString() + ". For further enquiries please contact the Service Centre on 01733 60 2000 or email customer.service@hochanda.com quoting your order number.";
			var transportWeb = new SendGrid.Web("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");
			transportWeb.DeliverAsync(myMessage);
		}

		public string FormatPrice(int currencyID, string currencysymbol, decimal price)
		{
			string finalPrice = string.Empty;
			if (currencyID == 2)
			{
				finalPrice = ((string.Format("{0:0.00}", price)) + currencysymbol).Replace(".", ",");
			}
			else
			{
				finalPrice = currencysymbol + (string.Format("{0:0.00}", price));
			}
			return finalPrice;
		}

		public async Task<ActionResult> AddCardStatus(int page = 0, Guid? reservationId = null, int? billingAddressId = null, int? shippingAddressId = null)
		{

			await GetStatusMessage();
			string newIdLook = string.Empty;

			if (transStatus)
			{
				newIdLook = await InsertSuccessValues();
			}

			ViewBag.Result = newIdLook;
			ViewBag.PageId = page;// 1 if myaccount
			return View("/Views/checkout/AddCardStatus.cshtml");
		}

		public async Task<string> InsertSuccessValues()
		{
			currencyForAvs = Convert.ToString(Session["currencyTypeNameP"]);
			string newIdLook = string.Empty;
			while (statusCode == "000.200.100" || statusCode == "000.200.200")
			{
				Thread.Sleep(10000);
				//  Utility.statusMonitor("Inside While Loop" + newline + (Context.Session[BusinessLayerConstants.sessionCustomerID] ?? string.Empty).ToString() + newline + "currentPage" + currentPageName + "XML STATUS:" + status + newline + "StatusCode:" + statusCode + newline);
				await GetStatusMessage();
			}

			if (statusCode == "000.000.000" || returnCode.Contains("Approved") == true)
			{
				//insert details to db - customer
				//inMngr.InsertPaymentIBLookUp(Convert.ToInt32(customerRefNum), CustomerId, sLastFourDigits, Convert.ToInt32(Session["chaseMid"]), 0, 1, DateTime.Now, Convert.ToInt32(Session["AddCardSelectedBillingId"]));
				InstantbuyLookupRequestModel request = new InstantbuyLookupRequestModel();
				request.active = 1;
				request.addressId = Convert.ToInt32(Session["AddCardSelectedBillingId"]);
				request.cardBrand = cardBrand;
				request.cardExpiryDate = cardExpiryDate;
				request.mitTxId = mitTxId;
				if (customerRefNum != "Null")
				{
					request.chaseCustRefNum = Convert.ToInt32(customerRefNum);
				}
				else
				{
					throw new Exception("InsertSuccessValue - customerRefNum cannot be null");
				}

				request.chaseMid = Convert.ToInt32(Session["chaseMid"]);
				CustomerId = Convert.ToInt32(Session["CustomerId"]);
				if (CustomerId == 0)
				{
					request.customerId = 0;
					int? cid = await GetCustomerId();
					if (cid != null)
					{
						request.customerId = Convert.ToInt32(cid);
						Session["CustomerId"] = request.customerId;
					}
					else
					{
						throw new Exception("Customer id is null, there is a problem on InsertSuccessValue");
					}
				}
				else
				{
					request.customerId = Convert.ToInt32(CustomerId);
				}
				request.lastFourDigits = lastFourDigits;
				request.webOrPhone = 0;

				if (avsResult == "F" || currencyForAvs == "EUR" || returnCode.Contains("Approved"))
				{
					WebRequestHelper wh = new WebRequestHelper();
					Uri uri = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Payment/InsertInstantbuyLookup");

					var postData = new List<KeyValuePair<string, string>>();

					postData.Add(new KeyValuePair<string, string>("active", request.active.ToString()));
					postData.Add(new KeyValuePair<string, string>("addressId", request.addressId.ToString()));
					postData.Add(new KeyValuePair<string, string>("cardBrand", request.cardBrand.ToString()));
					postData.Add(new KeyValuePair<string, string>("cardExpiryDate", request.cardExpiryDate.ToString()));
					postData.Add(new KeyValuePair<string, string>("chaseCustRefNum", request.chaseCustRefNum.ToString()));
					postData.Add(new KeyValuePair<string, string>("chaseMid", request.chaseMid.ToString()));
					postData.Add(new KeyValuePair<string, string>("customerId", request.customerId.ToString()));
					postData.Add(new KeyValuePair<string, string>("lastFourDigits", request.lastFourDigits.ToString()));
					postData.Add(new KeyValuePair<string, string>("webOrPhone", request.webOrPhone.ToString()));
					newIdLook = await wh.PostRequestString(uri, postData);

					inserted = true;
				}
			}
			return newIdLook;
		}
		//CustomerLookupId: result.data.CustomerLookupId, Email: $scope.email, Value: $scope.persSignIn, loginDate: loginDate


		private async Task<int?> GetCustomerId()
		{
			int? id = null;

			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				WebRequestHelper wh = new WebRequestHelper();
				string keepMeSignedInDecoded = HttpUtility.UrlDecode(Request.Cookies["keepMeSignedIn"].Value);
				var keepMeSignedIn = new JavaScriptSerializer().Deserialize<KeepMiSignedInModel>(keepMeSignedInDecoded);
				//if (keepMeSignedIn != null)
				//{
				string url = ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Account/GetCustomerIdByLookupId?lookupId=" + keepMeSignedIn.CustomerLookupId;
				string idString = await wh.GetItemString(url);
				int id2 = 0;
				Int32.TryParse(idString, out id2);
				if (id2 > 0)
				{
					id = id2;
				}
				//}
			}

			return id;
		}

		public async Task<XDocument> GetStatus()
		{
			XDocument xdoc = null;
			if (Session["doPrepGuid"] != null)
			{
				string doPrepGuid = Session["doPrepGuid"].ToString();
				string token = Session["token"].ToString();
				WebRequestHelper webHelper = new WebRequestHelper();
				string url = ConfigurationManager.AppSettings.Get("getStatusLive") + doPrepGuid + "&token=" + token;
				string result = await webHelper.GetItemString(url);
				xdoc = XDocument.Parse(result);
			}
			return xdoc;
		}

		public async Task<bool> GetStatusMessage()
		{
			XDocument xdocElements = await GetStatus();
			string statusMessage = string.Empty, transactionMode = string.Empty;
			string connectorTxID1 = string.Empty;
			mitTxId = string.Empty;
			if (xdocElements != null)
			{
				statusMessage = xdocElements.Root.Element("Transaction").Element("Processing").Element("Return") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("Return").Value : "Null";
				Session["orderStatusXML"] = statusMessage;
				transactionMode = xdocElements.Root.Element("Transaction").Attribute("mode") != null ? xdocElements.Root.Element("Transaction").Attribute("mode").Value : "";

				if (transactionMode == "WRONG" || statusMessage.Contains("invalid Request Message") || statusMessage.Contains("invalid identification") || statusMessage.Contains("declined"))
				{
					transStatus = false;
				}
				else
				{
					resId = xdocElements.Root.Element("Transaction").Element("Identification").Element("UUID") != null ? xdocElements.Root.Element("Transaction").Element("Identification").Element("UUID").Value : "Null";
					statusCode = xdocElements.Root.Element("Transaction").Element("Processing").Element("Return").Attribute("code") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("Return").Attribute("code").Value : "Null";
					avsResult = xdocElements.Root.Element("Transaction").Element("Processing").Element("AVSResult") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("AVSResult").Value : "Null";

					if (statusCode != "000.200.100" && statusCode != "000.200.200" && statusCode != "900.300.600")
					{
						chaseOrderId = xdocElements.Root.Element("Transaction").Element("Identification").Element("OrderID") != null ? xdocElements.Root.Element("Transaction").Element("Identification").Element("OrderID").Value : "Null";

						dateStamp = xdocElements.Root.Element("Transaction").Element("Processing").Attribute("requestTimestamp") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Attribute("requestTimestamp").Value : "Null";

						lastFourDigits = xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Last4") != null ? xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Last4").Value : "Null";

						cardBrand = xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Brand") != null ? xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Brand").Value : "Null";

						var cardMonth = xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Expiry") != null ? xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Expiry").Attribute("month").Value : "Null";
						var cardYear = xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Expiry") != null ? xdocElements.Root.Element("Transaction").Element("CreditCardAccount").Element("Expiry").Attribute("year").Value : "Null";
						cardExpiryDate = cardMonth + "/" + cardYear;

						customerRefNum = xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorTxID3") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorTxID3").Value : "Null";

						connectorTxID1 = xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorTxID1") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorTxID1").Value : "Null";
						if (cardBrand.ToLower() == "visa")
						{
							if (connectorTxID1 != "Null")
							{
								var desc = connectorTxID1.Split(';');
								if (desc.Length >= 3)
								{
									textRefNum = desc[0];
									mitTxId = desc[2];
								}
							}
						}
						//cvvResponseCode = xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorDetails") != null ? xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorDetails").Element("Result").Attributes("name")  : "Null";
						returnCode = xdocElements.Root.Element("Transaction").Element("ConnectorReceived") != null ? xdocElements.Root.Element("Transaction").Element("ConnectorReceived").Element("Returned") != null ? xdocElements.Root.Element("Transaction").Element("ConnectorReceived").Element("Returned").Value : "Null" : "Null";

						var cvvResponseCodesList = xdocElements.Root.Element("Transaction").Element("Processing").Element("ConnectorDetails");
						if (cvvResponseCodesList != null)
						{
							var cvvResponseCodes = cvvResponseCodesList.Elements();
							if (cvvResponseCodes != null)
							{
								foreach (var item in cvvResponseCodes)
								{
									if (item.FirstAttribute.Value.Contains("CVV2RespCode"))
									{
										cvvResponseCode = item.Value;
										break;
									}

								}
								if (!string.IsNullOrEmpty(cvvResponseCode))
								{
									Session["cvvRespCode"] = cvvResponseCode;
								}
							}
						}
					}
					transStatus = true;
				}
			}
			return transStatus;
		}

		public async Task<JsonResult> CreateXml(PaymentInfoModel info, bool isCallCentre = false)
		{
			string paymentUrl = string.Empty;
			string custoEmail = info.CustomerEmail;
			HttpBrowserCapabilitiesBase browser = Request.Browser;
			string browserInfo = browser != null ? browser.Type : "";
			WebRequestHelper webHelper = new WebRequestHelper();
			try
			{
				string redirectUrl = "";
				string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
				MerchantAccountModel account = null;

				if (isCallCentre)
				{
					account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetCallCentreMerchantAccount?currencyID=" + BaseCurrencyID);
				}
				else
				{
					account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetMerchantAccount?currencyID=" + BaseCurrencyID);
				}

				string merchantID = string.Empty;
				string transCategory = string.Empty;
				string currencyName = string.Empty;
				if (account != null)
				{
					merchantID = account.MerchantID;
					Session["chaseMid"] = Convert.ToInt32(merchantID);
					transCategory = account.IndustryName;
					currencyName = account.lettercode3;
					Session["currencyTypeNameP"] = currencyName;
				}

				if (info.pageId == 1)
				{
					redirectUrl = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings.Get("MyAccountRedirectUrl");

				}
				else if (info.pageId == -1)
				{
					var reservationId = Guid.Parse(Session["callCentreReservationId"].ToString());
					var shippingAddressId = int.Parse(Session["callCentreShippingAddressId"].ToString());
					var billingAddressId = int.Parse(Session["callCentreBillingAddressId"].ToString());

					var queryString = $"?page=2&reservationId={reservationId}&shippingAddressId={shippingAddressId}&billingAddressId={billingAddressId}";

					redirectUrl = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings.Get("redirectUrl") + queryString;
				}
				else
				{
					redirectUrl = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings.Get("redirectUrl");

				}

				string gateWayMode = ConfigurationManager.AppSettings.Get("gateWayMode");
				string requestedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				Session["requestedTime"] = requestedTime;
				int? cid = await GetCustomerId();
				int customerId = 0;
				if (cid != null)
				{
					customerId = Convert.ToInt32(cid);
				}
				string orderID = customerId.ToString() + DateTime.Now.ToString("ddMMyyHmmss");
				Session["chaseOrderID"] = orderID;
				string doPrep = Guid.NewGuid().ToString().Replace("-", "");
				/*if (doPrep.Length > 20)
                {
                    doPrep = doPrep.Substring(0, 20);
                }*/
				string gateWayUserID = ConfigurationManager.AppSettings.Get("gateWayUserID");
				string gateWayPassword = ConfigurationManager.AppSettings.Get("gateWayPassword");
				string cvvInstruction = string.Empty;
				string custoLastName = info.CustomerLastname;
				string custoGivenName = info.CustomerFirstname;
				string userIpAddress = string.Empty;
				string custoPhone = info.CustomerPhoneNo;
				string custoCountry = string.Empty;
				string custoCity = info.CustomerTown;
				string custoStreet = info.CustomerAddress;
				if (custoStreet.Length >= 30)
				{
					custoStreet = custoStreet.Substring(0, 30);
				}
				string custoPostcode = info.CustomerPostcode;
				string amount = info.BasketTotal.ToString();

				Session["AddCardSelectedBillingId"] = info.AddressId;
				//
				//Guid doPrepGuid = Guid.NewGuid();
				//MyWebRequest myRequest = new MyWebRequest(doPrepare, "POST", "transactionSecret=" + doPrepGuid.ToString().Replace("-", "") + "&requestXML=" + createsxml);

				custoCountry = CustomerCountryCode;
				Session["custoCountryAVS"] = custoCountry;

				if (custoCountry == "GB" || custoCountry == "US")
				{
					if (gateWayMode.Contains("TEST"))
					{
						cvvInstruction = "IGNORE";
					}
					else
					{
						cvvInstruction = "CHECK";
					}

				}
				else
				{
					cvvInstruction = "IGNORE";
				}

				XElement storedCredentials = new XElement("Parameter", new XAttribute("name", "StoredCredentialType"));
				storedCredentials.Value = "CIT";

				XElement mitMsgType = new XElement("Parameter", new XAttribute("name", "MIT_MSG_TYPE"));
				mitMsgType.Value = "CSTO";


				XDocument paymentXML = new XDocument(
				new XDeclaration("1.0", "UTF-8", string.Empty),
				new XElement("Request",
				 new XAttribute("version", "1.0"),
					new XElement("Frontend",
						 new XElement("UseJavascriptFieldValidation", "true"),

					new XElement("Form",

					 new XElement("Element",
					 new XAttribute("forClass", "custom_waitingDialog"),



					 //new XAttribute("label", GetLocalResourceObject("RescGlobalCardVerify_1.Text").ToString())),
					 new XAttribute("label", "Thank you. Your card is currently being verified. Please do not close this browser window. Your cards can be managed in your 'My Account'.")),



					  new XElement("Element",
					 new XAttribute("forClass", "custom_visaCardVerification"),
					  new XAttribute("mandatory", "true")),
					   new XElement("Element",
					 new XAttribute("forClass", "custom_masterCardVerification"),
					  new XAttribute("mandatory", "true")),
					   new XElement("Element",
					 new XAttribute("forClass", "custom_maestroCardVerification"),
					  new XAttribute("mandatory", "true")),
					   new XElement("Element",
					 new XAttribute("forClass", "custom_amexCardVerification"),
					  new XAttribute("mandatory", "true"))),


					new XElement("RedirectURL", redirectUrl),

					new XElement("CSSPath",
					//new XAttribute("basedOn", "SCREEN"),
					"https://hochanda.com/css/checkout-override.css"),

					  new XElement("Customer",
						  new XAttribute("secondStreetLine", "true"),
						   new XAttribute("streetLinesLengthValidation", "true"))),
					 new XElement("Transaction",
						 new XAttribute("mode", gateWayMode),//LIVE TEST
						 new XAttribute("requestTimestamp", requestedTime),//add current datetime with format "1:14:58 PM Tue, August 11th 2015"
											 new XElement("Identification",

						new XElement("OrderID", orderID), //"ORD"+chaseid = ORD8/ not more than 22 characters 
						new XElement("UUID", doPrep)),
					 new XElement("PaymentMethods",
					  new XElement("PaymentMethod",
						   new XAttribute("subTypes", "VISA, MASTER, MAESTRO, AMEX"),
						  new XElement("MerchantAccount",
						new XAttribute("type", "CHASEPAYMENTECH"),
						  new XAttribute("Secret", "true"),
						new XElement("MerchantID", merchantID),//will be changed based on the currency on header dropdown

						new XElement("TerminalID", "001"),
						 new XElement("Username", gateWayUserID),//P2583HOCH live T2583HOCH test
						new XElement("Password", gateWayPassword),//G45QYPT9W live GR43ZTRFX test
						 new XElement("TransactionCategory", transCategory),//change this bit after testing to transCatogery
						  new XElement("RecurringCode", "NONE"),
						   new XElement("CVVValidation",
							new XAttribute("instruction", cvvInstruction)),
							 new XElement("AVS",
							new XAttribute("instruction", cvvInstruction))
						//new XElement("MerchantUrl", ""),
						// new XElement("MerchantCountry", "826")//??


						))),
						new XElement("Payment",
							new XAttribute("type", "RA"),//RDauth capture
					 new XElement("Amount", amount),//replace this value with total price value
							new XElement("Currency", currencyName)),//replaace this value with the selected value
						 new XElement("Customer",//customer info from customer service table
						 new XElement("Name",
							new XElement("Family", custoLastName),
							new XElement("Given", custoGivenName)),
							new XElement("Contact",
							 new XElement("Ip", userIpAddress),//??
							  new XElement("Email", custoEmail),
						new XElement("Phone", custoPhone),
						new XElement("Mobile", "")),
							new XElement("Address",
							new XElement("Country", custoCountry),
							new XElement("City", custoCity),
							new XElement("Street", custoStreet),
						new XElement("Zip", custoPostcode))),
						new XElement("Parameters",
							storedCredentials,
							mitMsgType
							)
						)));


				if (ConfigurationManager.AppSettings.Get("EnableLogMonitor") == "1")
				{
					List<string> messages = new List<string>();
					messages.Add("FunctionName: CheckoutController/CreateXml");
					messages.Add("Email: " + custoEmail);
					messages.Add("Country: " + custoCountry);
					messages.Add("City: " + custoCity);
					messages.Add("Street: " + custoStreet);
					messages.Add("Zip: " + custoPostcode);
					messages.Add("BrowserName: " + browserInfo);
					Utility.WriteLogMonitor(messages);
				}

				Guid doPrepGuid = Guid.NewGuid();
				string doPrepGuidString = doPrepGuid.ToString().Replace("-", "");
				string doPrepare = ConfigurationManager.AppSettings.Get("doPrepareLive");
				Uri uri = new Uri(doPrepare);
				var postData = new List<KeyValuePair<string, string>>();
				postData.Add(new KeyValuePair<string, string>("transactionSecret", doPrepGuidString.ToString()));
				postData.Add(new KeyValuePair<string, string>("requestXML", paymentXML.ToString()));

				string result = await webHelper.PostRequestString(uri, postData);
				XmlDocument xmltest = new XmlDocument();
				xmltest.LoadXml(result);
				XmlNodeList tokenNodes = xmltest.GetElementsByTagName("Token");
				string token = String.Empty;
				if (tokenNodes.Count > 0)
				{
					token = tokenNodes[0].ChildNodes[0].Value;
				}
				string doPay = ConfigurationManager.AppSettings.Get("doPayLive");
				paymentUrl = doPay + token;
				Session["doPrepGuid"] = doPrepGuidString;
				Session["token"] = token;
			}
			catch (Exception e)
			{
				await webHelper.InsertErrorToPostgres("CreateXml customerEmail: " + custoEmail + " - Browser: " + browserInfo + ": " + e.Message, "1", e.ToString(), 1);
			}
			return Json(paymentUrl);
		}

		public async Task<ActionResult> SubmitOrder(PaymentByCardModel info, bool useCitMit = true, bool isCallCentre = false)
		{
			string res = string.Empty;

			WebRequestHelper webHelper = new WebRequestHelper();

			HttpBrowserCapabilitiesBase browser = Request.Browser;
			string browserInfo = browser != null ? browser.Type : "";

			try
			{
				if (info.AddressId != 0)
				{
					string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
					string merchantID = string.Empty;
					string industryType = string.Empty;
					string currencyType = string.Empty;

					MerchantAccountModel account = null;

					if (isCallCentre)
					{
						account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetCallCentreMerchantAccount?currencyID=" + BaseCurrencyID);
					}
					else
					{
						account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetMerchantAccount?currencyID=" + BaseCurrencyID);
					}

					if (account != null)
					{
						merchantID = account.MerchantID;
						industryType = account.IndustryType;
						currencyType = account.CurrencyType;
					}

					var postDataRes = new List<KeyValuePair<string, string>>();

					postDataRes.Add(new KeyValuePair<string, string>("BasketId", info.BasketId));
					postDataRes.Add(new KeyValuePair<string, string>("PerformCalculation", "true"));
					postDataRes.Add(new KeyValuePair<string, string>("CountryID", CustomerIPCountry.ToString()));
					postDataRes.Add(new KeyValuePair<string, string>("CurrencyID", BaseCurrencyID.ToString()));
					postDataRes.Add(new KeyValuePair<string, string>("LanguageID", BaseLanguageID.ToString()));
					postDataRes.Add(new KeyValuePair<string, string>("IsFlexi", "-1"));
					postDataRes.Add(new KeyValuePair<string, string>("IsItemFlexi", "-1"));

					var reservation = await webHelper.PostRequest<BasketCheckoutModel>(new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/GetBasketDisplay"), postDataRes);

					List<ReservationItemsModel> items = reservation.displayCheckout.reservationItemsModel;
					List<ReservationsModel> reservationModels = reservation.displayCheckout.reservationModel;
					ReservationsModel reservationModel = reservationModels[0];

					decimal deliveryCost = items.Where(i => i.ParentProductID == 22 || i.ParentProductID == 23 && i.Quantity > 0).Sum(i => i.UnitPrice);

					int isPendingOrder = items.Count(l => l.AuctionItem == 1 && l.PendingStatus == 0);

					if (isPendingOrder == 0) // no live auction items or live items in the reservation. Go ahead as normal
					{
						if (ConfigurationManager.AppSettings.Get("EnableLogMonitor") == "1")
						{
							List<string> messages = new List<string>();

							messages.Add("FunctionName: CheckoutController/SubmitOrder");
							messages.Add("BasketId: " + info.BasketId);
							messages.Add("BillingAddressId: " + info.CustomerBillingAddress.AddressId.ToString());
							messages.Add("BillingPostcode: " + info.CustomerBillingAddress.Postcode);
							messages.Add("BillingStreet1: " + info.CustomerBillingAddress.StreetAddress1);
							messages.Add("ShippingAddressId: " + info.CustomerShippingAddress.AddressId.ToString());
							messages.Add("ShippingPostcode: " + info.CustomerShippingAddress.Postcode);
							messages.Add("ShippingStreet1: " + info.CustomerShippingAddress.StreetAddress1);
							messages.Add("CardLast4Digits: " + info.CardLast4Digits);
							messages.Add("customerid: " + info.CustomerData.customerid);
							messages.Add("BrowserName: " + browserInfo);
							messages.Add("emailaddress: " + info.CustomerData.emailaddress);

							Utility.WriteLogMonitor(messages);
						}

						int payMinVal = Convert.ToInt32(ConfigurationManager.AppSettings.Get("payMinVal"));

						var postData = new List<KeyValuePair<string, string>>();
						postData.Add(new KeyValuePair<string, string>("BasketId", info.BasketId));

						string reservationId = await webHelper.PostRequest<string>(new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/GetReservationIdFromBasketId"), postData);

						string keepMeSignedInDecoded = HttpUtility.UrlDecode(Request.Cookies["keepMeSignedIn"].Value);
						var keepMeSignedIn = new JavaScriptSerializer().Deserialize<KeepMiSignedInModel>(keepMeSignedInDecoded);

						string idString = await webHelper.GetItemString(apiurl + "Account/GetCustomerIdByLookupId?lookupId=" + keepMeSignedIn.CustomerLookupId);

						var postDataFinal = new List<KeyValuePair<string, string>>();
						postDataFinal.Add(new KeyValuePair<string, string>("CustomerId", idString));
						postDataFinal.Add(new KeyValuePair<string, string>("CurrencyId", BaseCurrencyID.ToString()));
						postDataFinal.Add(new KeyValuePair<string, string>("OrderTotal", reservationModel.ToPayToday.ToString()));

						var uriToUseFinal = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Payment/GetFinalPrice");
						var customerCredit = await webHelper.PostRequest<CustomerCreditModel>(uriToUseFinal, postDataFinal);

						decimal finalPrice = reservationModel.ToPayToday;
						decimal creditValue = 0;
						string creditCurrencySymbol = "£";

						if (customerCredit != null)
						{
							finalPrice = (decimal)customerCredit.BillAmount;
							creditValue = (decimal)customerCredit.CreditValueFree;
							creditCurrencySymbol = customerCredit.CurrencySymbol;
						}

						string cc = GenericHelper.Instance.ConvertDecimalValueToCurrency(creditCurrencySymbol, (double)creditValue);
						decimal amount = finalPrice * 100;

						Session["TotalAmount"] = finalPrice;
						Session["CustomerEmail"] = info.CustomerData.emailaddress;

						Session["PaymentType"] = "Card";

						string amountString = Convert.ToInt32(amount).ToString();

						if (finalPrice < payMinVal)
						{
							int orderId = await CreateNewOrder(info.BasketId);

							PaymentNotificationModel notification = new PaymentNotificationModel();
							notification.processorid = 1;
							notification.orderid = orderId;
							notification.reservationid = reservationId;
							notification.result = 1;
							notification.datestamp = DateTime.Now;
							notification.last4digits = "";
							notification.currencytype = currencyType;
							notification.chaseorderid = "";
							notification.transactiontype = 1;
							notification.chaseresid = "";
							notification.txrefnum = "Purchase with Amount < " + payMinVal.ToString();
							notification.processed = 0;
							notification.paymentamount = finalPrice;
							notification.mitReceivedTxId = "";
							notification.mitMsgType = "";
							await InsertNotification(notification);
							res = orderId.ToString();
							bool r = await SendConfirmationOrder(info.BasketId, orderId);

						}
						else
						{
							////below test
							//OrbitalPaymentWSTest.PaymentechGatewayPortTypeClient client = new OrbitalPaymentWSTest.PaymentechGatewayPortTypeClient();
							//client.ClientCredentials.UserName.UserName = "T2583HOCH";
							//client.ClientCredentials.UserName.Password = "GR43ZTRFX";
							////below test
							//OrbitalPaymentWSTest.NewOrderRequestElement reqElement = new OrbitalPaymentWSTest.NewOrderRequestElement();
							//OrbitalPaymentWSTest.NewOrderResponseElement respElement = new OrbitalPaymentWSTest.NewOrderResponseElement();
							//reqElement.orbitalConnectionUsername = "T2583HOCH";
							//reqElement.orbitalConnectionPassword = "GR43ZTRFX";


							//below live
							OrbitalPaymentWS.PaymentechGatewayPortTypeClient client = new OrbitalPaymentWS.PaymentechGatewayPortTypeClient();
							client.ClientCredentials.UserName.UserName = "P2583HOCH";
							client.ClientCredentials.UserName.Password = "G45QYPT9W";
							// below live
							OrbitalPaymentWS.NewOrderRequestElement reqElement = new OrbitalPaymentWS.NewOrderRequestElement();
							OrbitalPaymentWS.NewOrderResponseElement respElement = new OrbitalPaymentWS.NewOrderResponseElement();
							reqElement.orbitalConnectionUsername = "P2583HOCH";
							reqElement.orbitalConnectionPassword = "G45QYPT9W";


							reqElement.industryType = industryType;
							reqElement.transType = "AC";
							reqElement.bin = "000001";
							reqElement.merchantID = merchantID;
							reqElement.terminalID = "001";
							reqElement.avsCountryCode = account.AVSCountryCode;

							string customerId = Session["CustomerId"] != null ? Session["CustomerId"].ToString() : string.Empty;
							string chaseOrderId = customerId + DateTime.Now.ToString("ddMMyyHmmss");
							reqElement.orderID = chaseOrderId;
							reqElement.amount = amountString;
							reqElement.customerRefNum = info.ChaseCustomerRefNum.ToString();

							/* ------------------------------ new implementation for cit/mit management ------------------------------  */
							//crec = freedom
							//cins = flexi
							//cgen = mix
							//cuse = no flexi no freedom
							List<ReservationItemsModel> itemsNoDelivery = items.Where(i => i.ParentProductID != 22 && i.ParentProductID != 23 && i.Quantity > 0).ToList();
							string mitMsgType = string.Empty;
							ReservationItemsModel freedomList = items.Find(i => i.ParentProductID == 26 && i.Quantity > 0);
							List<ReservationItemsModel> flexiList = items.FindAll(i => i.FlexiBuyItem == 1 && i.Quantity > 0);

							//CREC: if there is a freedom 
							if (freedomList != null)
							{
								mitMsgType = "CREC";
							}
							else
							{
								//CINS: if there is a flexiitem
								if (flexiList != null && flexiList.Count > 0)
								{
									mitMsgType = "CINS";
								}
								else
								{
									//CUSE: if no freedom, no flexibuy items means only one off items then set to CUSE
									mitMsgType = "CUSE";
								}
							}

							//There is an issue with Cards which are not VISA or MasterCard. So to fix this we are going to 
							if (useCitMit)
							{
								reqElement.mitMsgType = mitMsgType;
								reqElement.mitStoredCredentialInd = "Y";
								reqElement.mitSubmittedTransactionID = null;
							}
							else
							{
								try
								{
									var message = $"Attempting Non CIT/MIT for customer: {customerId}. ReservationId: {reservationId}";

									Utility.WriteLogMonitor(message);
								}
								catch
								{
									//Do nothing
								}
							}

							reqElement.version = "7.4.10";

							/* ------------------------------ new implementation for cit/mit management ------------------------------  */

							try
							{
								respElement = client.NewOrder(reqElement);

								string approvalStatus = respElement.approvalStatus;
								string profileProcStatus = respElement.profileProcStatus;
								string procStatus = respElement.procStatus;
								int processorID = 0;
								int orderId = 0;
								int payNResult = 0;
								string lastFourDigits = info.CardLast4Digits;
								int processed = 0;
								int transactionId = 1;
								string chaseResId = string.Empty;
								string txtRefNum = respElement.txRefNum;
								PaymentNotificationModel notification = new PaymentNotificationModel();
								//payment success
								if (approvalStatus == "1" && profileProcStatus == "0" && procStatus == "0")
								{
									processorID = 1;
									payNResult = 1;
									orderId = await CreateNewOrder(info.BasketId);
									notification.processorid = processorID;
									notification.orderid = orderId;
									notification.reservationid = reservationId;
									notification.result = payNResult;
									notification.datestamp = DateTime.Now;
									notification.last4digits = lastFourDigits;
									notification.processed = processed;
									notification.paymentamount = finalPrice;
									notification.currencytype = currencyType;
									notification.chaseorderid = chaseOrderId;
									notification.transactiontype = transactionId;
									notification.chaseresid = chaseResId;
									notification.txrefnum = txtRefNum;
									notification.mitReceivedTxId = respElement.mitReceivedTransactionID != null ? respElement.mitReceivedTransactionID : string.Empty;
									notification.mitMsgType = mitMsgType;
									res = orderId.ToString();
								}
								else
								{
									notification.processorid = processorID;
									notification.orderid = orderId;
									notification.reservationid = reservationId;
									notification.result = payNResult;
									notification.datestamp = DateTime.Now;
									notification.last4digits = lastFourDigits;
									notification.processed = processed;
									notification.paymentamount = finalPrice;
									notification.currencytype = currencyType;
									notification.chaseorderid = chaseOrderId;
									notification.transactiontype = transactionId;
									notification.chaseresid = chaseResId;
									notification.txrefnum = txtRefNum;
									notification.mitReceivedTxId = respElement.mitReceivedTransactionID != null ? respElement.mitReceivedTransactionID : string.Empty;
									notification.mitMsgType = mitMsgType;
									res = "failure-" + respElement.respCode;

									await webHelper.InsertErrorToPostgres("Chase Payment Transaction Failed", "Checkout - SubmitOrder reservationId: " + reservationId + " - customerId: " + customerId + " - Browser: " + browserInfo + " - ErrorCode: " + respElement.respCode, "1", 0);
								}
								await InsertNotification(notification);
								if (!res.Contains("failure"))
								{
									bool r = await SendConfirmationOrder(info.BasketId, orderId);
								}
							}
							catch (Exception ex)
							{
								//PaymentNotificationModel notification = new PaymentNotificationModel();
								//await InsertNotification(notification);
								res = "failure-05";
								await webHelper.InsertErrorToPostgres("Checkout - SubmitOrder customerId: " + customerId + " - Browser: " + browserInfo + " - Exception: " + ex.Message, "1", ex.ToString(), 1);

								var errorCode = ex.Message.Substring(0, 5);

								if (errorCode == "19795" && useCitMit)
								{
									return await SubmitOrder(info, false);
								}
							}
						}

					}
					else
					{
						//redirect to pending order page
					}
				}
			}
			catch (Exception e)
			{
				await webHelper.InsertErrorToPostgres("SubmitOrder BasketId: " + info.BasketIdToUse.ToString() + " - Exception: " + e.Message, "1", e.ToString(), 1);
				res = "failure-GEN";
			}

			if (isCallCentre)
			{
				var cookie = Request.Cookies["keepMeSignedIn"];

				if (cookie != null)
				{
					cookie.Value = null;
					cookie.Expires = DateTime.Now.AddDays(-20);

					Response.Cookies.Add(cookie);
				}

				cookie = Request.Cookies["BaseCookieSettings"];

				if (cookie != null)
				{
					cookie.Value = null;
					cookie.Expires = DateTime.Now.AddDays(-20);

					Response.Cookies.Add(cookie);
				}
			}


			return Json(res);
		}

		public async Task<int> CreateNewOrder(string basketId)
		{
			int orderId = 0;
			WebRequestHelper webHelper = new WebRequestHelper();
			string url = ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Order/CreateNewOrder";
			Uri uri = new Uri(url);
			var postData = new List<KeyValuePair<string, string>>();
			postData.Add(new KeyValuePair<string, string>("BasketId", basketId.ToString()));
			string idString = await webHelper.PostRequestString(uri, postData);
			if (idString != "1")
			{
				orderId = Convert.ToInt32(idString);
			}
			return orderId;
		}

		public async Task InsertNotification(PaymentNotificationModel model)
		{
			WebRequestHelper wh = new WebRequestHelper();
			Uri uri = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Payment/InsertPaymentNotification");

			string amountString = model.paymentamount.ToString();

			var postData = new List<KeyValuePair<string, string>>();
			postData.Add(new KeyValuePair<string, string>("processorid", model.processorid.ToString()));
			postData.Add(new KeyValuePair<string, string>("orderid", model.orderid.ToString()));
			postData.Add(new KeyValuePair<string, string>("reservationid", model.reservationid.ToString()));
			postData.Add(new KeyValuePair<string, string>("result", model.result.ToString()));
			postData.Add(new KeyValuePair<string, string>("datestamp", model.datestamp.ToString()));
			postData.Add(new KeyValuePair<string, string>("last4digits", model.last4digits.ToString()));
			postData.Add(new KeyValuePair<string, string>("processed", model.processed.ToString()));
			postData.Add(new KeyValuePair<string, string>("paymentamount", amountString));
			postData.Add(new KeyValuePair<string, string>("currencytype", model.currencytype.ToString()));
			postData.Add(new KeyValuePair<string, string>("chaseorderid", model.chaseorderid.ToString()));
			postData.Add(new KeyValuePair<string, string>("transactiontype", model.transactiontype.ToString()));
			postData.Add(new KeyValuePair<string, string>("chaseresid", model.chaseresid.ToString()));
			postData.Add(new KeyValuePair<string, string>("txrefnum", model.txrefnum.ToString()));
			postData.Add(new KeyValuePair<string, string>("mitReceivedTxId", model.mitReceivedTxId.ToString()));
			postData.Add(new KeyValuePair<string, string>("mitMsgType", model.mitMsgType.ToString()));
			await wh.PostRequestString(uri, postData);
		}

		public ActionResult AddCardFailed()
		{
			return View();
		}

		#region newpaypal


		public async Task ReservationsManageLock(string basketId, string lockValue)
		{
			WebRequestHelper wh = new WebRequestHelper();
			string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
			Uri uri2 = new Uri(apiurl + "Reservations/ReservationsManageLock");

			var postData2 = new List<KeyValuePair<string, string>>();
			postData2.Add(new KeyValuePair<string, string>("Lock", lockValue));
			postData2.Add(new KeyValuePair<string, string>("BasketId", basketId));
			await wh.PostRequestString(uri2, postData2);
		}

		public async Task<string> PaypalCreateOrderV2(string basketId, string customerFirstname)
		{
			string id = string.Empty;
			WebRequestHelper webHelper = new WebRequestHelper();
			HttpBrowserCapabilitiesBase browser = Request.Browser;
			string browserInfo = browser != null ? browser.Type : "";
			try
			{
				string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
				MerchantAccountModel account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetMerchantAccount?currencyID=" + BaseCurrencyID);
				string currencyName = "GBP";

				if (account != null)
				{
					currencyName = account.lettercode3;
				}
				if (currencyName == "GBP")
				{
					if (BaseCurrencyID == 2)//German
					{
						currencyName = "EUR";
					}
				}
				else
				{
					if (BaseCurrencyID == 1)//UK
					{
						currencyName = "GBP";
					}
				}

				var postData = new List<KeyValuePair<string, string>>();
				postData.Add(new KeyValuePair<string, string>("BasketId", basketId));
				var uriToUse = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/GetReservationIdFromBasketId");
				var reservationId = await webHelper.PostRequestString(uriToUse, postData);
				char quote = '"';
				reservationId = reservationId.Trim(quote);

				decimal deliveryprice = 0;
				List<ReservationItemsModel> itemsWithNoDelivery = new List<ReservationItemsModel>();

				var uri = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/GetBasketDisplay");

				var postDataRes = new List<KeyValuePair<string, string>>();
				postDataRes.Add(new KeyValuePair<string, string>("BasketId", basketId));
				postDataRes.Add(new KeyValuePair<string, string>("PerformCalculation", "true"));
				postDataRes.Add(new KeyValuePair<string, string>("CountryID", CustomerIPCountry.ToString()));
				postDataRes.Add(new KeyValuePair<string, string>("CurrencyID", BaseCurrencyID.ToString()));
				postDataRes.Add(new KeyValuePair<string, string>("LanguageID", BaseLanguageID.ToString()));
				postDataRes.Add(new KeyValuePair<string, string>("IsFlexi", "-1"));
				postDataRes.Add(new KeyValuePair<string, string>("IsItemFlexi", "-1"));

				var reservation = await webHelper.PostRequest<BasketCheckoutModel>(uri, postDataRes);

				foreach (var item in reservation.displayCheckout.reservationItemsModel)
				{
					if (item.ParentProductID == 22 || item.ParentProductID == 23)
					{
						if (item.Quantity > 0)
						{
							deliveryprice += item.UnitPrice * item.Quantity;
						}
					}
					else
					{
						if (item.Quantity > 0)
						{
							itemsWithNoDelivery.Add(item);
						}
					}
				}

				Session["PaymentType"] = "PayPal";
				Session["BasketId"] = basketId;
				Session["ReservationId"] = reservationId;

				OrderRequest orderRequest = new OrderRequest();

				ApplicationContext applicationContext = new ApplicationContext
				{
					BrandName = "HOCHANDA LTD",
					LandingPage = "LOGIN",
					CancelUrl = "https://www.paypal.com/checkoutnow/error",
					ReturnUrl = "https://www.paypal.com/checkoutnow/error",
					UserAction = "CONTINUE",
					ShippingPreference = "NO_SHIPPING"
				};
				orderRequest.ApplicationContext = applicationContext;

				List<PurchaseUnitRequest> purchaseUnits = new List<PurchaseUnitRequest>();

				List<Item> items = new List<Item>();
				foreach (var itemRes in itemsWithNoDelivery) //loop through order items and populate paypal items list
				{
					Item item = new Item
					{
						Name = itemRes.TvDescription,
						Description = itemRes.TvDescription,
						Sku = itemRes.ParentProductSKU,
						Quantity = itemRes.Quantity.ToString(),
						UnitAmount = new Money
						{
							CurrencyCode = currencyName,
							Value = itemRes.UnitPrice.ToString()
						},
						Tax = new Money
						{
							CurrencyCode = currencyName,
							Value = "0.00"
						}
					};
					items.Add(item);
				}

				int customerID = 0;
				decimal finalPrice = reservation.displayCheckout.reservationModel[0].ToPayToday;
				decimal finalPriceBeforeCustomerCredit = finalPrice;
				//get logged in user details
				string keepMeSignedInDecoded = HttpUtility.UrlDecode(Request.Cookies["keepMeSignedIn"].Value);
				var keepMeSignedIn = new JavaScriptSerializer().Deserialize<KeepMiSignedInModel>(keepMeSignedInDecoded);
				string url = apiurl + "Account/GetCustomerIdByLookupId?lookupId=" + keepMeSignedIn.CustomerLookupId;
				string idString = await webHelper.GetItemString(url);

				// store the customerid to session
				customerID = Convert.ToInt32(idString);
				Session["CustomerId"] = customerID;

				//get the final price - here is applied the customer credits if the customer has any
				var postDataFinal = new List<KeyValuePair<string, string>>();
				postDataFinal.Add(new KeyValuePair<string, string>("CustomerId", idString));
				postDataFinal.Add(new KeyValuePair<string, string>("CurrencyId", BaseCurrencyID.ToString()));
				postDataFinal.Add(new KeyValuePair<string, string>("OrderTotal", reservation.displayCheckout.reservationModel[0].ToPayToday.ToString()));
				var uriToUseFinal = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Payment/GetFinalPrice");
				var customerCredit = await webHelper.PostRequest<CustomerCreditModel>(uriToUseFinal, postDataFinal);
				decimal creditValue = 0;
				if (customerCredit != null)
				{
					finalPrice = (decimal)customerCredit.BillAmount;
					creditValue = (decimal)customerCredit.CreditValueTotal;
				}
				//store the final price to pay in session for next steps
				Session["TotalAmount"] = finalPrice;

				//if the final price is less then 0 we don't send the data to paypal but we store the order
				if (finalPrice <= 0)
				{
					int processorID = 2;
					int payNResult = 1;
					string transID = "Purchase with Amount = 0";
					string paypalInvoiceID = "PP_" + customerID + DateTime.Now.ToString("yyyyMMddHHmmss");
					int orderId = await CreateNewOrder(basketId); //get index of order just inserted
																  //Session["OrderId"] = orderId;

					PaymentNotificationModel notification = new PaymentNotificationModel
					{
						processorid = processorID,
						orderid = orderId,
						result = payNResult,
						reservationid = reservationId,
						datestamp = DateTime.Now,
						last4digits = "xxxx",
						processed = 0,
						paymentamount = finalPrice,
						currencytype = currencyName,
						chaseorderid = paypalInvoiceID,
						chaseresid = string.Empty,
						txrefnum = transID,
						transactiontype = 1,
						mitReceivedTxId = "",
						mitMsgType = ""
					};
					await InsertNotification(notification);
				}
				else
				{
					decimal amountValue = finalPrice;
					//if customer credit has been applied set amountValue to the reservation ToPayToday in order to calculate the itemTotal.
					//If I used the finalPrice to calculate the itemTotal the paypal APIs return error "total_mismatch" that's because the finalPrice has already the credit applied 
					//and when I deduct the delivery price from it the result won't match with the amount that paypal APIs calcuate adding up the items prices within the Items property.
					if (finalPrice != finalPriceBeforeCustomerCredit)
					{
						amountValue = finalPriceBeforeCustomerCredit;
					}
					decimal itemTotal = amountValue - deliveryprice;
					purchaseUnits.Add(new PurchaseUnitRequest
					{
						Description = "Order for " + keepMeSignedIn.Firstname + " made on " + DateTime.Now.ToString("dd-MM-yyyy HH:mm tt"),
						InvoiceId = "PP_" + customerID + DateTime.Now.ToString("yyyyMMddHHmmss"),
						Amount = new AmountWithBreakdown
						{
							CurrencyCode = currencyName,
							Value = finalPrice.ToString(),
							Breakdown = new AmountBreakdown
							{
								ItemTotal = new Money
								{
									CurrencyCode = currencyName,
									Value = itemTotal.ToString()
								},
								Shipping = new Money
								{
									CurrencyCode = currencyName,
									Value = deliveryprice.ToString()
								},
								Handling = new Money
								{
									CurrencyCode = currencyName,
									Value = "0.00"
								},
								TaxTotal = new Money
								{
									CurrencyCode = currencyName,
									Value = "0.00"
								},
								ShippingDiscount = new Money
								{
									CurrencyCode = currencyName,
									Value = creditValue.ToString()
								}
							}

						},
						Items = items
					});

					/*var firstLine = string.Empty;
                    var secondLine = string.Empty;

                    if (string.IsNullOrEmpty(info.CustomerBillingAddress.BuildingName))
                    {
                        bool hasSecondLineBeenUsed = false;
                        //Make sure we have a building number.
                        if (!string.IsNullOrEmpty(info.CustomerBillingAddress.BuildingNumber))
                        {
                            firstLine = info.CustomerBillingAddress.BuildingNumber;
                        }

                        if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress1))
                        {
                            firstLine += " " + info.CustomerBillingAddress.StreetAddress1;
                        }
                        else if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress2))
                        {
                            firstLine += " " + info.CustomerBillingAddress.StreetAddress2;
                            hasSecondLineBeenUsed = true;
                        }

                        if (!hasSecondLineBeenUsed)
                        {
                            if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress2))
                            {
                                secondLine = info.CustomerBillingAddress.StreetAddress2;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress3))
                            {
                                secondLine = info.CustomerBillingAddress.StreetAddress3;
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(info.CustomerBillingAddress.BuildingNumber))
                    {
                        if (!string.IsNullOrEmpty(info.CustomerBillingAddress.BuildingName))
                        {
                            firstLine = info.CustomerBillingAddress.BuildingName;

                            if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress1))
                            {
                                secondLine = info.CustomerBillingAddress.StreetAddress2;
                            }
                            else if (!string.IsNullOrEmpty(info.CustomerBillingAddress.StreetAddress2))
                            {
                                secondLine = info.CustomerBillingAddress.StreetAddress2;
                            }
                        }
                    }*/

					orderRequest.PurchaseUnits = purchaseUnits;
					orderRequest.Intent = "CAPTURE";
					var createOrderResult = new Order();
					try
					{
						var createOrderResponse = CreateOrderSdk.CreateOrder(orderRequest).Result;
						createOrderResult = createOrderResponse.Result<Order>();
						id = createOrderResult.Id;
						if (!string.IsNullOrEmpty(id))
						{
							await ReservationsManageLock(basketId, "1");
						}
					}
					catch (Exception e)
					{
						await webHelper.InsertErrorToPostgres("paypal - BasketId: " + basketId + " - Browser: " + browserInfo + "Exception: " + e.InnerException.Message, "1", e.ToString(), 1);
					}
				}
			}
			catch (Exception ex)
			{
				await webHelper.InsertErrorToPostgres("paypal - BasketId: " + basketId + " - Browser: " + browserInfo + "Exception: " + ex.Message, "1", ex.ToString(), 1);

			}
			return new JavaScriptSerializer().Serialize(id);
		}


		public async Task<string> PaypalApproveOrderV2(string paypalOrderId)
		{
			try
			{
				var message = $"Starting Paypal Approve Order V2";

				Utility.WriteLogMonitor(message);
			}
			catch
			{
				//Do nothing
			}

			OrderReturnToView ret = new OrderReturnToView
			{
				IsPaypal = true,
				Result = "failure-05"
			};

			try
			{
				WebRequestHelper webHelper = new WebRequestHelper();
				HttpBrowserCapabilitiesBase browser = Request.Browser;
				string browserInfo = browser != null ? browser.Type : "";
				string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
				/*-------------------------------------------------------------------------------*/
				var captureOrderResponse = await CaptureOrderSdk.CaptureOrder(paypalOrderId, false);
				var captureOrderResult = captureOrderResponse.Result<Order>();

				if (captureOrderResult.Status.Contains("COMPLETED"))
				{
					string paypalInvoiceID = captureOrderResult.PurchaseUnits[0].InvoiceId;
					string currencyName = captureOrderResult.PurchaseUnits[0].Items[0].UnitAmount.CurrencyCode;
					string transID = captureOrderResult.PurchaseUnits[0].Payments.Captures[0].Id;
					string basketId = Session["BasketId"] != null ? Session["BasketId"].ToString() : string.Empty;
					string rid = Session["ReservationId"] != null ? Session["ReservationId"].ToString() : string.Empty;
					decimal paymentAmount = Session["TotalAmount"] != null ? Convert.ToDecimal(Session["TotalAmount"]) : 0;
					int orderid = await CreateNewOrder(basketId);
					PaymentNotificationModel notification = new PaymentNotificationModel
					{
						processorid = 2,
						orderid = orderid,
						last4digits = "xxxx",
						reservationid = rid,
						result = 1,
						datestamp = DateTime.Now,
						processed = 0,
						currencytype = currencyName,
						chaseorderid = paypalInvoiceID,
						transactiontype = 1,
						paymentamount = paymentAmount,
						chaseresid = string.Empty,
						txrefnum = transID,
						mitReceivedTxId = "",
						mitMsgType = ""
					};
					ret.Result = orderid.ToString();
					await InsertNotification(notification);
					await SendConfirmationOrder(basketId, orderid);
				}
			}
			catch (Exception ex)
			{
				try
				{
					var message = $"An error has occured in PaypalApproveOrderV2. ";

					message += ex.Message;
					message += " " + ex.InnerException.ToString();

					Utility.WriteLogMonitor(message);
				}
				catch
				{
					//Do nothing
				}
			}

			return new JavaScriptSerializer().Serialize(ret);

		}

		public async Task<string> PaypalCancelOrderV2()
		{
			Session["isPaypal"] = true;
			string basketId = Session["BasketId"] != null ? Session["BasketId"].ToString() : string.Empty;
			if (!string.IsNullOrEmpty(basketId))
			{
				await ReservationsManageLock(basketId, "0");
			}
			return new JavaScriptSerializer().Serialize("ok");
		}
		#endregion

		#region Call Centre Area

		public async Task<ActionResult> CallCentre(Guid reservationId, int shippingAddressId, int billingAddressId)
		{
			//Remove all session for last time.
			Session.RemoveAll();

			var postDataRes = new List<KeyValuePair<string, string>>();

			postDataRes.Add(new KeyValuePair<string, string>("ReservationId", reservationId.ToString()));
			postDataRes.Add(new KeyValuePair<string, string>("ShippingAddressId", shippingAddressId.ToString()));
			postDataRes.Add(new KeyValuePair<string, string>("BillingAddressId", billingAddressId.ToString()));

			CallCentreBasketModel reservation = await new WebRequestHelper().PostRequest<CallCentreBasketModel>(new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "Reservations/GetCallCentreDetails"), postDataRes);

			if (reservation != null && reservation.AddressDetails != null)
			{
				var keepMeSignedIn = Request.Cookies["keepMeSignedIn"];
				//Set the cookie needed in Createorder
				var cookieValue = "{\"CustomerLookupId\":\"" + reservation.CustomerReference + "\",\"Email\":\"" + reservation.CustomerAccountDetails.emailaddress + "\",\"Value\":false,\"loginDate\":\"" + DateTime.Now.ToString("yyyy-MM-ddTHH:mmZ") + "\",\"Firstname\":\"" + reservation.CustomerAccountDetails.firstname + "\",\"Lastname\":\"" + reservation.CustomerAccountDetails.lastname + "\"}";

				if (keepMeSignedIn == null)
				{
					keepMeSignedIn = new HttpCookie("keepMeSignedIn");
				}

				keepMeSignedIn.Value = HttpUtility.UrlEncode(cookieValue);
				keepMeSignedIn.Expires = DateTime.Now.AddMinutes(20);

				if (Request.IsSecureConnection)
				{
					keepMeSignedIn.Secure = true;
				}

				Response.Cookies.Add(keepMeSignedIn);

				//Set the base cookie
				SetBaseCookieCallCentreCookie(reservation.CountryId, reservation.LanguageId, reservation.CurrencyId);

				if (reservation.CardDetails != null)
				{
					foreach (var card in reservation.CardDetails)
					{
						card.Last4DigitsToDisplay = "**** **** **** " + card.Last4Digits;

						if (reservation.CardDetails.IndexOf(card) == 0)
						{
							card.Last4DigitsToDisplay += " (Most recently added)";
						}
					}
				}

				Session["callCentreReservationId"] = reservationId;
				Session["callCentreShippingAddressId"] = shippingAddressId;
				Session["callCentreBillingAddressId"] = billingAddressId;


				var xmlDetails = new PaymentInfoModel()
				{
					AddressId = billingAddressId,
					BasketTotal = 0.00M,
					CustomerAddress = reservation.AddressDetails.StreetAddress1,
					CustomerCountry = reservation.AddressDetails.Country,
					CustomerEmail = reservation.CustomerAccountDetails.emailaddress,
					CustomerFirstname = reservation.CustomerAccountDetails.firstname,
					CustomerLastname = reservation.CustomerAccountDetails.lastname,
					CustomerPhoneNo = reservation.CustomerAccountDetails.phonenumber1,
					CustomerPostcode = reservation.AddressDetails.Postcode,
					CustomerTown = reservation.AddressDetails.City ?? reservation.AddressDetails.Town,
					pageId = -1
				};

				var cardUrl = await CreateXml(xmlDetails, true);

				reservation.NewCardUrl = cardUrl.Data.ToString();

			}

			return View("~/views/callcentre/payment.cshtml", reservation);
		}

		public ActionResult CallCentreOrderStatus(string result)
		{
			NewOrderResponseModel res = new NewOrderResponseModel();
			string errCode = string.Empty;
			if (result.Contains("failure"))
			{
				res.Result = false;
				res.ErrorCode = result.Split('-')[1];
			}
			else
			{
				res.Result = true;
				res.OrderId = result;
			}

			return View("~/views/checkout/orderstatus.cshtml", res);
		}

		#endregion
	}
}