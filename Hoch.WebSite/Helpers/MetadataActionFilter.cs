﻿using Hoch.Website.App_Start;
using ModelLayer.Models;
using System.Linq;
using System.Web.Mvc;

namespace Hoch.Website.Helpers
{
	public class MetadataActionFilter : ActionFilterAttribute
	{
		public bool IsBrand { get; set; }
		public bool IsCategory { get; set; }
		public bool IsProduct { get; set; }

		private bool IsBrandAmbassador { get; set; }

		public string MetaDataFriendlyName { get; set; }

		/// <summary>
		/// The name of the paramter which is the Id in the metadata. This MUST be int
		/// </summary>
		public string IdParameterName { get; set; }

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			int idToFind = -1;

			if (!string.IsNullOrEmpty(IdParameterName))
			{
				if (filterContext.ActionParameters.ContainsKey(IdParameterName))
				{
					var idValueString = filterContext.ActionParameters[IdParameterName].ToString();
					if (IsBrand)
					{
						var idLength = idValueString.Length;

						if (idValueString.Last().ToString().ToLower() == "b")
						{
							IsBrandAmbassador = true;
							idValueString = idValueString.Substring(0, idLength - 1);
						}
					}

					int.TryParse(idValueString, out idToFind);
				}
			}

			if (IsProduct)
			{
				filterContext.Controller.ViewBag.MetaType = "product";
			}
			else
			{
				filterContext.Controller.ViewBag.MetaType = "website";
			}

			//Get metadata stuff here from cache.
			var allMetaData = CacheConfig.GetMetadataCache();

			if (allMetaData != null)
			{
				MetaDataOutputModel singleMetaData = null;

				//get the correct meta depending on type.

				if (!string.IsNullOrEmpty(MetaDataFriendlyName))
				{
					singleMetaData = allMetaData.Where(i => i.PageFriendlyName == MetaDataFriendlyName && i.IsGeneral == true).FirstOrDefault();
				}
				else if (IsProduct)
				{
					singleMetaData = allMetaData.Where(i => i.TypeId == idToFind && i.IsProduct == true).FirstOrDefault();
				}
				else if (IsBrand)
				{
					if (IsBrandAmbassador)
					{
						singleMetaData = allMetaData.Where(i => i.TypeId == idToFind && i.IsBrand == true && i.PageId == 1).FirstOrDefault();
					}
					else
					{
						singleMetaData = allMetaData.Where(i => i.TypeId == idToFind && i.IsBrand == true && i.PageId == 0).FirstOrDefault();
					}
				}
				else if (IsCategory)
				{
					singleMetaData = allMetaData.Where(i => i.TypeId == idToFind && i.IsCategory == true).FirstOrDefault();
				}

				if (singleMetaData != null)
				{
					filterContext.Controller.ViewBag.PageTitle = singleMetaData.Title;
					filterContext.Controller.ViewBag.MetaKeywords = singleMetaData.Keyword;
					filterContext.Controller.ViewBag.MetaDescription = singleMetaData.Description;
					filterContext.Controller.ViewBag.MetaImage = singleMetaData.MetaImage;
				}
			}
		}
	}
}