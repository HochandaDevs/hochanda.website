﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class AddToBasketModel
	{
		public int ProductId { get; set; }
		public int VariationId { get; set; }
		public int CurrencyId { get; set; }
		public int LanguageId { get; set; }
		public int CountryId { get; set; }
		public long? BasketId { get; set; }
		public Guid? CustomerIdentifier { get; set; }
		public string UserEmail { get; set; }
		public int? PickAndMixIdentifier { get; set; }
	}
}
