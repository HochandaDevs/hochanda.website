﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetOrderHistoryRequestModel
    {
        public Guid CustomerRef { get; set; }
        public string CustomerEmail { get; set; }
        public int PageNumber { get; set; }
        public string OrderIdToSearch { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int OrderType { get; set; }
        public int OrderStatus { get; set; }
    }
}
