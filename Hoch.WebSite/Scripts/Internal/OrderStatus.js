﻿app.controller('OrderStatusController', ['$scope', '$http', 'CommonCode', '$timeout', '$rootScope', function ($scope, $http, CommonCode, $timeout, $rootScope) {
    $scope.orderId = null;
    //$scope.creditApplied = 0;
    $scope.orderItems = [];
    $scope.billingAddress1 = '';
    $scope.billingBuildingNo = '';
    $scope.billingPostcode = '';
    $scope.billingCounty = '';
    $scope.billingCountry = '';
    $scope.billingCity = '';

    $scope.shippingAddress1 = '';
    $scope.shippingBuildingNo = '';
    $scope.shippingPostcode = '';
    $scope.shippingCounty = '';
    $scope.shippingCountry = '';
    $scope.shippingCity = '';
    $scope.FlexiOrderSelected = 0;
    $scope.FlexiOrderInstallments = 0;
    $scope.deliveryCost = 0;
    $scope.installments = [];
    $scope.customerCampaignId = 0;
    $scope.PORCampaignId = 0;
    $scope.isNewCustomer = true;
    $scope.UICodeCom = "'UA-114429516-1'";

    //$scope.init = function (orderId, creditApplied) {
    $scope.init = function (orderId) {
        CommonCode.getBaseCookieSettings();
        $scope.keepMeSignedIn = getCookie("keepMeSignedIn");
        //var basketId = localStorage.getItem("basketId");
        var basketId = localStorage.getItem("tempBasketId");
        if ($scope.keepMeSignedIn != null && basketId != null) {
            $scope.orderId = orderId;
            //$scope.creditApplied = creditApplied;
            $scope.keepMeSignedIn = JSON.parse(decodeURIComponent($scope.keepMeSignedIn));
            $scope.getCustomerNoOfOrders();
            $scope.getCampaignIdForCustomer();
            $scope.getOrderStatus(basketId);
        }
    };

    $scope.isFlexiBuySelected = function (productId) {
        var items = $scope.orderItems.filter(function (prd) {
            return prd.ParentProductId == productId
        });

        if (items.length > 0)
            return items[0].FlexiBuyItem;
        else
            return 0;
    }

    $scope.getCampaignIdForCustomer = function () {
        $http({
            method: 'GET',
            url: baseUrl + "Campaign/GetCampaignIdByCustomerRef?customerRef=" + $scope.keepMeSignedIn.CustomerLookupId,
        }).then(function (campaignResult) {
            $scope.PORCampaignId = campaignResult.data.campaignid;
        });
    }

    $scope.getCampaignByName = function () {
        $http({
            method: 'GET',
            url: baseUrl + "Campaign/GetCampaignByName?campaignName=POR",
        }).then(function (campaignNameResult) {
            $scope.customerCampaignId = campaignResult.data;
        });
    }

    $scope.getCustomerNoOfOrders = function () {
        $http({
            method: 'POST',
            url: baseUrl + "Account/GetOrderHistory",
            data: { CustomerRef: $scope.keepMeSignedIn.CustomerLookupId, CustomerEmail: $scope.keepMeSignedIn.Email, PageNumber: 1 }
        }).then(function (orderHostoryeResult) {
            if (orderHostoryeResult.data.Orders.length > 1) {
                $scope.isNewCustomer = false;
            }
        });
    }

    $scope.PaidOnResultsTracking = function (purchase) {
        var url = "";
        if (CommonCode.CurrencyId == 1) {
            url = "https://portgk.com/create-sale?client=java&MerchantID=M2462&SaleID=" + $scope.orderId + "&Purchases=" + purchase;
        } else if (CommonCode.CurrencyId == 2) {
            url = "https://portgk.com/create-sale?client=java&MerchantID=M2462&SaleID=" + $scope.orderId + "&Purchases=" + purchase + "&OrderCurrency=EUR";
        } else if (CommonCode.CurrencyId == 3) {
            url = "https://portgk.com/create-sale?client=java&MerchantID=M2462&SaleID=" + $scope.orderId + "&Purchases=" + purchase + "&OrderCurrency=USD";
        } else {
            url = "https://portgk.com/create-sale?client=java&MerchantID=M2462&SaleID=" + $scope.orderId + "&Purchases=" + purchase;
        }
        var newcustomer = "&newcustomer=no";
        if ($scope.customerCampaignId == $scope.PORCampaignId && $scope.PORCampaignId != 0) {
            if ($scope.isNewCustomer) {
                newcustomer = "&newcustomer=yes";
            }
        }
        url = url + newcustomer;
        var quote = "\"";
        var element = "<script language=JavaScript src=" + "\"" + url + quote + "></script><noscript><img src=" + quote + url + quote + "width=" + quote + "10" + quote + " height=" + quote + "10" + quote + " border=" + quote + "0" + quote + "></noscript>";
        angular.element("head").append(element);
    }

    $scope.EcommerceTracking = function (pAddItems) {
        /*AddTransaction*/
        var revenue = $scope.toPayToday;
        var shippingCost = $scope.deliveryCost;
        var city = "";
        var state = "";
        var country = "";

        var pAddTrans = "_gaq.push(['_addTrans','" + $scope.orderId + "','Hochanda','" + revenue + "','0.0','" + shippingCost + "','" + city + "','" + state + "','" + country + "']);\n";
        /*AddTransaction end*/

        /*GetCurrencyCode*/
        var ecomCurrencyCode = "";
        if (CommonCode.CurrencyId == 1) {
            ecomCurrencyCode = "GBP";
        } else {
            if (CommonCode.CurrencyId == 3) {
                ecomCurrencyCode = "USD";
            }
            else {
                ecomCurrencyCode = "EUR";
            }
        }
        /*GetCurrencyCode end*/

        var pAddCurrency = "_gaq.push(['_set', 'currencyCode', '" + ecomCurrencyCode + "'])\n";
        var element = "<script type='text/javascript'>\n  var _gaq = _gaq || [];\n  _gaq.push(['_setAccount', " + $scope.UICodeCom + "]);\n _gaq.push(['_trackPageview']); \n" + pAddTrans + pAddItems + pAddCurrency + "\n _gaq.push(['_trackTrans']); \n \n(function() {    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; \n   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); \n })();\n \n</script>";
        angular.element("head").append(element);
    }

    $scope.getOrderStatus = function (basketId) {
        var purchase = '';
        var pAddItems = '';

        var orderStatusDetails = {
            "BasketId": basketId,
            "LanguageId": CommonCode.LanguageId
        }

        $http({
            method: 'POST',
            url: baseUrl + "Order/GetOrderStatus",
            contentType: 'application/json',
            data: JSON.stringify(orderStatusDetails),
        }).then(function (result) {
            if (result.data !== undefined && result.data != null) {               
                $scope.deliveryCost = result.data.DeliveryCostWithCurrency;
                $scope.total = result.data.ToPayFigure_TodayWithCurrency;
                $scope.orderItems = result.data.OrderItems;

                angular.forEach($scope.orderItems, function (value, index) {
                    purchase += value.ParentProductSKU + ',' + value.ItemPrice_Paid + '|';
                    pAddItems += "_gaq.push(['_addItem','" + $scope.orderId + "','" + value.TVDescription + "','" + value.ParentProductSKU + "','" + value.CategoryName + "','" + value.ItemPrice_Paid + "','" + value.Quantity + "']);\n";
                });
                purchase = purchase.slice(0, -1);
                $scope.PaidOnResultsTracking(purchase);
                $scope.EcommerceTracking(pAddItems);

                CommonCode.GetMerchantAccount(result.data.CurrencyId).then(function (merchant) {
                    if (merchant.data != null) {
                        var list = $rootScope.FbPixelScript($scope.orderItems, merchant.data.lettercode3, 3);
                        angular.element("head").append(list[0]);
                    }
                });
                //angular.element("head").append(element);
                ////billing
                $scope.billingAddress1 = result.data.b_StreetAddress1;
                $scope.billingAddress2 = result.data.b_StreetAddress2;
                $scope.billingBuildingNo = result.data.b_BuildingNumber;
                if ($scope.billingBuildingNo != null && $scope.billingBuildingNo != '') {
                    $scope.billingAddress1 = $scope.billingBuildingNo + ' ' + $scope.billingAddress1;
                }
                $scope.billingPostcode = result.data.b_Postcode;
                $scope.billingCounty = result.data.b_County;
                $scope.billingCountry = result.data.b_Country;
                $scope.billingCity = result.data.b_Town;
                //shipping
                $scope.shippingBuildingNo = result.data.BuildingNumber;
                $scope.shippingAddress1 = result.data.StreetAddress1;
                if ($scope.shippingBuildingNo != null && $scope.shippingBuildingNo != '') {
                    $scope.shippingAddress1 = $scope.shippingBuildingNo + ' ' + $scope.shippingAddress1;
                }
                $scope.shippingAddress2 = result.data.StreetAddress2;
                $scope.shippingPostcode = result.data.Postcode;
                $scope.shippingCounty = result.data.County;
                $scope.shippingCountry = result.data.Country;
                $scope.shippingCity = result.data.Town;

                $scope.FlexiOrderSelected = result.data.FlexiOrderSelected;
                $scope.FlexiOrderInstallments = result.data.FlexiOrderInstallments;
                $scope.Installments = result.data.Installments;
                $scope.InstallmentsWithCurrency = result.data.InstallmentsWithCurrency;

                ClearBasket();
                localStorage.removeItem("tempBasketId");
            }
        });
    }

    $scope.getInstDate = function (index) {
        if (index == 0) {
            return new Date();
        } else {
            var today = new Date();
            today.setMonth(today.getMonth() + index);
            return today;
        }
    }
}]);