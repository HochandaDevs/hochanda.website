﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public partial class DisplayReservations
    {
        //public int Id { get; set; }
        public bool ReservationActive { get; set; }
        public bool ReservationValid { get; set; }
        public bool ProductRemoved { get; set; }
        public bool HasCountryCurrencyOrLanguageChanged { get; set; }
        public bool FlexiBasketAvailable { get; set; }
        public bool FlexiBasketSelected { get; set; }
        public int FlexiBasketInstallments { get; set; }
        public decimal FlexiBasketPreview { get; set; }
        public decimal ToPayFlexiBuy { get; set; }
        public decimal ToPayToday { get; set; }
        public int? MessageCode { get; set; }
        public string MessageTitle { get; set; }
        public string MessageToDisplay { get; set; }
        public string OkButtonText { get; set; }
        public string CancelButtonText { get; set; }
    }
}
