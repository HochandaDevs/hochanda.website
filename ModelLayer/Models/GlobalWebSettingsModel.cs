﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class GlobalWebSettingsModel
	{
		public int SettingsId { get; set; }
		public int? CompanyId { get; set; }
		public string CompanyDomain { get; set; }
		public int? IsPhysicalLocSame { get; set; }
		public int? IsDefaultSetting { get; set; }
		public int? ShippingId { get; set; }
		public int? LanguageId { get; set; }
		public int? CurrencyId { get; set; }
		public string StreamingURL { get; set; }
		public string CinemaURL { get; set; }
		public int? ShowMsg { get; set; }
		public bool Active { get; set; }
		public int? ChannelId { get; set; }
		public decimal? GMTOffset { get; set; }
		public string ChannelNames { get; set; }
		public string SSLiveDataStream { get; set; }
		public string SSVODDataStream { get; set; }
		public int? AlterBasketTimerPopUp { get; set; }
		public int? SourceIPCountryId { get; set; }
	}
}
