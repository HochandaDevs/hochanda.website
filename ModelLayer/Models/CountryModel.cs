﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class CountryModel
	{
		public int CountryId { get; set; }
		public string CountryName { get; set; }
		public bool Active { get; set; }
		public string VATNumber { get; set; }
		public string CountryCode2Digit { get; set; }
		public string CountryCode3Digit { get; set; }
		public string EasyPostCountry2Digit { get; set; }
		public string DefaultStreamId { get; set; }
		public int DefaultCurrencyId { get; set; }
	}
}
