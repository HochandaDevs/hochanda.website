﻿using Hoch.Website.Helpers;
using ModelLayer.Models.Reservation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Hoch.Website.Controllers.Basket
{
	public class BasketController : BaseController
	{
		// GET: Basket
		[Route("basket")]
		public ActionResult Basket()
		{
			return View("~/Views/Basket/Basket.cshtml");

		}

		[Route("externalbasket")]
		public ActionResult EmptyExternalBasket()
		{
			ViewBag.IsExternal = true;
			return View("~/Views/Basket/Basket.cshtml");
		}

		[Route("externalbasket/{externalBasketLinkId}/{singleSignOn?}")]
		public async Task<ActionResult> ExternalBasket(Guid externalBasketLinkId, bool singleSignOn = false)
		{
			//Get data needed from Wiki
			WebRequestHelper webHelper = new WebRequestHelper();

			ViewBag.IsExternal = true;

			try
			{
				string apiURL = ConfigurationManager.AppSettings.Get("WebAPIUrl");
				string externalBasketDetails = ConfigurationManager.AppSettings.Get("ExternalBasketDetails");

				if (string.IsNullOrEmpty(externalBasketDetails))
				{
					externalBasketDetails = "reservations/GetExternalBasketDetails";
				}

				Uri uri = new Uri(apiURL + externalBasketDetails);

				var postData = new List<KeyValuePair<string, string>>();

				postData.Add(new KeyValuePair<string, string>("ExternalBasketLinkId", externalBasketLinkId.ToString()));

				ExternalBasketModel basketModel = await webHelper.PostRequest<ExternalBasketModel>(uri, postData);

				if (basketModel != null)
				{
					ViewBag.ExternalBasketId = basketModel.BasketId;

					if (Request.Cookies["keepMeSignedIn"] != null)
					{
						Request.Cookies.Remove("keepMeSignedIn");
					}

					//Recreate the login cookie if they logged in on the apps.
					if (basketModel.CustomerReference.HasValue && !string.IsNullOrEmpty(basketModel.CustomerEmail))
					{
						var cookieValue = "{\"CustomerLookupId\":\"" + basketModel.CustomerReference + "\",\"Email\":\"" + basketModel.CustomerEmail + "\",\"Value\":false,\"loginDate\":\"" + DateTime.Now.ToString("yyyy-MM-ddTHH:mmZ") + "\",\"Firstname\":\"" + basketModel.FirstName + "\",\"Lastname\":\"" + basketModel.LastName + "\"}";
						HttpCookie keepMeSignedIn = new HttpCookie("keepMeSignedIn");
						keepMeSignedIn.Value = HttpUtility.UrlEncode(cookieValue);
						keepMeSignedIn.Expires = DateTime.Now.AddMinutes(20);

						if (Request.IsSecureConnection)
						{
							keepMeSignedIn.Secure = true;
						}

						Response.Cookies.Add(keepMeSignedIn);

					}


					return View("~/Views/Basket/Basket.cshtml");
				}
				else
				{
					return View("~/Views/Basket/expiredbasket.cshtml");
				}


			}
			catch
			{
				return View("~/Views/Basket/expiredbasket.cshtml");

			}
		}

		public ActionResult ExpiredBasket()
		{
			return View("~/Views/Basket/ExpiredBasket.cshtml");
		}
	}
}