﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
 
    public class ReservationsModel 
    {
        public Boolean ReservationActive { get; set; }
        public Boolean ReservationValid { get; set; }

        public Boolean ProductRemoved { get; set; }

        public Boolean HasCountryCurrencyOrLanguageChanged { get; set; }//country currency language changed

        public Boolean FlexiBasketAvailable { get; set; }

        public Boolean FlexiBasketSelected { get; set; }


        public int FlexiBasketInstallments { get; set; }

        public decimal FlexiBasketPreview { get; set; }

        public decimal ToPayFlexiBuy { get; set; }

        public decimal ToPayToday { get; set; }

        public string MessageCode { get; set; }

        public string MessageTitle { get; set; }

        public string MessageToDisplay { get; set; }

        public string OkButtonText { get; set; }

        public string CancelButtonText { get; set; }
    }
}
