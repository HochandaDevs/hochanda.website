﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Customer
{
	public class EmailAndPasswordModel
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
}
