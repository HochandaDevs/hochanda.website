﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{

	public class FilteredModel
	{
		public int Id { get; set; }
		public bool IsFiltered { get; set; }
		public bool Active { get; set; }
		public string Name { get; set; }
		public string ParentName { get; set; }
	}

}
