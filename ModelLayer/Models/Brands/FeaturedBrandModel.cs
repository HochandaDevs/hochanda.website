﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Brands
{
    public class FeaturedBrandModel
    {
        public int BrandId { get; set; }
        public int Feature { get; set; }
        public string BrandImage { get; set; }
        public string BrandName { get; set; }
        public int LanguageId { get; set; }
		public bool IsBrandAmbassador { get; set; }
	}
}
