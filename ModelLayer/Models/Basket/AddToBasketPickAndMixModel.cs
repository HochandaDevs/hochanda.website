﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class AddToBasketPickAndMixModel
	{
		public int ProductId { get; set; }
		public int VariationId { get; set; }
		/// <summary>
		/// This is to ensure the pick and mix is unique.
		/// </summary>
		public int RandomNumber { get; set; }
		public int CurrencyId { get; set; }
		public int LanguageId { get; set; }
		public long? BasketId { get; set; }
	}
}
