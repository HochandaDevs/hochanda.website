﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using ModelLayer.Models.Settings;
using Hoch.Website.Helpers;
namespace Hoch.Website.Models
{
	public class Selectors
	{
		private static volatile object _SelectorLock = new object();

		public CurrencyCountryLanguageSelectorModel PopulateMenuData()
		{
			string cacheName = string.Empty;

			string baseMenuUrl = ConfigurationManager.AppSettings["MenuSelectURL"] != null ? ConfigurationManager.AppSettings["MenuSelectURL"] : "https://hope.wiki/Settings/GetSettingsSelectors";

			string sideMenuCache = "webSideMenuCache";
			var jsonMenuData = "";

			//Added Double Check locking - Sam.
			if (HttpContext.Current.Cache[sideMenuCache] == null)
			{
				lock (_SelectorLock)
				{
					if (HttpContext.Current.Cache[sideMenuCache] == null)
					{
						//HttpClient client = new HttpClient();
						WebRequestHelper webrequestHelper = new WebRequestHelper();
						jsonMenuData = webrequestHelper.WebClientRequest(baseMenuUrl);

						if (!string.IsNullOrEmpty(jsonMenuData))
						{
							HttpContext.Current.Cache.Insert(sideMenuCache, jsonMenuData, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);
						}
					}
				}
			}
			else
			{
				jsonMenuData = HttpContext.Current.Cache[sideMenuCache].ToString();
			}

			CurrencyCountryLanguageSelectorModel currencyCountryLanguageResultSet = null;

			//Put a catch in case wiki or something is down.
			if (!string.IsNullOrEmpty(jsonMenuData))
			{
				currencyCountryLanguageResultSet = JsonConvert.DeserializeObject<CurrencyCountryLanguageSelectorModel>(jsonMenuData);
			}
			else
			{
				//TODO: Send log over to the Postgres

				currencyCountryLanguageResultSet = new CurrencyCountryLanguageSelectorModel()
				{
					Countries = new List<CountrySelectorModel>()
					{
						new CountrySelectorModel()
						{
							Country2DigitISOCode = "GB",
							CountryId = 1,
							CountryName = "United Kingdom",
							DefaultCurrencyId = 1,
							DefaultLanguageId = 1,
							DefaultStreamId = "hochandauk",
							CompanyId = 1
						}
					},
					Currencies = new List<CurrencySelectorModel>()
					{
						new CurrencySelectorModel()
						{
							Currency3LetterCode = "GBP",
							CurrencyDisplayName = "British Pounds £ (GBP)",
							CurrencyId = 1,
							CurrencyName = "British Pounds",
							CurrencySymbol = "£",
							DefaultChannelId = 1,
							DefaultStreamUrl = "https://d3jwlm43fjnwxe.cloudfront.net/hochanda/live.m3u8",
							SimpleStreamLiveDataStream = "hochandauk",
							SimpleStreamRewindDataStream = "578"
						}
					},
					Languages = new List<LanguageSelectorModel>()
					{
						new LanguageSelectorModel()
						{
							CultureCode = "en-GB",
							DefaultCountryCode = 1,
							DefaultCurrencyId = 1,
							LanguageId = 1,
							LanguageName = "English",
							WebActive = true
						}
					}
				};
			}

			return currencyCountryLanguageResultSet;

		}


	}
}