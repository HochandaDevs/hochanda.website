﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetAllProductsListingInfoModel
    {
        public List<GetAllProductModel> getAllProductInfo { get; set; }
        public GetAllRefineMenuInfo getAllRefineMenuInfo { get; set; }
        public int totalRows { get; set; }
    }
}
