﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces
{
    public interface ICurrencyModel
    {

        int CurrencyId { get; set; }
        string CurrencySymbol { get; set; }
        string LetterCode2 { get; set; }
        string LetterCode3 { get; set; }
        string CurrencyName { get; set; }
        decimal GBPExchangeRateBuy { get; set; }
        decimal GBPExchangeRateSell { get; set; }
        bool Active { get; set; }
        int DisplayOrder { get; set; }
        int ISOCurrencyCode { get; set; }
        decimal FlexiBasketMinOrderValue { get; set; }
        string CurrencyDisplayName { get; set; }

    }
}
