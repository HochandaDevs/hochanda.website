﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Settings
{
	public class CurrencyCountryLanguageSelectorModel
	{
		public CurrencyCountryLanguageSelectorModel()
		{
			Languages = new List<LanguageSelectorModel>();
			Currencies = new List<CurrencySelectorModel>();
			Countries = new List<CountrySelectorModel>();
		}

		public List<LanguageSelectorModel> Languages { get; set; }
		public List<CurrencySelectorModel> Currencies { get; set; }
		public List<CountrySelectorModel> Countries { get; set; }
	}
}
