﻿using Hoch.Website.Helpers;
using ModelLayer.Models.Account;
using ModelLayer.Models.Customer;
using ModelLayer.Models.MobileApps;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Hoch.Website.Controllers.Account
{
    public class RegisterController : BaseController
    {
        // GET: Register
		[MetadataActionFilter(MetaDataFriendlyName = "Register")]
        public ActionResult Index(string pagename="")
        {
            ViewBag.pagename = pagename;
            return View("~/Views/Account/Register.cshtml");
        }       
        
    }
}