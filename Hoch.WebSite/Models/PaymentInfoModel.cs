﻿using ModelLayer.Helpers;
using ModelLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoch.Website.Models
{
    public class PaymentInfoModel
    {
        public string CustomerLastname { get; set; }
        public string CustomerFirstname { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhoneNo { get; set; }
        public string CustomerCountry { get; set; }
        public string CustomerTown { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPostcode { get; set; }
        public decimal BasketTotal { get; set; }
        public int AddressId { get; set; }
        public string CardLast4Digits { get; set; }
        public List<ReservationItemsModel> ReservationItems { get; set; }
		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse
		{
			get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}

		public int ChaseCustomerRefNum { get; set; }

        public int? pageId { get; set; }
    }

    public class PaymentByCardModel
    {
        public CustomerData CustomerData { get; set; }
        public AddressModel CustomerBillingAddress { get; set; }
        public AddressModel CustomerShippingAddress { get; set; }
        public decimal BasketTotal { get; set; }
        public int AddressId { get; set; }
        public string CardLast4Digits { get; set; }
        public List<ReservationItemsModel> ReservationItems { get; set; }
        public ReservationsModel ReservationModel { get; set; }

		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse
		{
			get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}

		public int ChaseCustomerRefNum { get; set; }
    }

    public class CustomerData
    {
        public int anonymous { get; set; }
        public int customerid { get; set; }
        public string emailaddress { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string phonenumber1 { get; set; }
        public string phonenumber2 { get; set; }
        public string title { get; set; }
    }

    public class OrderReturnToView
    {
        public string Result { get; set; }
        public bool IsPaypal { get; set; }
    }
}