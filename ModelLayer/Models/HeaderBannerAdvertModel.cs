﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class HomepageAdvertModel
    {
        public int HomepageAdvertId { get; set; }
        public string AdvertName { get; set; }
        public DateTime AdvertStartDate { get; set; }
        public DateTime AdvertEndDate { get; set; }
        public TimeSpan AdvertStartTime { get; set; }
        public TimeSpan AdvertEndTime { get; set; }
        public int HomepageAdvertTypeId { get; set; }
        public Dictionary<DayOfWeek, bool> AdvertDays { get; set; }
        public bool Active { get; set; }
        public int? LanguageId { get; set; }
        public int? CountryId { get; set; }
        public int? CurrencyId { get; set; }
        public int? CompanyId { get; set; }
        public string AdvertHTML { get; set; }
		public int? EventId { get; set; }
		public string VoucherCode { get; set; }
		public bool? ShowProducts { get; set; }
	}
}
