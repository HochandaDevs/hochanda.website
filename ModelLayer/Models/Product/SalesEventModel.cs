﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class SalesEventModel
	{
		public int SalesEventId { get; set; }
		public int ParentProductId { get; set; }
	}
}
