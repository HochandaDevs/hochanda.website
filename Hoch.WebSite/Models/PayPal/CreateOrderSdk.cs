﻿
using BraintreeHttp;
using Hoch.Website.Helpers;
using ModelLayer.Models;
using PayPalCheckoutSdk.Orders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hoch.Website.Models.PayPal
{
    public class CreateOrderSdk
    {
        /*
           Method to generate sample create order body with <b>CAPTURE</b> intent

           @return OrderRequest with created order request
        */
        private static OrderRequest BuildRequestBody()
        {
            OrderRequest orderRequest = new OrderRequest()
            {
                Intent = "CAPTURE",

                ApplicationContext = new ApplicationContext
                {
                    BrandName = "EXAMPLE INC",
                    LandingPage = "BILLING",
                    CancelUrl = "https://www.paypal.com/checkoutnow/error",
                    ReturnUrl = "https://www.paypal.com/checkoutnow/error",
                    UserAction = "CONTINUE",
                    ShippingPreference = "SET_PROVIDED_ADDRESS"
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                    new PurchaseUnitRequest{
                        ReferenceId =  "PUHF",
                        Description = "Sporting Goods",
                        CustomId = "CUST-HighFashions",
                        SoftDescriptor = "HighFashions",
                        Amount = new AmountWithBreakdown
                        {
                            CurrencyCode = "USD",
                            Value = "10.00",
                            Breakdown = new AmountBreakdown
                            {
                                ItemTotal = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "8.00"
                                },
                                Shipping = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "2.00"
                                },
                                Handling = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "0.00"
                                },
                                TaxTotal = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "0.00"
                                },
                                ShippingDiscount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "0.00"
                                }
                            }
                        },
                        Items = new List<Item>
                        {
                            new Item
                            {
                                Name = "T-shirt",
                                Description = "Green XL",
                                Sku = "sku01",
                                UnitAmount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "3.00"
                                },
                                Tax = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "0.00"
                                },
                                Quantity = "1",
                                Category = "PHYSICAL_GOODS"
                            },
                            new Item
                            {
                                Name = "Shoes",
                                Description = "Running, Size 10.5",
                                Sku = "sku02",
                                UnitAmount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "5.00"
                                },
                                Tax = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "0.00"
                                },
                                Quantity = "1",
                                Category = "PHYSICAL_GOODS"
                            }
                        },
                        Shipping = new ShippingDetails
                        {
                            Name = new Name
                            {
                                FullName = "John Doe"
                            },
                            AddressPortable = new AddressPortable
                            {
                                AddressLine1 = "123 Townsend St",
                                AddressLine2 = "Floor 6",
                                AdminArea2 = "San Francisco",
                                AdminArea1 = "CA",
                                PostalCode = "94107",
                                CountryCode = "US"
                            }
                        }
                    }
                }
            };

            return orderRequest;
        }

        /*
            Method to create order
            
            @param debug true = print response data
            @return HttpResponse<Order> response received from API
            @throws IOException Exceptions from API if any
        */
        public async static Task<HttpResponse> CreateOrder(OrderRequest requestBody)
        {
            var request = new OrdersCreateRequest();
            request.Headers.Add("prefer", "return=representation");
            request.RequestBody(requestBody);
            SynchronizationContext.SetSynchronizationContext(null);
            var response = await PayPalClient.client().Execute(request).ConfigureAwait(false);
            return response;
        }

        public static async Task<string> GetCurrencyName(int BaseCurrencyID)
        {
            string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
            WebRequestHelper webHelper = new WebRequestHelper();
            MerchantAccountModel account = await webHelper.GetItem<MerchantAccountModel>(apiurl + "Culture/GetMerchantAccount?currencyID=" + BaseCurrencyID);
            string currencyName = "GBP";
            if (account != null)
            {
                currencyName = account.lettercode3;
            }
            if (currencyName == "GBP")
            {
                if (BaseCurrencyID == 2)//Germany
                {
                    currencyName = "EUR";
                }
            }
            else
            {
                if (BaseCurrencyID == 1)//UK
                {
                    currencyName = "GBP";
                }
            }
            return currencyName;
        }

        public static async Task<BasketCheckoutModel> GetReservation(string basketId, int countryId, int currencyId, int languageId)
        {
            WebRequestHelper webHelper = new WebRequestHelper();
            string apiurl = ConfigurationManager.AppSettings.Get("WebAPIUrl");


            var uri = new Uri(apiurl + "Reservations/GetBasketDisplay");
            var postDataRes = new List<KeyValuePair<string, string>>();
            postDataRes.Add(new KeyValuePair<string, string>("BasketId", basketId));
            postDataRes.Add(new KeyValuePair<string, string>("PerformCalculation", "true"));
            postDataRes.Add(new KeyValuePair<string, string>("CountryID",  countryId.ToString()));
            postDataRes.Add(new KeyValuePair<string, string>("CurrencyID", currencyId.ToString()));
            postDataRes.Add(new KeyValuePair<string, string>("LanguageID", languageId.ToString()));
            postDataRes.Add(new KeyValuePair<string, string>("IsFlexi", "-1"));
            postDataRes.Add(new KeyValuePair<string, string>("IsItemFlexi", "-1"));
            var reservation = await webHelper.PostRequest<BasketCheckoutModel>(uri, postDataRes);
            return reservation;
        }
    }
}