﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class CategoryFilterModel
	{
		public int CategoryId { get; set; }
		public int CategoryLevel { get; set; }
		public string CategoryName { get; set; }
		public int ProductCountForCategory { get; set; }
		public int? BelongsTo { get; set; }

		//Deleted properties not referenced - Sam 05/02/2019
    }
}
