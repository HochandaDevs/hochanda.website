﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.ReferAFriend
{
    public interface IReferAFriendWelcomeModel
    {

        string firstname { get; set; }
        Nullable<int> discountvalue { get; set; }
        
        string fixedorPercent { get; set; }
    }
}
