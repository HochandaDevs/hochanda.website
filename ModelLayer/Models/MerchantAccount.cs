﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class MerchantAccountModel
    {
        public int CurrencyID { get; set; }
        public int WebOrPhone { get; set; }
        public string IndustryType { get; set; }
        public string IndustryName { get; set; }
        public string CurrencyType { get; set; }
        public int ProcessorID{ get; set; }
        public string lettercode3{ get; set; }
        public string AVSCountryCode { get; set; }
        public string MerchantID { get; set; }

    }
}
