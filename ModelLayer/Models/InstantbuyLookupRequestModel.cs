﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class InstantbuyLookupRequestModel
    {
        public int chaseCustRefNum { set; get; }
        public int customerId { set; get; }
        public string lastFourDigits { set; get; }
        public int chaseMid { set; get; }
        public int webOrPhone { set; get; }
        public int active { set; get; }
        public int addressId { set; get; }
        public string cardBrand { set; get; }
        public string cardExpiryDate { set; get; }
        public string mitTxId { get; set; }
    }
}
