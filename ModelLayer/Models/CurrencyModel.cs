﻿using ModelLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class CurrencyModel: ICurrencyModel
    {
		public int CurrencyId { get; set; }
		public string CurrencySymbol { get; set; }
		public string LetterCode2 { get; set; }
		public string LetterCode3 { get; set; }
		public string CurrencyName { get; set; }
		public decimal GBPExchangeRateBuy { get; set; }
		public decimal GBPExchangeRateSell { get; set; }
		public bool Active { get; set; }
		public int DisplayOrder { get; set; }
		public int ISOCurrencyCode { get; set; }
		public decimal FlexiBasketMinOrderValue { get; set; }
		public string CurrencyDisplayName { get; set; }
	}
}
