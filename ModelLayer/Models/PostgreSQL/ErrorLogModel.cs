﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.PostgreSQL
{
    public class ErrorLogModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int MessageType { get; set; }
        public int AppId { get; set; }
        public int IsDev { get; set; }
        public string Exception { get; set; }
        public int Notification { get; set; }
    }
}
