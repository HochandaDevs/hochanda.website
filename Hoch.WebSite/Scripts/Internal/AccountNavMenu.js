﻿app.controller('AccountNavMenuController', ['$scope', '$cookies', '$http', 'CommonCode', function ($scope, $cookies, $http, CommonCode) {
    $scope.firstname = '';

    $scope.customerid = "";
    $scope.freedommember = "";
    $scope.freedomholiday = "";
    $scope.hascustomercredit = ""

    $scope.init = function () {
        CommonCode.getBaseCookieSettings();
        $scope.currencySymbol = CommonCode.CurrencySymbol;
        var signinData = $cookies.get("keepMeSignedIn");
        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            $scope.firstname = signinData.Firstname;
        }
        if (signinData != null) {
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {

                var userAccInfo = { CustomerLookupId: signinData.CustomerLookupId, Email: signinData.Email, CurrencyId: CommonCode.CurrencyId };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/AccountOverview",
                    contentType: 'application/json',
                    data: userAccInfo
                }).then(function (result) {
                    if (result.data !== undefined) {
                        $scope.customerid = result.data.customerid;
                        $scope.freedommember = result.data.freedommember;
                        $scope.freedomholiday = result.data.freedomholiday;
                        $scope.hascustomercredit = result.data.hascustomercredit;
                        $scope.creditamount = result.data.creditamount;
                        $scope.creditamountwithcurrency = result.data.creditamountwithcurrency;

                        $scope.GetCustomerCredit(signinData);
                    }
                });
            }
        }
        
    };

    $scope.GetCustomerCredit = function (signinData) {
        var data = { CustomerLookupId: signinData.CustomerLookupId, CurrencyId: CommonCode.CurrencyId, CustomerEmail: signinData.Email };
        $http({
            method: 'POST',
            url: baseUrl + "Account/GetCustomerCredit",
            contentType: 'application/json',
            data: JSON.stringify(data)
        }).then(function (custresult) {
            $scope.CreditAmountData = custresult.data;
        });
    }
}]);