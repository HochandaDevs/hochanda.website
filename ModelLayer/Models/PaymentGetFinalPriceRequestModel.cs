﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class PaymentGetFinalPriceRequestModel
    {
        public int CustomerId { get; set; }
        public int CurrencyId { get; set; }
        public decimal OrderTotal { get; set; }
    }
}
