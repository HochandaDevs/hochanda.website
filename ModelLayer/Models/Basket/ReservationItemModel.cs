﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class ReservationItemModel
	{
		public int TableId { get; set; }
		public Guid ReservationId { get; set; }
		public int ParentProductId { get; set; }
		public int VariationId { get; set; }
		public int Quantity { get; set; }
		public decimal ItemPrice { get; set; }
		public DateTime? ExpiryDate { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public bool Active { get; set; }
		public bool Processed { get; set; }
		public bool ConvertedToOrder { get; set; }
		public bool FlexiBuyItem { get; set; }
		public bool DiscountApplied { get; set; }
		public bool PPIdHasFreedomDiscount { get; set; }
		public decimal PPIdRegularPrice { get; set; }
		public decimal PPIdPAndPSurcharge { get; set; }
		public int ServiceId { get; set; }
		public int ServiceIdDefault { get; set; }
		public int SupplierId { get; set; }
		public bool PAndPRuleApplied { get; set; }
		public bool FreeItem { get; set; }
		public bool UpSellItem { get; set; }
		public bool PickNMixItem { get; set; }
		public int SelectionId { get; set; }
		public int PickNMixTableId { get; set; }
		public int FreeItemChoose { get; set; }
		public bool AuctionItem { get; set; }
		public int PendingStatus { get; set; }
		public int OnAirId { get; set; }
		public int FreeItemBySupplier { get; set; }
		public int FreeItemBySupplierValidCount { get; set; }
		public int FreeItemBySupplierSupplierId { get; set; }
		public bool IsAutoship { get; set; }
		public int AutoshipRepeatDays { get; set; }
	}
}
