﻿using ModelLayer.Interfaces;
using ModelLayer.Interfaces.ReferAFriend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.ReferAFriend
{
    public class ReferAFriendDetailsModel
    {

        public string voucherCode { get; set; }
        public Nullable<int> CreditCurrency { get; set; }
        public Nullable<int> PendingCount { get; set; }
        public Nullable<int> SuccessfulCount { get; set; }
        public Nullable<int> TotalReferralsCount { get; set; }
        public Nullable<decimal> ActiveCredit { get; set; }
        public decimal youReceiveOffer { get; set; }
        public string youReceiveOfferWithCurrency { get; set; }
        public string youReceivePercentOrAmount { get; set; }
        public Nullable<decimal> theyReceiveOffer { get; set; }
        public string theyReceivePercentOrAmount { get; set; }
        public Nullable<int> VoucherId { get; set; }

        public string theirOffer { get; set; }


    }

    public class ReferAFriendConfigModel
    {
        public int ConfigID { get; set; }
        public Nullable<int> CurrencyID { get; set; }
        public bool IsPercentageOrAmountOff { get; set; }
        public decimal CreditOffValue { get; set; }
        public string CreditOffValueWithCurrency { get; set; }
		public Nullable<int> NoOfDaysToCredit { get; set; }
        public decimal CreditOffValueForFriend { get; set; }
        public string CreditOffValueForFriendCurrency { get; set; }
        public string minspend { get; set; }
    }
}
