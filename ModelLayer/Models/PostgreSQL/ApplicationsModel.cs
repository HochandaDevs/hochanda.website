﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.PostgreSQL
{
    public class ApplicationsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
