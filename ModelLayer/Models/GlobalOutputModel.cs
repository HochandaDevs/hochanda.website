﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GlobalOutputModel
    {
        public string MessageToDisplay { get; set; }
        public string OKMessage { get; set; }
        public string CancelMessage { get; set; }
        public string TitleMessage { get; set; }
        public bool Success { get; set; }
    }
}
