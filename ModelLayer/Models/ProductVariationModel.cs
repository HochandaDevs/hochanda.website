﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class ProductVariationModel
    {
        public int VariationId { get; set; }
        public string VariationName { get; set; }
        public int LanguageId { get; set; }
    }
}
