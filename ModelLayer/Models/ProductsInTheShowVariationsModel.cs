﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class ProductsInTheShowVariationsModel
    {
        public ProductsInTheShowVariationsModel()
        {
            ProductVariations = new List<ProductVariationModel>();
        }

        public int ParentProductId { get; set; }

        public List<ProductVariationModel> ProductVariations { get; set; }
    }
}
