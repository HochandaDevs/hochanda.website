﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class WebAPIStatusModel
    {
        public int CPUUsagePercent { get; set; }

        public int MemoryUsagePercent { get; set; }

        public bool UseBackups { get; set; }
    }
}
