﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Settings
{
	public class LanguageSelectorModel
	{
		public int LanguageId { get; set; }
		public string LanguageName { get; set; }

		public string CultureCode { get; set; }

		public int DefaultCountryCode { get; set; }
		public int DefaultCurrencyId { get; set; }
		public bool WebActive { get; set; }

	}
}
