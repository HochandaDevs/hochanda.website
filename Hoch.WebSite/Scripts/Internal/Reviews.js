﻿app.controller('ReviewsController', ['$scope', '$rootScope', '$http', '$filter', function ($scope, $rootScope, $http, $filter) {
    $rootScope.GetMetadata(false, false, false, 'Reviews', 0);

    $scope.GetCustomerExperience = function () {
        $http({
            method: 'GET',
            url: "https://api.feefo.com/api/10/reviews/summary/service?merchant_identifier=hochanda&since_period=all",
            contentType: 'application/json',
        }).then(function (response) {           
            $scope.serviceRating = response.data.rating.rating;
            $scope.servicePercentage = (100 * $scope.serviceRating) / 5;
            $scope.reviewCountService = response.data.rating.service.count;
            $scope.oneStarService = response.data.rating.service["1_star"];
            $scope.oneStarPercService = parseInt((100 * $scope.oneStarService) / $scope.reviewCountService);
            $scope.twoStarService = response.data.rating.service["2_star"];
            $scope.twoStarPercService = parseInt((100 * $scope.twoStarService) / $scope.reviewCountService);
            $scope.threeStarService = response.data.rating.service["3_star"];
            $scope.threeStarPercService = parseInt((100 * $scope.threeStarService) / $scope.reviewCountService);
            $scope.fourStarService = response.data.rating.service["4_star"];
            $scope.fourStarPercService = parseInt((100 * $scope.fourStarService) / $scope.reviewCountService);
            $scope.fiveStarService = response.data.rating.service["5_star"];
            $scope.fiveStarPercService = parseInt((100 * $scope.fiveStarService) / $scope.reviewCountService);
        });
    }

    $scope.GetProductListing = function () {
        $http({
            method: 'GET',
            url: "https://api.feefo.com/api/10/reviews/summary/product?merchant_identifier=hochanda&since_period=all",
            contentType: 'application/json',
        }).then(function (response) {
            $scope.productRating = response.data.rating.rating;
            $scope.productPercentage = (100 * $scope.productRating) / 5;
            $scope.reviewCount = response.data.rating.product.count;
            $scope.oneStar = response.data.rating.product["1_star"];
            $scope.oneStarPerc = parseInt((100 * $scope.oneStar) / $scope.reviewCount);
            $scope.twoStar = response.data.rating.product["2_star"];
            $scope.twoStarPerc = parseInt((100 * $scope.twoStar) / $scope.reviewCount);
            $scope.threeStar = response.data.rating.product["3_star"];
            $scope.threeStarPerc = parseInt((100 * $scope.threeStar) / $scope.reviewCount);
            $scope.fourStar = response.data.rating.product["4_star"];
            $scope.fourStarPerc = parseInt((100 * $scope.fourStar) / $scope.reviewCount);
            $scope.fiveStar = response.data.rating.product["5_star"];
            $scope.fiveStarPerc = parseInt((100 * $scope.fiveStar) / $scope.reviewCount);
        });
    }

    $scope.GetReviewList = function () {
        $http({
            method: 'GET',
            url: "https://api.feefo.com/api/10/reviews/service?merchant_identifier=hochanda&page=1&since_period=all&page_size=50",
            contentType: 'application/json',
        }).then(function (response) {
            $scope.reviews = response.data.reviews;
            $scope.summary = response.data.summary;
        });
    }

    $scope.getNth = function (dateInput) {
        var d = new Date(dateInput);
        var date = d.getDate();
        return nth(date);
    }

    function nth(d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }

    $scope.GetCustomerExperience();
    $scope.GetProductListing();
    $scope.GetReviewList();
}]);