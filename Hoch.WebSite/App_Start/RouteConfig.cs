﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hoch.Website
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.RouteExistingFiles = true;
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.LowercaseUrls = true;

			//Allows route prefix and routing attributes - Sam
			routes.MapMvcAttributeRoutes();

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional }
			);

			routes.MapRoute(
				name: "Product",
				url: "product/{parentProductId}/{webSafeTitle}/{sku}",
				defaults: new { controller = "Product", action = "Product" }
			);

			routes.MapRoute(
				name: "ProductPickAndMix",
				url: "product-pick-and-mix/{parentProductId}/{webSafeTitle}/{sku}",
				defaults: new { controller = "Product", action = "ProductPickAndMix" }
			);


			routes.MapRoute(
				name: "CategoryListing",
				url: "category/{categoryId}/{categoryName}",
				defaults: new { controller = "ProductListing", action = "CategoryListing" }
			);


			routes.MapRoute(
				name: "BrandListing",
				url: "brand/{brandId}/{brandName}",
				defaults: new { controller = "ProductListing", action = "BrandListing" }
			);

			routes.MapRoute(
				name: "About",
				url: "about/{urlToUse}",
				defaults: new { controller = "PageBuilder", action = "About" }
			);

			routes.MapRoute(
				name: "Sale",
				url: "sale/{urlToUse}",
				defaults: new { controller = "PageBuilder", action = "Sales" }
			);

			routes.MapRoute(
				name: "Event",
				url: "event/{urlToUse}",
				defaults: new { controller = "PageBuilder", action = "Event" }
			);


			routes.MapRoute(
				"404-PageNotFound",
				"{*url}",
				new { controller = "StaticContent", action = "PageNotFound" }
			);

			routes.MapRoute(
				name: "ExternalBasket",
				url: "externalbasket/{externalBasketLinkId}",
				defaults: new { controller = "Basket", action = "ExternalBasket"}
			);
		}
	}
}
