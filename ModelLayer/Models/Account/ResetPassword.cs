﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class ForgotPasswordResetLink
    {
        public string email { get; set; }
    }

    public class ResetPassword
    {
        public string rp { get; set; }
        public string newP { get; set; }
    }

    public class ResetPasswordTokenValidation
    {
        public string token { get; set; }
       
    }

    public class MyAccountResetPassword
    {
        public string oldP { get; set; }
        public string newP { get; set; }
        public string confirmP { get; set; }
        public string custEmail { get; set; }
    }
}
