﻿app.controller('accOverViewCtrl', ['$scope', '$cookies', '$http', 'loginService', 'CommonCode', function ($scope, $cookies, $http, loginService, CommonCode) {
    $scope.customerid = "";
    $scope.freedommember = "";
    $scope.freedomholiday = "";
    $scope.hascustomercredit = ""

    $scope.init = function () {
        var loginServiceRes = loginService.validateLogin();
        CommonCode.getBaseCookieSettings();
        if (loginServiceRes != null) {

            loginServiceRes.then(function (signinData) {
                if (signinData != null) {
                    if (signinData.Email != "" && signinData.CustomerLookupId != "") {

                        var userAccInfo = { CustomerLookupId: signinData.CustomerLookupId, Email: signinData.Email, CurrencyId: CommonCode.CurrencyId };
                        $http({
                            method: 'POST',
                            url: baseUrl + "Account/AccountOverview",
                            contentType: 'application/json',
                            data: userAccInfo
                        }).then(function (result) {
                            if (result.data !== undefined) {
                                $scope.customerid = result.data.customerid;
                                $scope.freedommember = result.data.freedommember;
                                $scope.freedomholiday = result.data.freedomholiday;
                                $scope.hascustomercredit = result.data.hascustomercredit;

                            }
                        });
                    }
                }
            });
        }
    }
}]);


app.directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);

app.controller('accDetailsCtrl', ['$scope', '$cookies', '$http', 'CommonCode', '$compile', '$rootScope', 'loginService', '$window', function ($scope, $cookies, $http, CommonCode, $compile, $rootScope, loginService, $window) {

    $scope.Register = {};
    $scope.customerid = "";
    $scope.title = "";
    $scope.firstname = "";
    $scope.lastname = "";
    $scope.dob = "";
    $scope.nonymous = "";
    $scope.phonenumber1 = "";
    $scope.phonenumber2 = "";
    $scope.emailaddress = "";
    CommonCode.getBaseCookieSettings();

    $scope.init = function () {
        var loginServiceRes = loginService.validateLogin();
        if (loginServiceRes != null) {
            loginServiceRes.then(function (signinData) {
                if (signinData != null && signinData.Email != "" && signinData.CustomerLookupId != "") {
                    $scope.signinData = signinData;
                    var userAccInfo = { CustomerLookupId: signinData.CustomerLookupId, Email: signinData.Email };
                    $http({
                        method: 'POST',
                        url: baseUrl + "Account/AccountDetails",
                        contentType: 'application/json',
                        data: userAccInfo
                    }).then(function (result) {
                        console.log('result', result.data);
                        if (result.data !== undefined) {
                            $scope.customerid = result.data.customerid;
                            $scope.title = result.data.title;
                            $scope.firstname = result.data.firstname;
                            $scope.lastname = result.data.lastname;
                            $scope.dob = result.data.dob;
                            $scope.anonymous = false;
                            if (result.data.anonymous == 1) {
                                $scope.anonymous = true;
                            }
                            $scope.phonenumber1 = result.data.phonenumber1;
                            $scope.phonenumber2 = result.data.phonenumber2;
                            $scope.emailaddress = result.data.emailaddress;
                            $scope.confirmEmailAddress = result.data.emailaddress;
                            $scope.phone1type = result.data.phone1type;
                            $scope.phone2type = result.data.phone2type;
                            if (result.data.phone1type == 0 || result.data.phone1type == null || result.data.phone1type == "") {
                                $scope.phone1type = 1;
                            }
                            if (result.data.phone2type == 0 || result.data.phone2type == null || result.data.phone2type == "") {
                                $scope.phone2type = 1;
                            }
                            if (result.data.dob != null && result.data.dob != "") {
                                $scope.DayId = result.data.dob.split('/')[0];
                                $scope.MonthId = result.data.dob.split('/')[1];
                            }
                        }
                    });

                    $http({
                        method: 'GET',
                        url: baseUrl + "Account/GetRegisterTitles",
                        contentType: 'application/json',
                        data: CommonCode.LanguageId
                    }).then(function (result) {
                        $scope.Register.Titles = result.data;
                        //$scope.Register.title = $scope.title;
                    });

                    $http({
                        method: 'GET',
                        url: baseUrl + "Account/GetPhoneTitles",
                        contentType: 'application/json'
                    }).then(function (result) {
                        $scope.Register.Phonetypes = result.data;
                    });

                    //$scope.Phonetypes = [{ id: 1, label: 'Mobile' }, { id: 2, label: 'Home' }];
                    $scope.Days = [{ id: 1, label: '1' }, { id: 2, label: '2' },
                    { id: 3, label: '3' }, { id: 4, label: '4' },
                    { id: 5, label: '5' }, { id: 6, label: '6' },
                    { id: 7, label: '7' }, { id: 8, label: '8' },
                    { id: 9, label: '9' }, { id: 10, label: '10' },
                    { id: 11, label: '11' }, { id: 12, label: '12' },
                    { id: 13, label: '13' }, { id: 14, label: '14' },
                    { id: 15, label: '15' }, { id: 16, label: '16' },
                    { id: 17, label: '17' }, { id: 18, label: '18' },
                    { id: 19, label: '19' }, { id: 20, label: '20' },
                    { id: 21, label: '21' }, { id: 22, label: '22' },
                    { id: 23, label: '23' }, { id: 24, label: '24' },
                    { id: 25, label: '25' }, { id: 26, label: '26' },
                    { id: 27, label: '27' }, { id: 28, label: '28' },
                    { id: 29, label: '29' }, { id: 30, label: '30' },
                    { id: 31, label: '31' }]


                    $scope.Months = [{ id: '1', label: 'Jan' }, { id: '2', label: 'Feb' },
                    { id: '3', label: 'Mar' }, { id: '4', label: 'Apr' },
                    { id: '5', label: 'May' }, { id: '6', label: 'Jun' },
                    { id: '7', label: 'Jul' }, { id: '8', label: 'Aug' },
                    { id: '9', label: 'Sep' }, { id: '10', label: 'Oct' },
                    { id: '11', label: 'Nov' }, { id: '12', label: 'Dec' }]
                }
            });
        }
    };

    $scope.UpdateCustomerPassword = function () {
        //$scope.validationMessage = "";
        //$scope.validation = null;
        //$scope.ValidationClass = "";

        var signinData = $cookies.get("keepMeSignedIn");

        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }
        var oldP = $scope.currentPassword;
        var newPassword = $scope.newPassword;
        var confirmPassword = $scope.confirmPassword;
        var termsAndConditions = $scope.termsAndConditions;
        var result = true;
        var regex = "";

        document.getElementById('validationError_pwd').innerHTML = "";
        $scope.validationMessages = "<div>";

        if (newPassword == null || newPassword == "") {
            result = false;
            $scope.validationMessages += "<label>New Password can't be empty</label>";
        }
        else {
            if (newPassword.length < 8) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least 8 characters</label>";
            }
            regex = /[0-9]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one number (0-9)</label>";
            }
            regex = /[a-z]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one lowercase letter (a-z)</label>";
            }
            regex = /[A-Z]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one uppercase letter (A-Z)</label>";
            }
        }
        if (confirmPassword == null || confirmPassword == "") {
            result = false;
            $scope.validationMessages += "<label>Confirm New Password can't be empty</label>";
        }
        else {
            if (newPassword != confirmPassword) {
                result = false;
                $scope.validationMessages += "<label>Passwords don't match</label>";
            }
        }
        if (termsAndConditions != true) {
            result = false;
            $scope.validationMessages += "<label>Please agree to our Terms and Conditions</label>"
            $scope.errorClass_termsAndConditions = 'errorClass';
        }
        $scope.validationMessages += "</div>";

        var errorDiv = $('#validationError_pwd');
        var htmlelement = angular.element($scope.validationMessages);
        var compileAppendedArticleHTML = $compile(htmlelement[0]);
        var element = compileAppendedArticleHTML($scope);
        errorDiv.append(element[0]);

        if (result == true) {
            var RPparams = { oldP: oldP, newP: newPassword, confirmP: confirmPassword, custEmail: $scope.signinData.Email };
            $http({
                method: 'POST',
                url: baseUrl + "Account/MyAccountResetPassword",
                contentType: 'application/json',
                data: RPparams
            }).then(function (result) {
                if (result.data.Success == true) {
                    delete_cookie("keepMeSignedIn");
                    localStorage.removeItem("userFavourites");
                    window.location.href = "/login";
                }
                if (result.data.Success == false) {
                    $scope.validationMessages = "<div>Failed</div>";
                    var errorDiv = $('#validationError_pwd');
                    var htmlelement = angular.element($scope.validationMessages);
                    var compileAppendedArticleHTML = $compile(htmlelement[0]);
                    var element = compileAppendedArticleHTML($scope);
                    errorDiv.append(element[0]);
                }
            });
        }
    }


    $scope.UpdateCustomer = function () {
        //$scope.validationMessage = "fsfsdf";
        //$scope.validation = null;
        //$scope.ValidationClass = "";
        var signinData = $cookies.get("keepMeSignedIn");

        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }
        document.getElementById('validationError').innerHTML = "";
        $scope.validationMessages = "<div>";
        var title = $scope.title;
        var firstName = $scope.firstname;
        var surName = $scope.lastname;
        var anonymousDisplay = $scope.anonymous;
        var dob_Day = $scope.DayId;
        var dob_Month = $scope.MonthId;
        var phoneNumber = $scope.phonenumber1;
        var phoneType = $scope.phone1type;
        var altPhoneNumber = $scope.phonenumber2;
        var altPhoneType = $scope.phone2type;
        var emailAddress = $scope.emailaddress;
        var confirmEmailAddress = $scope.confirmEmailAddress;
        //var newPassword = $scope.newPassword;
        //var confirmPassword = $scope.confirmPassword;
        var termsAndConditions = $scope.termsAndConditions;
        var result = true;
        var regex = "";

        if (firstName == null || firstName == "") {
            result = false;
            $scope.validationMessages += "<label>First Name can't be empty</label>"
            $scope.errorClass_firstName = 'errorClass';
        }
        else {
            regex = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9]*$/;
            if (!regex.test(firstName)) {
                result = false;
                $scope.validationMessages += "<label>Invalid First Name</label>"
                $scope.errorClass_firstName = 'errorClass';
            }
        }
        if (surName == null || surName == "") {
            result = false;
            $scope.validationMessages += "<label>Surname can't be empty</label>"
            $scope.errorClass_surName = 'errorClass';
        }
        else {
            regex = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9]*$/;
            if (!regex.test(surName)) {
                result = false;
                $scope.validationMessages += "<label>Invalid Surname</label>"
                $scope.errorClass_surName = 'errorClass';
            }
        }
        if (phoneNumber == null || phoneNumber == "") {
            result = false;
            $scope.validationMessages += "<label>Phone Number can't be empty</label>"
            $scope.errorClass_phoneNumber = 'errorClass';
        }
        else {
            regex = /^[^[a-zA-Z\s]*$/;
            if (!regex.test(phoneNumber)) {
                result = false;
                $scope.validationMessages += "<label>Invalid phone Number</label>"
                $scope.errorClass_phoneNumber = 'errorClass';
            }
        }
        if (emailAddress == null || emailAddress == "") {
            result = false;
            $scope.validationMessages += "<label>Email Address can't be empty</label>"
            $scope.errorClass_emailAddress = 'errorClass';
        }
        else {
            regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!regex.test(emailAddress)) {
                result = false;
                $scope.validationMessages += "<label>Invalid email Address</label>"
                $scope.errorClass_emailAddress = 'errorClass';
            }
        }
        if (confirmEmailAddress == null || confirmEmailAddress == "") {
            result = false;
            $scope.validationMessages += "<label>Confirm Email Address can't be empty</label>"
            $scope.errorClass_confirmEmailAddress = 'errorClass';
        }
        else {
            if (emailAddress != confirmEmailAddress) {
                result = false;
                $scope.validationMessages += "<label>Email Addresses don't match</label>"
                $scope.errorClass_confirmEmailAddress = 'errorClass';
            }
        }

        if (termsAndConditions != true) {
            result = false;
            $scope.validationMessages += "<label>Please agree to our Terms and Conditions</label>"
            $scope.errorClass_termsAndConditions = 'errorClass';
        }
        $scope.validationMessages += "</div>";
        var errorDiv = $('#validationError');
        var htmlelement = angular.element($scope.validationMessages);
        var compileAppendedArticleHTML = $compile(htmlelement[0]);
        var element = compileAppendedArticleHTML($scope);
        errorDiv.append(element[0]);
        if (result == true) {
            var registerDetails = {
                Title: title,
                FirstName: firstName,
                Surname: surName,
                DayOfBirth: dob_Day,
                MonthOfBirth: dob_Month,
                PhoneNumber1: phoneNumber,
                PhoneNumber2: altPhoneNumber,
                EmailAddress: emailAddress,
                ConfirmEmailAddress: confirmEmailAddress,
                Anonymous: anonymousDisplay,
                Phone1Type: phoneType,
                Phone2Type: altPhoneType,
                SignedInEmail: $scope.signinData.Email,
                CustomerLookUpId: $scope.signinData.CustomerLookupId
            };
            if (emailAddress == $scope.signinData.Email) {
                registerDetails.EmailAddress = "";
                registerDetails.ConfirmEmailAddress = "";

            }
            $.ajax({
                type: 'POST',
                url: baseUrl + "Account/UpdateUserDetails",
                contentType: 'application/x-www-form-urlencoded',
                data: registerDetails,
                success: function (data) {
                    if (data.Success == true) {
                        if ($scope.signinData.Email == $scope.emailaddress) {

                            $window.location.reload();
                            //delete_cookie("keepMeSignedIn");
                            //var expireDate = new Date();

                            //expireDate.setMinutes(expireDate.getMinutes() + loginDuration);

                            //delete_cookie("keepMeSignedIn");
                            //localStorage.removeItem("userFavourites");
                            //window.location.href = "/login";
                            //var loginDate = new Date();
                            //$scope.persSignIn = false;
                            //var keepMeSignedIn = { CustomerLookupId: $scope.signinData.CustomerLookupId, Email: $scope.emailaddress, Value: $scope.persSignIn, loginDate: loginDate, Firstname: $scope.firstname, Lastname: $scope.lastname };

                            //$cookies.put("keepMeSignedIn", JSON.stringify(keepMeSignedIn), { 'expires': expireDate,'secure': location.protocol == "https:" ? true : false });
                        }
                        else {
                            delete_cookie("keepMeSignedIn");
                            localStorage.removeItem("userFavourites");
                            window.location.href = "/login";
                        }

                        //$window.location.reload();
                    }
                    if (data.Success == false) {
                        $scope.validationMessages = "<div>Failed</div>";
                        var errorDiv = $('#validationError');
                        var htmlelement = angular.element($scope.validationMessages);
                        var compileAppendedArticleHTML = $compile(htmlelement[0]);
                        var element = compileAppendedArticleHTML($scope);
                        errorDiv.append(element[0]);
                    }

                }
            });
        }
    }

    $scope.setPartialName = function (name) {
        $rootScope.partialName = name;
    }
}]);

app.controller('OrderHistoryController', ['$scope', '$http', '$filter', '$cookies', 'loginService', 'CommonCode', '$rootScope', function ($scope, $http, $filter, $cookies, loginService, CommonCode, $rootScope) {
    var signinData = $cookies.get("keepMeSignedIn");
    $scope.signinData = JSON.parse(decodeURIComponent(signinData));

    $rootScope.GetProductUrl = function (ppid, ppsku, desc) {
        return GetProductUrl(ppid, ppsku, desc);
    }

    var today = new Date();
    $scope.startDate = new Date(2015, 0, 1);
    $scope.endDate = (new Date()).setDate((new Date()).getDate() + 1);

    $scope.options = {
        locale: {
            cancelLabel: 'Clear',
            format: 'DD/MM/YYYY'
        },
        showDropdowns: false
    };

    $scope.orders = [];
    $scope.currentPage = 1;
    $scope.numberOfPages = 0;
    $scope.orderTypeSelect = [{ value: 0, text: "All Orders" }, { value: 1, text: "Flexibuy Orders" }, { value: 2, text: "Downloadable Orders" }];
    $scope.orderStatusSelect = [{ value: 0, text: "All" }, { value: 1, text: "Processing" }, { value: 2, text: "Refunded" }, { value: 3, text: "Cancelled" }, { value: 4, text: "Shipped" }];
    $scope.orderTypeSelected = 0;
    $scope.orderStatusSelected = 0;
    $scope.orderNoSelected = '';
    $scope.disableForwardButtons = false;
    $scope.disableBackwardButtons = true;

    $scope.orderSearchButton = function () {
        $scope.currentPage = 1;
        $scope.orderSearch();
    }

    $scope.orderSearch = function () {
        var startDate = null;
        var endDate = null;

        if ($scope.startDate != null && $scope.endDate != null) {
            startDate = $filter('date')(new Date($scope.startDate), "yyyy-MM-dd");
            endDate = $filter('date')(new Date($scope.endDate), "yyyy-MM-dd");
        }
        var loginServiceRes = loginService.validateLogin();
        if (loginServiceRes != null) {
            loginServiceRes.then(function (signinData) {
                if (signinData != null) {
                    $http({
                        method: 'POST',
                        url: baseUrl + "Account/GetOrderHistory",
                        contentType: 'application/json',
                        data: {
                            CustomerRef: signinData.CustomerLookupId,
                            CustomerEmail: signinData.Email,
                            PageNumber: $scope.currentPage,
                            OrderIdToSearch: $scope.orderNoSelected,
                            DateStart: startDate,
                            DateEnd: endDate,
                            OrderType: $scope.orderTypeSelected.value,
                            OrderStatus: $scope.orderStatusSelected.value

                        }
                    }).then(function (result) {
                        if (result.data !== undefined && result.data != null) {
                            $scope.orders = result.data.Orders;
                            $scope.numberOfPages = result.data.NumberOfPages;
                        }
                    });
                }
            });
        }
    }

    $scope.orderSearch();

    $scope.first = function () {
        $scope.currentPage = 1;
        $scope.orderSearch();
    }

    $scope.prev = function () {
        $scope.currentPage--;
        $scope.orderSearch();
    }

    $scope.next = function () {
        $scope.currentPage++;
        $scope.orderSearch();
    }

    $scope.last = function () {
        $scope.currentPage = $scope.numberOfPages;
        $scope.orderSearch();
    }

    $scope.disableForwardButtons = function () {
        if ($scope.currentPage >= $scope.numberOfPages) {
            return true;
        }
        return false;
    }

    $scope.disableBackwardButtons = function () {
        if ($scope.currentPage == 1) {
            return true;
        }
        return false;
    }

    $scope.ShowOrderModal = function (id) {

        $rootScope.$applyAsync(function () {
            $rootScope.partialName = "orderSummary_template";
        });

        if (id != null) {
            if ($scope.signinData != null) {
                CommonCode.getBaseCookieSettings();
                var orderInfo =
                    {
                        orderId: id,
                        customerLookupId: $scope.signinData.CustomerLookupId,
                        email: $scope.signinData.Email,
                        languageid: CommonCode.LanguageId
                    };
                
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/OrderSummary_V2",//testing
                    //url: baseUrl + "/Account/OrderSummary?orderId=" + orderId + "&customerLookupId=" + customerRef + "&email=" + customerEmail + "&languageid=" + baseCookieSettings.split('&')[2].split('=')[1],
                    contentType: 'application/json',
                    data: orderInfo
                }).then(function (result) {

                    $rootScope.OrderSummary = {
                        orderId: result.data.orderId,
                        orderDate: result.data.orderDate,
                        orderValueTotal: result.data.orderValueTotaltValue,
                        orderDispatchStatus: result.data.orderDispatchStatus,
                        containsFlexi: result.data.containsFlexi,
                        containsDownload: result.data.containsDownload,
                        orderitems: result.data.orderitems,
                        deliveryCost: result.data.deliveryCostValue,
                        orderTotalCost: result.data.orderTotalCostValue,
                        orderCurrency: result.data.currency,
                        orderItems: result.data.items,
                        DeliveryName: result.data.courierName,
                        DeliveryDescription: result.data.typeOfDelivery,
                        DaysFrom: result.data.mindays,
                        DaysTo: result.data.maxdays,
                        FlexiOverView: result.data.flexiOverview
                    };

                    if (result.data.shippingAddress != null) {
                        $scope.OrderSummary.shippingAddress = {
                            customername: result.data.shippingAddress.split('¬')[0],
                            street1: result.data.shippingAddress.split('¬')[3],
                            street2: result.data.shippingAddress.split('¬')[4],
                            town: result.data.shippingAddress.split('¬')[5],
                            city: result.data.shippingAddress.split('¬')[6],
                            postcode: result.data.shippingAddress.split('¬')[7]
                        }
                    }

                    if (result.data.billingAddress != null) {

                        $scope.OrderSummary.billingAddress = {
                            customername: result.data.billingAddress.split('¬')[0],
                            street1: result.data.billingAddress.split('¬')[3],
                            street2: result.data.billingAddress.split('¬')[4],
                            town: result.data.billingAddress.split('¬')[5],
                            city: result.data.billingAddress.split('¬')[6],
                            postcode: result.data.billingAddress.split('¬')[7]
                        }
                    }

                    $scope.flexi = [];
                    $scope.totalPending = 0;
                    for (var i = 0; i < result.data.items.length; i++) {
                        if (result.data.items[i].flexiItem == 1) {
                            for (var j = 0; j < result.data.items[i].flexiDetails.length; j++) {
                                $scope.totalPending += result.data.items[i].flexiDetails[j].amountdue;
                                $scope.flexi.push({ 'scheduledate': result.data.items[i].flexiDetails[j].scheduledate, 'amountdue': result.data.items[i].flexiDetails[j].amountdue, 'itemid': result.data.items[i].itemid });
                            }
                        }
                    }
                    $scope.totalPending = parseFloat($scope.totalPending).toFixed(2);

                });
            }
        }
    }

}]);
app.filter('dispatchStatus', function () {
    return function (input) {
        var out = '';
        if (input == 0) {
            out = 'Processing';
        } else if (input == 1) {
            out = 'Approved';
        } else {
            out = 'Cancelled';
        }

        return out;
    };
});

app.controller('OrderSummaryCtrl', ['$scope', '$http', 'loginService', 'CommonCode', function ($scope, $http, loginService, CommonCode) {
    $scope.orderId = 0;
    var customerRef = "0";
    $scope.init = function (orderid) {
        $scope.orderId = orderid;
    }

    $scope.GetProductUrl = function (ppid, ppsku, desc) {
        return GetProductUrl(ppid, ppsku, desc);
    }

    var loginServiceRes = loginService.validateLogin();
    if (loginServiceRes != null) {
        loginServiceRes.then(function (signinData) {
            if (signinData != null) {
                CommonCode.getBaseCookieSettings();
                var orderInfo =
                    {
                        orderId: $scope.orderId,
                        customerLookupId: signinData.CustomerLookupId,
                        email: signinData.Email,
                        languageid: CommonCode.LanguageId
                    };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/OrderSummary_V2",//testing
                    //url: baseUrl + "/Account/OrderSummary?orderId=" + orderId + "&customerLookupId=" + customerRef + "&email=" + customerEmail + "&languageid=" + baseCookieSettings.split('&')[2].split('=')[1],
                    contentType: 'application/json',
                    data: orderInfo
                }).then(function (result) {

                    $scope.OrderSummary = {
                        orderId: result.data.orderId,
                        orderDate: result.data.orderDate,
                        orderValueTotal: result.data.orderValueTotaltValue,
                        orderDispatchStatus: result.data.orderDispatchStatus,
                        containsFlexi: result.data.containsFlexi,
                        containsDownload: result.data.containsDownload,
                        orderitems: result.data.orderitems,
                        deliveryCost: result.data.deliveryCostValue,
                        orderTotalCost: result.data.orderTotalCostValue,
                        orderCurrency: result.data.currency,
                        orderItems: result.data.items
                        
                    };

                    if (result.data.shippingAddress != null) {
                        $scope.OrderSummary.shippingAddress = {
                            customername: result.data.shippingAddress.split('¬')[0],
                            street1: result.data.shippingAddress.split('¬')[3],
                            street2: result.data.shippingAddress.split('¬')[4],
                            town: result.data.shippingAddress.split('¬')[5],
                            city: result.data.shippingAddress.split('¬')[6],
                            postcode: result.data.shippingAddress.split('¬')[7]
                        }
                    }

                    if (result.data.billingAddress != null) {

                        $scope.OrderSummary.billingAddress = {
                            customername: result.data.billingAddress.split('¬')[0],
                            street1: result.data.billingAddress.split('¬')[3],
                            street2: result.data.billingAddress.split('¬')[4],
                            town: result.data.billingAddress.split('¬')[5],
                            city: result.data.billingAddress.split('¬')[6],
                            postcode: result.data.billingAddress.split('¬')[7]
                        }
                    }

                    $scope.flexi = [];
                    $scope.totalPending = 0;
                    for (var i = 0; i < result.data.items.length; i++) {
                        if (result.data.items[i].flexiItem == 1) {
                            for (var j = 0; j < result.data.items[i].flexiDetails.length; j++) {
                                $scope.totalPending += result.data.items[i].flexiDetails[j].amountdue;
                                $scope.flexi.push({ 'scheduledate': result.data.items[i].flexiDetails[j].scheduledate, 'amountdue': result.data.items[i].flexiDetails[j].amountdue, 'itemid': result.data.items[i].itemid });
                            }
                        }
                    }
                    $scope.totalPending = parseFloat($scope.totalPending).toFixed(2);

                });
            }
        });
    }
}]);


app.controller('accSettingsCtrl', ['$scope', '$cookies', '$http', '$window', '$mdDialog', 'loginService', function ($scope, $cookies, $http, $window, $mdDialog, loginService) {



    $scope.currenices = [];
    $scope.customerRef = "";
    $scope.custEmail = "";
    $scope.customerid = "";
    $scope.firstName = "";
    $scope.defaultCurrency = 0;
    $scope.receivePost = false;
    $scope.receiveSms = false;
    $scope.receiveEmail = false;
    $scope.receiveEmail3rdParty = false;


    var oCurrency = {
        currencyid: 0,
        currencysymbol: "",
        lettercode2: "",
        lettercode3: "",
        currencyname: "",
        active: 1,
        displayorder: 0,
        isocurrencycode: "",
        CurrencyDisplayName: "Select Currency"
    };
    $scope.currenices.push(oCurrency);



    $scope.init = function () {
        var loginServiceRes = loginService.validateLogin();
        if (loginServiceRes != null) {
            loginServiceRes.then(function (signinData) {
                if (signinData != null && signinData.Email != "" && signinData.CustomerLookupId != "") {
                    $scope.customerRef = signinData.CustomerLookupId;
                    $scope.custEmail = signinData.Email;

                    var userAccInfo = { CustomerLookupId: signinData.CustomerLookupId, Email: signinData.Email };

                    $http({
                        method: 'POST',
                        url: baseUrl + "Account/GetAccountSettings",
                        contentType: 'application/json',
                        data: userAccInfo
                    }).then(function (result) {
                        if (result.data !== undefined) {
                            $scope.currenices.length = 0;
                            var oCurrency = {
                                currencyid: 0,
                                currencysymbol: "",
                                lettercode2: "",
                                lettercode3: "",
                                currencyname: "",
                                active: 1,
                                displayorder: 0,
                                isocurrencycode: "",
                                CurrencyDisplayName: "Select Currency"
                            };
                            $scope.currenices.push(oCurrency);
                            var iCurrCnt = result.data.availableCurrencies.length;
                            for (iNum = 0; iNum < iCurrCnt; iNum++) {
                                var oCurrency = {
                                    currencyid: result.data.availableCurrencies[iNum].CurrencyId,
                                    currencysymbol: result.data.availableCurrencies[iNum].CurrencySymbol,
                                    lettercode2: result.data.availableCurrencies[iNum].LetterCode2,
                                    lettercode3: result.data.availableCurrencies[iNum].LetterCode3,
                                    currencyname: result.data.availableCurrencies[iNum].CurrencyName,
                                    active: result.data.availableCurrencies[iNum].Active,
                                    displayorder: result.data.availableCurrencies[iNum].DisplayOrder,
                                    isocurrencycode: result.data.availableCurrencies[iNum].isocurrencycode,
                                    CurrencyDisplayName: result.data.availableCurrencies[iNum].CurrencyDisplayName
                                };
                                $scope.currenices.push(oCurrency);
                            }
                            $scope.customerId = result.data.customerId;
                            $scope.firstName = result.data.firstName;
                            $scope.defaultCurrency = result.data.defaultCurrency;
                            $scope.receivePost = result.data.receivePost == 1 ? true : false;
                            $scope.receiveSms = result.data.receiveSms == 1 ? true : false;
                            $scope.receiveEmail = result.data.receiveEmail == 1 ? true : false;
                            $scope.receiveEmail3rdParty = result.data.receiveEmail3rdParty == 1 ? true : false;

                        }
                    });
                }
            });
        }
    };

    $scope.SaveAccountPrefs = function () {

        var accountSettings = {};
        accountSettings = {
            availableCurrencies: $scope.currenices,
            customerRef: $scope.customerRef,
            custEmail: $scope.custEmail,
            customerid: $scope.customerId,
            firstName: $scope.firstName,
            defaultCurrency: $scope.defaultCurrency,
            receivePost: $scope.receivePost == true ? 1 : 0,
            receiveSms: $scope.receiveSms == true ? 1 : 0,
            receiveEmail: $scope.receiveEmail == true ? 1 : 0,
            receiveEmail3rdParty: $scope.receiveEmail3rdParty == true ? 1 : 0
        };

        $http({
            method: 'POST',
            url: baseUrl + "Account/SaveAccountPrefs",
            contentType: 'application/json',
            data: accountSettings
        }).then(function (result) {
            if (result.data !== undefined) {

                if (result.data.MessageCode != 200) {
                    $scope.openOffscreen(result.data.TitleMessage, result.data.MessageToDisplay, 'Close');
                }
                else {

                    $window.location.href = '/myaccount';

                }

            }
        });

    };

}]);

app.controller('RecentOrdersController', ['$scope', '$http', '$cookies', 'loginService', '$rootScope', 'CommonCode', function ($scope, $http, $cookies, loginService, $rootScope, CommonCode) {
    var signinData = $cookies.get("keepMeSignedIn");
    $scope.signinData = JSON.parse(decodeURIComponent(signinData));
    $scope.orders = [];

    $rootScope.GetProductUrl = function (ppid, ppsku, desc) {
        return GetProductUrl(ppid, ppsku, desc);
    }

    var loginServiceRes = loginService.validateLogin();
    if (loginServiceRes != null) {
        loginServiceRes.then(function (signinData) {
            if (signinData != null) {
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/GetOrderHistory",
                    contentType: 'application/json',
                    data: {
                        CustomerRef: signinData.CustomerLookupId,
                        CustomerEmail: signinData.Email,
                        PageNumber: 1
                    }
                }).then(function (result) {
                    if (result.data !== undefined && result.data != null) {
                        $scope.orders = result.data.Orders.slice(0, 4);
                    }
                });
            }
        });
    }

    $scope.ShowOrderModal = function (id) {

        $rootScope.$applyAsync(function () {
            $rootScope.partialName = "orderSummary_template";
        });

        if (id != null) {
            if ($scope.signinData != null) {
                CommonCode.getBaseCookieSettings();
                var orderInfo =
                    {
                        orderId: id,
                        customerLookupId: $scope.signinData.CustomerLookupId,
                        email: $scope.signinData.Email,
                        languageid: CommonCode.LanguageId
                    };              

                $http({
                    method: 'POST',
                    url: baseUrl + "Account/OrderSummary_V2",//testing
                    //url: baseUrl + "/Account/OrderSummary?orderId=" + orderId + "&customerLookupId=" + customerRef + "&email=" + customerEmail + "&languageid=" + baseCookieSettings.split('&')[2].split('=')[1],
                    contentType: 'application/json',
                    data: orderInfo
                }).then(function (result) {

                    $rootScope.OrderSummary = {
                        orderId: result.data.orderId,
                        orderDate: result.data.orderDate,
                        orderValueTotal: result.data.orderValueTotaltValue,
                        orderDispatchStatus: result.data.orderDispatchStatus,
                        containsFlexi: result.data.containsFlexi,
                        containsDownload: result.data.containsDownload,
                        orderitems: result.data.orderitems,
                        deliveryCost: result.data.deliveryCostValue,
                        orderTotalCost: result.data.orderTotalCostValue,
                        orderCurrency: result.data.currency,
                        orderItems: result.data.items,
                        DeliveryName: result.data.courierName,
                        DeliveryDescription: result.data.typeOfDelivery,
                        DaysFrom: result.data.mindays,
                        DaysTo: result.data.maxdays,
                        FlexiOverView: result.data.flexiOverview
                    };

                    if (result.data.shippingAddress != null) {
                        $scope.OrderSummary.shippingAddress = {
                            customername: result.data.shippingAddress.split('¬')[0],
                            street1: result.data.shippingAddress.split('¬')[3],
                            street2: result.data.shippingAddress.split('¬')[4],
                            town: result.data.shippingAddress.split('¬')[5],
                            city: result.data.shippingAddress.split('¬')[6],
                            postcode: result.data.shippingAddress.split('¬')[7]
                        }
                    }

                    if (result.data.billingAddress != null) {

                        $scope.OrderSummary.billingAddress = {
                            customername: result.data.billingAddress.split('¬')[0],
                            street1: result.data.billingAddress.split('¬')[3],
                            street2: result.data.billingAddress.split('¬')[4],
                            town: result.data.billingAddress.split('¬')[5],
                            city: result.data.billingAddress.split('¬')[6],
                            postcode: result.data.billingAddress.split('¬')[7]
                        }
                    }

                    $scope.flexi = [];
                    $scope.totalPending = 0;
                    for (var i = 0; i < result.data.items.length; i++) {
                        if (result.data.items[i].flexiItem == 1) {
                            for (var j = 0; j < result.data.items[i].flexiDetails.length; j++) {
                                $scope.totalPending += result.data.items[i].flexiDetails[j].amountdue;
                                $scope.flexi.push({ 'scheduledate': result.data.items[i].flexiDetails[j].scheduledate, 'amountdue': result.data.items[i].flexiDetails[j].amountdue, 'itemid': result.data.items[i].itemid });
                            }
                        }
                    }
                    $scope.totalPending = parseFloat($scope.totalPending).toFixed(2);

                });
            }
        }
    }

}]);

app.controller('FreedomMemberCtrl', ['$scope', '$cookies', '$http', 'loginService', '$rootScope', function ($scope, $cookies, $http, loginService, $rootScope) {
    $scope.magazine = [];
    var signinData = $cookies.get("keepMeSignedIn");
    $scope.signinData = JSON.parse(decodeURIComponent(signinData));

    $scope.isFreedomHoliday = null;
    $scope.orders = [];
    var loginServiceRes = loginService.validateLogin();
    $scope.SchedLength = 0;

    $scope.GetFreedomArea = function () {
        if (loginServiceRes != null) {
            loginServiceRes.then(function (signinData) {
                if (signinData != null) {
                    $http({
                        method: 'Post',
                        url: '/Account/freedom',
                        ContentType: 'application/json'
                    }).then(function (result) {
                        $scope.magazines = $.parseJSON(result.data).response.issues;
                    });

                    var accInfo =
                        {
                            CustomerLookupId: signinData.CustomerLookupId,
                            Email: signinData.Email
                        };
                    $http({
                        method: 'POST',
                        url: baseUrl + "Account/FreedomHolidaySchedule",
                        contentType: 'application/json',
                        data: accInfo
                    }).then(function (result) {
                        if (result.data != null) {
                            $scope.isFreedomHoliday = result.data.IsFreedomHoliday;

                            $scope.ScheduleList = result.data.PaymentSchedule;

                            $scope.SchedLength = result.data.PaymentSchedule.length;
                        }
                    });
                }
            });
        };
    }

    $scope.GetFreedomArea();

    $scope.setPartialName = function (name, partialCheck) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;

    }

    $scope.ActivateHoliday = function () {
        $scope.DaysValidation = "";
        var days = document.getElementById('HolidaySelectedDays').value;

        if (signinData != null) {

            $scope.signinData = signinData;

            if (days != -1) {
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/FreedomSetupHolidayMode",
                    contentType: 'application/json',
                    data: JSON.stringify({ CustomerLookupId: $scope.signinData.CustomerLookupId, Email: $scope.signinData.Email, DaySelector: days })
                }).then(function (result) {
                    $scope.isFreedomHoliday = true;
                    $scope.DaysValidation = "Holidays activated";
                    HideModal();
                    $scope.GetFreedomArea();

                });
            }
            else {

                $scope.DaysValidation = "Select the days";
            }
        }
    }
}

]);



