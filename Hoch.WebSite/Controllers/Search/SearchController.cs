﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Caching;
namespace Hoch.Website.Controllers.Search
{
    public class SearchController : BaseController
    {
        public ActionResult SearchResult(string keyword, int type = 0)
        {
            ViewBag.Keyword = keyword;
            ViewBag.Type = type;
            return View("~/Views/Search/SearchResult.cshtml");
        }
    }
}