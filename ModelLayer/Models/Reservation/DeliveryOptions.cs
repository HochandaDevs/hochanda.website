﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
    public class DeliveryOptions
    {
        public int CourierID { get; set; }

        public int ServiceID { get; set; }

        public string CourierName { get; set; }

        public int SelectedService { get; set; }

        public string CurrencySymbol { get; set; }

        public string SalesPoints { get; set; }

        public decimal FullPrice { get; set; }

        public string FullPriceWithCurrency { get; set; }
    }
}
