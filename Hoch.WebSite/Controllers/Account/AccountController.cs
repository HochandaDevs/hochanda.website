﻿using Hoch.Website.Helpers;
using Hoch.Website.Models;
using ModelLayer.Models;
using ModelLayer.Models.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Controllers.Account
{
	public class AccountController : BaseController
	{

		[MetadataActionFilter(MetaDataFriendlyName = "My Details")]
		public ActionResult MyDetails()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/MyDetails.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Payment Methods")]
		public ActionResult PaymentMethods()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/PaymentMethods.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[Route("myaccount")]
		[MetadataActionFilter(MetaDataFriendlyName = "My Account")]
		public ActionResult MyAccount()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{

				return View("~/Views/Account/MyAccount.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Forgot Password")]
		public ActionResult ForgotPassword()
		{

			return PartialView("~/Views/Shared/Partials/_forgot_password.cshtml");

		}

		[MetadataActionFilter(MetaDataFriendlyName = "Reset Password")]
		public ActionResult ResetPassword(string rp)
		{

			ViewBag.Token = rp;
			return View("~/Views/Account/RegisterCustomerPassword.cshtml");

		}

		[MetadataActionFilter(MetaDataFriendlyName = "Address Book")]
		public ActionResult AddressBook()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/AddressBook.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Freedom Area")]
		public ActionResult FreedomArea()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/FreedomMembersArea.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}


		[ExtendLoginCookie(DoNotExtend = true)]
		public async Task<JsonResult> freedom()
		{
			string paymentUrl = String.Empty;
			WebRequestHelper webHelper = new WebRequestHelper();
			var s = await webHelper.GetItem<object>("http://www.joomag.com/Frontend/WebService/restAPI.php?key=api_6fdac5b89deadb33348de2b0941c8e59&action=listIssues&magazine_ID=M0279661001481539455");

			return Json(s.ToString());
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Order History")]
		public ActionResult OrderHistory()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/OrderHistory.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Order Summary")]
		public ActionResult OrderSummary(string orderId)
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				ViewBag.OrderId = orderId;
				return View("~/Views/Account/OrderSummary.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}


		[MetadataActionFilter(MetaDataFriendlyName = "Settings")]
		public ActionResult Settings()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{

				return View("~/Views/Account/Settings.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

		[MetadataActionFilter(MetaDataFriendlyName = "Favourites")]
		public ActionResult Favourites()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return View("~/Views/Account/Favourites.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}


		public ActionResult AddAddress()
		{
			if (Request.Cookies["keepMeSignedIn"] != null)
			{
				return PartialView("~/Views/Shared/Partials/_add_address_form.cshtml");
			}
			else
			{
				return RedirectToAction("Index", "Login");

			}
		}

	}
}