﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class WebLanguageModel
	{
		public int LanguageId { get; set; }
		public string LanguageName { get; set; }
		public string Language2Digit { get; set; }
		public string Language3Digit { get; set; }
		public bool Active { get; set; }
		public bool WebActive { get; set; }
		public int IsPeechAPI { get; set; }
		public string IsPeechMale { get; set; }
		public string IsPeechFemale { get; set; }
		public int TVCurrencyId { get; set; }
		public string CultureCode { get; set; }
		public int TVCountryId { get; set; }
		public int? CountryId { get; set; }
	}
}
