﻿
//We only need external on basket really. So if we do not have it then we need to set it to false.
try {
    if (isExternalVariable == null) {
        var isExternalVariable = false;
    }
}
catch (err) {
    var isExternalVariable = false;
}


app.controller("CheckoutController", ['$scope', '$http', '$filter', '$compile', '$sanitize', '$rootScope', '$httpParamSerializer', '$window', '$sce', '$cookies', 'CommonCode', function ($scope, $http, $filter, $compile, $sanitize, $rootScope, $httpParamSerializer, $window, $sce, $cookies, CommonCode) {
    $scope.IsBusy = true;
    $scope.CanPay = true;

    var isFlexiVal = -1;
    var reservItemIDVal = 0;
    var isItemFlexiVal = -1;
    var basketId = localStorage.getItem("basketId");
    var signinData = $cookies.get("keepMeSignedIn");

    $scope.displaySpinner = false;

    if (signinData != null) {
        signinData = JSON.parse(decodeURIComponent(signinData));
        if (signinData.Email != "" && signinData.CustomerLookupId != "") {
            $scope.signinData = signinData;
        }
    }

    $rootScope.MyAccountpaymentPage = null;
    $rootScope.paymentPage = null;

    $scope.addresses = [];
    $scope.cardlookupID = 0;
    $scope.selectedBillingAddID;
    $scope.selectedShippingAddID;
    $scope.selectedCard;
    $scope.hid_MyaccountaddcardPartial = true;
    $scope.PaymentContainer = true;
    $scope.billingDiv = false;
    $scope.token = null;
    $scope.doPrepGuid = null;
    $scope.payPalEnable = false;
    $scope.payPalAvailable = false;
    $scope.info = null;
    $scope.messageCode = null;
    $scope.payPaginationCheck = 0;
    $scope.reservationData = [];
    $scope.customerFirstname = '';

    var basketSummaryValidation = false;

    $scope.disablePlaceOrderButton = function () {
        if (($rootScope.selectedBillingAddress != null && $rootScope.selectedBillingAddress.length > 0) && ($rootScope.selectedShippingAddress != null && $rootScope.selectedShippingAddress.length > 0) && $rootScope.selectedCardData != null) {
            return true;
        }
        return false;
    }

    $scope.CanShowDeliveryOptions = function () {

        if ($scope.messageCode != null || $scope.paymentWrap || $scope.billingDiv || $scope.IsPaymentArea || $scope.signinData == null || $scope.NeedsNewAddress()) {
            return false;
        }

        return true;
    }


    $scope.NeedsNewAddress = function () {
        if ($scope.messageCode != null || $scope.paymentWrap || $scope.billingDiv || $scope.IsPaymentArea || $scope.signinData == null || $scope.addressDataCount > 0 || $scope.shippingAddressLength > 0) {
            return false;
        }

        return true;
    }

    $scope.init = function () {


        var basketItemsCount = localStorage.getItem("basketCount");

        if (basketItemsCount == 0 || basketItemsCount == null || ($rootScope.buttonCheck != '1' && $rootScope.buttonCheck != undefined && $rootScope.buttonCheck != 1 && $rootScope.buttonCheck != null)) {
            //empty if we not external
            var externalQueryString = "";

            if (isExternalVariable) {
                externalQueryString = "&isexternal=true";
            }


            $window.location.href = "/Basket" + externalQueryString;
        }

        $scope.page = 2;

        $rootScope.IsDeliveryArea = false;
        $rootScope.IsPaymentArea = false;

        if (window.location.search.toLowerCase().indexOf("delivery") != -1) {
            $scope.deliveryPagination = 1;

            if (window.location.search.toLowerCase() == "?delivery") {
                $rootScope.ChangeURL(window.location.href.replace(window.location.search, ""));
                $rootScope.IsDeliveryArea = true;
                $rootScope.IsPaymentArea = false;
            }
        }

        if (window.location.href.indexOf('checkout') != -1) {
            $rootScope.checkoutPaginationCheck = 1;
        }

        var loginCheck = localStorage.getItem("loginCheckoutCheck");


        if (window.location.search.toLowerCase().indexOf("addedcard") != -1) {

            $scope.addedcardCheck = 1;
            $scope.selectedLookupID = window.location.search.split("=")[1];

            $rootScope.checkoutPaginationCheck = 0;
            $scope.payPaginationCheck = 1;

            $rootScope.IsDeliveryArea = false;
            $rootScope.IsPaymentArea = true;

            $rootScope.ChangeURL(window.location.href.replace(window.location.search, ""));

            loginCheck = 1;

        }


        if ($scope.signinData != null && loginCheck == 1) {
            PopulateCardDetails();

            if ($scope.selectedLookupID != undefined || $scope.selectedLookupID != null) {
                $scope.paymentWrapDiv = true;
            }
            else {
                $scope.paymentWrapDiv = false;

                $rootScope.IsDeliveryArea = true;
                $rootScope.IsPaymentArea = false;

                PopulateAddress(true);

                $('#checkoutDeliveryWrap').show();

                $('#checkoutUpsellcontainer').hide();
                localStorage.removeItem("loginCheckoutCheck");

                $scope.deliveryPagination = 0;
            }
        }
        else if ($scope.signinData != null) {
            PopulateCardDetails();
        }

        if ($rootScope.UpsellClick != 1 && $scope.addedcardCheck != 1 && loginCheck != 1) {
            PopulateUpSells();
        }
        else {
            $('#checkoutUpsellcontainer').hide();
        }

        if ($scope.paymentWrapDiv == false && $scope.deliveryWrapDiv == false && $scope.billingDiv == true) {
            $('#checkoutDeliveryWrap').hide();
        }
    }

    $rootScope.$on('modalAddAddress', function (event, value) {
        if (value != null && value != undefined) {
            if ($rootScope.partialCheck == "1" || $scope.partialCheckTemplate == 1) {//added this condition to differentaite between the billing address and shipping address from addrsssbook modal
                $scope.selectedBillingAddID = 0;
                $scope.selectedShippingAddID = value;
                $scope.modalSIDCheck = 1;
            }
            else {
                $scope.selectedBillingAddID = value;
                $scope.selectedShippingAddID = 0;
            }

            PopulateAddress();

        }


    });

    //When they have added an upsell to the basket this should be run.
    $scope.UpdateUpsell = function () {
        basketSummaryValidation = true;
        PopulateUpSells();
    }

    $rootScope.PopulateSelectedData = function (selectedBillingAddID, selectedShippingAddID, selectedCard) {
        if (selectedBillingAddID != null && selectedShippingAddID != null && selectedCard != null && selectedBillingAddID != 0 && selectedShippingAddID != 0) {
            $scope.selectedShippingAddID = selectedShippingAddID;
            $scope.selectedBillingAddID = selectedBillingAddID;
            $scope.selectedCard = selectedCard;

        }
        else {
            if ($rootScope.partialCheck == "1") {//added this condition to differentaite between the billing address and shipping address from addrsssbook modal
                $scope.selectedBillingAddID = 0;
                $scope.selectedShippingAddID = selectedShippingAddID;
            }
            else {
                $scope.selectedBillingAddID = selectedShippingAddID;
                $scope.selectedShippingAddID = 0;
            }

        }

        //if using another card first populate card details to get the iblookup id and then excute populateaddress
        if (($scope.selectedCard != undefined || $scope.selectedCard != null) && ($scope.selectedCard != 0 || $scope.selectedCard != "0")) {
            PopulateCardDetails($scope.selectedCard);
        }
        else {
            PopulateAddress();
        }

    }

    $scope.UpdateDelivery = function UpdateDelivery(serviceID) {
        $scope.selectedServiceID = serviceID;
        UpdateDeliveryOptions();
    }

    function PopulateDeliveryOptions(updateBasketSummary) {
        $scope.CanPay = false;

        var deliveryParams = { BasketId: basketId, CountryID: CommonCode.CountryId, CurrencyID: CommonCode.CurrencyId };//change this to dynamic values
        $http({
            method: 'POST',
            url: baseUrl + "Reservations/DisplayDeliveryOptions",
            contentType: 'application/json',
            data: JSON.stringify(deliveryParams),
        }).then(function (result) {

            if (result.data != null) {
                $scope.resDeliveryData = result.data;
            }

            $scope.selectedDeliveryService = $scope.resDeliveryData != null && $scope.resDeliveryData != undefined ? $filter('filter')($scope.resDeliveryData, { SelectedService: "1" }) : "";

            PopulateBasketSummary(true);

        });
    }

    function PopulateAddress(doDeliveryCheck) {

        if ($scope.signinData == null) {
            document.location.href = "/basket";
        }
        $scope.CanPay = false;
        var addressParams = {
            BasketId: basketId, CustomerRef: $scope.signinData.CustomerLookupId,
            ShippingID: $scope.selectedShippingAddID != undefined ? $scope.selectedShippingAddID : 0,
            BillingID: $scope.selectedBillingAddID != undefined ? $scope.selectedBillingAddID : 0,
            CardLookupID: $scope.cardLookupID != undefined ? $scope.cardLookupID : 0
        };

        $http({
            method: 'POST',
            url: baseUrl + "Reservations/UpdateReservationByCustomer",
            contentType: 'application/json',
            data: JSON.stringify(addressParams),
        }).then(function (result) {
            if (result.data != undefined) {

                var addressData = result.data;

                $scope.addressList = addressData;

                var countryAddress = $scope.addressList.filter(function (i) {
                    return i.CountryId == CommonCode.CountryId;
                })

                //All Address
                //We only want these to be used by cards.
                $rootScope.allAddressList = $scope.addressList;

                //Shipping Address
                //We only want these to be selected
                $rootScope.addressList = countryAddress;
                $scope.addressDataCount = $rootScope.addressList.length;
                $scope.shippingAddressLength = 0;

                if ($scope.addressDataCount != 0) {

                    $scope.partialCheckTemplate = 0;

                    if ($rootScope.UpsellClick == 1 || $scope.modalSIDCheck == 1) {
                        $('#emptyShippingAddress').hide();
                        $('#checkoutDeliveryWrap').show();

                    }

                    $scope.shippingAddressResults = $filter('filter')(addressData, { SelectedShippingAddress: "1" });
                    $scope.shippingAddressLength = $scope.shippingAddressResults.length == null ? 0 : $scope.shippingAddressResults.length;

                    if ($scope.shippingAddressLength != 0) {
                        $scope.shippingAddressId = $scope.shippingAddressResults[0].AddressId;
                    }

                    if ($scope.selectedBillingAddID != 0 && $scope.selectedBillingAddID != undefined && $scope.selectedBillingAddID != null) {
                        $scope.billingAddressResults = $filter('filter')(addressData, { AddressId: $scope.selectedBillingAddID }, true);
                        $scope.billingAddressId = $scope.billingAddressResults[0].AddressId;
                    }
                    else {
                        $scope.billingAddressResults = $filter('filter')(addressData, { SelectedBillingAddress: "1" });
                        $scope.billingAddressLength = $scope.billingAddressResults.length;

                        if ($scope.billingAddressLength != 0) {
                            $scope.billingAddressId = $scope.billingAddressResults[0].AddressId;
                        }
                    }
                }
                else {
                    $('#emptyShippingAddress').show();
                    $('#checkoutDeliveryWrap').hide();

                    $scope.partialCheckTemplate = 1;
                }

                //need to review this condition
                if (($scope.cardLookupID == null || $scope.cardLookupID == undefined) && ($scope.paymentWrapDiv == "true" || $scope.paymentWrapDiv == true)) {

                    $scope.payPaginationCheck = 1;
                    PopulateCardDetails();
                }

                $rootScope.selectedBillingAddress = $scope.billingAddressResults;
                $rootScope.selectedShippingAddress = $scope.shippingAddressResults;

                PopulateDeliveryOptions(true);
            }

            if (doDeliveryCheck) {
                if ($scope.signinData != null && $scope.signinData != undefined) {
                    $('#checkoutLoginWrapper').hide();

                    if ($scope.addressDataCount != 0 || $scope.shippingAddressLength != 0) {
                        $('#checkoutDeliveryWrap').show();
                        $('#checkoutUpsellcontainer').hide();
                        $('#emptyShippingAddress').hide();
                    }
                    else {
                        $('#emptyShippingAddress').show();
                    }
                }
                else {
                    $('#checkoutLoginWrapper').show();
                }
            }

            $scope.IsBusy = false;

        });

    }

    function PopulateBasketSummary(basketSummaryValidation, functionToCallWhenFinished) {
        $scope.CanPay = false;
        var checkoutParams = { BasketId: basketId, PerformCalculation: basketSummaryValidation, CountryID: CommonCode.CountryId, CurrencyID: CommonCode.CurrencyId, LanguageID: CommonCode.LanguageId, IsFlexi: isFlexiVal, ReservItemID: reservItemIDVal, IsItemFlexi: isItemFlexiVal };//dynamic values
        $scope.token = null;
        $rootScope.paymentPage = null;
        $scope.doPrepGuid = null;

        $scope.IsBusy = true;

        $http({
            method: 'POST',
            url: baseUrl + "Reservations/GetBasketDisplay",
            contentType: 'application/json',
            data: JSON.stringify(checkoutParams),
        }).then(function (result) {

            if (result.data != null) {
                var basketData = result.data;
                $scope.reservationData = basketData.displayCheckout.reservationModel;

                if ($scope.reservationData[0].ReservationLocked == null) {
                    $scope.reservationData[0].ReservationLocked = false;
                }

                $scope.flexiBasketAvailable = $scope.reservationData[0].FlexiBasketAvailable;
                $scope.numberofInstalments = $scope.reservationData[0].FlexiBasketInstallments;
                $scope.toPay = $scope.reservationData[0].ToPayTodayWithCurrency;
                $scope.subTotalAmount = $scope.reservationData[0].SubTotalWithCurrency;

                $scope.toPayToday = $scope.reservationData[0].ToPayToday;
                $scope.nextInstalmentAmount = $scope.reservationData[0].FBPreviewWithCurrency;
                $scope.reservationItemData = basketData.displayCheckout.reservationItemsModel;
                $scope.filterReservationITemData = $filter('filter')($scope.reservationItemData, function (d) { return d.ParentProductID != 23 && d.ParentProductID != 22 && d.ParentProductID != 19 && d.Quantity != 0 }).length
                //$scope.errorMessage = $scope.reservationData.MessageToDisplay;

                $scope.deliveryAmount = $scope.reservationData[0].DeliveryWithCurrency;

                if ($scope.reservationData[0].VoucherUsed) {
                    $rootScope.voucherApplied = $scope.reservationData[0].VoucherUsed;
                    $rootScope.vouchercode = $scope.reservationData[0].VoucherCode;
                    $rootScope.voucherDiscount = $scope.reservationData[0].VoucherDiscount;
                    $rootScope.displayDiscount = $scope.reservationData[0].VoucherDiscountString;
                }

                var disPaypal = $scope.reservationItemData.filter(function (item) {
                    return ((item.AuctionItem == 1 && item.PendingStatus == 0) || (item.ParentProductID == 26 && item.Quantity > 0) || (item.FlexiBuyItem == 1 && item.Quantity > 0));
                });

                //pendingItem.length checks whether there is an auction item or the item is pending
                if ($scope.reservationData[0].FlexiBasketSelected == false && disPaypal.length <= 0 && $scope.reservationData[0].ReservationLocked == false) {
                    $scope.payPalAvailable = true;
                }

                if ($scope.payPalAvailable == true) {
                    $scope.payWithPayPal();
                }

                $scope.messageCode = $scope.reservationData[0].MessageCode;
                if ($scope.messageCode != null || $scope.reservationItemData.length == 0 || $scope.filterReservationITemData == 0) {
                    //empty if we not external
                    var externalQueryString = "";

                    if (isExternalVariable) {
                        externalQueryString = "&isexternal=true";
                    }

                    $window.location.href = "/Basket" + externalQueryString;
                }

                //Call another function we need to show after we have completed.
                if (functionToCallWhenFinished != null && typeof (functionToCallWhenFinished) == "function") {
                    functionToCallWhenFinished();
                }

                $scope.IsBusy = false;
                $scope.CanPay = true;

            }
        }, function errorCallback(result) {
            $scope.errorMessage = result.data;
            $scope.CanPay = true;
        });


    }

    function PopulateUpSells() {
        $scope.CanPay = false;
        var loginCheck = localStorage.getItem("loginCheckoutCheck");

        if (loginCheck != '1' && $scope.deliveryPagination != 1) {//&& $scope.modalSIDCheck != 1

            if ($scope.upsellParams == null || $scope.upsellParams == undefined) {
                var upsellParams = { BasketId: basketId, CountryID: CommonCode.CountryId, CurrencyID: CommonCode.CurrencyId, LanguageID: CommonCode.LanguageId, TakeNumber: 5 };//change this to dynamic valuesCountryID: CommonCode.CountryId, CurrencyID: CommonCode.CurrencyId, LanguageID: CommonCode.LanguageId
                $scope.upsellParams = upsellParams;
            }

            $http({
                method: 'POST',
                url: baseUrl + "Reservations/GetBasketUpsells",
                contentType: 'application/json',
                data: $scope.upsellParams,
            }).then(function (result) {
                if (result.data != null) {

                    var upsellResultset = result.data;

                    $scope.UpsellResultsLength = 0;

                    if (upsellResultset != null && upsellResultset.length != 0) {
                        $scope.UpsellResultsLength = upsellResultset.length;
                        $rootScope.basketPaginationCheck = 1;
                        $rootScope.checkoutPaginationCheck = 0;
                        $scope.UpsellResults = upsellResultset;

                        if ($scope.addedcardCheck == 1) {
                            $('#checkoutUpsellcontainer').hide();
                        }
                        $('#checkoutUpsellcontainer').show();
                        $('#checkoutLoginWrapper').hide();
                        $('#checkoutDeliveryWrap').hide();
                        $('#emptyShippingAddress').hide();
                        $scope.partialCheckTemplate = 0;
                    }
                    else {
                        $rootScope.basketPaginationCheck = 0;
                        var keepMeSignedIn = $cookies.get('keepMeSignedIn');
                        if (keepMeSignedIn != null && keepMeSignedIn != undefined) {
                            $('#checkoutLoginWrapper').hide();
                            if ($scope.addressDataCount != 0 || $scope.shippingAddressLength != 0) {
                                $('#checkoutDeliveryWrap').show();
                                $('#checkoutUpsellcontainer').hide();
                                $('#emptyShippingAddress').hide();
                            }
                            else {
                                $('#emptyShippingAddress').show();
                            }
                        }
                        else//assuming added all the upsell items and count is 0
                        {
                            $('#checkoutUpsellcontainer').hide();
                            $('#checkoutLoginWrapper').show();

                        }
                    }

                    PopulateBasketSummary(basketSummaryValidation);
                }
                else {
                    PopulateBasketSummary(basketSummaryValidation);
                    $scope.UpsellSecurely();
                }

                basketSummaryValidation = true;

                $scope.IsBusy = false;

            });
        }
        else {

            $('#checkoutUpsellcontainer').hide();
            localStorage.removeItem("loginCheckoutCheck");

            $scope.deliveryPagination = 0;

            $scope.paymentWrapDiv = false;
            $scope.billingDiv = false;

            if ($scope.signinData != null && $scope.signinData != undefined) {

                $('#checkoutLoginWrapper').hide();
                PopulateAddress(true);
            }
            else {
                $('#checkoutLoginWrapper').show();
            }

        }
    }

    $scope.UpsellSecurely = function () {
        var keepMeSignedIn = $cookies.get('keepMeSignedIn');

        if (keepMeSignedIn != null && keepMeSignedIn != undefined) {
            $('#checkoutLoginWrapper').hide();
            if ($scope.selectedLookupID != undefined || $scope.selectedLookupID != null) {
                $scope.paymentWrapDiv = true;
                PopulateCardDetails();

                $rootScope.IsPaymentArea = true;
                $rootScope.IsDeliveryArea = false;
            }
            else {
                $scope.paymentWrapDiv = false;
                PopulateAddress(true);

                $('#checkoutDeliveryWrap').show();

                $('#checkoutUpsellcontainer').hide();
                localStorage.removeItem("loginCheckoutCheck");

                $scope.deliveryPagination = 0;

                $rootScope.IsPaymentArea = false;
                $rootScope.IsDeliveryArea = true;
            }
        }
        else {
            $('#checkoutLoginWrapper').show();

        }

        $('#checkoutUpsellcontainer').hide();

        $rootScope.basketPaginationCheck = 0;
        $rootScope.checkoutPaginationCheck = 1;
        $rootScope.UpsellClick = 1;
    }

    function UpdateDeliveryOptions() {

        if (!$scope.CanPay) {
            return false;
        }

        $scope.CanPay = false;
        var deliveryParams = { BasketId: basketId, SelectedServiceID: $scope.selectedServiceID };//change this to dynamic values
        $http({
            method: 'POST',
            url: baseUrl + "Reservations/UpdateDeliveryOption",
            contentType: 'application/json',
            data: JSON.stringify(deliveryParams),
        }).then(function (result) {
            if (result.data !== undefined) {

                var resultset = result.data;

                if (resultset == 1) {

                    basketSummaryValidation = true;
                    PopulateDeliveryOptions(true);

                }

            }
            $scope.IsBusy = false;
        });
    }

    $scope.payByCard = function (payCheck) {
        if (!$scope.CanPay) {
            return;
        }

        $rootScope.IsPaymentArea = true;
        $rootScope.IsDeliveryArea = false;

        var login = $cookies.get('keepMeSignedIn');
        if (login != null && login != undefined) {

            login = JSON.parse(login);

            var customerDetails = getCustomerDetails(login);

            customerDetails.then(function (details) {
                if (details.data !== undefined) {
                    $scope.currentUser = details.data;
                    if ($scope.cardNumber == null) {
                        if (payCheck == null) {
                            $scope.AddCardWithBilling();
                        } else if (payCheck == 1) {
                            $scope.billingDiv = false;

                            var info = {
                                CustomerLastname: $scope.currentUser.lastname,
                                CustomerFirstname: $scope.currentUser.firstname,
                                CustomerEmail: $scope.currentUser.emailaddress,
                                CustomerPhoneNo: $scope.currentUser.phonenumber1,
                                CustomerCountry: $rootScope.selectedBillingAddress[0].Country,
                                CustomerTown: $rootScope.selectedBillingAddress[0].City,
                                CustomerAddress: $rootScope.selectedBillingAddress[0].StreetAddress1,
                                CustomerPostcode: $rootScope.selectedBillingAddress[0].Postcode,
                                BasketTotal: "0.00",
                                AddressId: $rootScope.selectedBillingAddress[0].AddressId,
                                pageId: '0'
                            };

                            $http({
                                method: 'POST',
                                url: "/checkout/CreateXml",
                                data: info,
                            }).then(function (result) {
                                if (result.data !== undefined) {
                                    $rootScope.paymentPage = $sce.trustAsResourceUrl(result.data);
                                }
                            });
                        }
                    }
                    else if (payCheck != 1) {
                        PopulateAddress();
                        if ($scope.addressDataCount != 0) {
                            $scope.paymentWrapDiv = true;
                            $scope.billingDiv = false;
                        }

                    }
                    else {
                        $scope.paymentWrapDiv = false;
                        $scope.billingDiv = false;

                        var info = {
                            CustomerLastname: details.data.lastname,
                            CustomerFirstname: details.data.firstname,
                            CustomerEmail: details.data.emailaddress,
                            CustomerPhoneNo: details.data.phonenumber1,
                            CustomerCountry: $rootScope.selectedBillingAddress[0].Country,
                            CustomerTown: $rootScope.selectedBillingAddress[0].City,
                            CustomerAddress: $rootScope.selectedBillingAddress[0].StreetAddress1,
                            CustomerPostcode: $rootScope.selectedBillingAddress[0].Postcode,
                            BasketTotal: "0.00",
                            AddressId: $rootScope.selectedBillingAddress[0].AddressId,
                            pageId: '0'
                        };
                        $http({
                            method: 'POST',
                            url: "/checkout/CreateXml",
                            data: info,
                        }).then(function (result) {
                            if (result.data !== undefined) {
                                $rootScope.paymentPage = $sce.trustAsResourceUrl(result.data);
                            }
                        });

                    }

                }
                $scope.IsBusy = false;

            });

        }

    }

    $scope.submitOrder = function () {
        var login = $cookies.get('keepMeSignedIn');
        if (login != null) {
            $scope.displaySpinner = true;
            login = JSON.parse(login);
            var customerDetails = getCustomerDetails(login);
            if ($rootScope.selectedCardData != null && $rootScope.selectedCardData != undefined) {
                customerDetails.then(function (details) {

                    if (details.data !== undefined) {

                        //FIX FOR BAD BASKET.
                        var deliveryAddress = $.extend(true, {}, $rootScope.selectedBillingAddress[0]);

                        if (deliveryAddress.StreetAddress1 == null || deliveryAddress.StreetAddress1 == "") {
                            deliveryAddress.StreetAddress1 = "EMPTY ADDRESS";
                        }

                        var info = {
                            CustomerData: details.data,
                            CustomerBillingAddress: deliveryAddress,
                            CustomerShippingAddress: $rootScope.selectedShippingAddress[0],
                            BasketTotal: $filter('number')($scope.toPayToday, 2),
                            AddressId: $rootScope.selectedBillingAddress[0].AddressId,
                            CardLast4Digits: $rootScope.selectedCardData,
                            BasketId: basketId,
                            ChaseCustomerRefNum: $rootScope.chaseCustomerRefNum
                        };

                        localStorage.setItem("tempBasketId", basketId);

                        $http({
                            method: 'POST',
                            url: "/checkout/SubmitOrder",
                            data: info,
                        }).then(function (result) {
                            if (result.data !== undefined) {

                                //empty if we not external
                                var externalQueryString = "";

                                if (isExternalVariable) {
                                    externalQueryString = "isexternal=true";
                                }


                                if (result.data == 'error') {

                                    externalQueryString = "?" + externalQueryString;

                                    $window.location.href = '/errors/generic' + externalQueryString;
                                } else {
                                    externalQueryString = "&" + externalQueryString;

                                    $window.location.href = '/checkout/orderstatustemp?result=' + result.data + externalQueryString;
                                }
                            }
                            $scope.IsBusy = false;

                        });
                    }
                });
            }
            else {
                $scope.CardErrorMessage = "Please Add a Card!"
            }
        }
        else {
            $window.location.href = "/Basket";
        }
    }

    $rootScope.MyAccountAddCardChooseAddress = function (addr) {
        $scope.billingDiv = false;
        $rootScope.PaymentContainer = true;

        var login = $cookies.get('keepMeSignedIn');
        if (login != null && login != undefined) {
            login = JSON.parse(login);
            if (login != null && login != undefined) {
                var userAccInfo = {
                    CustomerLookupId: login.CustomerLookupId, Email: login.Email
                };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/AccountDetails",
                    contentType: 'application/json',
                    data: JSON.stringify({
                        CustomerLookupId: login.CustomerLookupId, Email: login.Email
                    })
                }).then(function (result) {
                    $scope.DefaultAddress = $rootScope.allAddressList.filter(function (x) {
                        return x.addressid === addr;
                    })[0];

                    if (result.data !== undefined) {
                        var info = {
                            CustomerLastname: result.data.lastname,
                            CustomerFirstname: result.data.firstname,
                            CustomerEmail: result.data.emailaddress,
                            CustomerPhoneNo: result.data.phonenumber1,
                            CustomerCountry: $scope.DefaultAddress.Country,
                            CustomerTown: $scope.DefaultAddress.City,
                            CustomerAddress: $scope.DefaultAddress.StreetAddress1,
                            CustomerPostcode: $scope.DefaultAddress.Postcode,
                            BasketTotal: '0.00',
                            AddressId: $scope.DefaultAddress.AddressId,
                            pageId: '1'
                        };

                        $rootScope.selectedBillingAddress =
                        [{
                            'AddressId': $scope.DefaultAddress.AddressId,
                            'BuildingNumber': $scope.DefaultAddress.BuildingNumber,
                            'StreetAddress1': $scope.DefaultAddress.StreetAddress1,
                            'City': $scope.DefaultAddress.City,
                            'Postcode': $scope.DefaultAddress.Postcode
                        }];

                        $http({
                            method: 'POST',
                            url: "/checkout/CreateXml",
                            data: info,
                        }).then(function (result) {
                            if (result.data !== undefined) {
                                $scope.billingDiv = false;
                                $rootScope.MyAccountpaymentPage = $sce.trustAsResourceUrl(result.data);
                            }
                        });
                    }
                })
            }

        }
    }

    function getCustomerDetails(login) {
        if (login != null && login != undefined) {
            var userAccInfo = { CustomerLookupId: login.CustomerLookupId, Email: login.Email };
            return $http({
                method: 'POST',
                url: baseUrl + "Account/AccountDetails",
                contentType: 'application/json',
                data: JSON.stringify({ CustomerLookupId: login.CustomerLookupId, Email: login.Email })
            });
        }
    }

    $scope.payWithPayPal = function () {
        var login = $cookies.get('keepMeSignedIn');

        if (login != null && $rootScope.selectedBillingAddress != null && $rootScope.selectedShippingAddress != null) {
            login = JSON.parse(login);
            var customerDetails = getCustomerDetails(login);
            customerDetails.then(function (details) {
                if (details.data !== undefined) {
                    if ($rootScope.selectedBillingAddress.length != 0 && $rootScope.selectedShippingAddress.length != 0) {

                        /*$scope.info = {
                            CustomerData: details.data,
                            CustomerBillingAddress: $rootScope.selectedBillingAddress[0],
                            CustomerShippingAddress: $rootScope.selectedShippingAddress[0],
                            BasketTotal: $scope.reservationData[0].ToPayToday,
                            AddressId: $scope.billingAddressResults[0].AddressId,
                            CardLast4Digits: $rootScope.selectedCardData,
                            BasketId: basketId,
                            ChaseCustomerRefNum: $rootScope.chaseCustomerRefNum
                        };*/
                        $scope.customerFirstname = details.data.firstname;
                        $scope.payPalEnable = true;
                    }
                }
            });
        }
    }

    function PopulateCardDetails(selectedCard) {
        $scope.CanPay = false;
        var cardResults;
        var addressCardData = 0;
        var cardData;
        var finalData = 0;
        var billingID = 0;
        $scope.paymentData = [];

        var cardDetailsParams = { CustomerReference: signinData.CustomerLookupId, BasketId: basketId };

        $http({
            method: 'POST',
            url: baseUrl + "Reservations/GetCardDetails",
            contentType: 'application/json',
            data: JSON.stringify(cardDetailsParams),
        }).then(function (result) {
            if (result.data !== undefined) {

                var cardsResultset = result.data;
                if (cardsResultset.length > 0 && cardsResultset != null) {

                    if ($scope.selectedLookupID != null || $scope.selectedLookupID != undefined) {
                        if (selectedCard == null || selectedCard == undefined) {
                            cardResults = $filter('filter')(cardsResultset, { iBLookUP: $scope.selectedLookupID });
                        } else {
                            cardResults = $filter('filter')(cardsResultset, { Last4Digits: selectedCard });
                        }
                    }
                    else {

                        if ($scope.selectedCard != undefined || $scope.selectedCard != null) {
                            cardResults = $filter('filter')(cardsResultset, { Last4Digits: $scope.selectedCard, AddressId: $scope.selectedBillingAddID }, true);
                        }
                        else {
                            if ($scope.selectedBillingAddID != null || $scope.selectedBillingAddID != undefined) {
                                cardResults = $filter('filter')(cardsResultset, { AddressId: $scope.selectedBillingAddID.AddressId }, true);
                            }
                            else {
                                cardResults = $filter('filter')(cardsResultset, { AddressId: $scope.billingAddressId }, true);
                            }

                        }

                    }

                    $rootScope.billingCardResults = cardsResultset;


                    if (cardResults.length > 0) {
                        $scope.cardNumber = cardResults[0].Last4Digits;
                        $scope.TestcardNumber = { card: cardResults[0].Last4Digits };
                        $scope.ChaseCustomerRefNum = cardResults[0].ChaseCustomerReferenceNumber;
                        selectedCard = $scope.cardNumber;//get selected card =1 value
                        //$rootScope.selectedCardData = selectedCard;
                        $scope.cardLookupID = cardResults[0].iBLookUP;
                    }

                    if ($scope.cardNumber != null && $scope.cardNumber != undefined) {
                        $rootScope.selectedCardData = $scope.cardNumber;
                        $rootScope.chaseCustomerRefNum = $scope.ChaseCustomerRefNum;

                    }
                    else {
                        $scope.AddCardWithBilling();
                    }


                    if ($scope.selectedCard != undefined || $scope.selectedCard != null || $scope.selectedLookupID != null || $scope.selectedLookupID != undefined) {
                        PopulateAddress();
                    }

                    $scope.IsBusy = false;

                }

            }
        });
    }

    $scope.setPartialName = function (name, partialCheck, ppid) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        $rootScope.IsDeliveryArea = true;
        if (ppid != null && ppid != undefined) {
            $rootScope.PopulateQuickLookModel(ppid);
        }
    }

    $scope.AddCardWithBilling = function (customer) {
        $scope.paymentWrapDiv = false;
        $scope.billingDiv = true;
        $rootScope.IsPaymentArea = true;
        $rootScope.IsDeliveryArea = false;
    }

    $scope.openOffscreen = function (pMsgTitle, pMsgContent, pOkBtnTitle) {
        $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title(pMsgTitle)
            .textContent(pMsgContent)
            .ariaLabel('AccountAddress')
            .ok(pOkBtnTitle)
            .openFrom({
                top: -50,
                width: 200,
                height: 80
            })
            .closeTo({
                left: 1500
            })
        );
    };

    $scope.addAddress = function () {

        var signinData = $cookies.get("keepMeSignedIn");
        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            $scope.newAddressData.customerRef = signinData.CustomerLookupId;
            $scope.newAddressData.CustomerEmail = signinData.Email;
            $rootScope.partialName = "add_address";
            $rootScope.newAddressData = $scope.newAddressData;
        }
    };


    $scope.init();

}]);

app.directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }
]);

app.controller("AccountPaymentController", ['$scope', '$http', '$filter', '$compile', '$sanitize', '$rootScope', '$httpParamSerializer', '$window', '$sce', '$cookies', 'CommonCode', function ($scope, $http, $filter, $compile, $sanitize, $rootScope, $httpParamSerializer, $window, $sce, $cookies, CommonCode) {

    if (window.location.href.toLowerCase().indexOf("/checkout") == -1) {
        $scope.hid_MyaccountaddcardPartial = true;
        $scope.page = 3;
        $scope.paymentWrapDiv = false;
    }
    else {
        $rootScope.page = 2;
    }

    $scope.PaymentContainer = true;

    if (document.getElementById("checkoutTemplate") != null) {
        $scope.shippingAddressId = angular.element(document.getElementById("checkoutTemplate")).scope().shippingAddressId;
    }

    var signinData = $cookies.get("keepMeSignedIn");
    var basketId = localStorage.getItem("basketId");

    if (signinData != null) {
        signinData = JSON.parse(decodeURIComponent(signinData));
        if (signinData.Email != "" && signinData.CustomerLookupId != "") {
            $scope.signinData = signinData;
        }
    }

    $scope.deleteCard = function (iblookup, lastdigits) {
        if (iblookup != null || iblookup != undefined) {
            var cardDetailsParams =
                {
                    iBLookup: iblookup,
                    custEmail: $scope.signinData.Email,
                    custRef: $scope.signinData.CustomerLookupId,
                    last4digits: lastdigits
                };
            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountDeleteCard",
                contentType: 'application/json',
                data: cardDetailsParams
            }).then(function (result) {

                if (result.data !== undefined) {

                    if (result.data.MessageCode == 350) {
                        MyAccountPopulateCardDetails();
                        //$scope.openOffscreen(result.data.TitleMessage, result.data.MessageToDisplay, 'Close');
                    }
                }
            });
        }
    };

    $rootScope.CardError = function () {

        $scope.$apply(function () {
            $rootScope.ShowiFrame = false;
            $rootScope.IsCardError = true;
        });

    }

    $rootScope.ShowiFrame = false;
    $rootScope.IsCardError = false;

    $rootScope.MyAccountAddCardChooseAddress = function (addr) {

        var accountController = angular.element(document.getElementById("accountPaymentController")).scope();

        var login = $cookies.get('keepMeSignedIn');
        if (login != null && login != undefined) {
            login = JSON.parse(login);
            if (login != null && login != undefined) {
                var userAccInfo = {
                    CustomerLookupId: login.CustomerLookupId, Email: login.Email
                };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/AccountDetails",
                    contentType: 'application/json',
                    data: JSON.stringify({
                        CustomerLookupId: login.CustomerLookupId, Email: login.Email
                    })
                }).then(function (result) {

                    accountController.DefaultAddress = accountController.allAddressList.filter(function (x) {
                        return x.AddressId === addr;
                    })[0];

                    if (result.data !== undefined) {
                        var info = {
                            CustomerLastname: result.data.lastname,
                            CustomerFirstname: result.data.firstname,
                            CustomerEmail: result.data.emailaddress,
                            CustomerPhoneNo: result.data.phonenumber1,
                            CustomerCountry: accountController.DefaultAddress.Country,
                            CustomerTown: accountController.DefaultAddress.Town,
                            CustomerAddress: accountController.DefaultAddress.StreetAddress1,
                            CustomerPostcode: accountController.DefaultAddress.Postcode,
                            BasketTotal: '0.00',
                            AddressId: accountController.DefaultAddress.AddressId,
                            pageId: '1'
                        };

                        $rootScope.selectedBillingAddress =
                        [{
                            'AddressId': accountController.DefaultAddress.AddressId,
                            'BuildingNumber': accountController.DefaultAddress.BuildingNumber,
                            'StreetAddress1': accountController.DefaultAddress.StreetAddress1,
                            'City': accountController.DefaultAddress.Town,
                            'Postcode': accountController.DefaultAddress.Postcode
                        }];

                        $http({
                            method: 'POST',
                            url: "/checkout/CreateXml",
                            data: info,
                        }).then(function (result) {
                            if (result.data !== undefined) {
                                accountController.billingDiv = false;

                                $rootScope.ShowiFrame = true;
                                $rootScope.IsCardError = false;

                                $rootScope.MyAccountpaymentPage = $sce.trustAsResourceUrl(result.data);
                            }
                        });
                    }
                })
            }

        }
    }

    $scope.MyAccoutDefaultCard = function (iblookup, last4digits) {

        if (iblookup != null || iblookup != undefined) {
            var cardDetailsParams = { iBLookup: iblookup, custEmail: $scope.signinData.Email, custRef: $scope.signinData.CustomerLookupId, last4digits: last4digits };//change this to dynamic values
            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountUpdateDefaultcard",
                contentType: 'application/json',
                data: cardDetailsParams,//change to model
            }).then(function (result) {
                if (result.data !== undefined) {

                    if (result.data.MessageCode == 300) {
                        MyAccountPopulateCardDetails();
                    }
                }
            });
        }
    }


    function MyAccountPopulateCardDetails() {

        var cardResults;
        var addressCardData = 0;
        var cardData;
        var finalData = 0;
        var billingID = 0;
        $scope.paymentData = [];
        var cardDetailsParams = { CustomerReference: signinData.CustomerLookupId, BasketId: basketId };//change this to dynamic values
        $http({
            method: 'POST',
            url: baseUrl + "Reservations/GetCardDetails",
            contentType: 'application/json',
            data: JSON.stringify(cardDetailsParams),//change to model
        }).then(function (result) {
            if (result.data !== undefined) {
                var cardsResultset = result.data;

                if (cardsResultset.length > 0 && cardsResultset != null) {

                    if ($scope.selectedLookupID != null || $scope.selectedLookupID != undefined) {
                        cardResults = $filter('filter')(cardsResultset, {
                            iBLookUP: $scope.selectedLookupID != null || $scope.selectedLookupID != undefined ? $scope.selectedLookupID : 0
                        });
                    }
                    else {
                        if ($scope.selectedCard != undefined || $scope.selectedCard != null) {
                            cardResults = $filter('filter')(cardsResultset, {
                                Last4Digits: $scope.selectedCard != undefined || $scope.selectedCard != null ? $scope.selectedCard : 0
                            });
                        }
                        else {
                            cardResults = $filter('filter')(cardsResultset, {
                                AddressId: $scope.selectedBillingAddID != null || $scope.selectedBillingAddID != undefined ? $scope.selectedBillingAddID.AddressId : $scope.billingAddressId
                            });
                        }
                    }


                    $scope.billingCardResults = cardsResultset;

                    if (cardResults.length > 0) {
                        $scope.cardNumber = cardResults[0].Last4Digits;
                        $scope.TestcardNumber = { card: cardResults[0].Last4Digits };
                        $scope.ChaseCustomerRefNum = cardResults[0].ChaseCustomerReferenceNumber;
                        selectedCard = $scope.cardNumber;//get selected card =1 value
                        $scope.cardLookupID = cardResults[0].iBLookUP;
                    }

                    if ($scope.cardNumber != null && $scope.cardNumber != undefined) {
                        $rootScope.selectedCardData = $scope.cardNumber;
                        $rootScope.chaseCustomerRefNum = $scope.chaseCustomerRefNum;
                    }
                    else {
                        $scope.AddCardWithBilling();
                    }
                }
                else {
                    $scope.billingCardResults = null;
                }

            }
        });

    }

    function getCustomerDetails(login) {
        if (login != null && login != undefined) {
            var userAccInfo = { CustomerLookupId: login.CustomerLookupId, Email: login.Email };
            return $http({
                method: 'POST',
                url: baseUrl + "Account/AccountDetails",
                contentType: 'application/json',
                data: JSON.stringify({ CustomerLookupId: login.CustomerLookupId, Email: login.Email })
            });
        }
    }

    $scope.setPartialName = function (name, partialCheck, ppid) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;

        if (ppid != null && ppid != undefined) {
            $rootScope.PopulateQuickLookModel(ppid);
        }
    }


    $scope.MyAccountPayByCard = function () {
        $scope.hid_MyaccountaddcardPartial = false;
        $scope.PaymentContainer = false;
        $scope.billingDiv = true;

        var login = $cookies.get('keepMeSignedIn');
        if (login != null && login != undefined) {
            login = JSON.parse(login);
            var customerDetails = getCustomerDetails(login);
            customerDetails.then(function (details) {
                if (details.data !== undefined) {
                    $rootScope.billingAddressLength = $scope.allAddressList.length;
                    $rootScope.allAddressListAccount = $scope.allAddressList;

                    if ($rootScope.billingAddressLength > 0) {

                        $scope.DefaultAddress = $scope.allAddressList[0];
                        if ($scope.DefaultAddress.length == 0) {
                            $scope.DefaultAddress = $scope.allAddressList[0];
                        };

                        $rootScope.selectedBillingAddress = [];
                        $rootScope.selectedBillingAddress.push($scope.DefaultAddress);

                    }

                }
            });

        }
        else {

        }

    }

    $scope.MyAccountPopulateAddress = function () {

        $scope.addresse = [];
        $scope.allAddressList = [];
        $scope.CusRef = "";
        $scope.CusEmail = "";

        $scope.newAddressData = {
            customerRef: "",
            CustomerEmail: "",
            addressid: "",
            customerid: "",
            companyname: "",
            buildingname: "",
            buildingnumber: "",
            streetaddress1: "",
            streetaddress2: "",
            streetaddress3: "",
            streetaddress4: "",
            streetaddress5: "",
            street: "",
            town: "",
            city: "",
            county: "",
            country: "",
            postcode: "",
            createddate: "",
            createdby: "",
            postcodeanywhereid: "",
            countryid: "",
            active: "",
            blacklisted: "",
            IsDefaultBilling: "",
            IsDefaultDevlivery: "",
            IsLinkedToCard: ""
        };

        var userAccInfo = { CustomerLookupId: $scope.signinData.CustomerLookupId, Email: $scope.signinData.Email };
        $http({
            method: 'POST',
            url: baseUrl + "Account/AccountAddresses",
            contentType: 'application/json',
            data: userAccInfo
        }).then(function (result) {
            $scope.MyAccountAddressCount = 0;
            var iNum = 0;

            if (result.data !== undefined) {
                if (result.data.length > 0) {
                    $scope.MyAccountAddressCount = result.data.length;
                    for (iNum = 0; iNum < $scope.MyAccountAddressCount; iNum++) {
                        var addressData = {
                            addressid: result.data[iNum].addressid,
                            customerid: result.data[iNum].customerid,
                            companyname: result.data[iNum].companyname,
                            buildingname: result.data[iNum].buildingname,
                            buildingnumber: result.data[iNum].buildingnumber,
                            streetaddress1: result.data[iNum].streetaddress1,
                            streetaddress2: result.data[iNum].streetaddress2,
                            streetaddress3: result.data[iNum].streetaddress3,
                            streetaddress4: result.data[iNum].streetaddress4,
                            streetaddress5: result.data[iNum].streetaddress5,
                            street: result.data[iNum].street,
                            town: result.data[iNum].town,
                            city: result.data[iNum].city,
                            county: result.data[iNum].county,
                            country: result.data[iNum].country,
                            postcode: result.data[iNum].postcode,
                            createddate: result.data[iNum].createddate,
                            createdby: result.data[iNum].createdby,
                            postcodeanywhereid: result.data[iNum].postcodeanywhereid,
                            countryid: result.data[iNum].countryid,
                            active: result.data[iNum].active,
                            blacklisted: result.data[iNum].blacklisted,
                            IsDefaultBilling: result.data[iNum].IsDefaultBilling,
                            IsDefaultDevlivery: result.data[iNum].IsDefaultDevlivery,
                            IsLinkedToCard: result.data[iNum].IsLinkedToCard

                        };
                        var convertedAddressData = {
                            "Active": addressData.active,
                            "AddressId": addressData.addressid,
                            "BuildingName": addressData.buildingname,
                            "BuildingNumber": addressData.buildingnumber,
                            "City": addressData.city,
                            "CompanyName": addressData.companyname,
                            "CountryId": addressData.countryid,
                            "Country": addressData.country,
                            "County": addressData.county,
                            "Postcode": addressData.postcode,
                            "StreetAddress1": addressData.streetaddress1,
                            "StreetAddress2": addressData.streetaddress2,
                            "StreetAddress3": addressData.streetaddress3,
                            "StreetAddress4": addressData.streetaddress4,
                            "StreetAddress5": addressData.streetaddress5,
                            "Town": addressData.town
                        }

                        if (result.data[iNum].IsLinkedToCard == 'y') {
                            var selectedBillingAddress = [];

                            selectedBillingAddress.push(convertedAddressData);

                            $rootScope.selectedBillingAddress = selectedBillingAddress;
                        }

                        $scope.allAddressList.push(convertedAddressData);

                    }
                }
            }

            MyAccountPopulateCardDetails();
        });
    }


    $scope.MyAccountPopulateAddress();

}]);

app.controller("AddressController", ['$scope', '$http', '$filter', '$compile', '$sanitize', '$rootScope', '$httpParamSerializer', '$window', '$sce', '$cookies', 'CommonCode', function ($scope, $http, $filter, $compile, $sanitize, $rootScope, $httpParamSerializer, $window, $sce, $cookies, CommonCode) {

    if (document.getElementById("checkoutTemplate") != null) {
        $scope.addressList = angular.element(document.getElementById("checkoutTemplate")).scope().addressList;
    }

    var signinData = $cookies.get("keepMeSignedIn");
    var basketId = localStorage.getItem("basketId");

    if (signinData != null) {
        signinData = JSON.parse(decodeURIComponent(signinData));
        if (signinData.Email != "" && signinData.CustomerLookupId != "") {
            $scope.signinData = signinData;
        }
    }

    $scope.updateDefaultShipping = function (addressId) {
        if (addressId != "") {
            var iAddrId = parseInt(addressId);
            if (isNaN(iAddrId)) { iAddrId = 0; }
            var userAddrInfo = { AddressId: iAddrId, custEmail: $scope.signinData.Email, custRef: $scope.signinData.CustomerLookupId };
            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountUpdateAddress",
                contentType: 'application/json',
                data: userAddrInfo
            }).then(function (result) {

                if (result.data !== undefined) {

                    if (result.data.MessageCode != 200) {
                        $scope.openOffscreen(result.data.TitleMessage, result.data.MessageToDisplay, 'Close');
                    }
                    else {

                        $window.location.reload();

                    }
                }
            });
        }
    };

    $scope.deleteAddress = function (pAddressId) {

        if (pAddressId != "") {
            var iAddrId = parseInt(pAddressId);
            if (isNaN(iAddrId)) { iAddrId = 0; }
            var addressInfo = { AddressId: iAddrId };
            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountDeleteAddress",
                contentType: 'application/json',
                data: addressInfo
            }).then(function (result) {

                if (result.data !== undefined) {

                    if (result.data.MessageCode != 200) {
                        $scope.openOffscreen(result.data.TitleMessage, result.data.MessageToDisplay, 'Close');
                    }
                    else {
                        $scope.MyAccountPopulateAddress();
                        //$window.location.reload();

                    }
                }
            });
        }
    };

    $scope.setPartialName = function (name, partialCheck, ppid) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;

        if (ppid != null && ppid != undefined) {
            $rootScope.PopulateQuickLookModel(ppid);
        }
    }

    $scope.MyAccountPopulateAddress = function () {

        $scope.addresses = [];
        $scope.CusRef = "";
        $scope.CusEmail = "";

        $scope.newAddressData = {
            customerRef: "",
            CustomerEmail: "",
            addressid: "",
            customerid: "",
            companyname: "",
            buildingname: "",
            buildingnumber: "",
            streetaddress1: "",
            streetaddress2: "",
            streetaddress3: "",
            streetaddress4: "",
            streetaddress5: "",
            street: "",
            town: "",
            city: "",
            county: "",
            country: "",
            postcode: "",
            createddate: "",
            createdby: "",
            postcodeanywhereid: "",
            countryid: "",
            active: "",
            blacklisted: "",
            IsDefaultBilling: "",
            IsDefaultDevlivery: "",
            IsLinkedToCard: ""
        };

        var userAccInfo = { CustomerLookupId: $scope.signinData.CustomerLookupId, Email: $scope.signinData.Email };
        $http({
            method: 'POST',
            url: baseUrl + "Account/AccountAddresses",
            contentType: 'application/json',
            data: userAccInfo
        }).then(function (result) {
            $scope.MyAccountAddressCount = 0;
            var iNum = 0;

            if (result.data !== undefined) {
                if (result.data.length > 0) {
                    $scope.MyAccountAddressCount = result.data.length;
                    for (iNum = 0; iNum < $scope.MyAccountAddressCount; iNum++) {
                        var addressData = {
                            AddressId: result.data[iNum].addressid,
                            CustomerId: result.data[iNum].customerid,
                            CompanyName: result.data[iNum].companyname,
                            BuildingName: result.data[iNum].buildingname,
                            BuildingNumber: result.data[iNum].buildingnumber,
                            StreetAddress1: result.data[iNum].streetaddress1,
                            StreetAddress2: result.data[iNum].streetaddress2,
                            StreetAddress3: result.data[iNum].streetaddress3,
                            StreetAddress4: result.data[iNum].streetaddress4,
                            StreetAddress5: result.data[iNum].streetaddress5,
                            Street: result.data[iNum].street,
                            Town: result.data[iNum].town,
                            City: result.data[iNum].city,
                            County: result.data[iNum].county,
                            Country: result.data[iNum].country,
                            Postcode: result.data[iNum].postcode,
                            CreatedDate: result.data[iNum].createddate,
                            CreatedBy: result.data[iNum].createdby,
                            Postcodeanywhereid: result.data[iNum].postcodeanywhereid,
                            CountryId: result.data[iNum].countryid,
                            Active: result.data[iNum].active,
                            Blacklisted: result.data[iNum].blacklisted,
                            IsDefaultBilling: result.data[iNum].IsDefaultBilling,
                            IsDefaultDevlivery: result.data[iNum].IsDefaultDevlivery,
                            IsLinkedToCard: result.data[iNum].IsLinkedToCard

                        };

                        if (result.data[iNum].IsLinkedToCard == 'y') {
                            $rootScope.selectedBillingAddress = addressData;
                        }

                        $scope.addresses.push(addressData);

                    }
                }
            }
        });
    }

    $scope.MyAccountPopulateAddress();
}]);
