﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class CreditAmountRecordModel
    {
        public decimal CreditAmount { get; set; }
        public int MinSpend { get; set; }
        public decimal MinSpendAmount { get; set; }
        public string CreditAmountWithCurrency { get; set; }
        public string MinSpendAmountWithCurrency { get; set; }
    }
}
