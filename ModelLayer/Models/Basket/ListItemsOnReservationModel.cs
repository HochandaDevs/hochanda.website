﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class ListItemOnReservationModel
	{
		public int ReservationItemId { get; set; }
		public int ParentProductId { get; set; }
		public int VariationId { get; set; }
		public decimal UnitPrice { get; set; }
		public int Quantity { get; set; }
		public int FlexiBuyItem { get; set; }
		public decimal? ItemTotal { get; set; }
		public string TVDescription { get; set; }
		public string VariationName { get; set; }
		public string VariationProductSKU { get; set; }
		public string ImageName { get; set; }
		public int FlexiBuy { get; set; }
		public int FlexiBuyInstallments { get; set; }
		public int ServiceId { get; set; }
		public int ServiceIdDefault { get; set; }
		public int PandPRuleApplied { get; set; }
		public int FreeItem { get; set; }
		public int PickNMixItem { get; set; }
		public int AuctionItem { get; set; }
		public int PendingStatus { get; set; }
	}
}
