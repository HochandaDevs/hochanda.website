﻿using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class CurrentItemOnAirWithAuctionModel
    {
        public bool IsAuctionHour { get; set; }

        public int? AuctionStatus { get; set; }

        public int? AuctionQuantity { get; set; }

        public decimal? AuctionStartPrice { get; set; }
        public string AuctionStartPriceString { get; set; }

        public decimal? AuctionNowPrice { get; set; }
        public string AuctionNowPriceString { get; set; }

        public int ParentProductId { get; set; }

        public int? CurrencyId { get; set; }

        public string CultureCode { get; set; }

		public ProductInformationModel ProductDetails { get; set; }
	}
}
