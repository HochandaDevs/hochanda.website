﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.ParametersModel
{
    public class paramsDeliveryOptions
    {
		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse
		{
			get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}

		public int CountryID { get; set; }
        public int CurrencyID { get; set; }
    }
}
