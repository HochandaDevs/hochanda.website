﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class AccountDetailsRequestModel
    {
        public string CustomerLookupId { get; set; } //feature 123
        public string Email { get; set; }
    }
}
