/// <autosync enabled="true" />
/// <reference path="../../../scripts/angular.min.js" />
/// <reference path="../../../scripts/angular-animate.min.js" />
/// <reference path="../../../scripts/angular-aria.min.js" />
/// <reference path="../../../scripts/angular-cookies.min.js" />
/// <reference path="../../../scripts/angular-daterangepicker.js" />
/// <reference path="../../../scripts/angular-loader.min.js" />
/// <reference path="../../../scripts/angular-material.min.js" />
/// <reference path="../../../scripts/angular-message-format.min.js" />
/// <reference path="../../../scripts/angular-messages.min.js" />
/// <reference path="../../../scripts/angular-mocks.js" />
/// <reference path="../../../scripts/angular-parse-ext.min.js" />
/// <reference path="../../../scripts/angular-recaptcha.min.js" />
/// <reference path="../../../scripts/angular-resource.min.js" />
/// <reference path="../../../scripts/angular-route.min.js" />
/// <reference path="../../../scripts/angular-sanitize.min.js" />
/// <reference path="../../../scripts/angular-touch.min.js" />
/// <reference path="../../../scripts/daterangepicker.js" />
/// <reference path="../../../scripts/internal/account.js" />
/// <reference path="../../../scripts/internal/accountnavmenu.js" />
/// <reference path="../../../scripts/internal/addvoucher.js" />
/// <reference path="../../../scripts/internal/basket.js" />
/// <reference path="../../../scripts/internal/brands.js" />
/// <reference path="../../../scripts/internal/checkout.js" />
/// <reference path="../../../scripts/internal/favourite.js" />
/// <reference path="../../../scripts/internal/home.js" />
/// <reference path="../../../scripts/internal/layout.js" />
/// <reference path="../../../scripts/internal/login.js" />
/// <reference path="../../../scripts/internal/lookuppostcode.js" />
/// <reference path="../../../scripts/internal/menuside.js" />
/// <reference path="../../../scripts/internal/orderstatus.js" />
/// <reference path="../../../scripts/internal/pagebuilder.js" />
/// <reference path="../../../scripts/internal/paypalcheckout.js" />
/// <reference path="../../../scripts/internal/product.js" />
/// <reference path="../../../scripts/internal/productlisting.js" />
/// <reference path="../../../scripts/internal/productquicklook.js" />
/// <reference path="../../../scripts/internal/referafriend.js" />
/// <reference path="../../../scripts/internal/register.js" />
/// <reference path="../../../scripts/internal/resetpassword.js" />
/// <reference path="../../../scripts/internal/reviews.js" />
/// <reference path="../../../scripts/internal/search.js" />
/// <reference path="../../../scripts/internal/testmodal.js" />
/// <reference path="../../../scripts/internal/tvschedule.js" />
/// <reference path="../../../scripts/jquery-3.3.1.min.js" />
/// <reference path="../../../scripts/jq-ui.js" />
/// <reference path="../../../scripts/moment.min.js" />
/// <reference path="../local/frontend.js" />
/// <reference path="addtohomescreen.min.js" />
/// <reference path="ai.0.15.0-build58334.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="owl.carousel.min.js" />
/// <reference path="photoswipe.min.js" />
/// <reference path="photoswipe-ui-default.min.js" />
/// <reference path="respond.js" />
