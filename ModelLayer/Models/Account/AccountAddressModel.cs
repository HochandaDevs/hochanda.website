﻿using ModelLayer.Interfaces;
using ModelLayer.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class AccountAddressModel: IAccountAddressModel,IModel
    {
        public string customerRef { get; set; }
        public string CustomerEmail { get; set; }
        public int addressid { get; set; }
        public int customerid { get; set; }
        public string companyname { get; set; }
        public string buildingname { get; set; }
        public string buildingnumber { get; set; }
        public string streetaddress1 { get; set; }
        public string streetaddress2 { get; set; }
        public string streetaddress3 { get; set; }
        public string streetaddress4 { get; set; }
        public string streetaddress5 { get; set; }
        public string street { get; set; }
        public string town { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string country { get; set; }
        public string postcode { get; set; }
        public DateTime createddate { get; set; }
        public int createdby { get; set; }
        public string postcodeanywhereid { get; set; }
        public int countryid { get; set; }
        public int active { get; set; }
        public int blacklisted { get; set; }
        public string IsDefaultBilling { get; set; }
        public string IsDefaultDevlivery { get; set; }
        public string IsLinkedToCard { get; set; }
    }

    public class AddressModel
    {
        public int AddressId { get; set; }
        public string custEmail { get; set; }
        public Guid custRef { get; set; }
    }
}
