﻿using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class BrandModel
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandImgURL { get; set; }
        public bool IsBrandAmbassador { get; set; }
        public int? BrandAmbassadorId { get; set; }

        public int ProductCountForBrand { get; set; }
        public string BrandLinkURL { get; set; }
        public string BrandDescription { get; set; }
        public string BrandBackgroundImage { get; set; }
        public string BrandSquareLogoImage { get; set; }
        public string BrandLogoSize { get; set; }
        public string BrandHeaderTheme { get; set; }
        public int LanguageId { get; set; }
		public string BrandTitle { get; set; }
	}

    public class BrandFilterModel
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandImgURL { get; set; }
        public bool IsBrandAmbassador { get; set; }
        public int? BrandAmbassadorId { get; set; }
        public int ProductCountForBrand { get; set; }
    }

   
}
