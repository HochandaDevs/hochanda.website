﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Customer
{
	public class RegisterModel
	{
		public string Title { get; set; }
		public string FirstName { get; set; }
		public string Surname { get; set; }
		public int DayOfBirth { get; set; }
		public int MonthOfBirth { get; set; }
		public string PhoneNumber1 { get; set; }
		public string PhoneNumber2 { get; set; }        
		public string EmailAddress { get; set; }
		public string ConfirmEmailAddress { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
		public bool ReceiveEmail { get; set; }
		public bool ReceivePost { get; set; }
		public bool ReceiveThirdPartyEmails { get; set; }
        public bool ReceiveSms { get; set; }
        public bool Anonymous { get; set; }
        public int Phone1Type { get; set; }
        public int Phone2Type { get; set; }
        public string SignedInEmail { get; set; }
        public Guid CustomerLookUpId { get; set; }
		public int? CampaignId { get; set; }
	}
}
