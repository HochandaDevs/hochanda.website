﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetSearchInfoModel
    {
		public GetSearchInfoModel()
		{

		}

        public int SearchId { get; set; }
        public string SearchName { get; set; }
        public string SearchCategory { get; set; }
        public string SearchThumbnail { get; set; }
        public string SearchProductSKU { get; set; }
        public int SearchBrandId { get; set; }
        public int IsPickAndMix { get; set; }
    }
}
