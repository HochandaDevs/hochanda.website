﻿app.controller("tvScheduleCtlr", ['$scope', '$http', '$filter', '$compile', '$timeout', '$rootScope', 'CommonCode', function ($scope, $http, $filter, $compile, $timeout, $rootScope, CommonCode) {
    $rootScope.GetMetadata(false, false, false, 'tv schedule', 0);
    CommonCode.getBaseCookieSettings();
    $scope.date = new Date();
    $scope.ShowTVSchedule = false;
    $scope.minDate = new Date();

    var CustomerLookupId = "0";
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    $scope.IsLoggedIn = 0;

    $scope.channelId = CommonCode.ChannelId;

    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        CustomerLookupId = keepMeSignedIn.CustomerLookupId;
        $scope.IsLoggedIn = 1;
    }

    $scope.TVScheduleData = null;

    $scope.IsBusy = true;

    //initalise as null but will be dates for the picker.
    $scope.MinimumDate = null;
    $scope.MaximumDate = null;

    $scope.GetAllData = function () {

        $http({
            method: 'GET',
            url: baseUrl + "TVSchedule/GetTVScheduleForChannel?channelId=" + $scope.channelId,
            contentType: 'application/json',
        }).then(function (result) {
            $scope.TVScheduleData = result.data;

            var uniqueDates = [];

            $.each($scope.TVScheduleData, function (index, value) {

                var dateToUse;

                if (isBrowserSafari) {
                    dateToUse = new Date(value.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/'));
                }
                else if (detectIE()) {
                    var newDateString = value.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/');
                    dateToUse = new Date(newDateString.substring(0, newDateString.length - 3));
                }
                else {
                    dateToUse = new Date(value.ShowDateTimeUTCFormat);
                }

                //If we are not already in there then add us.
                if ($.inArray(dateToUse, uniqueDates) == -1) {
                    //Safari Dates do not accept yyyy-MM-dd. So convert to yyyy/MM/dd
                    uniqueDates.push(dateToUse);
                }
            });

            $scope.MinimumDate = new Date(Math.min.apply(null, uniqueDates));
            $scope.MaximumDate = new Date(Math.max.apply(null, uniqueDates));

            $scope.date = new Date();

            $scope.displaySchedule();
        });
    }

    $scope.GetAllData();

    $scope.GetTVScheduleHour = function (indexValue) {
        var hour = indexValue.toString();

        if (hour.length == 1) {
            hour = "0" + hour;
        }

        return hour;
    }

    $scope.displaySchedule = function () {
        $scope.IsBusy = true;

        //We have the date in the format we need now add the hour of the show to see if this is correct.
        //This takes into account timezones etc.

        var dateToFind = new Date($scope.date);

        if (dateToFind.getDate() <= $scope.MinimumDate.getDate() && dateToFind.getMonth() == $scope.MinimumDate.getMonth()) {
            $scope.prevDate = false;
            $scope.nextDate = true;
        }
        else if (dateToFind.getDate() >= $scope.MaximumDate.getDate() && dateToFind.getMonth() == $scope.MaximumDate.getMonth()) {
            $scope.prevDate = true;
            $scope.nextDate = false;
        }
        else {
            $scope.prevDate = true;
            $scope.nextDate = true;
        }

        //Reset to 12am
        dateToFind.setHours(0, 0, 0, 0);

        var startTime = dateToFind.getTime();

        dateToFind.setHours(dateToFind.getHours() + 24);
        dateToFind.setMinutes(-1);

        var endTime = dateToFind.getTime();

        //Clones the array as grep will remove any we dont want.
        var allData = $scope.TVScheduleData.slice();

        allData = $.grep(allData, function (dataValue) {

            var dateOfShow;
            if (isBrowserSafari) {
                dateOfShow = new Date(dataValue.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/'));
            }
            else if (detectIE()) {
                var newDateString = dataValue.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/');
                dateOfShow = new Date(newDateString.substring(0, newDateString.length - 3));
            }
            else {
                dateOfShow = new Date(dataValue.ShowDateTimeUTCFormat);
            }

            var keep = dateOfShow.getTime() >= startTime && dateOfShow.getTime() < endTime;

            return keep;
        });

        $scope.TvScheduleSingleDayList = allData;

        $scope.ShowTVSchedule = true;

        $scope.TvScheduleSingleDayList.forEach(function (value, key) {
            $scope.TvScheduleSingleDayList[key].ProductsShown = false;
        });

        setTimeout(function () {
            $scope.IsBusy = false;
            $scope.$applyAsync();
        }, 250);

    }

    $scope.displayScheduleOld = function (fromScheduleForTheDay) {
        $scope.notUk = false;
        $http({
            method: 'GET',
            url: baseUrl + "TVSchedule/GetTVScheduleForChannelAndDate?channelId=" + $scope.channelId + "&date=" + $filter('date')($scope.date, "yyyy/MM/dd") + "&timeDiffInHours=" + $scope.timeDiff,
            contentType: 'application/json',
            //data: 1
        }).then(function (result) {
            if (result.data != null) {

                var TvScheduleSingleDayList = result.data;
                if ($scope.timeDiff != 0) {
                    $scope.notUk = true;
                }
                $scope.TvScheduleSingleDayList = TvScheduleSingleDayList;
                if (fromScheduleForTheDay) {
                    $scope.ShowTVSchedule = true;
                }

                $scope.TvScheduleSingleDayList.forEach(function (value, key) {
                    $scope.TvScheduleSingleDayList[key].ProductsShown = false;
                });
            }
        });
    }

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);

            //$rootScope.quicklookProductID = ppid;
        }
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }

    $rootScope.$on('selectedDate', function (event, value) {
        $scope.date = new Date(value);
        $scope.displaySchedule();
    });


    $scope.timeDiff = (new Date().getTimezoneOffset()) / 60;

    $scope.ShowTvSchedulesFortheDay = function (plusOrMinus) {
        $scope.date = new Date($scope.date.setDate($scope.date.getDate() + plusOrMinus));

        $scope.displaySchedule();
    }

    $scope.TVScheduleProducts = [];

    $scope.GetProductCarousel = function (tvScheduleId, ind) {

        var product = $scope.TVScheduleProducts.find(function (element) {
            return element.TVScheduleId == tvScheduleId;
        });

        var ShowProducts = true;
        if (product != null && product != undefined) {

            ShowProducts = !ShowProducts;
            //return;
        }
        else {
            ShowProducts = true;
        }
        $scope.TvScheduleSingleDayList.forEach(function (value, key) {
            if (key != ind) {
                $scope.TvScheduleSingleDayList[key].ProductsShown = false;
            }
        });
        $scope.TvScheduleSingleDayList[ind].ProductsShown = !$scope.TvScheduleSingleDayList[ind].ProductsShown;

        if (ShowProducts) {
            $http({
                method: 'GET',
                url: baseUrl + "ProductsInShow/GetProductsInACertainShow?tvScheduleId=" + $scope.TvScheduleSingleDayList[ind].TVScheduleId + "&languageId=" + CommonCode.LanguageId + "&currencyId=" + CommonCode.LanguageId + "&countryId=" + CommonCode.LanguageId,
                contentType: 'application/json',
            }).then(function (result1) {

                $scope.products = result1.data;
                var checkFavTrue;
                var favourites = JSON.parse(localStorage.getItem("userFavourites"));
                if (favourites != null && $scope.products != null) {
                    $scope.favouritesProducts = favourites.products;
                    for (var i = 0; i < $scope.products.length; i++) {
                        if ($scope.checkfav($scope.products[i].ParentProductId)) {
                            $scope.products[i].IsFav = 1;
                        }
                        else {
                            $scope.products[i].IsFav = 0;
                        }
                    }
                }

                $scope.TVScheduleProducts.push({ TVScheduleId: $scope.TvScheduleSingleDayList[ind].TVScheduleId, Data: $scope.products, ShowProducts: ShowProducts });

                $scope.LoadCarousel($scope.TvScheduleSingleDayList[ind].TVScheduleId);
            });
        }

    }

    $scope.checkfav = function (parentProdId) {
        for (var j = 0; j < $scope.favouritesProducts.length; j++) {
            if ($scope.favouritesProducts[j].ParentProductId == parentProdId) {
                return 1;
            }
        }
        return 0;
    }

    $scope.LoadCarousel = function (tvScheduleId) {
        var product = $scope.TVScheduleProducts.find(function (element) {
            return element.TVScheduleId == tvScheduleId && element.ShowProducts;
        });

        $http({
            method: "GET",
            url: '/Product/LoadCarousel?productScopeName=' + 'TVScheduleProducts[' + $scope.TVScheduleProducts.indexOf(product) + '].Data',
            contentType: "text/plain",
            dataType: "html"
        }).then(function (data) {
            var partial = angular.element(document.getElementById(tvScheduleId));
            partial.empty();
            var htmlelement = angular.element(data.data);
            var compileAppendedArticleHTML = $compile(htmlelement[0]);
            var element = compileAppendedArticleHTML($scope);

            partial.append(element[0]);

        });
    }

    $scope.ShowHideProducts = function () {
        $scope.ShowProductsInLive = !$scope.ShowProductsInLive;
    }

}]);



app.controller("CinemaCtrl", ['$scope', '$http', '$filter', '$compile', '$rootScope', 'CommonCode', '$window', "$sce", function ($scope, $http, $filter, $compile, $rootScope, CommonCode, $window, $sce) {
    //$scope.hidcarousel = true;
    $rootScope.GetMetadata(false, false, false, 'rewind', 0);
    var CustomerLookupId = "0";
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    CommonCode.getBaseCookieSettings();
    $scope.simpleStreamRewindDataStream = CommonCode.SimpleStreamRewindDataStream;
    $scope.IsLoggedIn = 0;

    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        CustomerLookupId = keepMeSignedIn.CustomerLookupId;
        $scope.IsLoggedIn = 1;
    }
    document.getElementById('featured_products').innerHTML = "";
    var tvid = document.getElementById('tvscheduleid').value;

    $scope.productsArray = [];

    $http({
        method: 'GET',
        url: baseUrl + "TVSchedule/GetTVScheduleInfo?tvScheduleId=" + tvid,
        contentType: 'application/json'
    }).then(function (result) {
        if (result.data != null) {

            if (!result.data.AllowWatchAgain) {
                $window.location.href = "/";
            }

            else {
                var dateToUse;

                if (isBrowserSafari) {
                    dateToUse = new Date(result.data.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/'));
                }
                else if (detectIE()) {
                    var newDateString = result.data.ShowDateTimeUTCFormat.replace(new RegExp('-', 'g'), '/');
                    dateToUse = new Date(newDateString.substring(0, newDateString.length - 3));
                }
                else {
                    dateToUse = new Date(result.data.ShowDateTimeUTCFormat);
                }

                result.data.TVShowDateUKToUse = dateToUse;

                var dateFormatted = $filter('date')(dateToUse, "yyyy-MM-dd", "UTC"); //TvScheduleInfo.TVShowDateUKToUse | date: 'yyyy-MM-dd' : 'UTC'}}
                var timeFormatted = $filter('date')(dateToUse, "HH:mm:ss", "UTC"); //{{TvScheduleInfo.TVShowDateUKToUse | date: 'HH:mm:ss' : 'UTC'}}

                $scope.RewindVideoStream = $sce.trustAsHtml("<div id=\"WatchVideo\" data-client=\"hochanda\" data-type=\"vod\" data-stream=\"" + $scope.simpleStreamRewindDataStream + "\" data-date=\"" + dateFormatted + "\" data-start=\"" + timeFormatted + "\"> <script ng-if=\"TvScheduleInfo != null\" src=\"https://dbxm993i42r09.cloudfront.net/ss.js\"></script></div>");

                $scope.TvScheduleInfo = result.data;

                $http({
                    method: 'GET',
                    url: baseUrl + "ProductsInShow/GetProductsInACertainShow?tvScheduleId=" + tvid + "&languageId=" + CommonCode.LanguageId + "&currencyId=" + CommonCode.CurrencyId + "&countryId=" + CommonCode.CountryId,
                    contentType: 'application/json',
                }).then(function (result1) {
                    $scope.productsArray = result1.data;
                    $scope.LoadGrid();
                });
            }
        }
    });


    $scope.LoadGrid = function () {

        $http({
            method: "GET",
            url: '/Product/LoadGrid?angularScopeVariable=productsArray&loadAngularProducts=true',
            contentType: "text/plain",
            dataType: "html"
        }).then(function (resultHtml) {
            var hiddenField = document.getElementById("featured_products");

            var htmlElement = angular.element(resultHtml.data);

            var compileAppendedArticleHTML = $compile(htmlElement[0]);
            var element = compileAppendedArticleHTML($scope);

            //hiddenField.parentNode.insertBefore(element[0], hiddenField.nextSibling);            
            hiddenField.appendChild(element[0]);
        });

    }

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);

            //$rootScope.quicklookProductID = ppid;
        }
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }

}]);