﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Settings
{
	public class CountrySelectorModel
	{ 
		public int CountryId { get; set; }
		public string CountryName { get; set; }
		public string DefaultStreamId { get; set; }
		public int DefaultLanguageId { get; set; }
		public int DefaultCurrencyId { get; set; }
		public string Country2DigitISOCode { get; set; }
		public int CompanyId { get; set; }
	}
}
