﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class LoqateFindRequestModel
    {
        public string Text { get; set; }
        public int Limit { get; set; }
        public string Countries { get; set; }
        public string Container { get; set; }
    }
}
