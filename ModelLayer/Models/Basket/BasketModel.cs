﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class BasketModel
	{
		/// <summary>
		/// The Id of the basket to obscure the real database value
		/// </summary>
		public long? BasketId { get; set; }
		/// <summary>
		/// When the basket was created
		/// </summary>
		public DateTime? BasketCreatedOn { get; set; }
		/// <summary>
		/// When the basket expires
		/// </summary>
		public DateTime? BasketExpiry { get; set; }
		/// <summary>
		/// The time left for the reservation. This is easier to use rather than BasketExpiry. But still want both as a backup.
		/// </summary>
		public TimeSpan? MinutesLeftInExpiry { get; set; }
		/// <summary>
		/// Amount of items in the basket
		/// </summary>
		public int BasketCount { get; set; }
		/// <summary>
		/// Is this a new Basket.
		/// </summary>
		public bool IsNewBasket { get; set; }
		/// <summary>
		/// Has the item been correctly added
		/// </summary>
		public bool HasItemBeenAddedToBasket { get; set; }
		/// <summary>
		/// Has an error occured. Could just be OOS
		/// </summary>
		public bool HasErrorOccurred { get; set; }
		/// <summary>
		/// The message we want to display to the user
		/// </summary>
		public string ErrorMessage { get; set; }
		/// <summary>
		/// The error code. A list of these will be available to whomever asks for it.
		/// </summary>
		public int MessageCode { get; set; }
		/// <summary>
		/// The text to appear for the OK button
		/// </summary>
		public string OKText { get; set; }
		/// <summary>
		/// The text to appear on the cancel button
		/// </summary>
		public string CancelText { get; set; }
		/// <summary>
		/// The title of the message
		/// </summary>
		public string MessageTitle { get; set; }
		/// <summary>
		/// The success Message to display
		/// </summary>
		public string SuccessMessage { get; set; }
		/// <summary>
		/// Has the basket finished its journey. So this is a way to tell the devices
		/// that the basket has finished and we need a new one.
		/// </summary>
		public bool HasBasketBeenCompleted { get; set; }
		public bool PickAndMixAdded { get; set; }
		public bool PickAndMixRemoved { get; set; }

		public bool HasBasketExpired { get; set; }
	}
}
