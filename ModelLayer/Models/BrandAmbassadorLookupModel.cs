﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class BrandAmbassadorLookupModel
    {
        public int id { get; set; }
        public int brandambid { get; set; }
        public int brandid { get; set; }
    }
}
