﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class FullProductDetailsModel
    {
        public FullProductDetailsModel()
        {
            ProductImages = new List<ProductImagesModel>();
        }

        public int ParentProductId { get; set; }
        public string ParentProductSKU { get; set; }
        public int LanguageId { get; set; }
        public string TVDescription { get; set; }
        public int CurrencyId { get; set; }
        public int CountryId { get; set; }
        public string CultureCode { get; set; }
        public string WebText { get; set; }
        public int IsPickAndMixItem { get; set; }
        public string DefaultImageAltText { get; set; }
        public string FreedomSashFilename { get; set; }
        public string FlexibuySashFilename { get; set; }
        public string FreedomBannerFilename { get; set; }
        public string FlexibuyBannerFilename { get; set; }
        public string ODSBannerFilename { get; set; }
        public int IsMembersDiscount { get; set; }
        public int IsFlexibuy { get; set; }
        public int FlexibuyInstallments { get; set; }
        public int? FeefoAveragePercentage { get; set; }
        public int? FeefoTotalReviews { get; set; }
        public decimal? FeefoAverageRating { get; set; }
        public int? DefaultVariationId { get; set; }
        public int ActivePrice { get; set; }
        public decimal IntroPrice { get; set; }
        public string IntroPriceString { get; set; }
        public decimal PromoPrice { get; set; }
        public string PromoPriceString { get; set; }
        public decimal UsualPrice { get; set; }
        public string UsualPriceString { get; set; }
        public decimal Price { get; set; }
        public string PriceString { get; set; }
        public decimal? FreedomPrice { get; set; }
        public string FreedomPriceString { get; set; }
        public decimal? FlexibuyPrice { get; set; }
        public string FlexibuyPriceString { get; set; }
        public decimal? FreedomFlexibuyPrice { get; set; }
        public string FreedomFlexibuyString { get; set; }

        public List<ProductImagesModel> ProductImages { get; set; }

        public List<ProductVariationModel> ProductVariations { get; set; }
    }
}
