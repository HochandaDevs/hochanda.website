﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class BasketUpdateModel
	{
		public bool IsActive { get; set; }
		public DateTime ExpiryDate { get; set; }
		public int BasketCount { get; set; }

		public TimeSpan TimeLeftInReservation
		{
			get
			{
				var timeLeft = TimeSpan.Zero;

				timeLeft = (ExpiryDate - DateTime.Now);
				return timeLeft;
			}
		}
	}
}
