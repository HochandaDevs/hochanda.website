﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class FreedomSetupHolidayRequestModel
    {
        public Guid CustomerLookupId { get; set; }
        public string Email { get; set; }
        public int DaySelector { get; set; }
    }
}
