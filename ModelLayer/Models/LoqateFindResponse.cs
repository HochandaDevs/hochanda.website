﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class LoqateResponse
    {
        public List<LoqateFindItems> Items { get; set; }
    }

    public class LoqateFindItems
    {
        public string Description { get; set; }
        public string Highlight { get; set; }
        public string Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
    }
}
