﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Customer
{
    public class ForgotPasswordModel
    {//comment
        public string ActivateLink { get; set; }
        public string ContactLink { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
    }
}
