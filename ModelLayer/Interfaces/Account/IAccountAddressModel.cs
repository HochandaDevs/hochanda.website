﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.Account
{
    public interface IAccountAddressModel
    {

        string customerRef { get; set; }
        string CustomerEmail { get; set; }
        int addressid { get; set; }
        int customerid { get; set; }
        string companyname { get; set; }
        string buildingname { get; set; }
        string buildingnumber { get; set; }
        string streetaddress1 { get; set; }
        string streetaddress2 { get; set; }
        string streetaddress3 { get; set; }
        string streetaddress4 { get; set; }
        string streetaddress5 { get; set; }
        string street { get; set; }
        string town { get; set; }
        string city { get; set; }
        string county { get; set; }
        string country { get; set; }
        string postcode { get; set; }
        DateTime createddate { get; set; }
        int createdby { get; set; }
        string postcodeanywhereid { get; set; }
        int countryid { get; set; }
        int active { get; set; }
        int blacklisted { get; set; }
        string IsDefaultBilling { get; set; }
        string IsDefaultDevlivery { get; set; }
       string IsLinkedToCard { get; set; }

    }
}
