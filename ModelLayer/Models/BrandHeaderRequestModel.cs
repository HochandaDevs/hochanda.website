﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class BrandHeaderRequestModel
	{
		public int LanguageId { get; set; }
		public int CurrencyId { get; set; }
		public int CountryId { get; set; }
		public bool TryingAgain { get; set; }
		public int ActualBrandId
		{
			get
			{
				//Clone it
				var tempBrandId = BrandId.ToString();
				if (tempBrandId.Contains("b"))
				{
					IsBrandAmbassador = true;
					tempBrandId = tempBrandId.Substring(0, tempBrandId.Length - 1);
				}

				var outputBrandId = 0;

				int.TryParse(tempBrandId, out outputBrandId);

				return outputBrandId;
			}
		}
		public bool IsBrandAmbassador { get; set; }
		public string BrandId { get; set; }
		//int languageId, int currencyid, int countryid, bool tryingAgain = false, int brandId = 0
	}
}
