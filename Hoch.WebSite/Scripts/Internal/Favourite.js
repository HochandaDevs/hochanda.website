﻿app.controller('FavouritesCtrl', ['$scope', '$http', "$compile", function ($scope, $http, $compile) {
    $scope.IsLoggedIn = 1;
    var favourites = JSON.parse(localStorage.getItem("userFavourites"));
    if (favourites != null) {
        
        $scope.products = favourites.products;
        $scope.Pages = favourites.pages;
        if ($scope.products != null && $scope.products != undefined) {
            for (var i = 0; i < $scope.products.length; i++) {
                $scope.products[i].IsFav = 1;
            }
        }
        $scope.SalesEventData = $scope.products;
        $http({
            method: "GET",
            url: '/Product/LoadGrid?angularScopeVariable=SalesEventData&loadAngularProducts=true',
            contentType: "text/plain",
            dataType: "html"
        }).then(function (resultHtml) {
            var hiddenField = document.getElementById("Prodcarousel");

            var htmlElement = angular.element(resultHtml.data);

            var compileAppendedArticleHTML = $compile(htmlElement[0]);
            var element = compileAppendedArticleHTML($scope);

            hiddenField.parentNode.insertBefore(element[0], hiddenField.nextSibling);
        });
    }
      
}]);