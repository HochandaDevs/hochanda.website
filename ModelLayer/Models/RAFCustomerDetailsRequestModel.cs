﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class RAFCustomerDetailsRequestModel
    {
        public Guid CustomerlookupId { get; set; }
        public string Email { get; set; }
    }
}
