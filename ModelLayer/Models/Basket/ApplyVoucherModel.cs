﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
    public class ApplyVoucherModel
    {
        public int VoucherApplied { get; set; }
        public int VoucherUsed { get; set; }
        public decimal AmountSaved { get; set; }
        public string VoucherCode { get; set; }

        public string AmountSavedWithCurrency { get; set; }
    }
}
