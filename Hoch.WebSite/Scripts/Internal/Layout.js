﻿//TODO: if items are in the basket extend login cookie to 20mmins more
//var baseUrl = "http://localhost:63302/"; //localhost api url
//var baseUrl = 'https://hope.wiki/testapi/'; //test hope.wiki
//var baseUrl = 'https://hope.wiki/'; //live hope.wiki
var baseUrl = 'https://gitlabapi.hochanda.xyz/'; //GitlabtestUrl

var fiveMinsInMs = 300000;
var currentItemOnAirTimer = 15000; //15 seconds
var oneDaySpecialTimer = 300000; // 5 minutes
var productsInThisShowTimer = 600000; // 10 minutes
var loginDuration = 20; //in minutes - this is the time after the user is logged out if keepmesignedin is not checked
var app = null;

//We have to have this in because %27 breaks angular. %27 is an apostophe. It should NEVER be in a URL.
var currentUrl = document.location.href;

if (currentUrl.indexOf("%27") != -1) {
    if (history.pushState) {
        currentUrl = currentUrl.replace(/(%27)+/gi, '');
        window.history.pushState({ path: currentUrl }, '', currentUrl);
    }
}

var isBrowserSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

//Taken from https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
function detectIE(excludeEdge) {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

//Taken from https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
function detectIEExcludeEdge() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;
}

//Reload the site after 3 hours
setTimeout(function () {
    window.location.reload(true);
}, 3600000);

if (window.location.href.toLowerCase().indexOf('checkout') === -1) {
    //angularjs.daterangepicker

    //Config, gets rid of the #!# in some urls
    app = angular.module("HochWebsite", ["vcRecaptcha", "ngRoute", "ngMaterial", "ngCookies", "ngSanitize", "angularjs.daterangepicker", "authInputs"]).config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            rewriteLinks: false,
            requireBase: false
        });
    }]);
}
else {
    //Config, gets rid of the #!# in some urls
    app = angular.module("HochWebsiteCheckout", ["vcRecaptcha", "ngSanitize", "ngCookies", "paypal-checkout", "authInputs"]).config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            rewriteLinks: false,
            requireBase: false
        });
    }]);



}

var daysLeftInBasket = 0;
var hoursLeftInBasket = 0;
var minuteLeftInBasket = 0;
var secondsLeftInBasket = 0;


function UpdateBasket() {
    var isExternal = GetParameterByName("isexternal") == "true";

    if (!isExternal) {

        var basketId = localStorage.getItem("basketId") != null ? localStorage.getItem("basketId") : null;

        var dataToSend = {
            "BasketId": basketId
        };

        if (basketId != null) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: baseUrl + 'reservations/GetBasketUpdate',
                data: JSON.stringify(dataToSend),
                success: function (data) {

                    if (data != null) {
                        if (data.IsActive) {
                            localStorage.setItem('basketCount', data.BasketCount);
                            //Mark that we have extended it

                            var reservationExpiry = new Date();
                            reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(data.TimeLeftInReservation.split(":")[0]));
                            reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(data.TimeLeftInReservation.split(":")[1]));
                            reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(data.TimeLeftInReservation.split(":")[2]));
                            localStorage.setItem("basketExpiry", reservationExpiry);
                        }
                        else {
                            ClearBasket();
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                }
            });


        }
    }
}

setTimeout(function () {
    UpdateBasket();
}, 1500);

//Expiry Timer
function BasketTimer() {

    if (localStorage.getItem('basketExpiry') && localStorage.getItem("basketCount")) {
        var basketCount = localStorage.getItem("basketCount") != null ? localStorage.getItem("basketCount") : 0;

        var finishTime = new Date(localStorage.getItem("basketExpiry"));

        var delta = Math.abs(finishTime - new Date()) / 1000;

        daysLeftInBasket = ("0" + Math.floor(delta / 86400)).slice(-2);
        delta -= daysLeftInBasket * 86400;

        hoursLeftInBasket = ("0" + Math.floor(delta / 3600) % 24).slice(-2);
        delta -= hoursLeftInBasket * 3600;

        minuteLeftInBasket = ("0" + Math.floor(delta / 60) % 60).slice(-2);
        delta -= minuteLeftInBasket * 60;

        secondsLeftInBasket = ("0" + Math.floor(delta % 60)).slice(-2);

        if (((finishTime - new Date() / 1000) / 60) <= 1) {
            //One minute left display the message to extend??
        }

        if (finishTime - new Date() < 0) {

            ClearBasket();

            if (finishTime - new Date() > -60000) {
                //Only redirect if we expired in the last minute.
                //window.location.href = "/basket?expiredBasket=true";
                angular.element(document).scope().ShowModal("expiredBasket");
                ShowModal();
                $(".basket_number").hide();
            }
        }

        if (basketCount > 0) {

            var timeLeft = minuteLeftInBasket + ":" + secondsLeftInBasket;

            if (hoursLeftInBasket > 1) {
                $(".has_timer").hide();
                $(".has_timer_wrap").hide();
            }
            else {
                $(".has_timer_wrap").show();
                $(".has_timer").show();
            }

            $(".timer").html(timeLeft);

            if (finishTime - new Date() >= 0 && finishTime - new Date() <= 60000) {
                //only display if we have not been displayed before.
                if (localStorage.getItem('basketWarning') == null || localStorage.getItem('basketWarning') == 0) {
                    localStorage.setItem('basketWarning', 1);

                    angular.element(document).scope().ShowModal("expiringBasket");
                    ShowModal();

                    ShowHideExtendBasket();

                }
            }
        }
        else {
            $(".has_timer").hide();
            $(".has_timer_wrap").hide();
        }

        $(".basket_number").html(basketCount);

    }
    else {
        $(".has_timer").hide();
        $(".has_timer_wrap").hide();
    }
    setTimeout(BasketTimer, 500);
}

function ExtendBasket() {
    var basketId = localStorage.getItem("basketId") != null ? localStorage.getItem("basketId") : null;

    var dataToSend = {
        "BasketId": basketId
    };

    if (basketId != null) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: baseUrl + 'reservations/UpdateBasketTime',
            data: JSON.stringify(dataToSend),
            success: function (data) {

                if (data != null) {
                    //Reset the warning
                    localStorage.setItem('basketWarning', 0);
                    //Mark that we have extended it
                    localStorage.setItem('basketExtended', 1)

                    var reservationExpiry = new Date();
                    reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(data.split(":")[0]));
                    reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(data.split(":")[1]));
                    reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(data.split(":")[2]));
                    localStorage.setItem("basketExpiry", reservationExpiry);

                }
                HideModal();
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });


    }
}

function ShowHideExtendBasket() {
    setTimeout(function () {

        var displayExtend = false;
        //We want to display button to extend the reservation time.
        if (localStorage.getItem('basketExtended') == null || localStorage.getItem('basketExtended') == 0) {
            displayExtend = true;
        }

        var extendButton = $("#moreTime");

        if (extendButton != null && extendButton.length != 0) {
            if (displayExtend) {
                extendButton.show();
            }
            else {
                extendButton.hide();
            }
        }
        else {
            ShowHideExtendBasket(show);
        }

    }, 500);
}

//Always have it running
BasketTimer();

var canAdd = true;

$(document).ready(function () {
    $('#DynamicModal').on('hide.bs.modal', function () {
        HidePop();
        $('html').removeClass("frozen");
    });

    $('#DynamicModal').on('show.bs.modal', function () {
        HidePop();
        $('html').addClass("frozen");
    });
});

function AddToBasketAuction(element) {
    var parentProductId = $(element).parent().find('[name=\'parentProductId\']').val();
    var variationId = $(element).parent().find('[name=\'variationId\']').val();

    //Can only have one per basket 
    var currentlyOnAirScope = angular.element(document.getElementById('currentlyOnAirController')).scope();

    if (currentlyOnAirScope != null && parentProductId != null && variationId != null) {
        if (currentlyOnAirScope.AddedToBasket == 0) {
            //Check if we are logged in. We can only place a bid if we are logged in.
            //TODO: Use check function.

            var rootScope = angular.element(document).scope();

            var loggedInPromise = rootScope.ValidateLogin();

            if (loggedInPromise != null) {
                loggedInPromise.then(function (signinData) {
                    if (signinData != null && signinData.CustomerLookupId != null) {
                        AddToBasket(parentProductId, variationId, element);

                        currentlyOnAirScope.AddedToBasket = 1;
                        //Have to apply as we are making a change
                        currentlyOnAirScope.$applyAsync()
                    }
                    else {
                        ShowLoginModal();
                    }
                }).catch(function (error) {
                    ShowLoginModal();
                });
            }
            else {
                ShowLoginModal();
            }
        }

    }
}

function ClearBasket() {
    localStorage.removeItem("basketId");
    localStorage.removeItem("basketExpiry");
    localStorage.removeItem("basketCount");
    localStorage.removeItem("basketWarning");
    localStorage.removeItem("basketExtended");
}

function ShowLoginModal() {
    angular.element(document).scope().ShowLoginModal();
    ShowModal();
}

function ShowQuickLookModal(parentProductId) {
    angular.element(document).scope().ShowProductModal(parentProductId);
}

function AddToBasket(parentProductId, variationId, element) {

    //If we are zero then we want to display the variation selector.
    if (variationId == 0) {

        GetVariationSelector(parentProductId, element);
        //Display Modal with variation Selector

        return;
    }

    var basketExpiredDate = new Date(localStorage.getItem("basketExpiry"));

    if (basketExpiredDate == null || basketExpiredDate < new Date()) {
        ClearBasket();
    }

    var basketId = localStorage.getItem("basketId");

    angular.element(document.body).injector().get("CommonCode").getBaseCookieSettings();

    $(element).addClass("adding");
    $(element).removeClass("added");

    var dataToSend = {
        ParentProductId: parentProductId,
        VariationId: variationId,
        Quantity: 1,
        LanguageId: angular.element(document.body).injector().get("CommonCode").LanguageId,
        CurrencyId: angular.element(document.body).injector().get("CommonCode").CurrencyId,
        CountryId: angular.element(document.body).injector().get("CommonCode").CountryId,
        BasketId: localStorage.getItem("basketId") == undefined ? null : localStorage.getItem("basketId"),
        PickAndMixIdentifier: 0
    }

    if (canAdd) {
        canAdd = false;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: baseUrl + 'reservations/UpdateItemOnReservation',
            data: JSON.stringify(dataToSend),
            success: function (data) {

                if (data != null) {
                    //Get datetime of expiry based on minutes left in reservation. ExpiryDate does not work ot use as it will be GMT time and other timezones can mess that up.
                    var reservationExpiry = new Date();
                    reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(data.TimeLeftInReservation.split(":")[0]));
                    reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(data.TimeLeftInReservation.split(":")[1]));
                    reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(data.TimeLeftInReservation.split(":")[2]));
                    localStorage.setItem("basketExpiry", reservationExpiry);

                    localStorage.setItem("basketCount", data.BasketCount);

                    var basketId = localStorage.getItem("basketId");
                    if (data.BasketId != null && data.BasketId != 0) {
                        localStorage.setItem("basketId", data.BasketId);
                    }

                    setTimeout(function () {


                        HidePop();

                        //Gets the left and top offset ($.offset == position on page; $.position == relative position) 
                        var leftPosition = $(element).offset().left;

                        var topPosition = $(element).offset().top;

                        //Width of pop under.
                        var buttonWidth = $(element).outerWidth();
                        $(".pop").width(buttonWidth);

                        $(".pop").css("left", leftPosition);
                        $(".pop").css("top", topPosition + 50);

                        $(element).removeClass("adding");
                        if (data.HasProductBeenAdded) {

                            $(element).addClass("added");

                            $("#inStockPop").show();
                            $("#outOfStockPop").hide();
                            $("#errorMessagePop").hide();
                            HidePop();

                            var upsell = $(element).parent().find("[name='upsellItem']");

                            if ((upsell != null && upsell.val() == 0) || upsell.length == 0) {
                                ShowPop();
                            }


                            if (upsell != null && upsell.val() == 1) {

                                var checkoutScope = angular.element(document.getElementById('checkoutTemplate')).scope();
                                checkoutScope.UpdateUpsell();
                            }
                        }
                        else {
                            if (data.IsProductInStock) {
                                $("#inStockPop").hide();
                                $("#outOfStockPop").hide();

                                $("#errorMessagePop").html(data.MessageToDisplay);

                                $("#errorMessagePop").show();

                                ShowPop();
                            }
                            else {
                                //Out of stock
                                $("#inStockPop").hide();
                                $("#outOfStockPop").show();
                                $("#errorMessagePop").hide();
                                ShowPop();
                            }
                        }

                        setTimeout(function () {
                            //fadeout after 5 seconds to look awesome...
                            $('.pop').fadeOut(500);
                        }, 5000);

                    }, 500);
                }

                setTimeout(function () {
                    $(element).removeClass("adding");
                    $(element).removeClass("added");
                }, 500);

                canAdd = true;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //Uncomment if testing.
                $(element).removeClass("adding");
                $(element).removeClass("added");
                canAdd = true;
            }
        });
    }
}

function GetWebSafeURL(urlPartToMakeSafe) {
    var url = eval(JSON.stringify(urlPartToMakeSafe.replace(/[^a-z0-9]+/gi, '-').replace(/^-+/, '').replace(/-+$/, '')));

    if (url != null) {
        url = url.toLowerCase();
    }

    return url;
}

function GetProductUrlFromFullProduct(product) {
    return GetProductUrl(product.ParentProductId, product.ParentProductSKU, product.TVDescription);
}

function GetProductUrl(parentProductId, parentProductSKU, tvDescription) {

    var desc = GetWebSafeURL(tvDescription);

    return "/product/" + parentProductId + "/" + desc + "/" + parentProductSKU;
}

function GetCategoryUrl(categoryId, categoryName) {

    var desc = GetWebSafeURL(categoryName);

    return "/category/" + categoryId + "/" + desc;
}

//To call this function you need the parentProductId and the element so we can display the pop.
//This creates a modal
function GetVariationSelector(parentProductId, element) {

    var rootScope = angular.element(document).scope();

    rootScope.ShowModal("variation_selector");
    var variationController = angular.element(document.getElementById('variationControllerDiv')).scope();

    if (variationController != null) {
        variationController.SetVariations(parentProductId, element);
    }
    else {
        //If the variation controller has not been loaded yet in the modal we need to use rootscope values.
        rootScope.ParentProductId = parentProductId;
        rootScope.VariationElement = element;
    }
}

function SetupSmoothScroll() {
    var controls = $("a[href^='#']");

    $.each(controls, function (index, value) {
        if ($(value).attr("href").length > 1) {
            //Add current onclick followed by smooth scroll
            var onclick = '';

            if ($(value).attr("onclick") != null) {
                onclick = $(value).attr("onclick");
            }

            $(value).attr("onclick", onclick + " SmoothScroll(this); return false;");
        }
    });
}

function ChangeBasketVariationAndVariationId(elementParent, timesDone, isUpsell) {
    var selectedParentProductId = $(elementParent).find("[name='selectedParentProductId']");
    var selectedVariationId = $(elementParent).find("[name='selectedVariationId']");

    if (timesDone == null) {
        timesDone = 1;
    }
    //If we don't have the values wait for angular to finish and try again.
    if (selectedParentProductId.val() == null || selectedParentProductId.val() == "") {

        if (timesDone < 10) {
            setTimeout(function () {
                ChangeBasketVariationAndVariationId(elementParent, timesDone + 1)
            }, 250);
            return;
        }
    }

    if (selectedVariationId.val() == null || selectedVariationId.val() == "") {
        if (timesDone < 10) {
            setTimeout(function () {
                ChangeBasketVariationAndVariationId(elementParent, timesDone + 1)
            }, 250);
            return;
        }
    }

    if (selectedParentProductId.length == 1 && selectedParentProductId.val() != null && selectedParentProductId.val() != "") {
        $(elementParent).find("[name='parentProductId']").val(selectedParentProductId.val());
    }

    if (selectedVariationId.length == 1 && selectedVariationId.val() != null && selectedVariationId.val() != "") {
        $(elementParent).find("[name='variationId']").val(selectedVariationId.val());
    }

    if (isUpsell) {
        $(elementParent).find("[name='upsellItem']").val(isUpsell);
    }
}

function VariationSelectorChanged(selectChangedElement, changeParentProductId) {
    var hiddenProductId = $(selectChangedElement).parent().find("[name='parentProductId']");
    var hiddenVariationId = $(selectChangedElement).parent().find("[name='variationId']");

    if (changeParentProductId) {

        var newParentProductId = $(selectChangedElement).parent().find("[name='selectorParentProductId']").val();

        //Angular adds "number:" to a value in a select if you are using options. (Have to on product page as it adds a blank otherwise.
        if (newParentProductId.split(":").length == 2) {
            newParentProductId = newParentProductId.split(":")[1];
        }

        hiddenProductId.val(newParentProductId);
    }

    var newVariationId = $(selectChangedElement).find(":selected").val();
    //Angular adds "number:" to a value in a select if you are using options. (Have to on product page as it adds a blank otherwise.
    if (newVariationId.split(":").length == 2) {
        newVariationId = newVariationId.split(":")[1];
    }

    hiddenVariationId.val(newVariationId);

}

/* directive to use the owl carousel on angularjs */
app.directive("owlCarousel", function ($timeout) {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            var changeEndTimeout = null;
            scope.initCarousel = function (element) {
                // provide any default options you want
                var defaultOptions = {
                    loop: false,
                    margin: 10,
                    responsiveClass: true,
                    dots: true,
                    rewind: false,
                    responsive: {
                        0: {
                            items: 2,
                            nav: false
                        },
                        600: {
                            items: 3,
                            nav: false
                        },
                        768: {
                            margin: 20,
                            nav: true
                        },
                        1000: {
                            items: 4,
                            margin: 30,
                            nav: true
                        },
                        1200: {
                            items: 5,
                            margin: 30,
                            nav: true
                        },
                        1280: {
                            items: 6,
                            margin: 30,
                            nav: true
                        },
                        1500: {
                            items: 7,
                            margin: 30,
                            nav: true
                        }
                    },
                    onDrag: function (event) {
                        //HidePop();
                    }
                };
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for (var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }

                var autoRun = scope.$eval($(element).attr('data-auto-run'));

                if (autoRun) {
                    $(element).owlCarousel(defaultOptions);
                }

                //$(element).owlCarousel(defaultOptions);
                // Event to remove the carousel on data change start
                scope.$on('owlCarousel.changeStart', function (data) {
                    $(element).owlCarousel('destroy');
                });
                // Event to create the carousel back when data change is completed
                scope.$on('owlCarousel.changeEnd', function (data) {
                    changeEndTimeout = $timeout(function () {
                        $(element).owlCarousel(defaultOptions);
                    });
                });
            };
            scope.$on('$destroy', function () {
                if (changeEndTimeout) {
                    $timeout.cancel(changeEndTimeout);
                }
            });
        }
    };
})
    .directive('owlCarouselItem', function ($timeout) {
        return {
            restrict: 'A',
            transclude: false,
            link: function (scope, element) {
                // wait for the last item in the ng-repeat then call init
                if (scope.$last) {
                    scope.initCarousel(element.parent());
                }
            }
        };
    });


app.directive('googleSchema', ['$filter', '$sce', '$timeout', function ($filter, $sce, $timeout) {
    return {
        restrict: 'EA',
        link: function (scope, element) {
            var timeout = $timeout(function () {
                scope.$watch('ld', function (value) {
                    var val = $sce.trustAsHtml($filter('json')(value));
                    element[0].outerHTML = '<script type="application/ld+json">' + val + '</script>';
                });
                scope.$watch('ldExtra', function (value) {
                    var val = $sce.trustAsHtml($filter('json')(value));

                    var jsonLd = document.createElement("script")
                    jsonLd.type = "application/ld+json";
                    jsonLd.innerHTML = val;

                    document.body.appendChild(jsonLd);
                });
            }, 1000);
        }
    };
}]);

app.directive('changeBasketValues', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        priority: -10000,
        link: function ($scope, element, attributes) {
            attributes.$observe('parentproductid', function () {
                element.ready(function () {
                    $scope.$apply(function () {
                        ChangeBasketVariationAndVariationId(attributes.$$element, 0, attributes.upsellcheckout);
                    });
                });
            });
        }
    }
}]);

app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        priority: -100000,
        link: function (scope, element, attributes) {
            var unwatch = scope.$watch('MinimumDate', function (newVal, oldVal) {
                // or $watchCollection if students is an array
                if (newVal) {
                    element.datepicker({
                        dateFormat: 'yy/mm/dd',
                        minDate: new Date(scope.MinimumDate),
                        maxDate: new Date(scope.MaximumDate),
                        onSelect: function (date) {
                            scope.$emit('selectedDate', date);
                            scope.$apply();
                        }
                    });

                    $(element).parent().click(function () {
                        $(element).datepicker("show");
                    });

                    //Remove the watcher.
                    unwatch();
                }
            });
        }
    }
});

var numberOfTimes = 0;

app.directive('getProductLink', function () {
    return {
        restrict: 'A',
        priority: -100000,
        link: function (scope, element, attributes) {
            attributes.$observe('parentproductid', function () {
                element.ready(function () {
                    var parentProductId = element[0].dataset.parentproductid != null && element[0].dataset.parentproductid != "" ? element[0].dataset.parentproductid : scope.item != null ? scope.item.ParentProductId : null;
                    var parentProductSKU = element[0].dataset.parentproductsku != null && element[0].dataset.parentproductsku != "" ? element[0].dataset.parentproductsku : scope.item != null ? scope.item.ParentProductSKU : null;
                    var description = element[0].dataset.description != null && element[0].dataset.description != "" ? element[0].dataset.description : scope.item != null ? scope.item.TVDescription : null;

                    if (parentProductId != null) {
                        element[0].href = GetProductUrl(parentProductId, parentProductSKU, description);
                    }
                });
            });
        }
    }
});

/* factory for common functions */
app.factory('CommonCode', function ($window, $cookies, $http) {
    var root = {};

    root.CountryId = 1;
    root.CurrencyId = 1;
    root.LanguageId = 1;
    root.ChannelId = 1;
    root.CompanyId = 1;

    root.CountryCode = "GB";
    root.CurrencySymbol = "£";
    root.CurrencyCode = "";
    root.DefaultStreamingId = "hochandauk";
    root.SimpleStreamRewindDataStream = "578";

    root.partialName = '';
    root.partialCheck = '';//added this by suga3 for change address modal to differentaate between di
    root.basketPaginationCheck = 0;
    root.checkoutPaginationCheck = 0;
    root.favBaseUrl = "";

    root.UpsellClick = 0;
    root.topMenuData = '';
    root.tvsCheck = '';
    root.CheckoutCreateCustomer = 0;
    root.buttonCheck == '-1';



    root.setPartialName = function (name, partialCheck, ppID) {
        root.partialName = name;
        root.partialCheck = partialCheck;
        root.quicklookProductID = ppID;
        root.favBaseUrl = favBaseUrl;
    }


    root.recentlyViewed = JSON.parse(localStorage.getItem('recentlyViewed'));
    root.show = function (msg) {

    };

    root.getBaseCookieSettings = function () {
        var baseCookieSettings = $cookies.get('BaseCookieSettings');

        var valueError = false;

        if (baseCookieSettings != undefined && baseCookieSettings != null && baseCookieSettings != "") {
            try {
                root.CountryId = baseCookieSettings.split('&')[0].split('=')[1];
                root.CurrencyId = baseCookieSettings.split('&')[1].split('=')[1];
                root.LanguageId = baseCookieSettings.split('&')[2].split('=')[1];
                root.ChannelId = baseCookieSettings.split('&')[3].split('=')[1];
                root.CountryCode = baseCookieSettings.split('&')[4].split('=')[1];
                root.CurrencySymbol = baseCookieSettings.split('&')[5].split('=')[1];
                root.DefaultStreamingId = baseCookieSettings.split('&')[6].split('=')[1];
                root.SimpleStreamRewindDataStream = baseCookieSettings.split('&')[7].split('=')[1];
                root.CompanyId = baseCookieSettings.split('&')[8].split('=')[1];
            }
            catch (error) {
                valueError = true;
            }

        }

        if (valueError) {
            root.CountryId != undefined ? root.CountryId : 1;
            root.CurrencyId != undefined ? root.CurrencyId : 1;
            root.LanguageId != undefined ? root.LanguageId : 1;
            root.ChannelId != undefined ? root.ChannelId : 1;
            root.CountryCode != undefined ? root.CountryCode : "GB";
            root.CurrencySymbol != undefined ? root.CurrencySymbol : "£";
            root.DefaultStreamingId != undefined ? root.DefaultStreamingId : "hochandauk";
            root.SimpleStreamRewindDataStream != undefined ? root.SimpleStreamRewindDataStream : "578";
            root.CompanyId != undefined ? root.CompanyId : 1;
        }

    }

    root.GetMerchantAccount = function (currencyId) {
        var http = $http({
            method: 'GET',
            url: baseUrl + 'Culture/GetMerchantAccount?currencyID=' + currencyId,
        });
        return http;
    }

    root.jsonEqual = function (a, b) {
        return JSON.stringify(a) === JSON.stringify(b);
    }

    return root;
});

app.factory('loginService', function ($http, $log, $q, $cookies) {
    return {
        validateLogin: function () {
            var loginDetails = $cookies.get("keepMeSignedIn");
            if (loginDetails != null) {
                loginDetails = JSON.parse(decodeURIComponent(loginDetails));
                var deferred = $q.defer();
                var url = baseUrl + "Account/ValidateLogin";
                var params = { CustomerLookupId: loginDetails.CustomerLookupId, Email: loginDetails.Email };
                $http({
                    method: 'POST',
                    url: url,
                    data: JSON.stringify(params)
                })
                    .then(function (result) {
                        deferred.resolve({
                            CustomerLookupId: result.data.CustomerLookupId,
                            Email: loginDetails.Email,
                            FirstName: result.data.FirstName,
                            LastName: result.data.LastName
                        });
                    });
                return deferred.promise;
            }
            return null;
        }
    }
});

app.controller('FooterController', ['$scope', 'CommonCode', '$rootScope', '$timeout', "$http", function ($scope, CommonCode, $rootScope, $timeout, $http) {
    $rootScope.$broadcast('owlCarousel.changeStart');

    CommonCode.getBaseCookieSettings();

    var recentlyViewed = CommonCode.recentlyViewed;

    if (recentlyViewed != null) {
        var productIds = recentlyViewed.map(function (i) {
            return i.id
        });

        var productRequest = {
            "ParentProductIds": productIds,
            "LanguageId": CommonCode.LanguageId,
            "CurrencyId": CommonCode.CurrencyId,
            "CountryId": CommonCode.CountryId
        }

        $http({
            method: 'POST',
            url: baseUrl + "Products/GetMultipleProductInformation",
            data: JSON.stringify(productRequest)
        }).then(function (result) {
            if (result.data != null) {
                $scope.products = result.data;
            }
        });

        $scope.getProductUrl = function (id, sku, desc) {
            return GetProductUrl(id, sku, desc);
        }

        var changeEndTimeout = $timeout(function () {
            $rootScope.$broadcast('owlCarousel.changeEnd');
        }, 1000);

        $scope.$on('$destroy', function () {
            if (changeEndTimeout)
                $timeout.cancel(changeEndTimeout);
        });
    }
}]);

//ValidateLogin();

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function ValidateLogin() {
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        var http = new XMLHttpRequest();
        var url = baseUrl + "Account/ValidateLogin";
        var params = 'CustomerLookupId=' + keepMeSignedIn.CustomerLookupId + '&email=' + keepMeSignedIn.Email;
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function () {//Call a function when the state changes.
            if (http.readyState == 4 && http.status == 200) {
                var loginInfo = JSON.parse(http.responseText);
                if (loginInfo != null) {
                    if (loginInfo.CustomerLookupId != null) {
                        return loginInfo;
                    }
                } else {
                    return false;
                }
            }
        }
        http.send(params);
    }
}

app.controller('EmptyController', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    //This controller is used purely to access the rootscope. 


    //Some values I want to use.
    if (document.getElementById("checkoutTemplate") != null) {
        $scope.shippingAddressId = angular.element(document.getElementById("checkoutTemplate")).scope().shippingAddressId;
    }
}]);

app.controller('ModalController', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    $rootScope.MyAccountpaymentPage = null;
    $scope.$watch('$root.partialName', function () {
        $scope.template = $rootScope.partialName;
    });

    $scope.$watch('$root.selectedAddressData', function () {
        $scope.selectedBillingAddress = $rootScope.selectedBillingAddress;
        $scope.selectedCardData = $rootScope.selectedCardData;
        $scope.selectedShippingAddress = $rootScope.selectedShippingAddress;
        $scope.ChaseCustomerRefNum = $rootScope.chaseCustomerRefNum;
    });

    $scope.$watch('$root.adresscard', function () {
        $scope.adresscard = $rootScope.adresscard;
    });

    $scope.saveAddress = function (pAddressData) {

        if (pAddressData.companyname != ""
            && pAddressData.streetaddress1 != ""
            && pAddressData.town != ""
            && pAddressData.county != ""
            && pAddressData.country != ""
            && pAddressData.postcode != ""
        ) {
            var addressData = {};
            addressData = pAddressData;

            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountSaveAddress",
                contentType: 'application/json',
                data: addressData
            }).then(function (result) {
                var iCnt = 0;
                var iNum = 0;
                if (result.data !== undefined) {
                    if (result.data.length > 0) {

                    }
                }
            });

        }

    };
}]);

function SmoothScroll(element) {

    if (element.hash == null || element.hash == "") {
        return false;
    }

    var target = $(element.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {

        ScrollToElement(target);

        window.location.hash = element.hash;

        return false;
    }

}

var isScrolling = false;

//Element must be the jquery element $('#test');
function ScrollToElement(element) {
    //Stops current animation
    $('html').stop();

    var htmlScroll = parseInt($('html').scrollTop());
    var elementScroll = parseInt(element.offset().top - 60);

    if (htmlScroll != elementScroll) {

        if (detectIE()) {
            element[0].scrollIntoView();
        }
        else {
            $('html').animate({
                scrollTop: elementScroll
            }, 1000)
        }
    }
}

//Taken from old site to remove crap that can possible get in.
function Asciify(inputString) {
    inputString = inputString.replace(/'’'/gi, '')
        .replace(/'–'/gi, '-')
        .replace(/'‘'/gi, '')
        .replace(/'”'/gi, '\'')
        .replace(/'“'/gi, '\'')
        .replace(/'…'/gi, '...')
        .replace(/'£'/gi, 'GBP')
        .replace(/'•'/gi, '*')
        .replace(/' '/gi, ' ')
        .replace(/'é'/gi, 'e')
        .replace(/'ï'/gi, 'i')
        .replace(/'´'/gi, '')
        .replace(/'—'/gi, '-')
        .replace(/'·'/gi, '*')
        .replace(/'„'/gi, '\'')
        .replace(/'€'/gi, 'EUR')
        .replace(/'®'/gi, '')
        .replace(/'¹'/gi, '(1)')
        .replace(/'«'/gi, '\'')
        .replace(/'è'/gi, 'e')
        .replace(/'á'/gi, 'a')
        .replace(/'™'/gi, 'TM')
        .replace(/'»'/gi, '\'')
        .replace(/'ç'/gi, 'c')
        .replace(/'½'/gi, '')
        .replace(/'­'/gi, '-')
        .replace(/'°'/gi, ' degrees ')
        .replace(/'ä'/gi, 'a')
        .replace(/'É'/gi, 'E')
        .replace(/'‚'/gi, ',')
        .replace(/'ü'/gi, 'u')
        .replace(/'í'/gi, 'i')
        .replace(/'ë'/gi, 'e')
        .replace(/'ö'/gi, 'o')
        .replace(/'à'/gi, 'a')
        .replace(/'¬'/gi, ' ')
        .replace(/'ó'/gi, 'o')
        .replace(/'â'/gi, 'a')
        .replace(/'ñ'/gi, 'n')
        .replace(/'ô'/gi, 'o')
        .replace(/'¨'/gi, '')
        .replace(/'å'/gi, 'a')
        .replace(/'ã'/gi, 'a')
        .replace(/'ˆ'/gi, '')
        .replace(/'©'/gi, '')
        .replace(/'Ä'/gi, 'A')
        .replace(/'Ï'/gi, 'I')
        .replace(/'ò'/gi, 'o')
        .replace(/'ê'/gi, 'e')
        .replace(/'î'/gi, 'i')
        .replace(/'Ü'/gi, 'U')
        .replace(/'Á'/gi, 'A')
        .replace(/'ß'/gi, 'ss')
        .replace(/'¾'/gi, '')
        .replace(/'È'/gi, 'E')
        .replace(/'¼'/gi, '')
        .replace(/'†'/gi, '+')
        .replace(/'³'/gi, '')
        .replace(/'²'/gi, '')
        .replace(/'Ø'/gi, 'O')
        .replace(/'¸'/gi, ',')
        .replace(/'Ë'/gi, 'E')
        .replace(/'ú'/gi, 'u')
        .replace(/'Ö'/gi, 'O')
        .replace(/'û'/gi, 'u')
        .replace(/'Ú'/gi, 'U')
        .replace(/'Œ'/gi, 'Oe')
        .replace(/'º'/gi, '?')
        .replace(/'‰'/gi, '')
        .replace(/'Å'/gi, 'A')
        .replace(/'ø'/gi, 'o')
        .replace(/'˜'/gi, '~')
        .replace(/'æ'/gi, 'ae')
        .replace(/'ù'/gi, 'u')
        .replace(/'‹'/gi, '<')
        .replace(/'±'/gi, '')

    return inputString;

}

//Updates the query string 
function UpdateQueryStringParameter(queryKey, queryValue, urlToChange) {
    if (urlToChange == null) {
        urlToChange = window.location.href;
    }

    var re = new RegExp("([?&])" + queryKey + "=.*?(&|$)", "i");
    var separator = urlToChange.indexOf('?') !== -1 ? "&" : "?";

    if (urlToChange.match(re)) {
        return urlToChange.replace(re, '$1' + queryKey + "=" + queryValue + '$2');
    }
    else {
        return urlToChange + separator + queryKey + "=" + queryValue;
    }
}

//"taken" from https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
function GetParameterByName(name) {
    var url = window.location.href.toLowerCase();

    name = name.replace(/[\[\]]/g, '\\$&').toLowerCase();

    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    var results = regex.exec(url);

    if (!results) {
        return null
    };

    if (!results[2]) {
        return '';
    }

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

//function AddToFav(type, catUrl, bId, bName, prodId, ProdSku, tvdesc) {
function AddToFav(url, e) {
    var customerRef = "0";
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        customerRef = keepMeSignedIn.CustomerLookupId;
        var email = keepMeSignedIn.Email;
        var rootScope = angular.element(document).scope();

        var baseCookieDetails = rootScope.GetBaseCookieDetails();

        var pageInfo =
        {
            customerRef: customerRef,
            customerEmail: email,
            pageUrl: url,
            languageId: baseCookieDetails.LanguageId,
            currencyId: baseCookieDetails.CurrencyId,
            countryId: baseCookieDetails.CountryId
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: baseUrl + "Account/InsertFavouritePage",
            //contentType: 'application/json',
            data: JSON.stringify(pageInfo),
            success: function (data) {
                if (e != null && e != undefined) {
                    e.classList.value = 'favourite';
                    e.textContent = 'Remove from favourites';

                    $(e).off();
                    $(e).attr("onclick", "").unbind("click");

                    $(e).on('click', function () { RemoveFromFav(url, e) });
                    angular.element(document).scope().StoreFavouriteList();
                    return true;
                }



                return true;

            }
        });
    }
    else {
        return false;
    }

};

function RemoveFromFav(url, e) {

    var customerRef = "0";
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        customerRef = keepMeSignedIn.CustomerLookupId;
        var email = keepMeSignedIn.Email;
        var rootScope = angular.element(document).scope();

        var baseCookieDetails = rootScope.GetBaseCookieDetails();

        var pageInfo =
        {
            customerRef: customerRef,
            customerEmail: email,
            pageUrl: url,
            languageId: baseCookieDetails.LanguageId,
            currencyId: baseCookieDetails.CurrencyId,
            countryId: baseCookieDetails.CountryId
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: baseUrl + "Account/RemoveFavouritePage",
            data: JSON.stringify(pageInfo),
            success: function (data) {
                if (e != null && e != undefined) {
                    e.classList.value = 'notfavourite';
                    e.textContent = 'Add to favourites';

                    $(e).off();
                    $(e).attr("onclick", "").unbind("click");

                    $(e).on('click', function () { AddToFav(url, e) });
                    angular.element(document).scope().StoreFavouriteList();
                    return true;
                }

            }
        });
    }
    else {
        return false;
    }

}

function delete_cookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
};

function logout() {
    delete_cookie("keepMeSignedIn");
    localStorage.removeItem("userFavourites");
    ClearBasket();
    window.location.href = "/";
}

app.controller('HeaderMainController', ['$scope', '$cookies', function ($scope, $cookies) {

    $scope.isLoggedIn = false;
    $scope.path = '/login';
    var keepMeSignedIn = $cookies.get('keepMeSignedIn');
    if (keepMeSignedIn != null && keepMeSignedIn != undefined) {
        keepMeSignedIn = JSON.parse(keepMeSignedIn);
        $scope.isLoggedIn = true;
        $scope.path = '/myaccount';
    }


}]);

app.controller('HeaderBannerHero', ['$scope', "$sanitize", "CommonCode", "$http", "$sce", function ($scope, $sanitize, CommonCode, $http, $sce) {

    $scope.Hero = null;
    $scope.theInterval = null;

    CommonCode.getBaseCookieSettings();

    $scope.GetHeroHtml = function () {
        if ($scope.Hero != null) {
            return $sce.trustAsHtml($scope.Hero);
        }

        return null;
    };

    $scope.GetHeaderBannerHero = function () {
        $http({
            method: 'GET',
            url: baseUrl + 'heroes/GetHeaderBannerHero?companyId=' + CommonCode.CompanyId + '&languageId=' + CommonCode.LanguageId + '&currencyId=' + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
            contentType: "application/json; charset=utf-8",
        }).then(function (result) {
            if (result.data != null) {
                $scope.Hero = result.data.AdvertHTML;
            }
            else {
                $scope.Hero = null;
            }

            //5 minutes
            var interval = 300000;

            var nextHour = ((60 - new Date().getMinutes()) * 60000) + 3000;
            if (nextHour < interval) {
                interval = nextHour;
            }

            setTimeout(function () {
                $scope.GetHeaderBannerHero();
            }, interval);

        }, function errorCallback(error) {
            var interval = 60000;

            var nextHour = ((60 - new Date().getMinutes()) * 60000) + 3000;
            if (nextHour < interval) {
                interval = nextHour;
            }

            setTimeout(function () {
                $scope.GetHeaderBannerHero();
            }, interval);
        });

    }

    $scope.GetHeaderBannerHero();

}]);

app.controller('VariationController', ['$scope', 'CommonCode', '$rootScope', '$http', function ($scope, CommonCode, $rootScope, $http) {
    $scope.Variations = [];
    $scope.ParentProductId = 0;

    $scope.SetVariations = function (parentProductId, element) {

        $scope.ParentProductId = parentProductId;
        CommonCode.getBaseCookieSettings();
        $scope.Variations = [];

        if ($scope.ParentProductId != null) {
            $http({
                method: 'GET',
                url: baseUrl + 'products/GetProductVariations?parentproductId=' + $scope.ParentProductId + '&languageId=' + CommonCode.LanguageId + '&currencyId=' + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
                contentType: "application/json; charset=utf-8",
            })
                .then(function (result) {
                    if (result.data != null) {
                        if (result.data.length == 1) {
                            AddToBasket(parentProductId, result.data[0].VariationId, element);
                        }
                        else {
                            $scope.Variations = result.data;

                            var hiddenProductId = $("#variationControllerDiv").parent().find("[name='parentProductId']");
                            var hiddenVariationId = $("#variationControllerDiv").parent().find("[name='variationId']");

                            hiddenProductId.val($scope.ParentProductId);
                            hiddenVariationId.val(result.data[0].VariationId);

                            ShowModal();
                        }
                    }
                    else {
                        //TODO: OUT OF STOCK MESSAGE
                    }
                }, function (error) {


                });
        }
    }

    //Defaults 
    $scope.SetVariations($rootScope.ParentProductId, $rootScope.VariationElement);

}]);

app.filter('replace', [function () {
    return function (input, from, to) {
        if (input === undefined) {
            return;
        }
        var regex = new RegExp(from, 'g');
        return input.replace(regex, to);
    };
}]);

app.run(function ($rootScope, CommonCode, $http, $rootElement, loginService, $compile, $sce) {
    CommonCode.getBaseCookieSettings();

    $rootScope.VariationElement = null;

    $rootScope.ValidateLogin = function () {
        return loginService.validateLogin();
    }

    $rootScope.ShowModal = function (modalName) {
        if (modalName != null && modalName != "") {
            $rootScope.$applyAsync(function () {
                $rootScope.partialName = modalName;
            });
        }
    }

    $rootScope.ShowLoginModal = function () {
        $rootScope.ShowModal("login_template")

        $rootScope.SetLoginRedirect(false);
    }

    //Tries to set it, if it cannot it tries again.
    $rootScope.SetLoginRedirect = function (redirect, functionToCall) {
        setTimeout(function () {
            var loginOnlyScope = angular.element(document.getElementById('loginOnly')).scope();

            if (loginOnlyScope != null) {
                loginOnlyScope.Redirect = redirect;

                if (functionToCall != null) {
                    loginOnlyScope.FunctionToRun = functionToCall;
                }
            }
            else {
                $rootScope.SetLoginRedirect(redirect);
            }

        }, 500)
    }

    $rootScope.AddRecentlyViewed = function (parentProductId) {

        if (parentProductId != null) {

            var recentlyViewed = JSON.parse(localStorage.getItem('recentlyViewed'));

            var inArray = false;

            if (recentlyViewed != null) {
                for (var i = 0; i < recentlyViewed.length; i++) {
                    if (recentlyViewed[i].id == parentProductId) {
                        inArray = true;
                        break;
                    }
                }

                if (!inArray) {
                    if (recentlyViewed.length > 29) {
                        recentlyViewed.pop();
                    }
                }
            } else {
                recentlyViewed = [];
            }

            if (!inArray) {
                recentlyViewed.unshift({ id: parentProductId });
            }

            localStorage.setItem('recentlyViewed', JSON.stringify(recentlyViewed));
        }
    }

    $rootScope.GetProductLink = function (product) {
        if (product != null) {
            return GetProductUrlFromFullProduct(product);
        }
        else {
            return "#";
        }
    }

    $rootScope.CheckCategory = function (categoryId, isChecked) {
        if (!$rootScope.IsBusy) {
            if ($rootScope.ProductListing != null && $rootScope.ProductListing.Filter.FilteredCategories != null && $rootScope.ProductListing.Filter.FilteredCategories.length != 0) {
                $rootScope.IsBusy = true;

                var selectedCategory = $rootScope.ProductListing.Filter.FilteredCategories.find(function (i) {
                    return i.Id == categoryId;
                });

                if (selectedCategory != null) {
                    selectedCategory.IsFiltered = isChecked;
                }

                $rootScope.ApplyCategoryBrandFilter();
            }
        }
    }

    $rootScope.ApplyCategoryBrandFilter = function () {
        //Reset back to page number 1. Which also will call the filtering on category and brands
        $rootScope.ChangeQueryInURL("pageNumber", 1);
    }

    $rootScope.CheckBrand = function (brandId, isChecked) {
        if (!$rootScope.IsBusy) {
            if ($rootScope.ProductListing != null && $rootScope.ProductListing.Filter.FilteredBrands != null && $rootScope.ProductListing.Filter.FilteredBrands.length != 0) {

                $rootScope.IsBusy = true;

                var selectedBrand = $rootScope.ProductListing.Filter.FilteredBrands.find(function (i) {
                    return i.Id == brandId;
                });

                if (selectedBrand != null) {
                    selectedBrand.IsFiltered = isChecked;
                }

                $rootScope.ApplyCategoryBrandFilter();

            }
        }
    }

    $rootScope.ChangePageNumber = function (newPageNumber) {
        if (!$rootScope.IsBusy) {
            //change the URL
            if ($rootScope.currentPageNumber != newPageNumber && newPageNumber > 0 && newPageNumber <= $rootScope.ProductListing.Filter.PageCount) {
                $rootScope.IsBusy = true;
                $rootScope.ChangeQueryInURL("pageNumber", newPageNumber);
                ShowHidePageNumbers(true);
                ScrollToElement($(".product_filter"));
            }
        }
    }


    $rootScope.ChangeSortBy = function (newSortBy) {
        if (!$rootScope.IsBusy) {
            //change the URL
            if ($rootScope.sortBy != newSortBy) {
                $rootScope.IsBusy = true;
                $rootScope.ChangeQueryInURL("sortBy", newSortBy);
                ShowHideSortBy(true);
            }
        }
    }

    $rootScope.GetSortName = function () {
        if ($rootScope.ProductListing != null && $rootScope.ProductListing.Filter.SortByValuesToDisplay != null) {
            var sortValue = $rootScope.ProductListing.Filter.SortByValuesToDisplay.find(function (i) {
                return i.ValueToUse == $rootScope.sortBy;
            });

            if (sortValue != null) {
                return sortValue.ValueToShow;
            }
            else {
                return "";
            }
        }
    }

    $rootScope.ChangeIsFreedomOnly = function () {
        if (!$rootScope.IsBusy) {
            //change the URL
            //Inverse the value
            $rootScope.IsBusy = true;
            $rootScope.ChangeQueryInURL("isFreedomOnly", !$rootScope.isFreedomOnly);
        }
    }

    $rootScope.ChangeIsFlexibuyOnly = function () {
        if (!$rootScope.IsBusy) {
            $rootScope.IsBusy = true;
            //change the URL
            $rootScope.ChangeQueryInURL("isFlexibuyOnly", !$rootScope.isFlexibuyOnly);
        }
    }

    $rootScope.ClearFilter = function () {
        var url = document.location.href.split("?")[0];

        //if we are a search page get the search term
        var searchTerm = GetParameterByName("keyword");

        if (searchTerm != null && searchTerm != "") {
            url = url + "?keyword=" + searchTerm;
        }

        var searchType = GetParameterByName("type")

        $rootScope.ChangeURL(url);

        $rootScope.ProductListing.Filter.FilteredCategories = {};
        $rootScope.ProductListing.Filter.FilteredBrands = {};

        $rootScope.GetProducts();


    }

    $rootScope.StoreFavouriteList = function () {
        CommonCode.getBaseCookieSettings();

        //GET FROM WIKI
        var favouriteModel = null;
        var keepMeSignedIn = getCookie("keepMeSignedIn");
        if (keepMeSignedIn != null) {
            keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
            var baseCookieSettings = getCookie("BaseCookieSettings");
            if (baseCookieSettings != null) {
                favouriteModel = {
                    custRef: keepMeSignedIn.CustomerLookupId,
                    custEmail: keepMeSignedIn.Email,
                    languageId: CommonCode.LanguageId,
                    currencyId: CommonCode.CurrencyId,
                    countryId: CommonCode.CountryId
                };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/GetFavouritesList",
                    contentType: 'application/json',
                    data: JSON.stringify(favouriteModel)
                }).then(function (result) {
                    if (result.data != null) {
                        localStorage.setItem("userFavourites", JSON.stringify(result.data));
                    }
                });
            }
        }
        else {
            localStorage.removeItem("userFavourites");
        }
    }

    $rootScope.StoreFavouriteList();

    $rootScope.GetFavouriteList = function () {
        var favourites = null;
        if (localStorage.getItem("userFavourites") != null) {
            favourites = JSON.parse(localStorage.getItem("userFavourites"));
        }

        return favourites;
    }

    $rootScope.IsFavouriteProduct = function (parentProductId) {
        var favourites = $rootScope.GetFavouriteList();
        //Check if parentProductId is in list.
        if (favourites != null) {
            for (var i = 0; i < favourites.products.length; i++) {
                if (parentProductId == favourites.products[i].ParentProductId) {
                    return true;
                }
            }
        }
        return false;
    }

    $rootScope.IsFavouritePage = function (page) {
        var favourites = GetFavouriteList();
        //Check if page is in list.
    }

    $rootScope.ChangePerPage = function () {

        if ($rootScope.perPage == 20) {
            $rootScope.perPage = 200;
        }
        else {
            $rootScope.perPage = 20;
        }

        $rootScope.ChangeQueryInURL("perPage", $rootScope.perPage);
    }

    $rootScope.ChangeIsDownloadsOnly = function () {
        if (!$rootScope.IsBusy) {
            $rootScope.IsBusy = true;
            //change the URL
            $rootScope.ChangeQueryInURL("isDownloadsOnly", !$rootScope.isDownloadsOnly);
        }
    }

    $rootScope.ChangeIsPickAndMixOnly = function () {
        if (!$rootScope.IsBusy) {
            $rootScope.IsBusy = true;
            //change the URL
            $rootScope.ChangeQueryInURL("isPickAndMixOnly", !$rootScope.isPickAndMixOnly);
        }
    }

    $rootScope.ChangeQueryInURL = function (queryKey, queryValue) {
        if (history.pushState) {

            var newURL = window.location.href;

            if (queryKey != "pageNumber") {
                newURL = UpdateQueryStringParameter("pageNumber", 1, newURL);
            }

            //gets current query
            newURL = UpdateQueryStringParameter(queryKey, queryValue, newURL);
            $rootScope.ChangeURL(newURL);
            $rootScope.GetProducts();
        }
    }

    $rootScope.ChangeURL = function (newURL) {
        if (history.pushState) {
            window.history.pushState({ path: newURL }, '', newURL);
        }
    }

    $rootScope.IsBusy = false;

    $rootScope.IdToUse = null;
    $rootScope.URLToUse = '';


    $rootScope.checkfav = function (parentProdId) {
        if ($rootScope.favouritesProducts != null) {
            for (var j = 0; j < $rootScope.favouritesProducts.length; j++) {
                if ($rootScope.favouritesProducts[j].ParentProductId == parentProdId) {
                    return 1;
                }
            }
        }
        return 0;
    }

    //Pass in the url for wiki for the correct call. On category, brands and sales event you need to pass in an Id. Search is fine as it comes from URL.
    $rootScope.GetProducts = function (urlToUse, idToUse, isBrandAmbassador) {
        if (urlToUse != null) {
            $rootScope.URLToUse = urlToUse;
        }

        if (idToUse != null) {
            $rootScope.IdToUse = idToUse;
        }

        if (isBrandAmbassador) {
            $rootScope.IsBrandAmbassador = isBrandAmbassador;
        }

        if ($rootScope.URLToUse != null && $rootScope.URLToUse != "") {

            $rootScope.IsBusy = true;

            var pageNumber = GetParameterByName("pageNumber");
            $rootScope.currentPageNumber = pageNumber != null ? parseInt(pageNumber) : 1;

            var perPage = GetParameterByName("perPage");
            $rootScope.perPage = perPage != null ? perPage : 20;

            var isFreedomOnly = GetParameterByName("isFreedomOnly");
            $rootScope.isFreedomOnly = isFreedomOnly != null ? (isFreedomOnly.toLowerCase() == "true" ? true : false) : false;

            var isFlexibuyOnly = GetParameterByName("isFlexibuyOnly");
            $rootScope.isFlexibuyOnly = isFlexibuyOnly != null ? (isFlexibuyOnly.toLowerCase() == "true" ? true : false) : false;

            var isDownloadsOnly = GetParameterByName("isDownloadsOnly");
            $rootScope.isDownloadsOnly = isDownloadsOnly != null ? (isDownloadsOnly.toLowerCase() == "true" ? true : false) : false;

            var isPickAndMixOnly = GetParameterByName("isPickAndMixOnly");
            $rootScope.isPickAndMixOnly = isPickAndMixOnly != null ? (isPickAndMixOnly.toLowerCase() == "true" ? true : false) : false;

            var searchTerm = GetParameterByName("keyword");
            $rootScope.searchTerm = searchTerm != null ? searchTerm.toLowerCase() : null;

            var sortBy = GetParameterByName("sortBy");
            $rootScope.sortBy = sortBy != null ? sortBy : null; //Use default sort provided by wiki.

            $rootScope.filteredCategories = $rootScope.ProductListing != null ? $rootScope.ProductListing.Filter.FilteredCategories : {};
            $rootScope.filteredBrands = $rootScope.ProductListing != null ? $rootScope.ProductListing.Filter.FilteredBrands : {};

            var listingModel = {
                "LanguageId": CommonCode.LanguageId,
                "CurrencyId": CommonCode.CurrencyId,
                "CountryId": CommonCode.CountryId,
                "ShowFreedomOnly": $rootScope.isFreedomOnly,
                "ShowFlexibuyOnly": $rootScope.isFlexibuyOnly,
                "ShowDownloadsOnly": $rootScope.isDownloadsOnly,
                "ShowPickAndMixOnly": $rootScope.isPickAndMixOnly,
                "PageNumber": $rootScope.currentPageNumber,
                "PageSize": $rootScope.perPage,
                "SortByValue": $rootScope.sortBy,
                "FilteredCategories": $rootScope.filteredCategories,
                "FilteredBrands": $rootScope.filteredBrands,
                "SearchTerm": $rootScope.searchTerm,
                "IdToUse": $rootScope.IdToUse,
                "IsBrandAmbassador": $rootScope.IsBrandAmbassador
            }
            $http({
                method: 'POST',
                url: baseUrl + $rootScope.URLToUse,
                contentType: 'application/json',
                data: JSON.stringify(listingModel)
            }).then(function (result) {
                if (result.data != null) {
                    if (result.data.Products != null) {
                        var favourites = JSON.parse(localStorage.getItem("userFavourites"));

                        if (favourites != null) {
                            $rootScope.favouritesProducts = favourites.products;
                            for (var i = 0; i < result.data.Products.length; i++) {
                                if ($rootScope.checkfav(result.data.Products[i].ParentProductId)) {
                                    result.data.Products[i].IsFav = 1;
                                }
                                else {
                                    result.data.Products[i].IsFav = 0;
                                }
                            }
                        }
                        else {
                            for (var i = 0; i < result.data.Products.length; i++) {
                                result.data.Products[i].IsFav = null;
                            }
                        }
                    }

                    if (result.data.Filter != null && result.data.Products != null && result.data.Filter.Categories != null && result.data.Filter.Brands != null) {
                        $rootScope.sortBy = result.data.Filter.SortByValue;

                        $rootScope.ProductListing = result.data;

                        $rootScope.PageNumbers = [];
                        $rootScope.PageSizeDisplay = result.data.Filter.PageSize == 20 ? 200 : 20;

                        for (i = 1; i <= $rootScope.ProductListing.Filter.PageCount; i++) {
                            $rootScope.PageNumbers.push(i);
                        }

                        //check if we are the search page. If so we need to set brands
                        var searchController = angular.element(document.getElementById("SearchResultController")).scope();

                        if (searchController != null) {
                            searchController.filteredBrandsData = result.data.Filter.Brands;
                        }
                    }
                }
                else {
                    //Show we have no products
                    $rootScope.ProductListing = {
                        "Filter": {
                            "PageCount": 0
                        }
                    }
                }

                setTimeout(function () {
                    $rootScope.$apply(function () {
                        //SCOTT COMMENT THIS LINE BELOW TO TEST HOLDING IMAGES
                        $rootScope.IsBusy = false;
                        FilterToggle();
                    });
                }, 250);
            });
        }
    }


    //NO LONGER NEEDED. TODO: Remove
    $rootScope.GetMetadata = function (isBrand, isCategory, isProduct, pageName, idToUse) {
        //var head = document.getElementsByTagName('head')[0];
        //var script = document.createElement('script');
        //script.type = 'text/javascript';
        //script.src = 'https://www.googletagmanager.com/gtag/js?id=UA-114429516-1';
        //head.appendChild(script);

        //var script_tag_dl = document.createElement('script');
        //script_tag_dl.type = '';
        //script_tag_dl.text = 'window.dataLayer = window.dataLayer || [];function gtag() { dataLayer.push(arguments); }gtag("js", new Date());gtag("config", "UA-114429516-1");';
        //document.head.appendChild(script_tag_dl);

        //var script_tag_dl2 = document.createElement('script');
        //script_tag_dl2.type = '';
        //script_tag_dl2.text = '(function (w, d, s, l, i) {w[l] = w[l] || []; w[l].push({"gtm.start":new Date().getTime(), event: "gtm.js"}); var f = d.getElementsByTagName(s)[0],j = d.createElement(s), dl = l != "dataLayer" ? "&l=" + l : ""; j.async = true; j.src ="https://www.googletagmanager.com/gtm.js?id=" + i + dl; f.parentNode.insertBefore(j, f);})(window, document, "script", "dataLayer", "GTM-M53DWBR");';
        //document.head.appendChild(script_tag_dl2);
    }


    $rootScope.PopulateQuickLookModel = function (ppid, isUpsell) {

        if (isUpsell == null) {
            isUpsell = false
        }

        var productParams = {
            parentProductId: ppid,
            languageId: CommonCode.LanguageId,
            currencyId: CommonCode.CurrencyId,
            countryId: CommonCode.CountryId,
            isUpsell: isUpsell != null ? isUpsell : false
        };

        $http({
            method: 'GET',
            url: baseUrl + "Products/GetProductInformation",
            contentType: 'application/json',
            params: productParams,
        }).then(function (qproductResult) {
            if (qproductResult.data !== undefined && qproductResult.data != null) {
                $rootScope.quickLookproducts = qproductResult.data;

                if ($rootScope.quickLookproducts) {
                    var productDescription = qproductResult.data.TVDescription.replace(/[^a-z0-9]+/gi, '-');
                    $rootScope.quickLookproducts.ProductUrl = $rootScope.GetProductLink($rootScope.quickLookproducts);
                }

                $rootScope.quickLookproductsLength = $rootScope.quickLookproducts.length;
                $rootScope.ProductImageVariationId = $rootScope.quickLookproducts.DefaultVariationId;

                $rootScope.ChangeImagesQuickLook($rootScope.ProductImageVariationId);

                $rootScope.qUpsellCheckout = isUpsell ? 1 : 0;
            }
            else {
                $rootScope.quickLookproductsLength = 0;
            }

        });
    }

    $rootScope.ApplyVoucherCode = function (vouchercode) {//check if it is hitting basket controller after popup is closed

        if (vouchercode != "" && vouchercode != null && vouchercode != undefined) {

            $rootScope.selectedVoucher = vouchercode;
            var basketId = localStorage.getItem("basketId");
            var voucherParams = { BasketId: basketId, VoucherCode: $rootScope.selectedVoucher, CurrencyID: CommonCode.CurrencyId };//dynamic values
            $http({
                method: 'POST',
                url: baseUrl + "/Reservations/VoucherUpdate",
                contentType: 'application/json',
                data: JSON.stringify(voucherParams),
            }).then(function (result) {
                if (result.data !== undefined) {

                    var basketVoucherData = result.data;
                    if (basketVoucherData != null) {
                        $('#DynamicModal').hide();
                        $rootScope.$emit('modalVoucher', basketVoucherData);
                    }
                    else {
                        $rootScope.voucherApplied = -1;
                    }
                }
            });
        }
    }


    $rootScope.ShowProductModal = function (parentProductId, isUpsell) {

        if (isUpsell == null) {
            isUpsell = false;
        }

        $rootScope.$applyAsync(function () {
            $rootScope.partialName = "myproductPreview_template";
        });

        $rootScope.quicklookProductID = parentProductId;

        if (parentProductId != null) {

            $rootScope.PopulateQuickLookModel(parentProductId, isUpsell);
        }

        ShowModal();
    }

    $rootScope.CheckPickAndMix = function (item) {
        var isPickAndMix = false;
        if (typeof item.IsPickAndMix !== 'undefined') {
            if (item.IsPickAndMix) {
                isPickAndMix = true;
            }
        } else {
            if (typeof item.IsPickAndMixItem !== 'undefined') {
                if (item.IsPickAndMixItem == 1) {
                    isPickAndMix = true;
                }
            }
        }
        return isPickAndMix;
    }

    $rootScope.VariationSelectorImagesQuickLook = function (variationId) {
        $rootScope.ProductImageVariationId = variationId;
        $rootScope.ChangeImagesQuickLook($rootScope.ProductImageVariationId);
    }

    $rootScope.ShowImages = false;

    $rootScope.ChangeImagesQuickLook = function (variationId) {
        $rootScope.ShowImages = false;

        $http({
            method: 'GET',
            url: '/product/GetImageCarousel?scopeVariable=ProductImagesQuickLook&productScopeVariable=quickLookproducts',
            contentType: 'application/json',
        }).then(function (result) {
            if (result.data != undefined) {
                var imagesToUse = $rootScope.quickLookproducts.ProductImages.filter(function (image) {
                    return image.VariationId == variationId
                });

                if (imagesToUse.length == 0) {
                    imagesToUse = $rootScope.quickLookproducts.ProductImages;
                }

                //Reset the array.
                $rootScope.ProductImagesQuickLook = [];
                if (imagesToUse == null || imagesToUse.length == 0) {
                    imagesToUse.push({
                        "Filename": $rootScope.quickLookproducts.DefaultThumbnailFilename,
                        "ImageAltText": $rootScope.quickLookproducts.DefaultImageAltText,
                        "VariationId": null,
                        "DisplayOrder": 1
                    });
                }

                $rootScope.ProductImagesQuickLook = imagesToUse;

                var compiledHtml = $compile(result.data);
                var element = compiledHtml($rootScope);

                //Clear it.
                document.getElementById("productImagesQuicklook").innerHTML = "";

                //Append It.
                $("#productImagesQuicklook").append(element[0]);

                setTimeout(function () {
                    $rootScope.$applyAsync(function () {
                        $rootScope.ShowImages = true;
                    });
                }, 500);
            }
        });
    }

    $rootScope.getBrandUrl = function (brandid, brandname, isBrandAmbassador) {
        var brandAmbassadorSuffix = "";

        if (isBrandAmbassador) {
            brandAmbassadorSuffix = "b";
        }

        return "/brand/" + brandid + brandAmbassadorSuffix + "/" + GetWebSafeURL(brandname);
    }

    $rootScope.GetBasketURL = function () {
        if (GetParameterByName("isexternal") == "true") {
            return "/basket?isExternal=true";
        }
        else {
            return "/basket";
        }
    }


    $rootScope.GetCheckoutDeliveryURL = function () {
        if (GetParameterByName("isexternal") == "true") {
            return "/checkout?Delivery&isExternal=true";
        }
        else {
            return "/checkout?Delivery";
        }
    }

    $rootScope.FormatAddress = function (address) {
        var formattedAddress = '<ul>';

        if (address.CompanyName != null && address.CompanyName != "") {
            formattedAddress += address.CompanyName + ",</li>";
        }

        if (address.BuildingNumber != null && address.BuildingNumber != "") {
            formattedAddress += address.BuildingNumber + ",</li> ";
        }
        if (address.BuildingName != null && address.BuildingName != "") {
            formattedAddress += address.BuildingName + ",</li>";
        }

        if (formattedAddress.substring(formattedAddress.length - 1) != " ") {
            formattedAddress += "<li>";
        }

        if (address.StreetAddress1 != null && address.StreetAddress1 != "") {
            formattedAddress += address.StreetAddress1 + ",</li>";
        }

        if (address.StreetAddress2 != null && address.StreetAddress2 != "") {
            formattedAddress += "<li>" + address.StreetAddress2 + ",</li>";
        }

        if (address.StreetAddress3 != null && address.StreetAddress3 != "") {
            formattedAddress += "<li>" + address.StreetAddress3 + ",</li>";
        }

        //if (address.StreetAddress4 != null && address.StreetAddress4 != "") {
        //    formattedAddress += "<li>" + address.StreetAddress4 + ",</li>";
        //}

        if (address.Town != null && address.Town != "") {
            formattedAddress += "<li>" + address.Town + ",</li>";
        }

        if (address.City != null && address.City != "") {
            formattedAddress += "<li>" + address.City + ",</li>";
        }

        if (address.Postcode != null && address.Postcode != "") {
            formattedAddress += "<li>" + address.Postcode + ",</li>";
        }

        if (address.Country != null && address.Country != "") {
            formattedAddress += "<li>" + address.Country + ".</li>";
        }

        if (formattedAddress.substring(formattedAddress.length - 6, formattedAddress.length - 5) == ",") {
            formattedAddress = formattedAddress.substring(0, formattedAddress.length - 6) + "</li>";
        }

        formattedAddress += '</ul>'

        return $sce.trustAsHtml(formattedAddress);
    }

    $rootScope.FbPixelScript = function (orderItems, currencyCode, fbpixelScriptype) {
        var fbpixelScript = [];

        var fbPixelList = PopulateFBPixelData(orderItems, currencyCode, fbpixelScriptype);
        var typeOfScript = GetFBPixelScriptType(fbpixelScriptype);
        var fbContentTypes = "";

        for (var i = 0; i < fbPixelList.length; i++) {
            var item = fbPixelList[i];
            if (fbpixelScriptype == 1) {
                fbpixelScript.push("content_name: '" + item.Product_Name + "',  product_sku: '" + item.Product_Sku + "',  content_ids: ['" + item.Product_ID + "'], content_type: 'product', value: " + item.Product_Price + ", quantity: " + item.Quantity + ",  currency: '" + item.Currency + "'");
            } else {
                fbContentTypes += "{'id': '" + item.Product_ID + "','content_name': \"" + item.Product_Name + "\",  'product_sku': '" + item.Product_Sku + "','quantity': " + item.Quantity + ",'item_price': " + item.Product_Price + " },";
            }
        }
        if (fbpixelScriptype != 1) {
            var total = 0;
            var currencyName = currencyCode;
            for (var i = 0; i < orderItems.length; i++) {
                if (fbpixelScriptype == 2) {
                    total += orderItems[i].UnitPrice;
                } else {
                    total += orderItems[i].ItemPrice;
                }
            }
            fbpixelScript.push("<script>fbq('track', '" + typeOfScript + "',{contents: [" + fbContentTypes + "], content_type: 'product',value:" + total + ",currency:'" + currencyName + "' });</script>");
        }
        return fbpixelScript;
    }

    var GetFBPixelScriptType = function (scriptType) {
        var scriptypeResult = "";
        switch (scriptType) {
            case 1:
                scriptypeResult = "AddToCart";
                break;
            case 2:
                scriptypeResult = "InitiateCheckout";
                break;
            case 3:
                scriptypeResult = "Purchase";
                break;
            default:
                scriptypeResult = "none";
                break;
        }
        return scriptypeResult;
    }

    var PopulateFBPixelData = function (orderItems, currencyCode, fbpixelScriptype) {
        var fbPixelList = [];
        if (orderItems != null && orderItems.length > 0) {
            for (var i = 0; i < orderItems.length; i++) {
                var pixel = {};
                if (fbpixelScriptype == 2) {
                    pixel = {
                        Product_ID: orderItems[i].ParentProductID,
                        Quantity: orderItems[i].Quantity,
                        Product_Price: orderItems[i].UnitPrice,
                        Product_Name: orderItems[i].TvDescription,
                        Product_Sku: orderItems[i].ParentProductSKU,
                        Currency: currencyCode
                    };
                } else {
                    pixel = {
                        Product_ID: orderItems[i].ParentProductID,
                        Quantity: orderItems[i].Quantity,
                        Product_Price: orderItems[i].ItemPrice,
                        Product_Name: orderItems[i].TVDescription,
                        Product_Sku: orderItems[i].ParentProductSKU,
                        Currency: currencyCode
                    };
                }
                fbPixelList.push(pixel);
            }
        }
        return fbPixelList;
    }

    //Used for things outside of angular
    $rootScope.GetBaseCookieDetails = function () {
        CommonCode.getBaseCookieSettings();

        return CommonCode;
    }

});

var openPhotoSwipe = function () {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    //CREATE EMPTY CONTAINER / ARRAY - STORE ELEMENTS
    var items = [];

    //FIND IMAGES IN PRODUCT SLIDER BY CLASS
    $.each($(".productImage"), function (index, value) {
        //FIND FIRST IMAGE (IGNORE SASHES)
        var img = $(value);

        if (img != undefined && img.attr("src") != undefined) {
            items.push(
                {
                    //CHANGE THE URL OF THE IMAGE SIZE
                    src: img.attr("src").replace("products/main/", "products/large/"),
                    w: 1500,
                    h: 1500
                });
        }
    });

    // define options (if needed)
    var options = {
        // history & focus options are disabled on CodePen
        history: false,
        focus: false,

        showAnimationDuration: 0,
        hideAnimationDuration: 0

    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};


//Taken from https://www.w3schools.com/js/js_cookies.asp
function GetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

angular.module('authInputs', []).directive('authInput', function ($compile) {
    var inputTemplate = '';
    var textTemplate = '';
    var textareaTemplate = '';

    var getTemplate = function (objType, editable) {
        var template = '';
        switch (objType) {
            case 'input':
                template = (editable) ? inputTemplate : textTemplate;
                break;
            case 'textarea':
                template = (editable) ? textareaTemplate : textTemplate;
                break;
        }
        return template;
    };

    var setupTemplates = function (attrs) {
        // Need to build in more flexibility for other input tags, and make this part better.
        // Will eventually have an array of accepted attribute names and if attrs contains a matching key
        // then inject it by building the input element in the for (var k in attrs) loop.
        // Also need to do the same for other element types.
        inputTemplate = (attrs.id) ? '<input type="' + attrs.type + '" id="' + attrs.id + '" ng-model="model" class="red" />' : '<input type="' + attrs.type + '" ng-model="model" class="red" />';
        textareaTemplate = (attrs.id) ? '<textarea ng-model="model" id="' + attrs.id + '" class="red">{{ model }}</textarea>' : '<textarea ng-model="model" class="red">{{ model }}</textarea>';
        textTemplate = '<p>{{ model }}</p>';
    };

    return {
        restrict: "E",
        replace: true,
        link: function (scope, element, attrs) {
            setupTemplates(attrs);
            scope.$watch('watch', function () {
                element.html(getTemplate(attrs.obj, Boolean(scope.$parent[attrs.canEdit])));
                $compile(element.contents())(scope);
            });
        },
        scope: {
            model: '=model',
            watch: '=canEdit'
        }
    };
});

app.directive('loadRewindStreamCarousel', [
    function () {
        return {
            replace: false,
            scope: {
                'ngStreamHtml': '='
            },
            link: function (scope, element) {
                if (scope.ngStreamHtml != null) {
                    element.html(scope.ngStreamHtml);

                    //Once loaded load in the carousel
                    $(document).ready(function () {
                        LoadProductVideoCarousel();
                    });
                }

            }
        };
    }
]);

//Function to load the failed card partial in the page on my account area, rather to redirect to checkout area...
function MyAccountCardError() {
    var rootScope = angular.element(document).scope();

    rootScope.CardError();

}