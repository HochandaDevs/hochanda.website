﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class CatergoriesModel
    {
        public int categoryid { get; set; }
        public int languageid { get; set; }
        public int belongsto { get; set; }
        public int categorylevel { get; set; }
        public int isblank { get; set; }
        public string categoryimage { get; set; }
        public int activeproducts { get; set; }
    }
}
