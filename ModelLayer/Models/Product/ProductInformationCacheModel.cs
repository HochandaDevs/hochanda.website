﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class ProductInformationCacheModel
	{
		public int ParentProductId { get; set; }
		public string ParentProductSKU { get; set; }
		public bool IsPickAndMix { get; set; }
		public int PickAndMixQuantity { get; set; }
		public bool IsFreedom { get; set; }
		public bool IsVirtualProduct { get; set; }
		public bool IsFlexibuy { get; set; }
		public int FlexibuyInstallments { get; set; }
		public bool IsBundle { get; set; }
		public int? FeefoAveragePercentage { get; set; }
		public int? FeefoTotalReviews { get; set; }
		public decimal? FeefoAverageRating { get; set; }
		public int BrandId { get; set; }
		public string BrandName { get; set; }
		public List<int> AllLanguages { get; set; }
		public List<ProductPriceModel> Prices { get; set; }
		public Dictionary<int,string> AllCurrenciesAndCultures { get; set; }
		public List<int> AllCountries { get; set; }
		public Dictionary<int,string> AllTVDescriptionsWithLanguages { get; set; }
		public Dictionary<int,string> AllWebTextWithLanguages { get; set; }
		public Dictionary<int,string> DefaultImageAltTextWithLangauges { get; set; }
		public string DefaultImageFilename { get; set; }
		public string DefaultThumbnailFilename { get; set; }
		public string FreedomSashFilename { get; set; }
		public string FlexibuySashFilename { get; set; }
		public string LargeFreedomSashFilename { get; set; }
		public string LargeFlexibuySashFilename { get; set; }
		public string FreedomBannerFilename { get; set; }
		public string FlexibuyBannerFilename { get; set; }
		public string ODSBannerFilename { get; set; }
		public int DefaultVariationId { get; set; }
		public int VariationCount { get; set; }
		public List<ProductVariationModel> ProductVariations { get; set; }
		public List<ProductImagesModel> ProductImages { get; set; }
		public int StockLevel { get; set; }
		public int CategoryLevel1Id { get; set; }
		public int CategoryLevel2Id { get; set; }
		public int CategoryLevel3Id { get; set; }
		public int CategoryLevel4Id { get; set; }
		public int CategoryLevel5Id { get; set; }
		public DateTime CreatedDate { get; set; }
		public bool Active { get; set; }
		public DateTime ProductLastUpdated { get; set; }
        public int IsFav { get; set; }

		public bool UpsellOnly { get; set; }
        public string YoutubeVideo { get; set; }
    }
}
