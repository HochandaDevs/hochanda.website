﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class ProductDetailsModel
    {
        public int ParentProductId { get; set; }
        public string ParentProductSKU { get; set; }
        public List<int> RefineCategoryIds { get; set; }
        public Dictionary<int, string> FirstLevelCatergoryNames { get; set; }
        public Dictionary<int, string> SecondLevelCatergoryNames { get; set; }
        public Dictionary<int, string> ThirdLevelCatergoryNames { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public List<int> AllLanguages { get; set; }
        public Dictionary<int, string> AllCurrenciesAndCulture { get; set; }
        public List<int> AllCountries { get; set; }
        public Dictionary<int, string> TVDescription { get; set; }
        public int IsPickAndMixItem { get; set; }
        public string DefaultThumbnailFileName { get; set; }
        public int isStockCheck { get; set; }
        public int isBundleCheck { get; set; }
        /// <summary>
        /// Contains a comma seperated string for the languageIds and the image alt text. E.G 1:Test-Product, 2:Testprodukt
        /// </summary>
        public Dictionary<int, string> DefaultImageAltText { get; set; }
        public string FreedomSashFilename { get; set; }
        public string FlexibuySashFilename { get; set; }
        public int IsMembersDiscount { get; set; }
        public int VirtualProduct { get; set; }
        public Nullable<int> brandId { get; set; }
        public string brandName { get; set; }
        public int IsFlexibuy { get; set; }
        public int FlexibuyInstallments { get; set; }
        public Nullable<int> FeefoAveragePercentage { get; set; }
        public Nullable<int> FeefoTotalReviews { get; set; }
        public int IsMasterClassTicket { get; set; }
        public Nullable<decimal> FeefoAverageRating { get; set; }
        public Nullable<int> DefaultVariationId { get; set; }
        public Nullable<int> DefaultVariationCount { get; set; }
        public string ActivePriceWithCurrency { get; set; }
        public string IntroPriceWithCurrency { get; set; }
        public string UsualPriceWithCurrency { get; set; }
        public string FreedomPriceWithCurrency { get; set; }
        public string PriceWithCurrency { get; set; }

        public List<ProductPriceModelTwo> ProductPrices { get; set; }

    }
}
