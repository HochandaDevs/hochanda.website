﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.Account
{
    public interface IAccountDetailsModel
    {
        int customerid { get; set; }
        string title { get; set; }
        string firstname { get; set; }
        string lastname { get; set; }
        string dob { get; set; }
        int anonymous { get; set; }
        string phonenumber1 { get; set; }
        string phonenumber2 { get; set; }
        string emailaddress { get; set; }
    }
}
