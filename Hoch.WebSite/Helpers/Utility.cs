﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Hoch.Website.Helpers
{
    public static class Utility
    {
        public static void WriteLogMonitor(List<string> messages)
        {
			try
			{
				string filePath = ConfigurationManager.AppSettings.Get("LogMonitorPath");
				using (StreamWriter writer = new StreamWriter(filePath, true))
				{
					writer.WriteLine(Environment.NewLine + DateTime.Now.ToString() + Environment.NewLine);
					foreach (var message in messages)
					{
						writer.WriteLine(message);
					}
					writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
				}
			}
			catch
			{
				//Do nothing just do not log.

			}
        }

		public static void WriteLogMonitor(string message)
		{
			try
			{
				string filePath = ConfigurationManager.AppSettings.Get("LogMonitorPath");
				using (StreamWriter writer = new StreamWriter(filePath, true))
				{
					writer.WriteLine(Environment.NewLine + DateTime.Now.ToString() + Environment.NewLine);
					writer.WriteLine(message);
					writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
				}
			}
			catch
			{
				//Do nothing just do not log.

			}
		}

		public static string GetWebSafeString(string originalString)
		{
			if (string.IsNullOrEmpty(originalString))
			{
				return string.Empty;
			}

			var newWebSafeTitle = string.Concat(originalString.Normalize(NormalizationForm.FormD).Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));

			string pattern = "[\\~#%&!£;@¬`€$*!'{}/:<>?|\"-]";
			string replacement = "-";

			Regex regEx = new Regex(pattern);
			newWebSafeTitle = Regex.Replace(regEx.Replace(newWebSafeTitle, replacement), @"\s+", " ");

			//Just to be safe
			newWebSafeTitle = newWebSafeTitle.Replace("---", "-").Replace("--", "-");

			return newWebSafeTitle;
		}
	}
}