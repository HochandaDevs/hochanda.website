﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoch.Website.Models
{
    public class MessageCodeModel
    {
        public int ErrorCode { get; set; }
        public string ErrorTitle { get; set; }
        public string ErrorDescription { get; set; }
    }
}