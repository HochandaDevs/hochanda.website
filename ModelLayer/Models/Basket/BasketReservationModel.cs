﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class BasketReservationModel
	{
		public long BasketId { get; set; }
		public Guid ReservationId { get; set; }
		public Guid? LinkId { get; set; }
		public bool LinkActive { get; set; }
	}
}
