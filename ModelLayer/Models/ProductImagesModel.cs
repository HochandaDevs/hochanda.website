﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class ProductImagesModel
    {
        public string Filename { get; set; }
        public string ImageAltText { get; set; }

        public int VariationId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
