﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.MobileApps
{
	public class AdvertImageModel
	{
		public string AdvertName { get; set; }
		public string VoucherCode { get; set; }
		public string ImageURL { get; set; }
		public bool ShowProducts { get; set; }
		public int? EventId { get; set; }

	}
}
