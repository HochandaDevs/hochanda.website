﻿/// <reference path="../angular.min.js" />

app.controller('PostcodeController', ['$scope', '$http', '$timeout', '$httpParamSerializer', '$rootScope', 'CommonCode', '$compile', '$window', '$cookies', '$sce', function ($scope, $http, $timeout, $httpParamSerializer, $rootScope, CommonCode, $compile, $window, $cookies, $sce) {

    $scope.text = '';
    $scope.locations = [];
    $scope.addresses = [];

    $scope.houseName = '';
    $scope.companyName = '';
    $scope.houseNo = '';
    $scope.address1 = '';
    $scope.address2 = '';
    $scope.address3 = '';
    $scope.town = '';
    $scope.county = '';
    $scope.country = '';
    $scope.postcode = '';
    $scope.showFields = false;

    $('#DynamicModal').on('hide.bs.modal', function () {
        $scope.validationMessages = "<div></div>";
    });

    CommonCode.getBaseCookieSettings();
    $scope.baseCountry = CommonCode.CountryId;
    console.log('$scope.baseCountry', $scope.baseCountry);
    if ($scope.baseCountry == 1) {
        $scope.admin = false;
    } else {
        $scope.admin = true;
    }

    $scope.admin = false;
    if ($scope.baseCountry != 1) {
        $scope.country = localStorage.getItem("countrySelected");
    }

    $scope.keepMeSignedIn = getCookie("keepMeSignedIn");
    if ($scope.keepMeSignedIn != null) {
        $scope.keepMeSignedIn = JSON.parse(decodeURIComponent($scope.keepMeSignedIn));
    }

    $scope.LoqateFind = function (text, limit, countries, container) {
        var findData = {
            Text: text,
            Limit: limit,
            Countries: countries,
            Container: container
        };
        var http = $http({
            method: 'POST',
            url: baseUrl + "Settings/LoqateFind",
            contentType: 'application/json',
            data: JSON.stringify(findData)
        });
        return http;
    }

    $scope.LoqateRetrieve = function (id) {
        var retrieveData = {
            Id: id
        };
        var http = $http({
            method: 'POST',
            url: baseUrl + "Settings/LoqateRetrieve",
            contentType: 'application/json',
            data: JSON.stringify(retrieveData)
        });
        return http;
    }

    $scope.$watch('text', function (tmpStr) {
        if (!tmpStr || tmpStr.length == 0) {
            return 0;
        }
        // if searchStr is still the same..
        // go ahead and retrieve the data
        if (tmpStr === $scope.text && tmpStr.length > 2) {
            $scope.LoqateFind($scope.text, 100, 'GB', null).then(function (result) {
                $scope.locations = result.data.Items;
            });
        } else {
            $scope.locations = [];
            $scope.addresses = [];
        }
    });

    $scope.onClickAddress = function (address) {
        retrieveAddress(address);
        $scope.showFields = true;
        fillAddressFields(address);
        $scope.locations = [];
        $scope.addresses = [];
    }

    $scope.onCLick = function (location) {
        if (location.Type == "Address") {
            retrieveAddress(location);
        } else {
            searchForAddress(location);
        }
    }

    var retrieveAddress = function (location) {
        $scope.LoqateRetrieve(location.Id).then(function (result) {
            $scope.locations = [];
            fillAddressFields(result.data.Items[0]);
        });
    }

    var fillAddressFields = function (address) {
        //var addr = null;
        //if (address != undefined && address != "" && address != null) {
        //    addr = address.Label.split('/n');
        //}
        if (address.SubBuilding != "" && address.SubBuilding != null && address.SubBuilding != undefined) {
            $scope.houseName = address.SubBuilding + ', ' + address.BuildingName;
        }
        else {
            $scope.houseName = address.BuildingName;
        }

        $scope.companyName = address.Company;
        $scope.houseNo = address.BuildingNumber;
        $scope.address1 = address.Street;
        $scope.address2 = address.District;
        //$scope.address3 = address.Line5;
        //$scope.address4 = address.Line4;
        $scope.town = address.City;
        $scope.county = address.ProvinceName;
        $scope.country = address.CountryName;
        $scope.postcode = address.PostalCode;
    }

    var searchForAddress = function (location) {
        $scope.LoqateFind($scope.text, 100, 'GB', location.Id).then(function (result) {
            $scope.locations = [];
            $scope.addresses = result.data.Items;
        });
    }

    $scope.saveAddress = function () {
        var result = true;
        $scope.validationMessages = "<div>";
        var addressData = {
            customerRef: $scope.keepMeSignedIn.CustomerLookupId,
            customerEmail: $scope.keepMeSignedIn.Email,
            companyname: $scope.companyName,
            buildingname: $scope.houseName,
            buildingnumber: $scope.houseNo,
            streetaddress1: $scope.address1,
            streetaddress2: $scope.address2,
            //streetaddress3: $scope.address3,
            //streetaddress4: $scope.address4,
            town: $scope.town,
            county: $scope.county,
            country: $scope.country,
            postcode: $scope.postcode,
            countryid: $scope.baseCountry
        };

        if (addressData.streetaddress1 == null || addressData.streetaddress1 == "") {
            result = false;
            $scope.validationMessages += "<label>Address Line 1 can't be empty</label>";
            $scope.errorClass_streetaddress1 = 'errorClass';
        }
        if (addressData.town == null || addressData.town == "") {
            result = false;
            $scope.validationMessages += "<label>Town can't be empty</label>";
            $scope.errorClass_town = 'errorClass';
        }
        if (addressData.country == null || addressData.country == "") {
            result = false;
            $scope.validationMessages += "<label>Country can't be empty</label>";
            $scope.errorClass_country = 'errorClass';
        }
        if (addressData.postcode == null || addressData.postcode == "") {
            result = false;
            $scope.validationMessages += "<label>Post Code can't be empty</label>";
            $scope.errorClass_postcode = 'errorClass';
        }

        $scope.validationMessages += "</div>";


        if (result == true) {
            $http({
                method: 'POST',
                url: baseUrl + "Account/AccountSaveAddress",
                contentType: 'application/json',
                data: addressData
            }).then(function (result) {
                if (result.data !== undefined) {
                    if (result.data.MessageCode == 200) {
                        if ($window.location.href.indexOf("account/paymentmethods") > -1) {
                            var addr = result.data.ObjectId;
                            var login = $cookies.get('keepMeSignedIn');
                            if (login != null && login != undefined) {
                                login = JSON.parse(login);
                                if (login != null && login != undefined) {
                                    var userAccInfo = {
                                        CustomerLookupId: login.CustomerLookupId, Email: login.Email
                                    };
                                    $http({
                                        method: 'POST',
                                        url: baseUrl + "Account/AccountDetails",
                                        contentType: 'application/json',
                                        data: JSON.stringify({
                                            CustomerLookupId: login.CustomerLookupId, Email: login.Email
                                        })
                                    }).then(function (result) {
                                        if (result.data !== undefined) {
                                            var info = {
                                                CustomerLastname: result.data.lastname,
                                                CustomerFirstname: result.data.firstname,
                                                CustomerEmail: result.data.emailaddress,
                                                CustomerPhoneNo: result.data.phonenumber1,
                                                CustomerCountry: $scope.country,
                                                CustomerTown: $scope.town,
                                                CustomerAddress: $scope.address1,
                                                CustomerPostcode: $scope.postcode,
                                                BasketTotal: '0.00',
                                                AddressId: addr,
                                                pageId: '1'
                                            };

                                            $rootScope.selectedBillingAddress =
                                            [{
                                                'AddressId': addr,
                                                'BuildingNumber': $scope.houseNo,
                                                'StreetAddress1': $scope.address1,
                                                'City': $scope.town,
                                                'Postcode': $scope.postcode
                                            }];

                                            $http({
                                                method: 'POST',
                                                url: "/checkout/CreateXml",
                                                data: info,
                                            }).then(function (result) {
                                                if (result.data !== undefined) {
                                                    var accountController = angular.element(document.getElementById("accountPaymentController")).scope();
                                                    accountController.billingDiv = false;
                                                    $rootScope.MyAccountpaymentPage = $sce.trustAsResourceUrl(result.data);

                                                    $rootScope.ShowiFrame = true;
                                                    $rootScope.IsCardError = false;

                                                    HideModal();
                                                }
                                            });
                                        }
                                    })
                                }

                            }
                        }
                        else if ($window.location.href.toLowerCase().indexOf("/account/addressbook") > -1) {
                            $window.location.reload();
                        }
                        else {
                            $rootScope.$emit('modalAddAddress', result.data.ObjectId);
                            $scope.houseName = '';
                            $scope.companyName = '';
                            $scope.houseNo = '';
                            $scope.address1 = '';
                            $scope.address2 = '';
                            $scope.address3 = '';
                            $scope.town = '';
                            $scope.county = '';
                            $scope.country = '';
                            $scope.postcode = '';
                            $scope.showFields = false;
                            $scope.text = '';
                            HideModal();
                        }



                    }

                }
            });
        }
    }

}]);