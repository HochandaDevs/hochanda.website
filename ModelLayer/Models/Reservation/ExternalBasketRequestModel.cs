﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class ExternalBasketRequestModel
	{
		public Guid ExternalBasketLinkId { get; set; }
	}
}
