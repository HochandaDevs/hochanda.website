﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class ProductsInTheShowCacheModel
    {
        public ProductsInTheShowCacheModel()
        {
            ProductVariations = new List<ProductVariationModel>();
            ProductImages = new List<ProductImagesModel>();

			//Always default to true.
			InStock = true;
        }

        public int AuctionHour { get; set; }
        public int ParentProductId { get; set; }
        public string ParentProductSKU { get; set; }
        public List<int> AllLanguages { get; set; }
        public Dictionary<int, string> TVDescription { get; set; }
        public Dictionary<int, string> AllCurrenciesAndCulture { get; set; }
        public List<int> AllCountries { get; set; }
        public Dictionary<int, string> WebText { get; set; }
        public int IsPickAndMixItem { get; set; }
        public string DefaultImageFileName { get; set; }
        public string DefaultThumbnailFileName { get; set; }
        /// <summary>
        /// Contains a comma seperated string for the languageIds and the image alt text. E.G 1:Test-Product, 2:Testprodukt
        /// </summary>
        public Dictionary<int, string> DefaultImageAltText { get; set; }
        public string FreedomSashFilename { get; set; }
        public string FlexibuySashFilename { get; set; }
        public string LargeFreedomSashFilename { get; set; }
        public string LargeFlexibuySashFilename { get; set; }
        public string FreedomBannerFilename { get; set; }
        public string FlexibuyBannerFilename { get; set; }
        public string ODSBannerFilename { get; set; }
        public int IsMembersDiscount { get; set; }
        public int IsFlexibuy { get; set; }
        public int FlexibuyInstallments { get; set; }
        public int? FeefoAveragePercentage { get; set; }
        public int? FeefoTotalReviews { get; set; }
        public decimal? FeefoAverageRating { get; set; }
        public int? DefaultVariationId { get; set; }

        public List<ProductPriceModel> ProductPrices { get; set; }

        public List<ProductVariationModel> ProductVariations { get; set; }

        public List<ProductImagesModel> ProductImages { get; set; }
		public bool InStock { get; set; }
	}
}
