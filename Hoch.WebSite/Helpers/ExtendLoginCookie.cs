﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Helpers
{
	/// <summary>
	/// Extends the login cookie by 20 minutes
	/// This should be used on any request which just gets the view for the browser. Not an AJAX request.
	/// </summary>
	public class ExtendLoginCookie : ActionFilterAttribute
	{
		public bool DoNotExtend { get; set; }

		public ExtendLoginCookie()
		{
			DoNotExtend = false;
		}

		public override void OnResultExecuted(ResultExecutedContext filterContext)
		{
			if (!DoNotExtend)
			{
				var minutesToAdd = 20; //Default
				if (ConfigurationManager.AppSettings.Get("extendCookieLength") != null)
				{
					int.TryParse(ConfigurationManager.AppSettings.Get("extendCookieLength").ToString(), out minutesToAdd);
				}

				//If we have the cookie we want to extend it every time the page refreshes or a call to the controller happens. 
				//Also making sure it has not expired
				if (filterContext.RequestContext.HttpContext.Request.Cookies["keepMeSignedIn"] != null)
				{
					if (string.IsNullOrEmpty(filterContext.RequestContext.HttpContext.Request.Cookies["keepMeSignedIn"].Value))
					{
						filterContext.RequestContext.HttpContext.Response.Cookies["keepMeSignedIn"].Expires = DateTime.Now.AddDays(-10);
						filterContext.RequestContext.HttpContext.Response.Cookies["keepMeSignedIn"].Value = null;
					}
					else
					{
						filterContext.RequestContext.HttpContext.Response.Cookies["keepMeSignedIn"].Expires = DateTime.Now.AddMinutes(minutesToAdd);
						filterContext.RequestContext.HttpContext.Response.Cookies["keepMeSignedIn"].Value = filterContext.RequestContext.HttpContext.Request.Cookies["keepMeSignedIn"].Value;
					}
				}
				else
				{

				}

			}

			base.OnResultExecuted(filterContext);

		}
	}
}