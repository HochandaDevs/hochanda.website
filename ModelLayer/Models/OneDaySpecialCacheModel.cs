﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class OneDaySpecialCacheModel
    {
        public int ParentProductId { get; set; }
        public string ParentProductSKU { get; set; }
        public List<int> AllLanguages { get; set; }
        public Dictionary<int,string> TVDescription { get; set; }
        public Dictionary<int,string> AllCurrenciesAndCulture { get; set; }
        public Dictionary<int,string> WebText { get; set; }
        public int IsPickAndMixItem { get; set; }
        public string DefaultImageFileName { get; set; }
        public string DefaultThumbnailFileName { get; set; }
        public Dictionary<int,string> DefaultImageAltText { get; set; }
        public string FreedomSashFilename { get; set; }
        public string FlexibuySashFilename { get; set; }
        public int IsMembersDiscount { get; set; }
        public int IsFlexibuy { get; set; }
        public int FlexibuyInstallments { get; set; }
        public int? FeefoAveragePercentage { get; set; }
        public int? FeefoTotalReviews { get; set; }
        public decimal? FeefoAverageRating { get; set; }
        public int? DefaultVariationId { get; set; }
        public List<int> AllCountries { get; set; }
        public bool ODSInStock { get; set; }

        public List<ProductPriceModel> ProductPrices { get; set; }
    }
}
