﻿using BraintreeHttp;
using PayPalCheckoutSdk.Core;
using System;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Hoch.Website.Models.PayPal
{
    public class PayPalClient
    {
        /**
            Setting up PayPal environment with credentials with sandbox cerdentails. 
            For Live, this should be LiveEnvironment Instance. 
         */
        public static PayPalEnvironment environment()
        {
            string isDev = ConfigurationManager.AppSettings.Get("IsDev") != null ? ConfigurationManager.AppSettings.Get("IsDev") : "1";
            if (isDev == "1")
            {
                return new SandboxEnvironment(ConfigurationManager.AppSettings.Get("PaypalSandboxClientId"), ConfigurationManager.AppSettings.Get("PaypalSandboxClientSecret"));
            }
            else
            {
                return new LiveEnvironment(ConfigurationManager.AppSettings.Get("PaypalLiveClientId"), ConfigurationManager.AppSettings.Get("PaypalLiveClientSecret"));
            }
        }

        /**
            Returns PayPalHttpClient instance which can be used to invoke PayPal API's.
         */
        public static HttpClient client()
        {
            return new PayPalHttpClient(environment());
        }

        public static HttpClient client(string refreshToken)
        {
            return new PayPalHttpClient(environment(), refreshToken);
        }

        /**
            This method can be used to Serialize Object to JSON string.
        */
        public static String ObjectToJSONString(Object serializableObject)
        {
            MemoryStream memoryStream = new MemoryStream();
            var writer = JsonReaderWriterFactory.CreateJsonWriter(
                        memoryStream, Encoding.UTF8, true, true, "  ");
            DataContractJsonSerializer ser = new DataContractJsonSerializer(serializableObject.GetType(), new DataContractJsonSerializerSettings { UseSimpleDictionaryFormat = true });
            ser.WriteObject(writer, serializableObject);
            memoryStream.Position = 0;
            StreamReader sr = new StreamReader(memoryStream);
            return sr.ReadToEnd();
        }
    }
}