﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class CustomerCreditModel
    {
        public decimal? BillAmount { get; set; }
        public decimal? CreditValueTotal { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal? CreditValueFree { get; set; }
        public decimal? CreditValueMinSpend { get; set; }
    }
}
