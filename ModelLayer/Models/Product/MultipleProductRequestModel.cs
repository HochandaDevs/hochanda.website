﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class MultipleProductRequestModel
	{
		public MultipleProductRequestModel()
		{
			ParentProductIds = new List<int>();
		}

		public List<int> ParentProductIds { get; set; }
		public int LanguageId { get; set; }
		public int CurrencyId { get; set; }
		public int CountryId { get; set; }
	}
}
