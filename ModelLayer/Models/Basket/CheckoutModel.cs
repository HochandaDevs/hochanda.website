﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class CheckoutModel
	{
		public long BasketId { get; set; }
		public string BasketUrl { get; set; }
		public string ErrorMessage { get; set; }
	}
}
