﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class ItemToUpdateModel
	{
		public ItemToUpdateModel()
		{
			//Defaults
			PickAndMixIdentifier = 0;
			IsOrderThroughApps = false;
			BasketId = null;
		}

		public int ParentProductId { get; set; }
		public int VariationId { get; set; }
		public int Quantity { get; set; }
		public int LanguageId { get; set; }
		public int CurrencyId { get; set; }
		public int CountryId { get; set; }
		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse { get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}
		public int PickAndMixIdentifier { get; set; }
		public bool IsOrderThroughApps { get; set; }
		public int? ReservationItemId { get; set; }
	}
}
