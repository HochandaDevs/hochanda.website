﻿using Hoch.Website.App_Start;
using Hoch.Website.Controllers.Errors;
using Hoch.Website.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Hoch.Website
{
	public class MvcApplication : System.Web.HttpApplication
	{
		public static string ExceptionMessage { get; set; }
		public static string ExceptionObject { get; set; }
		public static string PreviousUrl { get; set; }
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			MvcHandler.DisableMvcResponseHeader = true;

			//Removed the task so we know we have data ready for first page to load.
			CacheConfig.CreateMetadataCache();
		}

		protected void Application_PreSendRequestHeaders()
		{
			Response.Headers.Remove("X-Powered-By");
			Response.Headers.Remove("X-AspNet-Version");
			Response.Headers.Remove("X-AspNetMvc-Version");
		}

		protected void Application_Error()
		{

		}

		protected void Application_BeginRequest()
		{

		}

		protected void Application_EndRequest()
		{
			var context = new HttpContextWrapper(Context);

			if (context.Response.StatusCode == 400)
			{
				context.Response.Redirect("~/Errors/BadRequest");
			}
			else if (context.Response.StatusCode == 401)
			{

				context.Response.Redirect("~/Errors/Unauthorized");
			}
			else if (context.Response.StatusCode == 403)
			{

				context.Response.Redirect("~/Errors/Forbidden");
			}
			else if (context.Response.StatusCode == 404)
			{
				string requestUrl = context.Request.RawUrl;
				PreviousUrl = requestUrl;
				string campaignName = !string.IsNullOrEmpty(requestUrl) ? requestUrl.Split('/').Last().ToString() : "";

				context.Response.Redirect("~/Campaign?&campaignName=" + campaignName);
			}
			else if (context.Response.StatusCode == 500)
			{
				Exception exceptionMessage = Server.GetLastError();
				Server.ClearError();
				ExceptionMessage = exceptionMessage.Message;
				ExceptionObject = exceptionMessage.ToString();
				Response.RedirectToRoute(new { Controller = "Errors", Action = "Generic" });
			}
			else if (context.Response.StatusCode == 503)
			{

				context.Response.Redirect("~/Errors/ServiceUnavailable");
			}
		}
	}
}
