﻿/// <reference path="../angular.js" />
app.controller("registerCtlr", ['$scope', '$http', '$compile', 'vcRecaptchaService', 'CommonCode', '$window', '$rootScope', '$cookies', function ($scope, $http, $compile, vcRecaptchaService, CommonCode, $window, $rootScope, $cookies) {
    $scope.RegisterPageHide = true;
    CommonCode.getBaseCookieSettings();


    var signinData = $cookies.get("keepMeSignedIn");
    if (signinData == null) {
        $scope.RegisterPageHide = false;
        $scope.errorFromWiki = false;
        $scope.Register = {};

        $scope.getRecaptchaSiteKey = function () {
            $http({
                method: 'GET',
                url: baseUrl + "Settings/GetRecaptchaSiteKey",
                contentType: 'application/json'
            }).then(function (result) {
                $scope.siteKey = result.data;
            });
        }
        $scope.getRecaptchaSiteKey();

        $scope.Register.emailMarketing = false;
        $scope.Register.emailMarketing3rdParties = false;
        $scope.Register.postMarketing = false;
        $scope.Register.smsMarketing = false;
        $scope.Register.termsAndConditions = false;
        $scope.Register.anonymousDisplay = false;
        CommonCode.getBaseCookieSettings();
        $scope.UserCreateValid = false;

        $scope.Register.DayId = 1;
        $scope.Register.Days = [{ id: 1, label: '1' }, { id: 2, label: '2' },
        { id: 3, label: '3' }, { id: 4, label: '4' },
        { id: 5, label: '5' }, { id: 6, label: '6' },
        { id: 7, label: '7' }, { id: 8, label: '8' },
        { id: 9, label: '9' }, { id: 10, label: '10' },
        { id: 11, label: '11' }, { id: 12, label: '12' },
        { id: 13, label: '13' }, { id: 14, label: '14' },
        { id: 15, label: '15' }, { id: 16, label: '16' },
        { id: 17, label: '17' }, { id: 18, label: '18' },
        { id: 19, label: '19' }, { id: 20, label: '20' },
        { id: 21, label: '21' }, { id: 22, label: '22' },
        { id: 23, label: '23' }, { id: 24, label: '24' },
        { id: 25, label: '25' }, { id: 26, label: '26' },
        { id: 27, label: '27' }, { id: 28, label: '28' },
        { id: 29, label: '29' }, { id: 30, label: '30' },
        { id: 31, label: '31' }]

        $scope.Register.MonthId = 1;
        $scope.Register.Months = [{ id: 1, label: 'Jan' }, { id: 2, label: 'Feb' },
        { id: 3, label: 'Mar' }, { id: 4, label: 'Apr' },
        { id: 5, label: 'May' }, { id: 6, label: 'Jun' },
        { id: 7, label: 'Jul' }, { id: 8, label: 'Aug' },
        { id: 9, label: 'Sep' }, { id: 10, label: 'Oct' },
        { id: 11, label: 'Nov' }, { id: 12, label: 'Dec' }]

        $scope.Register.phoneType = 1;
        $scope.Register.altPhoneType = 1;

        $http({
            method: 'GET',
            url: baseUrl + "Account/GetPhoneTitles",
            contentType: 'application/json'
        }).then(function (result) {
            $scope.Register.Phonetypes = result.data;
        });


        $http({
            method: 'GET',
            url: baseUrl + "Account/GetRegisterTitles",
            contentType: 'application/json',
            data: CommonCode.LanguageId
        }).then(function (result) {
            $scope.Register.TitleId = result.data[0].title;
            $scope.Register.Titles = result.data;
        });

        $scope.phoneNumbr = /^[^[a-zA-Z\s]*$/;

        $scope.onSubmit = function () {
            $scope.ValidateRegistration();
        }

        $scope.captchaExecute = function () {
            vcRecaptchaService.execute();
        }

        $scope.ValidateRegistration = function () {
            $scope.pagename = document.getElementById('pagename').value;
            document.getElementById('validationErrorDiv').innerHTML = "";
            $scope.validationMessages = "<div>";
            var title = $scope.Register.TitleId;
            var firstName = $scope.Register.firstName;
            var surName = $scope.Register.surName;
            var anonymousDisplay = $scope.Register.anonymousDisplay;
            var dob_Day = $scope.Register.DayId;
            var dob_Month = $scope.Register.MonthId;
            var phoneNumber = $scope.Register.phoneNumber;
            var phoneType = $scope.Register.phoneType;
            var altPhoneNumber = $scope.Register.altPhoneNumber;
            var altPhoneType = $scope.Register.altPhoneType;
            var emailAddress = $scope.Register.emailAddress;
            var confirmEmailAddress = $scope.Register.confirmEmailAddress;
            var emailMarketing = $scope.Register.emailMarketing;
            var emailMarketing3rdParties = $scope.Register.emailMarketing3rdParties;
            var postMarketing = $scope.Register.postMarketing;
            var smsMarketing = $scope.Register.smsMarketing;
            var newPassword = $scope.Register.newPassword;
            var confirmPassword = $scope.Register.confirmPassword;
            var termsAndConditions = $scope.Register.termsAndConditions;
            var result = true;

            var regex = "";

            if (firstName == null || firstName == "") {
                result = false;
                $scope.validationMessages += "<label>First Name can't be empty</label>"
                $scope.errorClass_firstName = 'errorClass';
            }
            else {
                regex = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9]*$/;
                if (!regex.test(firstName)) {
                    result = false;
                    $scope.validationMessages += "<label>Invalid First Name</label>"
                    $scope.errorClass_firstName = 'errorClass';
                }
            }
            if (surName == null || surName == "") {
                result = false;
                $scope.validationMessages += "<label>Surname can't be empty</label>"
                $scope.errorClass_surName = 'errorClass';
            }
            else {
                regex = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9]*$/;
                if (!regex.test(surName)) {
                    result = false;
                    $scope.validationMessages += "<label>Invalid Surname</label>"
                    $scope.errorClass_surName = 'errorClass';
                }
            }
            if (phoneNumber == null || phoneNumber == "") {
                result = false;
                $scope.validationMessages += "<label>Phone Number can't be empty</label>"
                $scope.errorClass_phoneNumber = 'errorClass';
            }
            else {
                regex = /^[^[a-zA-Z\s]*$/;
                if (!regex.test(phoneNumber)) {
                    result = false;
                    $scope.validationMessages += "<label>Invalid phone number</label>"
                    $scope.errorClass_phoneNumber = 'errorClass';
                }
            }
            if (emailAddress == null || emailAddress == "") {
                result = false;
                $scope.validationMessages += "<label>Email Address can't be empty</label>"
                $scope.errorClass_emailAddress = 'errorClass';
            }
            else {
                regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!regex.test(emailAddress)) {
                    result = false;
                    $scope.validationMessages += "<label>Invalid email Address</label>"
                    $scope.errorClass_emailAddress = 'errorClass';
                }
            }
            if (confirmEmailAddress == null || confirmEmailAddress == "") {
                result = false;
                $scope.validationMessages += "<label>Confirm Email Address can't be empty</label>"
                $scope.errorClass_confirmEmailAddress = 'errorClass';
            }
            else {
                if (emailAddress != confirmEmailAddress) {
                    result = false;
                    $scope.validationMessages += "<label>Email Addresses don't match</label>"
                    $scope.errorClass_confirmEmailAddress = 'errorClass';
                }
            }
            if (newPassword == null || newPassword == "") {
                result = false;
                $scope.validationMessages += "<label>New Password can't be empty</label>"
                $scope.errorClass_newPassword = 'errorClass';
            }
            else {
                if (newPassword.length < 8) {
                    result = false;
                    $scope.validationMessages += "<label>Password must contain at least 8 characters</label>"
                    $scope.errorClass_newPassword = 'errorClass';
                }
                regex = /[0-9]/;
                if (!regex.test(newPassword)) {
                    result = false;
                    $scope.validationMessages += "<label>Password must contain at least one number (0-9)</label>"
                    $scope.errorClass_newPassword = 'errorClass';
                }
                regex = /[a-z]/;
                if (!regex.test(newPassword)) {
                    result = false;
                    $scope.validationMessages += "<label>Password must contain at least one lowercase letter (a-z)</label>"
                    $scope.errorClass_newPassword = 'errorClass';
                }
                regex = /[A-Z]/;
                if (!regex.test(newPassword)) {
                    result = false;
                    $scope.validationMessages += "<label>Password must contain at least one uppercase letter (A-Z)</label>"
                    $scope.errorClass_newPassword = 'errorClass';
                }
            }
            if (confirmPassword == null || confirmPassword == "") {
                result = false;
                $scope.validationMessages += "<label>Confirm New Password can't be empty</label>"
                $scope.errorClass_confirmPassword = 'errorClass';
            }
            else {
                if (newPassword != confirmPassword) {
                    result = false;
                    $scope.validationMessages += "<label>Passwords don't match</label>"
                    $scope.errorClass_confirmPassword = 'errorClass';
                }
            }
            if (termsAndConditions != true) {
                result = false;
                $scope.validationMessages += "<label>Please agree to our Terms and Conditions</label>"
                $scope.errorClass_termsAndConditions = 'errorClass';
            }
            $scope.validationMessages += "</div>";

            var errorDiv = $('#validationErrorDiv');
            var htmlelement = angular.element($scope.validationMessages);
            var compileAppendedArticleHTML = $compile(htmlelement[0]);
            var element = compileAppendedArticleHTML($scope);
            errorDiv.append(element[0]);

            if (result == true) {
                //delete_cookie("keepMeSignedIn");
                //$cookies.remove('keepMeSignedIn');
                $scope.UserCreateValid = true;
                delete_cookie("keepMeSignedIn");


                $scope.captchaExecute();
            }
        }

        $scope.RegisterCustomer = function () {
            delete_cookie("keepMeSignedIn");

            if ($scope.UserCreateValid) {
                $scope.UserCreateValid = false;

                var campaignId = 1;

                var cookieValue = GetCookie("CampaignIDCookie");

                if (cookieValue != null && cookieValue != "") {
                    try {
                        campaignId = parseInt(cookieValue);
                    }
                    catch (err) {
                        //Do nothing
                    }
                }

                var registerDetails = {
                    Title: $scope.Register.TitleId,
                    FirstName: $scope.Register.firstName,
                    Surname: $scope.Register.surName,
                    DayOfBirth: $scope.Register.DayId,
                    MonthOfBirth: $scope.Register.MonthId,
                    PhoneNumber1: $scope.Register.phoneNumber,
                    PhoneNumber2: $scope.Register.altPhoneNumber,
                    EmailAddress: $scope.Register.emailAddress,
                    ConfirmEmailAddress: $scope.Register.confirmEmailAddress,
                    Password: $scope.Register.newPassword,
                    ConfirmPassword: $scope.Register.confirmPassword,
                    ReceiveEmail: $scope.Register.emailMarketing,
                    ReceivePost: $scope.Register.postMarketing,
                    ReceiveThirdPartyEmails: $scope.Register.emailMarketing3rdParties,
                    ReceiveSms: $scope.Register.smsMarketing,
                    Anonymous: $scope.Register.anonymousDisplay,
                    Phone1Type: $scope.Register.phoneType,
                    Phone2Type: $scope.Register.altPhoneType,
                    CampaignId: campaignId
                };


                $.ajax({
                    type: 'POST',
                    url: baseUrl + "Account/RegisterUserNew",
                    contentType: 'application/x-www-form-urlencoded',
                    data: registerDetails,
                    success: function (data) {
                        if (data.TitleMessage == 'Success') {
                            if (data.custRef != null || data.custRef != undefined || data.custRef || data.custRef != "") {
                                if ($scope.pagename == "fromPurchase") {

                                    var expireDate = new Date();
                                    if ($scope.persSignIn) {
                                        expireDate.setDate(expireDate.getDate() + 20);
                                    } else {

                                        expireDate.setMinutes(expireDate.getMinutes() + loginDuration);
                                    }

                                    var loginDate = new Date();
                                    $scope.persSignIn = false;
                                    var keepMeSignedIn = { CustomerLookupId: data.custRef, Email: $scope.Register.emailAddress, Value: $scope.persSignIn, loginDate: loginDate, Firstname: $scope.Register.firstName, Lastname: $scope.Register.surName };

                                    $cookies.put("keepMeSignedIn", JSON.stringify(keepMeSignedIn), { 'expires': expireDate });

                                    $rootScope.CheckoutCreateCustomer = 1;
                                    $window.location.href = "/checkout";

                                }
                                else {
                                    $window.location.href = "/Login/Index?newAccount=" + true;
                                }
                            }
                        }

                        else {
                            $scope.errorFromWiki = true;
                            var errorDiv = $('#validationErrorDiv');
                            var htmlelement = angular.element("<label>" + data.TitleMessage + "</label>");
                            var compileAppendedArticleHTML = $compile(htmlelement[0]);
                            var element = compileAppendedArticleHTML($scope);
                            errorDiv.append(element[0]);
                        }
                    }
                });
            }
        }
    }
    else {
        $scope.RegisterPageHide = true;
        $window.location.href = "/";
    }

    
}]);