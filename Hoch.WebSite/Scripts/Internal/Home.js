﻿/// <reference path="../angular.min.js" />
app.controller('LiveStreamController', ['$scope', 'CommonCode', '$sce', function ($scope, CommonCode, $sce) {
    CommonCode.getBaseCookieSettings();

    $scope.defaultStreamingId = CommonCode.DefaultStreamingId;

    if ($scope.defaultStreamingId == null) {
        $scope.defaultStreamingId = "hochandauk";
    }


    $scope.init = function (stopIE, loadOnOtherBrowsers) {
        var canRun = true;

        if (!loadOnOtherBrowsers && !detectIEExcludeEdge()) {
            canRun = false;
        }
        else if (stopIE) {
            if (detectIEExcludeEdge()) {
                canRun = false;
            }
        }

        if (canRun) {
            $scope.LiveStream = $sce.trustAsHtml("<div id=\"ssmp\" data-client=\"hochanda\" data-stream=\"" + $scope.defaultStreamingId + "\" data-type=\"live\"></div><script src=\"https://dbxm993i42r09.cloudfront.net/ss.js\"></script>");
        }
    }

}]);


app.controller('CurrentlyOnairController', ['$scope', '$http', 'CommonCode', '$interval', '$cookies', '$rootScope', function ($scope, $http, CommonCode, $interval, $cookies, $rootScope) {
    $scope.controllerName = 'CurrentlyOnair';
    $scope.productId = 0;
    $scope.product = null;
    $scope.productPath = '';

    $scope.IsLoggedIn = 0;

    var keepMeSignedIn = $cookies.get('keepMeSignedIn');

    if (keepMeSignedIn != null) {

        $scope.keepMeSignedIn = keepMeSignedIn;
        $scope.IsLoggedIn = 1;
    }

    if (window.location.href.toLowerCase().indexOf('brands') === -1 && window.location.href.toLowerCase().indexOf('category') === -1) {
        $rootScope.GetMetadata(false, false, false, 'homepage', 0);
    }

    var interval = currentItemOnAirTimer;

    $scope.AddedToBasket = 0;

    $scope.IsAuctionHour = false;

    $scope.TimesShownEnd = 0;
    $scope.ShowAuctionWait = $scope.TimesShownEnd < 2;
    $scope.ShowAuctionEnd = !$scope.ShowAuctionWait;
    $scope.IsBusy = true;


    $scope.init = function () {
        interval = currentItemOnAirTimer;

        $http({
            method: 'GET',
            url: baseUrl + "Products/GetCurrentItemOnAir?channelId=" + CommonCode.ChannelId + "&currencyId=" + CommonCode.CurrencyId + "&languageId=" + CommonCode.LanguageId + "&countryId=" + CommonCode.CountryId,
            contentType: 'application/json'
        }).then(function (result) {
            if (result.data !== undefined && result.data != null) {
                $scope.CurrentItemDetails = result.data;
                $scope.IsAuctionHour = result.data.IsAuctionHour;

                //Reset the added To Basket as we have a new item.
                if ($scope.productId != $scope.CurrentItemDetails.ParentProductId) {
                    $scope.AddedToBasket = 0;
                    $scope.TimesShownEnd = 0;
                }
                else {
                    if (result.data.IsAuctionHour && result.data.AuctionStatus == 2) {
                        $scope.TimesShownEnd++;
                    }
                }

                if (result.data.IsAuctionHour) {
                    interval = 8000;
                }

                $scope.ShowAuctionWait = $scope.TimesShownEnd > 2;
                $scope.ShowAuctionEnd = !$scope.ShowAuctionWait;

                $scope.productId = $scope.CurrentItemDetails.ParentProductId;

                if ($scope.CurrentItemDetails.ProductDetails != null) {

                    $scope.product = $scope.CurrentItemDetails.ProductDetails;
                    var fav = $rootScope.IsFavouriteProduct($scope.productId);
                    if (fav == true) {
                        $scope.product.IsFav = 1;
                    }
                    else {
                        $scope.product.IsFav = 0;
                    }

                    $scope.ProductPath = GetProductUrl($scope.CurrentItemDetails.ProductDetails.ParentProductId,
                        $scope.CurrentItemDetails.ProductDetails.ParentProductSKU,
                        $scope.CurrentItemDetails.ProductDetails.TVDescription);
                }

                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.IsBusy = false;
                    });

                }, 250);


            }
            else {
                interval = 10000;
            }

            var nextHour = ((60 - new Date().getMinutes()) * 60000) + 3000;
            if (nextHour < interval) {
                interval = nextHour;
            }

            setTimeout(function () {
                $scope.init();
            }, interval);

        }, function errorCallback(response) {

            interval = 10000;

            var nextHour = ((60 - new Date().getMinutes()) * 60000) + 3000;
            if (nextHour < interval) {
                interval = nextHour;
            }

            setTimeout(function () {
                $scope.init();
            }, interval);

        });
    }

    $scope.init();

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);

            //$rootScope.quicklookProductID = ppid;
        }
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }

}]);

app.controller('OneDaySpecialController', ['$scope', '$http', 'CommonCode', '$interval', '$rootScope', '$cookies', function ($scope, $http, CommonCode, $interval, $rootScope, $cookies) {
    $scope.controllerName = 'OneDaySpecial';
    $scope.productId = 0;
    $scope.product = null;
    $scope.productPath = '';
    $scope.showOutOfStock = false;
    $scope.ODSInStock = true;
    $scope.IsBusy = true;
    CommonCode.getBaseCookieSettings();

    var interval = oneDaySpecialTimer;

    $scope.init = function () {

        interval = oneDaySpecialTimer;

        $http({
            method: 'GET',
            url: baseUrl + "Products/GetOneDaySpecial?channelId=" + CommonCode.ChannelId + "&languageId=" + CommonCode.LanguageId + "&currencyId=" + CommonCode.CurrencyId + "&countryId=" + CommonCode.CountryId,
            contentType: 'application/json'
        }).then(function (result) {
            if (result.data !== undefined && result.data != null) {
                $scope.ODSInStock = result.data.ODSInStock;

                $scope.productId = result.data.ParentProductId;
                $scope.product = result.data;
                var descForImage = eval(JSON.stringify(result.data.TVDescription.replace(/[^a-z0-9]+/gi, '-').replace(/^-+/, '').replace(/-+$/, '')));

                $scope.ProductPath = GetProductUrl($scope.product.ParentProductId, $scope.product.ParentProductSKU, $scope.product.TVDescription);

                if ($scope.product.IsPickAndMixItem != null) {
                    $scope.product.IsPickAndMix = $scope.product.IsPickAndMixItem;
                }

                if (!result.data.ODSInStock) {
                    $scope.showOutOfStock = true;
                    interval = 60000;
                }

                var nextHour = ((60 - new Date().getMinutes()) * 60000) + 3000;
                if (nextHour < oneDaySpecialTimer) {
                    interval = nextHour;
                }


            }
            else {
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ODSInStock = false;
                    });

                }, 250);

                interval = 60000;
            }

            setTimeout(function () {
                $scope.init();
            }, interval);

            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.IsBusy = false;
                });

            }, 250);

        }, function errorCallback(response) {
            interval = 10000;

            setTimeout(function () {
                $scope.init();
            }, interval);
        });
    }

    $scope.init();

    $scope.setPartialName = function (name, partialCheck, ppid) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);
        }
    }
}]);

app.controller('ProductsInShowController', ['$scope', '$http', 'CommonCode', '$interval', '$rootScope', '$timeout', '$compile', function ($scope, $http, CommonCode, $interval, $rootScope, $timeout, $compile) {
    $scope.IsLoggedIn = 0;
    //var custRef;
    //var custEmail;
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        keepMeSignedIn = JSON.parse(decodeURIComponent(keepMeSignedIn));
        //custRef = keepMeSignedIn.CustomerLookupId;
        //custEmail = keepMeSignedIn.Email;
        $scope.IsLoggedIn = 1;
    }

    $scope.commonCode = CommonCode;
    $scope.IsBusy = true;
    $scope.products = [{}, {}, {}, {}];
    $scope.isInit = true;
    CommonCode.getBaseCookieSettings();
    var changeEndTimeout = null;

    var interval = productsInThisShowTimer;

    $scope.init = function () {

        interval = productsInThisShowTimer;

        $http({
            method: 'GET',
            url: baseUrl + "ProductsInShow/GetProductsInTheShow?channelId=" + CommonCode.ChannelId + "&languageId=" + CommonCode.LanguageId + "&currencyId=" + CommonCode.CurrencyId + "&countryId=" + CommonCode.CountryId,
            contentType: 'application/json'
        }).then(function (result) {
            if (result.data !== undefined && result.data != null) {
                /* new implementation */
                //Get the minutes to next hour, the convert to MS and add 30 seconds.
                var nextHour = ((60 - new Date().getMinutes()) * 60000) + 7500;

                if (nextHour < interval) {
                    interval = nextHour;
                }

                if (!CommonCode.jsonEqual(result.data, $scope.products) || $scope.isInit) {
                    $scope.products = result.data;

                    angular.forEach($scope.products, function (value, key) {
                        value.IsPickAndMix = value.IsPickAndMixItem == 1;
                    });


                    var checkFavTrue;
                    var favourites = JSON.parse(localStorage.getItem("userFavourites"));
                    if (favourites != null && $scope.products != null) {
                        $scope.favouritesProducts = favourites.products;
                        for (var i = 0; i < $scope.products.length; i++) {
                            if ($scope.checkfav($scope.products[i].ParentProductId)) {
                                $scope.products[i].IsFav = 1;
                            }
                            else {
                                $scope.products[i].IsFav = 0;
                            }
                        }
                    }

                    $scope.isInit = false;
                    $http({
                        method: "GET",
                        url: '/Product/LoadCarousel?productScopeName=products',
                        contentType: "text/plain",
                        dataType: "html"
                    }).then(function (data) {
                        var myEl = angular.element(document.querySelector('#HomeProductsInTheShow'));
                        myEl.empty();
                        $rootScope.$broadcast('owlCarousel.changeStart');
                        var partial = document.getElementById("HomeProductsInTheShow");
                        var htmlelement = angular.element(data.data);
                        var compileAppendedArticleHTML = $compile(htmlelement[0]);
                        var element = compileAppendedArticleHTML($scope);
                        partial.appendChild(element[0]);
                        changeEndTimeout = $timeout(function () {
                            $rootScope.$broadcast('owlCarousel.changeEnd');
                        }, 1000);

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.IsBusy = false;
                            });

                        }, 250);

                        setTimeout(function () {
                            $scope.init();
                        }, interval);
                    });
                }
            }
            else {
                interval = 60000;

                setTimeout(function () {
                    $scope.init();
                }, interval);
            }
        }, function errorCallback(response) {

            interval = 10000;
            setTimeout(function () {
                $scope.init();
            }, interval);

        });
    }

    $scope.init();

    $scope.checkfav = function (parentProdId) {
        if ($scope.favouritesProducts != null) {
            for (var j = 0; j < $scope.favouritesProducts.length; j++) {
                if ($scope.favouritesProducts[j].ParentProductId == parentProdId) {
                    return 1;
                }
            }
        }
        return 0;
    }

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        $rootScope.partialCheck = partialCheck;
        $rootScope.quicklookProductID = ppid;
        if (ppid != null && ppid != undefined) {

            $rootScope.PopulateQuickLookModel(ppid);

            //$rootScope.quicklookProductID = ppid;
        }
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }
}]);

app.controller('HomepageVideoHeroes', ['$scope', "$sanitize", "CommonCode", "$http", "$sce", function ($scope, $sanitize, CommonCode, $http, $sce) {

    $scope.GetHeroHtml = function () {
        if ($scope.Hero != null) {
            return $sce.trustAsHtml($scope.Hero);
        }

        return null;
    };

    $http({
        method: 'GET',
        url: baseUrl + 'heroes/GetHomepageVideoFeatures?companyId=' + CommonCode.CompanyId + '&languageId=' + CommonCode.LanguageId + '&currencyId=' + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
        contentType: "application/json; charset=utf-8",
    }).then(function (result) {
        if (result.data != null) {
            $scope.Hero = null;

            var htmlToUse = '';

            angular.forEach(result.data, function (value, key) {
                htmlToUse += value.AdvertHTML;
            });

            if (htmlToUse != '') {
                $scope.Hero = htmlToUse;
            }
        }
        else {
            $scope.Hero = null;
        }

    }, function errorCallback(error) {

    });

}]);

app.controller('HomepageFeaturedAreas', ['$scope', "$sanitize", "CommonCode", "$http", "$sce", function ($scope, $sanitize, CommonCode, $http, $sce) {

    $scope.GetHeroHtml = function () {
        if ($scope.Hero != null) {
            return $sce.trustAsHtml($scope.Hero);
        }

        return null;
    };

    $http({
        method: 'GET',
        url: baseUrl + 'heroes/GetHomepageFeaturedAreas?companyId=' + CommonCode.CompanyId + '&languageId=' + CommonCode.LanguageId + '&currencyId=' + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
        contentType: "application/json; charset=utf-8",
    }).then(function (result) {
        if (result.data != null) {
            $scope.Hero = null;

            var htmlToUse = '';

            angular.forEach(result.data, function (value, key) {
                htmlToUse += value.AdvertHTML;
            });

            if (htmlToUse != '') {
                $scope.Hero = htmlToUse;
            }
        }
        else {
            $scope.Hero = null;
        }

    }, function errorCallback(error) {

    });
}]);

app.controller('NowNextShowsController', ['$scope', '$http', 'CommonCode', '$interval', '$filter', function ($scope, $http, CommonCode, $interval, $filter) {
    $scope.showOnNow = null;
    $scope.showOnNext = null;
    $scope.nextTvSchedule = '';
    $scope.nowTvSchedule = '';
    CommonCode.getBaseCookieSettings();
    var interval = fiveMinsInMs;

    $scope.init = function () {
        $http({
            method: 'GET',
            url: baseUrl + "TVSchedule/GetNowAndNextTVScheduleDetailsByChannelId?channelId=" + CommonCode.ChannelId,
            contentType: 'application/json',
        }).then(function (result) {
            if (result.data !== undefined) {

                $scope.showOnNow = result.data.OnNow;
                $scope.showOnNext = result.data.OnNext;
                $scope.nowTvSchedule = $filter('date')($scope.showOnNow.ShowDateTime, "HHa");
                $scope.nextTvSchedule = $filter('date')($scope.showOnNext.ShowDateTime, "HHa");

                //Get the minutes to next hour, the convert to MS and add 15 seconds.
                interval = ((60 - new Date().getMinutes()) * 60000) + 15000;

                setTimeout(function () {
                    $scope.init();
                }, interval);

            }
        }, function errorCallback(response) {
            interval = 60000;

            setTimeout(function () {
                $scope.init();
            }, interval);
        });
    }

    $scope.init();


}]);

app.controller('FeaturedBrandsController', ['$scope', 'CommonCode', '$http', '$filter', function ($scope, CommonCode, $http, $filter) {
    CommonCode.getBaseCookieSettings();
    $scope.featuredBrands = [];

    $http({
        method: 'GET',
        url: baseUrl + "featuredbrands/GetFeaturedBrands?languageId=" + CommonCode.LanguageId,
        contentType: 'application/json',
    }).then(function (result) {
        if (result.data !== undefined && result.data != null) {
            $scope.featuredBrands = $filter('orderBy')(result.data.slice(0, 9), 'Feature');
        }
    });
}]);