﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.Account
{
    public interface IAccountMessageModel
    {
        int MessageCode { get; set; }
        int ObjectId { get; set; }
        string TitleMessage { get; set; }
        string MessageDescription { get; set; }
        string MessageToDisplay { get; set; }
        string OKMessage { get; set; }
        string CancelMessage { get; set; }
      
    }
}
