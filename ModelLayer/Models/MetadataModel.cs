﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class MetadataModel
	{
		public int PageId { get; set; }
		public string PageFriendlyName { get; set; }
		public Dictionary<int, string> AllKeywordsWithLanguages { get; set; }
		public Dictionary<int, string> AllDescriptionsWithLanguages { get; set; }
		public Dictionary<int, string> AllTitlesWithLanguages { get; set; }
		public int Id { get; set; }
		public bool IsBrand { get; set; }
		public bool IsCategory { get; set; }
		public bool IsProduct { get; set; }
		public bool IsGeneral { get; set; }
		public string MetaImage { get; set; }
	}

	public class MetadataTag
	{
		public string Keyword { get; set; }
		public string Description { get; set; }
		public string Title { get; set; }
		public string MetaImage { get; set; }

	}

	public class MetaDataOutputModel
	{
		public int PageId { get; set; }
		/// <summary>
		/// This can be brandId, categoryId, parentProductId etc.
		/// </summary>
		public int TypeId { get; set; }
		public string Keyword { get; set; }
		public string Description { get; set; }
		public string Title { get; set; }
		public string MetaImage { get; set; }
		public bool IsBrand { get; set; }
		public bool IsCategory { get; set; }
		public bool IsProduct { get; set; }
		public bool IsGeneral { get; set; }
		public string PageFriendlyName { get; set; }

	}
}