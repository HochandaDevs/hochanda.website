﻿using ModelLayer.Models.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class ProductsWithFilterModel
	{
		public ProductsWithFilterModel()
		{
			Products = new List<ProductInformationModel>();
		}

		public int ProductCount { get; set; }

		public FilterAndSortOptionsModel Filter { get; set; }

		public List<ProductInformationModel> Products { get; set; }

	}
}
