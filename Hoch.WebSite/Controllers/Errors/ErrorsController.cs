﻿using Hoch.Website.Helpers;
using Hoch.Website.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Controllers.Errors
{
    public class ErrorsController : BaseController
    {
        // GET: Errors
        public ActionResult Index()
        {
            return View("Errors");
        }

        public ActionResult NotFound()
        {
            MessageCodeModel model = (MessageCodeModel)TempData["MessageCode"];

			if (model != null)
			{
				ViewData["MessageCode"] = model;
			}

            return View();
        }

       
        public async Task<ActionResult> Generic()
        {
            string message = await Task.Run(() => GetMessageToDisplay(500));
            string exceptionMessage = MvcApplication.ExceptionMessage;
            string exceptionObject = MvcApplication.ExceptionObject;
            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                int r = await new WebRequestHelper().InsertErrorToPostgres(exceptionMessage, "1", exceptionObject);
            }


            var viewParams = new MessageCodeModel
            {
                ErrorCode = 500,
                ErrorDescription = message,
                ErrorTitle = "Internal Error"
            };
            ViewData["MessageCode"] = viewParams;
            return View();
        }

        public async Task<ActionResult> BadRequest()
        {
            string message = await Task.Run(() => GetMessageToDisplay(400));
            ViewData["message"] = message;
            return View();
        }

        public async Task<ActionResult> Unauthorized()
        {
            string message = await Task.Run(() => GetMessageToDisplay(401));
            var viewParams = new MessageCodeModel
            {
                ErrorCode = 401,
                ErrorDescription = message,
                ErrorTitle = "Unauthorized"
            };
            ViewData["MessageCode"] = viewParams;
            return View();
        }
        public async Task<ActionResult> Forbidden()
        {
            string message = await Task.Run(() => GetMessageToDisplay(403));
            var viewParams = new MessageCodeModel
            {
                ErrorCode = 403,
                ErrorDescription = message,
                ErrorTitle = "Forbidden"
            };
            ViewData["MessageCode"] = message;
            return View();
        }
        public async Task<ActionResult> ServiceUnavailable()
        {
            string message = await Task.Run(() => GetMessageToDisplay(503));
            ViewData["message"] = message;
            return View();
        }



        public async Task<ActionResult> Test()
        {
            WebRequestHelper w = new WebRequestHelper();
            try
            {
                throw new Exception("This is a test exception");
            }
            catch (Exception e)
            {
                HttpBrowserCapabilitiesBase browser = Request.Browser;
                string browserInfo = browser != null ? browser.Type : "";
                await w.InsertErrorToPostgres("ErrorsController Browser: "+browserInfo+" - Test: "+ e.Message, "1", e.ToString(), 1);
            }
            return Json(1);
        }

        private async Task<string> GetMessageToDisplay(int code)
        {
            WebRequestHelper request = new WebRequestHelper();
            string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
            string message = "";
            try
            {
                message = await request.GetItem<string>(apiUrl + "Messages/GetMessageByMessageCode?messageCode=" + code);
            }
            catch (Exception)
            {
                message = "Server Connection Timeout";
            }
            
            return message;
        }
    }
}