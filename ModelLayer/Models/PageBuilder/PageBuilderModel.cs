﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.PageBuilder
{
	public class PageBuilderModel
	{
		public PageBuilderModel()
		{
			WebsiteLocation = null;
		}

		public string PageTitle { get; set; }
		public string MetaKeywords { get; set; }
		public string MetaDescription { get; set; }
		public string ContentHTML { get; set; }
		public int LandingPageId { get; set; }
		public int LanguageId { get; set; }
		public string RedirectURL { get; set; }
		public string WebsiteLocation { get; set; }
	}
}
