﻿//Angular
app.controller('PageBuilderController', ['$scope', '$http', 'CommonCode', "$window", "$sanitize", "$compile", "$timeout", "$rootScope", "$sce", function ($scope, $http, CommonCode, $window, $sanitize, $compile, $timeout, $rootScope, $sce) {

    CommonCode.getBaseCookieSettings();
    $scope.ShowPageBuilderContent = false;
    $scope.PageBuilderData = null;

    $scope.LoadSalesEvent = function (productListingFilter) {
        $http({
            method: "GET",
            url: '/Product/LoadGridWithFilter',
            contentType: "text/plain",
            dataType: "html"
        }).then(function (resultHtml) {
            var hiddenField = document.getElementById("salesEventId");

            var htmlElement = angular.element(resultHtml.data);

            for (var i = 0; i < htmlElement.length; i++) {
                hiddenField.parentNode.appendChild($compile(htmlElement[i])($scope)[0]);
            }

            $rootScope.GetProducts("Products/ProductsForSalesEvent", productListingFilter.IdToUse);
        });
    }

}]);

$(document).ready(function () {
    LoadPage();
    SetupSmoothScroll();

    $.each($("#pageBuilder").find("[data-target='#myModalFeatured']"), function (index, value) {
        var parentProductId = $(value).parent().find(".parentProductId").val();
        $(value).click(function () {
            ShowQuickLookModal(parentProductId)
        });
    });
});

function LoadPage() {

    $("#landingPage").hide();

    var languageId = angular.element(document.body).injector().get("CommonCode").LanguageId;
    var currencyId = angular.element(document.body).injector().get("CommonCode").CurrencyId;
    var countryId = angular.element(document.body).injector().get("CommonCode").CountryId;

    var basketDiv = $('.addToBasketButton')

    if (basketDiv.length > 0) {

        var button = basketDiv.find('.add_to_basket');

        var hiddenField = basketDiv.find('.productAddToBasketSKU');

        button.attr("onclick", "return false;");

        if (hiddenField.val() != null) {
            $.ajax({
                type: "GET",
                url: baseUrl + 'products/GetProductInformation?parentProductId=' + hiddenField.val() + '&languageId=' + languageId + '&currencyId=' + currencyId + '&countryId=' + countryId,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status) {
                    if (data != null) {

                        var variationToUse = 0;
                        if (data.ProductVariations.length = 1) {
                            variationToUse = data.DefaultVariationId;
                        }

                        if (data.IsPickAndMix) {
                            button.attr("href", GetProductUrlFromFullProduct(data));
                            button.html("<span>&nbsp;</span><span class=\"text\">Select Pick N Mix</span>");
                        }
                        else {
                            button.attr("onclick", "AddToBasket($(this).parent().find('[name=\"parentProductId\"]').val(), $(this).parent().find('[name=\"variationId\"]').val(), this); return false;");
                            button.after("<input type=\"hidden\" name=\"parentProductId\" value=\"" + hiddenField.val() + "\" /><input type=\"hidden\" name=\"variationId\" value=\"" + variationToUse + "\" />");
                        }
                    }
                }
            })
        }
    }

    $(".landingPageFormSubmitButton").click(function (e) {
        e.preventDefault();
        $('.form_section').each(function () {
            var formLength = this.getElementsByClassName('content_type_wrap').length;
            var formLabels = this.getElementsByClassName('content_type_wrap');
            SubmitFormData(formLength, formLabels);
        });

    });



    LoadProductInformation(languageId, currencyId, countryId);

    LoadSalesEvent(languageId, currencyId, countryId);

    AddClampBodyText();

    //TODO: Hide Spinner
    $("#landingPage").show();

    LoadCarousel();

    //Fix old go to tops...
    $('a[href$="#cntPlContent_landing_page"]').attr("href", "#landingPage");

}

function LoadCarousel(languageId, currencyId, countryId) {
    //Create Carousel
    //Has to be in a timeout as there is a small bug when it does not load.
    setTimeout(function () {
        LoadOwlCarousel($("#landingPage").find(".owl-carousel").not(".ct_carousel_contol_1_2_3"));
        LoadImageCarousel($("#landingPage").find(".owl-carousel").not(".ct_carousel_contol_2_4_6"));
    }, 250);
}

function LoadSalesEvent(languageId, currencyId, countryId) {
    var salesEvent = $("#salesEventId");

    //We can only have one
    if (salesEvent.length == 1) {
        var salesEventId = salesEvent.val();


        var productListingFilter = {
            IdToUse: salesEventId,
            LanguageId: languageId,
            CurrencyId: currencyId,
            CountryId: countryId
        }

        var scope = angular.element(document.getElementById('pageBuilder')).scope()
        scope.LoadSalesEvent(productListingFilter);
    }
}

function LoadProductInformation(languageId, currencyId, countryId) {
    var parentProductIds = [];


    $('.product_wrap').each(function (index, value) {
        //Get the parent product id
        var parentProductId = $(value).find(".parentProductId").first().val();

        if (parentProductId != null) {
            if ($.inArray(this.firstElementChild.defaultValue, parentProductIds)) {
                if (this.firstElementChild.defaultValue != undefined) {
                    parentProductIds.push(this.firstElementChild.defaultValue);
                }
            }
        }
    });

    //Make sure we have data to do.
    if (parentProductIds.length != 0) {

        $.ajax({
            type: 'POST',
            url: baseUrl + "Products/GetMultipleProductInformation",
            data: JSON.stringify({ 'ParentProductIds': parentProductIds, 'LanguageId': languageId, 'CurrencyId': currencyId, 'CountryId': countryId }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {

                var obj1 = data;

                $('.product_wrap').each(function (index, value) {

                    //The parent productId to Find.
                    var parentProductId = $(value).find(".parentProductId").first().val();

                    //get the data for the productId;
                    var productInformation = data.find(function (element) {
                        return element.ParentProductId == parentProductId;
                    });

                    if (productInformation == null) {
                        //Remove it as it is not in wiki so it is not active.
                        $(value).remove();
                        return;
                    }

                    //ProductUrl
                    var productDescription = productInformation.TVDescription.replace(/[^a-z0-9]+/gi, '-');

                    var productUrl = GetProductUrlFromFullProduct(productInformation);

                    $(value).find("a[href*='{ProductURL}']").not(".product_title").attr("href", productUrl);
                    $(value).find(".product_title").attr("href", productUrl);

                    //Sashes
                    if (!productInformation.IsFreedom) {
                        $(value).find(".freedom_sash").remove();
                    }

                    if (!productInformation.IsFlexibuy) {
                        $(value).find(".flexibuy_sash").remove();
                    }

                    //Product Image
                    $(value).find("img[src*='{ProductImage}']").attr("onerror", "this.onerror=null; this.src='https://www.hoch.media/product-images/thumb/no-image.jpg';");
                    $(value).find("img[src*='{ProductImage}']").attr("src", productInformation.DefaultThumbnailFilename);
                    $(value).find("img[alt*='{ProductTitle}']").attr("alt", productInformation.DefaultImageAltText);

                    //Stars for Reviews

                    var starPercent = 0;
                    if (productInformation.FeefoAveragePercentage != null) {
                        starPercent = productInformation.FeefoAveragePercentage;
                        $(value).find(".gold_stars").css("width", starPercent + "%")
                    }
                    else {
                        $(value).find(".gold_stars").parent().parent().addClass("ng-hide");
                    }


                    //Product Title, SKU && prices;
                    $(value).find(".product_title").find("h5").html(productInformation.TVDescription);

                    $(value).find(".product_code").html(productInformation.ParentProductSKU);
                    $(value).find(".product_title").find("h5").html(productInformation.TVDescription);

                    var wasPriceToUse = null;

                    //Freedom price.
                    if ($(value).hasClass("FP") && productInformation.IsFreedom) {
                        wasPriceToUse = productInformation.FreedomPriceString
                        $(value).find("span:contains('{ProductPrice}')").val(productInformation.PriceString);
                    }
                    else {
                        switch (productInformation.ActivePriceType) {
                            //Intro
                            case 1:
                                if (productInformation.IntroPrice != null && productInformation.IntroPrice != productInformation.Price) {
                                    wasPriceToUse = productInformation.IntroPriceString;
                                }
                                break;

                                //ODS:
                            case 2:
                                if (productInformation.PromoPrice != null && productInformation.PromoPrice != productInformation.Price) {
                                    wasPriceToUse = productInformation.PromoPriceString;
                                }
                                break;
                                //usualPrice
                            default: case 3:
                                if (productInformation.UsualPrice != null && productInformation.UsualPrice != productInformation.Price) {
                                    wasPriceToUse = productInformation.UsualPriceString;
                                }
                                break;
                        }
                    }

                    if (wasPriceToUse != null) {
                        $(value).find(".was_price").html(wasPriceToUse);
                    }
                    else {
                        $(value).find(".was_price").remove();
                    }

                    $(value).find("span:contains('{ProductPrice}')").html(productInformation.PriceString);

                    //If we have multiple variations set it to 0 so we can show the picker.
                    var variationToUse = productInformation.ProductVariations != null && productInformation.ProductVariations.length == 1 ? productInformation.DefaultVariationId : 0;

                    if (productInformation.IsPickAndMix) {
                        $(value).find(".add-to-basket").attr("href", GetProductUrlFromFullProduct(productInformation));
                        $(value).find(".add-to-basket").html("<span>&nbsp;</span><span class=\"text\">Select Pick N Mix</span>");
                    }
                    else {
                        $(value).find(".add-to-basket").attr("onclick", "AddToBasket($(this).parent().find('[name=\"parentProductId\"]').val(), $(this).parent().find('[name=\"variationId\"]').val(), this); return false;")
                        $(value).find(".add-to-basket").after("<input type=\"hidden\" name=\"parentProductId\" value=\"" + productInformation.ParentProductId + "\" /><input type=\"hidden\" name=\"variationId\" value=\"" + variationToUse + "\" />");
                    }

                    //Add To Basket Button
                });
            }
        })
    }
}

function AddClampBodyText() {
    if ($('div').hasClass('content_type_wrap ct_body clamp_text_class') == true) {
        var showChar = 250;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Read more";
        var lesstext = "Show less";

        $('.clamp_text_class').each(function () {
            var c = "";
            var h = "";
            var _html = "";
            var content = $(this).html();

            if (content.length > showChar) {
                if (content.indexOf("</p>") != -1) {
                    c = content.split('</p>')[0] + "</p>";
                    if ($(this)[0].getElementsByTagName("p").length > 1) {
                        splitCount = content.split('</p>');
                        for (var y = 0; y < splitCount.length; y++) {
                            if (y != 0) {
                                h += splitCount[y] + "</p>";
                            }
                        }
                    }
                    else {
                        splitCount = content.split('</p>');
                        for (var y = 0; y < splitCount.length; y++) {
                            if (y != 0) {
                                h += splitCount[y];
                            }
                        }
                    }
                }
                if (h != "") {
                    _html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                    $(this).html(_html);
                }

            }
        });

        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    }
}

function SubmitFormData(formLength, formLabels) {
    var html = '';

    //still checked boxes
    for (i = 0; i < formLength; i++) {
        var inputs = formLabels[i].children[1];

        if (inputs != undefined && inputs != "undefined") {

            if (formLabels[i].className == "content_type_wrap ct_form_checkboxinput" || formLabels[i].className == "content_type_wrap ct_form_checkboxinput error") {
                var checkboxinputcount = formLabels[i].children[1].getElementsByClassName("checkbox_item_wrap").length;

                if (checkboxinputcount > 0) {
                    html += GetCheckBoxList(inputs);
                }
            }
            else {
                if (formLabels[i].className == "content_type_wrap ct_form_radioinput" || formLabels[i].className == "content_type_wrap ct_form_radioinput error") {
                    var checkboxinputcount = formLabels[i].children[1].getElementsByClassName("radio_item_wrap").length;
                    if (checkboxinputcount > 0) {

                        html += GetRadioButtonList(inputs);

                    }

                }
                else {
                    if (formLabels[i].getElementsByTagName("label")[0].parentNode.className != undefined && formLabels[i].getElementsByTagName("label")[0].parentNode.className.indexOf("ct_form_slidecontainer") != -1) {
                        html += formLabels[i].getElementsByTagName("label")[0].querySelector("span:not(.ct_rating_wrap)").innerText;
                    }
                    else {
                        html += formLabels[i].getElementsByTagName("label")[0].innerText;
                    }

                    if ($(inputs).val() == "") {

                        if ($(inputs).parent().hasClass("error")) {
                        }
                        else {
                            $(inputs).parent().addClass(" error");
                            $(inputs).after("<div class='lp_error_msg'>Please complete the above</div>");
                        }

                    }
                    else {
                        if ($(inputs).parent().hasClass("error")) {
                            $(inputs).parent().removeClass("error");
                            formLabels[i].children[1].nextSibling.remove();
                        }
                        html += '¦' + $(inputs).val() + '¬';
                    }
                }

            }
        }

    }

    var reqErrors = '';

    $('.form_section').each(function () {
        reqErrors = this.getElementsByClassName("error").length;
    });

    if (reqErrors == 0) {


        var data = {
            PageBuilderId: $("#hdnPageBuilderId").val(),
            FormData: html
        };


        $.ajax({
            type: "POST",
            url: baseUrl + "PageBuilder/SendFormData",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data) {

                    ClearFormElements('form_section');
                }
                else {

                }

            },
            error: function (ejqXHR, textStatus, errorThrown) {
            }
        });


    }
}

function GetCheckBoxList(inputs) {
    var html = '';
    var checkCount = $("input[type=checkbox]:checkbox:checked");
    if (checkCount.length > 0) {

        $('input[type=checkbox]').each(function () {
            if ($(this).parent().parent().parent().hasClass("error")) {
                $(this).parent().parent().parent().removeClass("error");
                $(this).parent().parent().next().remove();
            }

            var checkbox = $(this).next('label').text();

            if ($(this).is(':checked')) {

                html += checkbox + '¦' + '1' + '¬';
            }
            else {
                html += checkbox + '¦' + '0' + '¬';
            }

        });
    }
    else {
        if ($(inputs).parent().hasClass("error")) {
        }
        else {
            $(inputs).parent().addClass(" error");
            $(inputs).after("<div class='lp_error_msg'>Please complete the selection</div>");
        }
    }

    return html;

}

function GetRadioButtonList(inputs) {
    var html = "";
    var radioCount = $("input[type=radio]:checked");
    if (radioCount.length > 0) {
        $('input[type=radio]').each(function () {
            if ($(this).parent().parent().parent().hasClass("error")) {
                $(this).parent().parent().parent().removeClass("error");
                $(this).parent().parent().next().remove();

            }
            var radiobutton = $(this).next('label').text();

            if ($(this).is(':checked')) {

                html += radiobutton + '¦' + '1' + '¬';
            }
            else {
                html += radiobutton + '¦' + '0' + '¬';
            }

        });
    }
    else {
        if ($(inputs).parent().hasClass("error")) {
        }
        else {
            $(inputs).parent().addClass(" error");
            $(inputs).after("<div class='lp_error_msg'>Please complete the selection</div>");
        }
    }

    return html;

}

function ClearFormElements(className) {
    $("." + className).find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'text':
            case 'textarea':
            case 'file':
            case 'select-one':
            case 'select-multiple':
            case 'date':
            case 'number':
            case 'tel':
            case 'email':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
                break;
            case 'range':
                $("#rateme_number").html($(this).attr("min"));
                $(this).val($(this).attr("min"));
                break;
        }
    });
}