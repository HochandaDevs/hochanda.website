﻿using ModelLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetAllProductModel : IModel
    {
        public int parentproductid { get; set; }
        public string parentproductsku { get; set; }
        public string imageAltText { get; set; }
        public int membersdiscount { get; set; }
        public decimal costprice { get; set; }
        public int flexibuy { get; set; }
        public int flexibuyinstallments { get; set; }
        public System.DateTime createddate { get; set; }
        public int masterClassTicket { get; set; }
        public string usualprice { get; set; }
        public decimal usualprice_d { get; set; }
        public int pricecat { get; set; }
        public string tvdescription { get; set; }
        public int brandid { get; set; }
        public string brandName { get; set; }
        public int refinecategory1id { get; set; }
        public int refinecategory2id { get; set; }
        public int refinecategory3id { get; set; }
        public int refinecategory4id { get; set; }
        public int refinecategory5id { get; set; }
        public string firstlevelcatergoryname { get; set; }
        public string secondlevelcatergoryname { get; set; }
        public string thirdlevelcatergoryname { get; set; }
        public string brandscatergory { get; set; }
        public int virtualproduct { get; set; }
        public int stockQuantity { get; set; }
        public int variationid { get; set; }
        public int isPickNMixItem { get; set; }
        public string feeforeviewavgpercentage { get; set; }
        public string feefotototalreviews { get; set; }
        public int isBundleCheck { get; set; }
        public string defaultThumbnailfilename { get; set; }
        //Price
        public string price { get; set; }
        public string freedomprice { get; set; }
        public decimal? freedomprice_d { get; set; }
        public string startPrice { get; set; }
        public decimal startPrice_d { get; set; }
        public string freedomSashfilename { get; set; }
        public string felxibuySashFilename { get; set; }
    }
}
