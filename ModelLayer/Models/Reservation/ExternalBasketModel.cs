﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class ExternalBasketModel
	{
		public string BasketId { get; set; }

		public Guid? CustomerReference { get; set; }

		public string CustomerEmail { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
