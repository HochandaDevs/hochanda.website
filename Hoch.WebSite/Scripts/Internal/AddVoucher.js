﻿app.controller("VoucherController", ['$scope', '$http', '$filter', '$compile', '$sanitize', '$rootScope', function ($scope, $http, $filter, $compile, $sanitize, $rootScope) {
    //not using this any more its comming from layout
    $scope.ApplyVoucherCode = function ApplyVoucherCode(vouchercode) {

        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }
        if ($scope.signinData != null || $scope.signinData != undefined) {
            $scope.loginPopup = false;
            if (vouchercode != "" && vouchercode != null && vouchercode != undefined) {


                $scope.selectedVoucher = vouchercode;
                ApplyVoucher();
            }
        }
        else
        {
            $scope.loginPopup = true;
        }
    }

    function ApplyVoucher()
    {
        var basketId = localStorage.getItem("basketId");
        var voucherParams = { BasketId: basketId, VoucherCode: $scope.selectedVoucher};//dynamic values
        $http({
            method: 'POST',
            url: baseUrl + "/Reservations/VoucherUpdate",
            contentType: 'application/json',
            data: JSON.stringify(voucherParams),
        }).then(function (result) {
            if (result.data !== undefined) {

                var basketVoucherData = result.data;
                //if (basketVoucherData != null)
                //{
                    $('#DynamicModal').hide();
                // window.location.href = '/Basket/Basket';
                    basketVoucherData = basketVoucherData != null ? basketVoucherData : "-1";
                    $rootScope.$emit('modalVoucher', basketVoucherData);
                   
                //}
               
                

            }
        });
    }

}]);