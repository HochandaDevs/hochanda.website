﻿using ModelLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetBrandFeatureMenuModel : IModel
    {
        public int brandid { get; set; }
        public string brandname { get; set; }
        public string urlbrandname { get; set; }
        public string brandimgurl { get; set; }
    }
}
