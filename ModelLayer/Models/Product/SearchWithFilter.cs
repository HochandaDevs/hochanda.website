﻿using ModelLayer.Models.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class SearchWithFilter
	{
		public SearchWithFilter()
		{
			Products = new List<ProductInformationModel>();
		}
		public FilterAndSortOptionsModel Filter { get; set; }
		public int ProductCount
		{
			get; set;
		}

		public List<ProductInformationModel> Products { get; set; }

	}
}
