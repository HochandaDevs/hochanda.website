﻿using Hoch.Website.Controllers;
using Hoch.Website.Helpers;
using ModelLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Timers;
using System.Web;

namespace Hoch.Website.App_Start
{
    public enum TimeIntervalEnum
    {
        Second = 1,
        Minute,
        Hour
    }
    public class CacheConfig
    {
        public static System.Timers.Timer timer = null;

        public static async void CreateMetadataCache()
        {
            BaseController bc = new BaseController(); //feature 123

            string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");

            List<MetaDataOutputModel> metadata = await new WebRequestHelper().GetItem<List<MetaDataOutputModel>>(apiUrl + "metadata/GetAllMetaDataForEverything");

            DateTime expiry = GetExpiryTime(TimeIntervalEnum.Hour, 1);

			var timeNow = DateTime.Now;
			timeNow = timeNow.AddMilliseconds(timeNow.Millisecond * -1);

			if (metadata == null)
			{
				expiry = DateTime.Now.AddMinutes(1);
				HttpRuntime.Cache.Insert("metadata", string.Empty);
			}
			else
			{
				HttpRuntime.Cache.Insert("metadata", metadata);
			}

			TimeSpan timeToExpire = expiry - timeNow;
			timeToExpire = new TimeSpan(timeToExpire.Hours, timeToExpire.Minutes, timeToExpire.Seconds);

            timer = new System.Timers.Timer();
            timer.Elapsed += CacheExpired;
            timer.Enabled = true;
            timer.Interval = timeToExpire.TotalMilliseconds;
            timer.Start();
        }

        public static List<MetaDataOutputModel> GetMetadataCache()
        {
			if (HttpRuntime.Cache["metadata"] != null && HttpRuntime.Cache["metadata"].ToString() != "")
			{
				return HttpRuntime.Cache["metadata"] as List<MetaDataOutputModel>;
			}

			return null;
        }

        public static void CacheExpired(object sender, ElapsedEventArgs e)
        {
			lock (_Lock)
			{
				if (timer != null)
				{
					//Stop Previous instances of it.
					timer.Enabled = false;
					timer.Close();
					timer.Dispose();
				}
				CreateMetadataCache();
			}
        }

		private static readonly object _Lock = new object();

        public static DateTime GetExpiryTime(TimeIntervalEnum interval, int timeToAdd)
        {
            var expiry = DateTime.Now;

            //Use a variable as we are going to be doing a while, and it could be a long one.
            var nowTime = DateTime.Now.AddSeconds(DateTime.Now.Second * -1);

            switch (interval)
            {
                case TimeIntervalEnum.Hour:
                case TimeIntervalEnum.Minute:
                    expiry = expiry.AddMinutes(expiry.Minute * -1);
                    break;

                default:
                case TimeIntervalEnum.Second:
                    //Don't need to do anything as it's done down below.
                    break;
            }

            expiry = expiry.AddSeconds(expiry.Second * -1);

            while (expiry <= nowTime)
            {
                switch (interval)
                {
                    case TimeIntervalEnum.Hour:
                        expiry = expiry.AddHours(timeToAdd);
                        break;

                    case TimeIntervalEnum.Minute:
                        expiry = expiry.AddMinutes(timeToAdd);
                        break;

                    default:
                    case TimeIntervalEnum.Second:
                        expiry = expiry.AddSeconds(timeToAdd);
                        break;
                }
            }

            return expiry;
        }
    }
}