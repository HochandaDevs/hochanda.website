﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Customer
{
	public class ValidatedCustomerModel
	{
		public int MobileCustomerLookupId { get; set; }
		public int CustomerId { get; set; }
		public string EmailAddress { get; set; }
		public Guid CustomerIdLookup { get; set; }
		public DateTime CustomerIdLookupLastUpdated { get; set; }
		public int? CustomerLoginPin { get; set; }
		public bool CustomerLoginPinActive { get; set; }
		public DateTime? CustomerLoginPinExpire { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
