﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Hoch.Website.Models
{
    public class ViewSiteMapModel
    {

        public IEnumerable<SiteMapModel> siteMapData { get; set; }
       
    }

    [Serializable]
    [XmlRoot("SiteMapWeb")]
    public class SiteMapModel
    {
        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("categoryid")]
        public int categoryId { get; set; }

        [XmlElement( "translation")]
        public string translation { get; set; }

        [XmlElement( "belongsto")]
        public int belongsTo { get; set; }

        [XmlElement( "catlevel")]
        public int catLevel { get; set; }

        [XmlElement("catUrl")]
        public string catUrl { get; set; }
    
    }
}