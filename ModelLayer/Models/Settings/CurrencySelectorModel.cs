﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Settings
{
	public class CurrencySelectorModel
	{
		public int CurrencyId { get; set; }
		public string CurrencySymbol { get; set; }
		public string CurrencyName { get; set; }
		public int DefaultChannelId { get; set; }
		public string DefaultStreamUrl { get; set; }
		public string CurrencyDisplayName { get; set; }
		public string Currency3LetterCode { get; set; }
		public string SimpleStreamLiveDataStream { get; set; }
		public string SimpleStreamRewindDataStream { get; set; }
	}
}
