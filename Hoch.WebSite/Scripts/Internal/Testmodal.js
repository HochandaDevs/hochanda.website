﻿app.controller('ui.bootstrap.demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
app.controller('ui.bootstrap.demo').controller('ModalDemoCtrl', function ($scope, $uibModal, $log, $document) {
    $scope.animationsEnabled = true;
    $scope.Venue = "India"; // declare venue

    $scope.open = function (size, parentSelector) {
        var parentElem = parentSelector ?
          angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                values: function () {
                    return $scope.Venue; //we are passing venue as values
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.msg = "Submitted";
            $scope.suc = true;
        }, function (error) {
            $scope.msg = 'Cancelled';
            $scope.suc = false;
        });
    };
});


app.controller('ui.bootstrap.demo').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, values) { // inject that resolved values
    $scope.Venue = values; // we are getting & initialize venue from values
    $scope.ok = function () {
        $uibModalInstance.close('ok');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});