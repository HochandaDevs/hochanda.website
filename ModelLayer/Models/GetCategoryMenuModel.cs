﻿using ModelLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetCategoryMenuModel : IModel
    {
        public int id { get; set; }
        public int categoryid { get; set; }
        public string translation { get; set; }
        public int belongsto { get; set; }
        public int catlevel { get; set; }
        public string catUrl { get; set; }
        public int eventShow { get; set; }
    }
}
