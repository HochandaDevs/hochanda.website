﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hoch.Website.Controllers;
using Hoch.Website.App_Start;
using ModelLayer.Models;
using Hoch.Website.Helpers;

namespace Hoch.Website.Controllers
{
	//TODO: REMOVE THIS CONTROLLER
    public class DefaultController : BaseController
    {
        //
        // GET: /Default/
		[MetadataActionFilter(MetaDataFriendlyName = "Homepage")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This returns a test view for all base text styles
        /// </summary>
        /// <returns></returns>
        public ActionResult Test()
        {
            List<MetaDataOutputModel> m = CacheConfig.GetMetadataCache();

            return View();
        }
    }
}