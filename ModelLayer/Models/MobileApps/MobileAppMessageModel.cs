﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.MobileApps
{
	public class MessageModel
	{
		public int MessageCode { get; set; }
		public string MessageDescription { get; set; }
		public int LanguageId { get; set; }
		public string MessageToDisplay { get; set; }
		public string OKMessage { get; set; }
		public string CancelMessage { get; set; }
		public string TitleMessage { get; set; }
        public string custRef { get; set; }
	}
}
