﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.TVSchedule
{
	public class TVScheduleInformationModel
	{
		public int TVScheduleId { get; set; }
		public DateTime ShowDateTime { get; set; }
		public int LastUpdatedBy { get; set; }
		public DateTime LastUpdatedOn { get; set; }
		public int ChannelId { get; set; }
		public int ShowNameId { get; set; }
		public int Presenter { get; set; }
		public int Producer { get; set; }
		public int Planner { get; set; }
		public int RecordedShow { get; set; }
		public bool Active { get; set; }
		public int RepeatShowId { get; set; }
		public decimal ShowTarget { get; set; }
		public decimal ShowForecast { get; set; }
		public bool Locked { get; set; }
		public string BuyerNotes { get; set; }
		public int PolVersion { get; set; }
		public string EPGText { get; set; }
		public string ShowTranscript { get; set; }
		public string TVNotes { get; set; }
		public DateTime TVNotesUpdatedLast { get; set; }
		public string TVNotesUpdatedBy { get; set; }
		public int? IncInMinutes { get; set; }
		public string TVNotesOther { get; set; }
		public int Hour2 { get; set; }
		public bool AllowWatchAgain { get; set; }
		public int EventId { get; set; }
		public bool IsODSHour { get; set; }
		public bool AuctionHour { get; set; }
		public bool HotSpotShow { get; set; }
	}
}
