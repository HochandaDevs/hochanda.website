﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoch.Website.Models
{
    public class KeepMiSignedInModel
    {
        public string CustomerLookupId { get; set; }
        public string Email { get; set; }
        public bool Value { get; set; }
        public DateTime loginDate { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}