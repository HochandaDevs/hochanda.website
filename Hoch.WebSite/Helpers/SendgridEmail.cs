﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Hoch.Website.Helpers
{
    public class SendgridEmail
    {
        public static async System.Threading.Tasks.Task<int> Send(string emailTo, string emailFrom, string subject, string htmlMessage, bool hasBcc = false, List<string> bccs = null)
        {
            try
            {
                var myMessage = new SendGrid.SendGridMessage();

                myMessage.AddTo(emailTo);
                myMessage.EnableTemplateEngine("87ab3b00-7aa4-4dcf-8c72-c8e3464d2fe5");
                List<MailAddress> bccAddresses = new List<MailAddress>();
                if (hasBcc)
                {
                    bccAddresses = new List<MailAddress>();
                    foreach (var item in bccs)
                    {
                        MailAddress bccEmails = new MailAddress(item);
                        bccAddresses.Add(bccEmails);
                    }
                }

                myMessage.From = new MailAddress(emailFrom, "Hochanda");
                myMessage.Bcc = bccAddresses.ToArray();
                myMessage.Subject = subject;
                myMessage.Html = htmlMessage;

                var transportWeb = new SendGrid.Web("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");
                await transportWeb.DeliverAsync(myMessage);
            }
            catch (Exception e)
            {
                return 0;
            }
            return 1;
        }
    }
}