﻿using Hoch.Website.Helpers;
using Hoch.Website.Models;
using ModelLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Hoch.Website.Controllers.Product
{
	[RouteArea("product")]
	public class ProductController : BaseController
	{
		[MetadataActionFilter(IsProduct = true, IdParameterName = "parentProductId")]
		public ActionResult Product(int parentProductId, string webSafeTitle, string sku)
		{
			if (parentProductId == 26)
			{
				return RedirectPermanent("/about/freedom");
			}

			var newWebSafeTitle = Utility.GetWebSafeString(webSafeTitle);

			//Check to see if the webSafeTitle is actually safe

			if (newWebSafeTitle != webSafeTitle)
			{
				return RedirectToRoutePermanent("Product", new { parentProductId = parentProductId, webSafeTitle = newWebSafeTitle, sku = sku });
			}


			return View("~/Views/product/Product.cshtml");
		}


		/// <summary>
		/// Redirects Permanently to /product
		/// </summary>
		/// <param name="parentProductId"></param>
		/// <param name="webSafeTitle"></param>
		/// <param name="sku"></param>
		/// <returns></returns>
		public ActionResult ProductPickAndMix(int parentProductId, string webSafeTitle, string sku)
		{
			return RedirectToRoutePermanent("Product", new { parentProductId = parentProductId, webSafeTitle = webSafeTitle, sku = sku });
		}

		[HttpGet]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult LoadCarousel(string productScopeName = "Products")
		{

			//We are dynamically adding the name to use in the ng-repeat. So we can have what ever we want in the name. This is for multiple on same page with same control.
			ViewBag.ProductScopeName = productScopeName;

			return View("~/Views/shared/partials/_carousel.cshtml");
		}

		[HttpGet]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult LoadGrid(string angularScopeVariable = "Products", bool loadAngularProducts = false)
		{
			ViewBag.AngularProducts = loadAngularProducts;
			ViewBag.AngularProductScopeVariable = angularScopeVariable;

			return PartialView("~/Views/shared/partials/_prodGrid.cshtml");
		}

		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult LoadGridWithFilter()
		{
			return PartialView("~/Views/shared/partials/_product_grid_with_filter.cshtml");
		}

		[HttpGet]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult GetPickAndMixVariation()
		{
			return PartialView("~/Views/Shared/Partials/_pick_mix_picker.cshtml");
		}

		[HttpPost]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult GetAddToBasketButton(ProductVariationDetails productVariations)
		{
			ViewBag.ParentProductId = productVariations.ParentProductId;
			ViewBag.VariationId = productVariations.VariationId;

			return PartialView("~/Views/Shared/Partials/_add_to_basket.cshtml");
		}

		/// <summary>
		/// This is a post as the list could be bigger than the url. Unlikely But still. It is a complex type so better to be safe.
		/// </summary>
		/// <param name="parentProductId"></param>
		/// <param name="variations"></param>
		/// <returns></returns>
		[HttpPost]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult GetVariationSelector(ProductVariationDetails productVariations)
		{
			ViewBag.ParentProductId = productVariations.ParentProductId;
			ViewBag.VariationId = productVariations.VariationId;

			return PartialView("~/Views/Shared/Partials/_variation_selector.cshtml");
		}

		[HttpGet]
		[ExtendLoginCookie(DoNotExtend = true)]
		public ActionResult GetImageCarousel(string scopeVariable = "ProductImages", string productScopeVariable = "product")
		{
			ViewBag.ScopeVariable = scopeVariable;
			ViewBag.ProductScopeVariable = productScopeVariable;

			return PartialView("~/Views/Shared/Partials/_carousel_single.cshtml");
		}

	}

	public class ProductVariationDetails
	{
		public int ParentProductId { get; set; }
		/// <summary>
		/// The variationId to use 
		/// </summary>
		public int VariationId { get; set; }
		public List<ProductVariationModel> Variations { get; set; }
	}
}