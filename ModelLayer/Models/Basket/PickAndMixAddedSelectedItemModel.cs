﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class PickAndMixAddedSelectedItemModel
	{
		public bool ItemAdded { get; set; }
		public int SelectedCount { get; set; }
		public int CorrectCount { get; set; }
		public bool OutOfStock { get; set; }
		public bool CountError { get; set; }
	}
}
