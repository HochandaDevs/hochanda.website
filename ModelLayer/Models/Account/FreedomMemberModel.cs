﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
	public class FreedomMemberModel
	{
		public List<PaymentScheduleModel> PaymentSchedule { get; set; }

		public bool IsFreedomHoliday { get; set; }
	}

	public class PaymentScheduleModel
	{
		public DateTime Date { get; set; }

		public double Amount { get; set; }

		public string AmountString { get; set; }

		public string Status { get; set; }

		/// <summary>
		/// To be used for correct formatting based on location.
		/// </summary>
		public string DateString { get; set; }
	}
}
