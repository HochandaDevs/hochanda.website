﻿using ModelLayer.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Interfaces;

namespace ModelLayer.Models.Account
{
    public class AccountSettingsModel : IAccountSettingsModel
    {
        
        public List<ICurrencyModel> availableCurrencies { get; set; }
        public string customerRef { get; set; }
        public string custEmail { get; set; }
        public int customerId { get; set; }
        public string firstName { get; set; }
        public int defaultCurrency { get; set; }
        public int receivePost { get; set; }
        public int receiveSms { get; set; }
        public int receiveEmail { get; set; }
        public int receiveEmail3rdParty { get; set; }

    }
}
