﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class PickAndMixReservationModel
	{

		public string BasketId
		{
			get
			{
				string basketId = string.Empty;

				if (BasketIdToUse.HasValue)
				{
					basketId = GenericHelper.Instance.ProtectBasketId(BasketIdToUse.Value);
				}

				return basketId;
			}

		}

		[JsonIgnore]
		public long? BasketIdToUse { get; set; }

		public bool ItemAdded { get; set; }
		public bool ItemRemoved { get; set; }
		public int SelectedCount { get; set; }
		public int CorrectCount { get; set; }
		public bool IsOutOfStock { get; set; }
		public bool IsCountError { get; set; }
		public bool ReservationValid { get; set; }
		public int PickAndMixIdentifier { get; set; }
		public bool InvalidProduct { get; set; }
		public DateTime? ExpiryDate { get; set; }

		public TimeSpan TimeLeftInReservation
		{
			get
			{
				var timeLeft = TimeSpan.Zero;

				if (ExpiryDate.HasValue)
				{
					timeLeft = (ExpiryDate.Value - DateTime.Now);
				}

				return timeLeft;
			}
		}
	}
}
