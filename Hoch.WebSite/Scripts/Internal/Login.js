﻿/// <reference path="../angular.min.js" />
//FIXES
//"checkout" refresh is redirecting to login screen


app.controller("recapCtrl", ['$scope', '$http', '$sce', 'vcRecaptchaService', '$window', '$cookies', 'CommonCode', '$rootScope', function ($scope, $http, $sce, vcRecaptchaService, $window, $cookies, CommonCode, $rootScope) {
    var keepMeSignedIn = $cookies.get('keepMeSignedIn');
    var baseCookieSettings = getCookie("BaseCookieSettings");
    $scope.Redirect = true;
    //If we are want to run a function after login then set this value. See basket.js.
    $scope.FunctionToRun = null;

    $scope.getRecaptchaSiteKey = function () {
        $http({
            method: 'GET',
            url: baseUrl + "Settings/GetRecaptchaSiteKey",
            contentType: 'application/json'
        }).then(function (result) {
            $scope.siteKey = result.data;
        });
    }
    $scope.getRecaptchaSiteKey();

    //$scope.siteKey = "6LcA1I8UAAAAAOV5uwA-jNctU9T2vLWU8Ijottv3";
    $scope.isPasswordError = false;
    $scope.isEmailError = false;
    $scope.isEmailNotValid = false;
    $scope.isError = false;
    if (keepMeSignedIn != null && keepMeSignedIn != undefined) {
        keepMeSignedIn = JSON.parse(keepMeSignedIn);
        $('#checkoutLoginWrapper').hide();
        //$('#checkoutUpsellcontainer').show();
        $('#checkoutDeliveryWrap').show();
        if (window.location.search.indexOf("AddedCard") == -1) {
            //$('#checkoutUpsellcontainer').show();
            $('#checkoutDeliveryWrap').show();
        }
        else {
            $('#checkoutDeliveryWrap').hide();
            $('#checkoutUpsellcontainer').hide();
        }
    } else {
        // $('#checkoutUpsellcontainer').show();
        $('#checkoutLoginWrapper').show();
        $('#checkoutDeliveryWrap').hide();

        $scope.email = "";
        $scope.password = "";
        $scope.errorMessage = "";

        $scope.persSignIn = false;
        $scope.errorFromWiki = null;

        $scope.onSubmit = function () {
            $scope.validateEmail();
        }

        $scope.captchaExecute = function () {
            vcRecaptchaService.execute();
        }

        $scope.customerLogin = [];

        $scope.validateEmail = function () {
            $scope.isPasswordError = false;
            $scope.isEmailError = false;
            $scope.isError = false;
            $scope.isEmailNotValid = false;
            // set error if email is empty
            if ($scope.email == "") {
                $scope.isError = true;
                $scope.isEmailError = true;
            } else {
                // if email is not empty check if a valid address is entered
                regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!regex.test($scope.email)) {
                    $scope.isError = true;
                    $scope.isEmailNotValid = true;
                }
            }
            if ($scope.password == "") {
                $scope.isError = true;
                $scope.isPasswordError = true;
            }

            if (!$scope.isError) {
                var loginDetails = { Email: $scope.email, Password: $scope.password };
                $http({
                    method: 'POST',
                    url: baseUrl + "Account/Login",
                    contentType: 'application/json',
                    data: loginDetails
                }).then(function (result) {
                    $scope.customerLogin = result.data;
                    if ($scope.customerLogin !== undefined) {
                        if ($scope.customerLogin.CustomerLookupId != null) {
                            $scope.captchaExecute();
                        }
                        else {
                            $scope.errorFromWiki = $scope.customerLogin.ErrorMessage.MessageToDisplay;
                            $scope.isError = true;
                        }
                    }
                });
            }
        }

        $scope.signup = function () {
            if ($scope.customerLogin !== undefined) {
                if ($scope.customerLogin.CustomerLookupId != null) {
                    var expireDate = new Date();
                    if ($scope.persSignIn) {
                        expireDate.setDate(expireDate.getDate() + 20);
                    } else {
                        //expireDate.setHours(expireDate.getHours() + 1);
                        expireDate.setMinutes(expireDate.getMinutes() + loginDuration);
                    }
                    //delete_cookie("keepMeSignedIn");
                    var loginDate = new Date();
                    var keepMeSignedIn = { CustomerLookupId: $scope.customerLogin.CustomerLookupId, Email: $scope.email, Value: $scope.persSignIn, loginDate: loginDate, Firstname: $scope.customerLogin.FirstName, Lastname: $scope.customerLogin.LastName };

                    //We need to put the secure tag on this cookie.
                    //TODO: secure tag.
                    $cookies.put("keepMeSignedIn", JSON.stringify(keepMeSignedIn), {
                        'expires': expireDate,
                        'secure': location.protocol == "https:" ? true : false
                    });
                    var cookie = $cookies.get('keepMeSignedIn');
                    

                    // login fav redirects to the same url after logged in
                    if (CommonCode.favBaseUrl != null && CommonCode.favBaseUrl != "" && CommonCode.favBaseUrl != undefined) {
                        var url = CommonCode.favBaseUrl;
                        CommonCode.favBaseUrl = null;
                        $window.location.href = url;
                    }
                    else {
                        if (window.location.href.indexOf('checkout') != -1) {//if its on checkout
                            $('#checkoutLoginWrapper').hide();
                            localStorage.setItem("loginCheckoutCheck", '1');
                            if (window.location.search.indexOf("AddedCard") == -1) {

                                // $('#checkoutUpsellcontainer').show();
                                $('#checkoutDeliveryWrap').show();
                                location.reload();
                            }
                            else {
                                $('#checkoutDeliveryWrap').hide();
                            }

                            //may be local storage and use a differnt script since using same controller is having issues
                            //update customer with reservation
                        } else {

                            if (window.location.search.indexOf('addVoucher') == 1) {
                                $window.location.href = '/basket';
                            }
                            else {

                                $("#account-name").text($scope.customerLogin.FirstName);

                                if ($scope.Redirect) {
                                    $window.location.href = '/myaccount';
                                }
                                else {
                                    if ($rootScope.partialCheck == 0)//coming from basket add voucher
                                    {
                                        if ($scope.FunctionToRun != null && typeof ($scope.FunctionToRun) == "function") {
                                            $scope.FunctionToRun();
                                        }


                                        //$window.location.href = '/basket';
                                        $rootScope.partialCheck = 1;
                                        $rootScope.ShowModal("voucher_template");
                                    }
                                    else
                                    {

                                   
                                        angular.element(document.getElementById('headerMainController')).scope().isLoggedIn = true;
                                        HideModal();
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
        }
    }

}]);





