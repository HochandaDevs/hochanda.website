﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hoch.Website.Helpers;

namespace Hoch.Website.Controllers.Brands
{
    public class BrandsController : BaseController
    {
        // GET: Brands
		[MetadataActionFilter(MetaDataFriendlyName = "Brands - Overview Page")]
        public ActionResult Index()
        {
          //  GetBrandsData();
            return View("/Views/Brands/Brands.cshtml");
            
        }
        


    }
}