﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class GetCardDetailsModel
	{
		public Guid CustomerReference { get; set; }

		public string BasketId { get; set; }

		[JsonIgnore]
		public long? BasketIdToUse
		{
			get
			{
				long? basketId = null;

				if (!string.IsNullOrEmpty(BasketId))
				{
					basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
				}

				return basketId;
			}
		}
	}

	public class CallCentreCardDetailsModel : GetCardDetailsModel
	{
		public CallCentreCardDetailsModel() : base()
		{

		}

		public int BillingAddressId { get; set; }
	}
}
