﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;

namespace ModelLayer.Models.Reservation
{
	public class UpdateItemReservationModel
	{
		public string BasketId
		{
			get
			{
				string basketId = string.Empty;

				if (BasketIdToUse.HasValue)
				{
					basketId = GenericHelper.Instance.ProtectBasketId(BasketIdToUse.Value);
				}

				return basketId;
			}

		}

		[JsonIgnore]
		public long? BasketIdToUse { get; set; }

		public bool HasValidationErrorOccured { get; set; }
		public bool IsReservationActive { get; set; }
		public bool HasReservationExpired { get; set; }
		public DateTime? ExpiryDate { get; set; }

		public TimeSpan TimeLeftInReservation
		{
			get
			{
				var timeLeft = TimeSpan.Zero;

				if (ExpiryDate.HasValue)
				{
					timeLeft = (ExpiryDate.Value - DateTime.Now);
				}

				return timeLeft;
			}
		}

		public bool HasCountryCurrencyOrLanguageChanged { get; set; }
		public bool HasProductLimitBeenReached { get; set; }
		public bool HasProductBeenAdded { get; set; }
		public bool HasProductBeenRemoved { get; set; }
		public bool IsProductInStock { get; set; }
		public bool IsProductVariationValid { get; set; }
		public bool IsProductAvailableInCountry { get; set; }
		public bool IsProductAvailableInCurrency { get; set; }
		public bool IsVariationActive { get; set; }
		public bool IsProductActive { get; set; }
		public bool IsPickAndMix { get; set; }
		public bool PickAndMixValidCount { get; set; }
		public int BasketCount { get; set; }
		public int? MessageCode { get; set; }
		public string MessageTitle { get; set; }
		public string MessageToDisplay { get; set; }
		public string OKButtonText { get; set; }
		public string CancelButtontext { get; set; }
	}
}