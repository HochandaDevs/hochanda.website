﻿using Hoch.Website.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Hoch.Website.Controllers.Account
{
    public class LoginController : BaseController
    {
        // GET: Login
		[MetadataActionFilter(MetaDataFriendlyName = "Login")]
        public ActionResult Index(bool newAccount = false)
        {
            if (Request.Cookies["keepMeSignedIn"] != null)
            {
                return RedirectToAction("myaccount", "account");
            }
            else
            {
                if (newAccount == true)
                {
                    ViewBag.ForNewCustomer = "Account created successfully. Please login to your account.";
                }
                else
                {
                    ViewBag.ForNewCustomer = "";
                }
                return View("~/Views/Account/Login.cshtml");
            }
           
        }

        public class GoogleVerifyModel
        {
            public string RecaptchaResponse { get; set; }
        }

        public class GoogleResponseModel
        {
            public bool success { get; set; }
            public string challenge_ts { get; set; }
            public string hostname { get; set; }
        }

		[ExtendLoginCookie(DoNotExtend = true)]
        public async Task<ActionResult> GoogleVerify(GoogleVerifyModel request)
        {
            WebRequestHelper wh = new WebRequestHelper();
            Uri uri = new Uri(ConfigurationManager.AppSettings.Get("recaptchaSiteVerify"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("secret", ConfigurationManager.AppSettings.Get("recaptchaSecretKey")));
            postData.Add(new KeyValuePair<string, string>("response", request.RecaptchaResponse));
            var response = await wh.PostRequest<GoogleResponseModel>(uri, postData);
            return Json(response);
        }
    }
}