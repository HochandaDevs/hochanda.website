﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class AccountPaymentMethodModel
    {
        public int iBLookup { get; set; }
        public string custEmail { get; set; }
        public Guid custRef { get; set; }
        public string last4digits { get; set; }
    }
}
