﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.Account
{
    public interface IAccountSettingsModel
    {

        List<ICurrencyModel> availableCurrencies { get; set; }
        string customerRef { get; set; }
        string custEmail { get; set; }
        int customerId { get; set; }
        string firstName { get; set; }
        int defaultCurrency { get; set; }
        int receivePost { get; set; }
        int receiveSms { get; set; }
        int receiveEmail { get; set; }
        int receiveEmail3rdParty { get; set; }

    }
}
