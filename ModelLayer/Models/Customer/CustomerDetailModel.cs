﻿using ModelLayer.Models.MobileApps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Customer
{
	public class CustomerDetailModel
	{
		public CustomerDetailModel()
		{
			//We always want this to be null unless there is a problem
			ErrorMessage = null;
		}

		public Guid? CustomerLookupId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public MessageModel ErrorMessage { get; set; }
	}
}
