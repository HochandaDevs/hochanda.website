﻿using ModelLayer.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Order
{
	public class GetOrderStatusModel
	{
        public string BasketId { get; set; }

        [JsonIgnore]
        public long? BasketIdToUse
        {
            get
            {
                long? basketId = null;

                if (!string.IsNullOrEmpty(BasketId))
                {
                    basketId = GenericHelper.Instance.UnprotectBasketId(BasketId);
                }

                return basketId;
            }
        }

        public int LanguageId { get; set; }
	}
}
