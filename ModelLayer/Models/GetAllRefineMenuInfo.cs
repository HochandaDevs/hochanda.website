﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetAllRefineMenuInfo
    {
        public List<List<RefineCategoriesInfo>> refineCategoriesInfo { get; set; }
        public List<RefineBrandInfo> refineBrandsInfo { get; set; }
        public List<RefineBrandAmbassadorInfo> refineBrandAmb { get; set; }
    }

    public class RefineCategoriesBrandsInfo
    {
        public List<RefineCategoriesInitialInfo> refineCategories { get; set; }
        public List<RefineBrandInfo> refineBrands { get; set; }
    }

    public class RefineCategoriesInitialInfo
    {
        public int parentproductid { get; set; }
        public int refinecategory1id { get; set; }
        public int refinecategory2id { get; set; }
        public int refinecategory3id { get; set; }
        public string firstlevelcatergoryname { get; set; }
        public string secondlevelcatergoryname { get; set; }
        public string thirdlevelcatergoryname { get; set; }
        public int total { get; set; }
        public int freedomtotal { get; set; }
        public int felxibuytotal { get; set; }
        public int downloadtotal { get; set; }
        public int freedomfelxidownloadtotal { get; set; }
        public int freedomfelxitotal { get; set; }
        public int freedomdownloadtotal { get; set; }
        public int felxidownloadtotal { get; set; }
    }

    public class RefineBrandInfo
    {
        public string brandcatergory { get; set; }
        public string brandname { get; set; }
        public string brdId { get; set; }
        public int brdtotal { get; set; }
        public int catergory2idB { get; set; }
        public int catergory3idB { get; set; }
        public int freedomflexidownload { get; set; }
        public int freedom { get; set; }
        public int felxibuy { get; set; }
        public int download { get; set; }
        public int freedomfelxibuydownloadtotal { get; set; }
        public int freedomfelxibuytotal { get; set; }
        public int felxibuydownloadtotal { get; set; }
        public int freedomdownloadtotal { get; set; }
        public int freedomtotal { get; set; }
        public int felxibuytotal { get; set; }
        public int downloadtotal { get; set; }
        public int productbrandlink { get; set; }
    }

    public class RefineCategoriesInfo
    {
        public RefineCategoriesInfoFirstLevel refineCategoriesFirstLevel = new RefineCategoriesInfoFirstLevel();
    }

    public class RefineCategoriesInfoFirstLevel
    {
        public int category1id { get; set; }
        public string firstlevelcatergoryname { get; set; }
        public List<RefineCategoriesInfoSecondLevel> refineCategoriesSecondLevel = new List<RefineCategoriesInfoSecondLevel>();
    }

    public class RefineCategoriesInfoSecondLevel
    {
        public int category2id { get; set; }
        public string secondlevelcatergoryname { get; set; }
        public int levelTwoCount { get; set; }
        public int levelTwoFreedomCount { get; set; }
        public int levelTwoFelxibuyCount { get; set; }
        public int levelTwoDownloadsCount { get; set; }
        public int levelTwoFreedomFelxiDownloadCount { get; set; }
        public int levelTwoFreedomFelxiCount { get; set; }
        public int levelTwoFreedomDownloadCount { get; set; }
        public int levelTwoFelxiDownloadCount { get; set; }
        public List<RefineCategoriesInfoThirdLevel> refineCategoriesThirdLevel = new List<RefineCategoriesInfoThirdLevel>();
    }
    public class RefineCategoriesInfoThirdLevel
    {
        public int category3id { get; set; }
        public string thirdlevelcatergoryname { get; set; }
        public int levelThreeCount { get; set; }
        public int levelThreeFreedomCount { get; set; }
        public int levelThreeFelxibuyCount { get; set; }
        public int levelThreeDownloadsCount { get; set; }
        public int levelThreeFreedomFelxiDownloadCount { get; set; }
        public int levelThreeFreedomFelxiCount { get; set; }
        public int levelThreeFreedomDownloadCount { get; set; }
        public int levelThreeFelxiDownloadCount { get; set; }
    }

    public class RefineBrandAmbassadorInfo
    {
        public int BrandAmbID { get; set; }
        public string BrandAmbName { get; set; }
    }
}
