﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.PageBuilder
{
	public class PageBuilderFormDataModel
	{
		public int PageBuilderId { get; set; }
		public string FormData { get; set; }
	}
}
