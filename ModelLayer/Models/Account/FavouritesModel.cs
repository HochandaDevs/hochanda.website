﻿using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class FavouritesPagesModel
    {
        public string PageTitle { get; set; }
        public int PageUrl { get; set; }
    }

    public class SetFavouritePageModel
    {
        public Nullable<int> active { get; set; }
        public string costprice { get; set; }
        public Nullable<int> customerid { get; set; }
        public int id { get; set; }
        public string instock { get; set; }
        public string pagename { get; set; }
        public Nullable<int> pageorproduct { get; set; }
        public string pageurl { get; set; }
        public Nullable<int> productid { get; set; }
        public string productimg { get; set; }
        public Nullable<decimal> productprice { get; set; }
        public string productsku { get; set; }
        public string producttitle { get; set; }
        public System.Guid CustomerIdLookup { get; set; }
        public string emailaddress { get; set; }
    }

    public class SetFavourite
    {

        public List<SetFavouritePageModel> pageDetails { get; set; }
        public int productcount { get; set; }
        public List<SetFavouritePageModel> productDetails { get; set; }
    }

    public class FavouritesListClass
    {
        public List<ProductInformationModel> products { get; set; }
        public List<SetFavouritePageModel> pages { get; set; }
    }

    public class FavouriteInputModel
    {
        public Guid custRef { get; set; }
        public string custEmail { get; set; }
        public int languageId { get; set; }
        public int currencyId { get; set; }
        public int countryId { get; set; }
    }

    public class FavouritePageInfo
    {
        public Guid customerRef { get; set; }
        public string customerEmail { get; set; }
        public string pageUrl { get; set; }
		public int languageId { get; set; }
        public int currencyId { get; set; }
		public int countryId { get; set; }
	}
}
