﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.TVSchedule
{
	public class TVScheduleModel
	{
		public int TVScheduleId { get; set; }
		public int ChannelId { get; set; }
		public string ShowName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string DefaultEPGText { get; set; }
		public bool AllowWatchAgain { get; set; }
		public bool Hour2 { get; set; }
		public DateTime ShowDateTime { get; set; }
		public string ShowDateTimeUTCFormat { get; set; }
		public bool ShowProducts { get; set; }
		public bool ShowWatch { get; set; }
		public bool IsODSHour { get; set; }
		public int DisplayHour { get; set; }
		public int RewindDisplayHour { get; set; }
		public string AMOrPM { get; set; }
		public bool Locked { get; set; }
		public bool IsLive { get; set; }
		public int EPGCategoryId { get; set; }
		public string EPGLink { get; set; }
		public string EPGSrc { get; set; }
		public string EPGAltText { get; set; }
		public string URLDateTime { get; set; }

	}
}
