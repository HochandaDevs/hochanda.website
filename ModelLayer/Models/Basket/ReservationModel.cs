﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class ReservationModel
	{
		public Guid ReservationId { get; set; }
		public int CustomerId { get; set; }
		public int CreatedBy { get; set; }
		public DateTime? CreatedOn { get; set; }
		public DateTime? ExpiryDate { get; set; }
		public bool Active { get; set; }
		public int ReservationSource { get; set; }
		public int CurrencyId { get; set; }
		public int LanguageId { get; set; }
		public int CountryId { get; set; }
		public int OrderId { get; set; }
		public int FreedomDiscountApplied { get; set; }
		public string GuestId { get; set; }
		public int ClientMinsDiff { get; set; }
		public int PriorityOrder { get; set; }
		public string IPAddress { get; set; }
		public int ServiceIdOverride { get; set; }
		public int CheapestCourierPricing { get; set; }
		public int CampaignId { get; set; }
		public int PushedToWebByCC { get; set; }
		public int CompanyId { get; set; }
		public int FlexiBasketPossible { get; set; }
		public bool FlexiBasketSelected { get; set; }
		public decimal ToPayFigure { get; set; }
		public decimal FlexiBasketPreview { get; set; }
		public decimal ToPayFigureToday { get; set; }
		public int BasketCount { get; set; }
	}
}
