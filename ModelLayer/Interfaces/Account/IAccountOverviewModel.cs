﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Interfaces.Account
{
    public interface IAccountOverviewModel
    {

        int customerid { get; set; }
        string freedommember { get; set; }
        string freedomholiday { get; set; }
        string hascustomercredit { get; set; }
        decimal? creditamount { get; set; }
        string creditamountwithcurrency { get; set; }
    }
}
