﻿using Hoch.Website.Helpers;
using Hoch.Website.Models;
using ModelLayer.Models;
using ModelLayer.Models.Product;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hoch.Website.Controllers.Campaign
{
	public class CampaignController : BaseController
	{
		private WebRequestHelper _WebRequest;

		public CampaignController()
		{
			_WebRequest = new WebRequestHelper();
		}

		public async Task<ActionResult> Index(string campaignName)
		{
			if (!string.IsNullOrEmpty(campaignName))
			{
				CampaignModel campaign = await CheckForValidCampaignByName(campaignName);

				if (campaign != null)
				{
					await SetCampaignCookie(campaign);

					return Redirect(campaign.redirecturl);
				}
			}

			string Insert404InLog = ConfigurationManager.AppSettings.Get("Insert404InLog");

			if (Insert404InLog == "1")
			{
				string message = MvcApplication.PreviousUrl != null ? MvcApplication.PreviousUrl : String.Empty;
				message = "404 on url: " + message;
				int r = await new WebRequestHelper().InsertErrorToPostgres(message, "1");
			}

			string notFoundMessage = await GetNotFoundMessage();

			var viewParams = new MessageCodeModel
			{
				ErrorCode = 404,
				ErrorDescription = notFoundMessage,
				ErrorTitle = "Page not found"
			};

			TempData["MessageCode"] = viewParams;

			return RedirectToAction("NotFound", new RouteValueDictionary(new { controller = "Errors", action = "NotFound" }));
		}

		[Route("paidonresults/{parentProductId}")]
		[Route("paidonresults")]
		[Route("POR")]
		[Route("POR/{parentProductId}")]
		public async Task<ActionResult> PaidOnResults(int? parentProductId)
		{
			if (parentProductId.HasValue)
			{
				CampaignModel campaign = await CheckForValidCampaignByName("POR");

				if (campaign != null)
				{
					await SetCampaignCookie(campaign);
				}

				//Now get the product information
				int parentProductIdToUse = 0;
				string parentProductSKU = string.Empty;
				string parentProductTitle = string.Empty;

				if (parentProductId.HasValue)
				{
					parentProductIdToUse = parentProductId.Value;

					try
					{
						string url = ConfigurationManager.AppSettings.Get("WebAPIUrl") + $"products/GetProductInformation?parentProductId={parentProductIdToUse}&languageId={BaseLanguageID}&currencyId={BaseCurrencyID}&countryId={CustomerIPCountry}";

						var product = await new WebRequestHelper().GetItem<ProductInformationModel>(url);

						if (product != null)
						{
							parentProductSKU = product.ParentProductSKU;

							parentProductTitle = Utility.GetWebSafeString(product.TVDescription);
						}
					}
					catch
					{
						//Do nothing. Just don't fail the rest of the code.
					}
				}
				//Go to product page.
				if (parentProductIdToUse != 0)
				{
					return RedirectToRoute("Product", new { parentProductId = parentProductIdToUse, webSafeTitle = parentProductTitle, sku = parentProductSKU });
				}
				else
				{
					return Redirect("/");
				}
			}

			//Redirect to homepage.
			return Redirect("/");
		}
		private async Task<CampaignModel> CheckForValidCampaignByName(string campaignName)
		{
			string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
			CampaignModel campaign = await _WebRequest.GetItem<CampaignModel>(apiUrl + "Campaign/GetCampaignByName?CampaignName=" + campaignName);
			return campaign;
		}

		private async Task<string> GetNotFoundMessage()
		{
			string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");
			string message = await _WebRequest.GetItem<string>(apiUrl + "Messages/GetMessageByMessageCode?messageCode=" + 404);
			return message;
		}

		private async Task SetCampaignCookie(CampaignModel campaign)
		{
			if (campaign != null)
			{

				int cookieCampaignid = campaign.campaignid;

				HttpCookie campaignCookie = Request.Cookies["CampaignIDCookie"];
				if (campaignCookie == null)
				{
					campaignCookie = new HttpCookie("CampaignIDCookie");
				}

				campaignCookie.Value = cookieCampaignid.ToString();
				campaignCookie.Expires = DateTime.Now.AddDays(30);
				Response.Cookies.Add(campaignCookie);
				//Update hit for the campaignid
				bool islocal = Request.UserHostAddress == "127.0.0.1" || Request.UserHostAddress == "::1";

				if (!islocal)
				{
					await UpdateHit(campaign.campaignid);
				}
			}

		}

		private async Task<bool> UpdateHit(int campaignId)
		{
			string apiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl");

			var postData = new List<KeyValuePair<string, string>>();
			postData.Add(new KeyValuePair<string, string>("campaignId", campaignId.ToString()));

			bool success = await _WebRequest.PostRequest<bool>(new Uri(apiUrl + "Campaign/UpdateHit"), postData);
			return success;
		}



        public ActionResult CallCentre()
        {
            ViewBag.IsExternal = true;
            return View("~/Views/CallCentre/Payment.cshtml");
        }



    }
}