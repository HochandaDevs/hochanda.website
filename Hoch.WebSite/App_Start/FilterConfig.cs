﻿using Hoch.Website.Helpers;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

			//Every request will now extend the cookie request. It is up to the person who has created the controller action to decide which ones to not extend.
			filters.Add(new ExtendLoginCookie());
        }
    }
}
