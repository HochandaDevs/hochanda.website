﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class OrderHistoryModel
    {
        public DateTime? OrderDate { get; set; }
        public int OrderId { get; set; }
        public decimal? OrderValue { get; set; }
        public int? OrderDespatched { get; set; }
        public string FlexiItems { get; set; }
        public string DownloadItems { get; set; }
        public string OrderStatus { get; set; }
        public string OrderValueString { get; set; }
        public int CurrencyId { get; set; }
    }

    public class OrderHistoryListModel
    {
        public int NumberOfPages { get; set; }
        public List<OrderHistoryModel> Orders = new List<OrderHistoryModel>();
    }
}
