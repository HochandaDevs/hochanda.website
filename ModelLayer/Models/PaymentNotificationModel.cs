﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class PaymentNotificationModel
    {
        public int? processorid { get; set; }
        public int? orderid { get; set; }
        public string reservationid { get; set; }
        public int? result { get; set; }
        public DateTime? datestamp { get; set; }
        public string last4digits { get; set; }
        public int? processed { get; set; }
        public decimal? paymentamount { get; set; }
        public string currencytype { get; set; }
        public string chaseorderid { get; set; }
        public int? transactiontype { get; set; }
        public string chaseresid { get; set; }
        public string txrefnum { get; set; }

        public string mitReceivedTxId { get; set; }
        public string mitMsgType { get; set; }
    }
}
