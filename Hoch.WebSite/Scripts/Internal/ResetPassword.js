﻿app.controller("resetPasswordCtrl", ['$scope', '$http', '$compile', function ($scope, $http, $compile) {

    $scope.TokenValidation = function () {
        $scope.validlink = 0;
        var token = document.getElementById('hid_rp').value;
        var params = { token: token}
        $http({
            method: 'POST',
            url: baseUrl + "Account/ResetPasswordTokenValidation",
            contentType: 'application/json',
            data: params
        }).then(function (result) {
            if (result != null && result != undefined) {
                if (result.data == true) {
                    $scope.validlink = 1;
                }
            }
        });
    }

    $scope.ResetPassword = function (newP, confirmP) {

        var newPassword = newP;
        var confirmPassword = confirmP;
        var result = true;
        var regex = "";
        var rp = document.getElementById('hid_rp').value;
        document.getElementById('validationErrorDiv').innerHTML = "";
        $scope.validationMessages = "<div>";

        if (newPassword == null || newPassword == "") {
            result = false;
            $scope.validationMessages += "<label>New Password can't be empty</label>";
        }
        else {
            if (newPassword.length < 8) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least 8 characters</label>";
            }
            regex = /[0-9]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one number (0-9)</label>";
            }
            regex = /[a-z]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one lowercase letter (a-z)</label>";
            }
            regex = /[A-Z]/;
            if (!regex.test(newPassword)) {
                result = false;
                $scope.validationMessages += "<label>Password must contain at least one uppercase letter (A-Z)</label>";
            }
        }
        if (confirmPassword == null || confirmPassword == "") {
            result = false;
            $scope.validationMessages += "<label>Confirm New Password can't be empty</label>";
        }
        else {
            if (newPassword != confirmPassword) {
                result = false;
                $scope.validationMessages += "<label>Passwords don't match</label>";
            }
        }

        $scope.validationMessages += "</div>";

        var errorDiv = $('#validationErrorDiv');
        var htmlelement = angular.element($scope.validationMessages);
        var compileAppendedArticleHTML = $compile(htmlelement[0]);
        var element = compileAppendedArticleHTML($scope);
        errorDiv.append(element[0]);

        if (result == true) {
           var param = { rp: rp, newP: newP };
            $http({
                method: 'POST',
                url: baseUrl + "Account/ResetPassword",
                contentType: 'application/json',
                data: param
            }).then(function (result) {
                var errorDiv;
                //Display message password reset completed
                if (result.data.Success == true) {
                    $scope.validationMessages = "<div>Password Updated Successfully</div>";
                    errorDiv = $('#validationSuccessDiv');
                }
                else {
                    $scope.validationMessages = "<div>Please try again later</div>";
                    errorDiv = $('#validationErrorDiv');
                }
                
                var htmlelement = angular.element($scope.validationMessages);
                var compileAppendedArticleHTML = $compile(htmlelement[0]);
                var element = compileAppendedArticleHTML($scope);
                errorDiv.append(element[0]);

                //$window.location.href = "/";
            });
        }
    }
}]);


app.controller("forgotPasswordCtlr", ['$scope', '$http', function ($scope, $http) {
    //$scope.HideReset = false;
    $scope.MessageToDisplay = true;

    //function activateBtn() {
    //    $scope.HideReset = false;
    //    $scope.MessageToDisplay = true;
    //};

    $scope.ResetPassword = function (EmailAddress) {
        
        if (EmailAddress != null) {
            var email = EmailAddress;
            $http({
                method: 'POST',
                url: baseUrl + "Account/ForgotPasswordResetLink",
                contentType: 'application/json',
                data: { email: email }
            }).then(function (result) {
                if (result.data.Success == true) {
                    $scope.Success = true;
                    $scope.MessageToDisplay = "Email has been sent.";
                }
                else {
                    $scope.Success = false;
                    $scope.MessageToDisplay = result.data.MessageToDisplay;
                }
                //$scope.HideReset = true;
                
                //$timeout(activateBtn, 900000);// 15 minutes
            });
        }
        
    };
}]);