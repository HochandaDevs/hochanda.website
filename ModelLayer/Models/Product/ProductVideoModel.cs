﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Product
{
	public class ProductVideoModel
	{
		public int TVScheduleId { get; set; }
		public DateTime StartDate { get; set; }
		public string ShowDateUTC { get; set; }
		public string ShowName { get; set; }
	}
}
