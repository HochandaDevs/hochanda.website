﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class CampaignModel
    {
        public int campaignid { get; set; }
        public string campaignname { get; set; }
        public string campaignurl { get; set; }
        public int affiliateid { get; set; }
        public Nullable<System.DateTime> startdate { get; set; }
        public Nullable<System.DateTime> enddate { get; set; }
        public Nullable<int> active { get; set; }
        public string redirecturl { get; set; }
        public int hits { get; set; }
        public Nullable<int> voucherid { get; set; }
        public string CampaignLogoUrl { get; set; }
        public Nullable<int> parentproductID { get; set; }
        public string campaigndescription { get; set; }
        public string campaignmode { get; set; }
    }
}
