﻿using Hoch.Website.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Controllers.TVSchedule
{
    public class TVScheduleController : BaseController
    {
        // GET: TVSchedule
        [Route("tv-schedule")]
		[MetadataActionFilter(MetaDataFriendlyName = "TV Schedule")]
        public ActionResult TVSchedule()
        {
            ViewBag.CurrencyId = BaseCurrencyID;
            ViewBag.LanguageId = BaseLanguageID;
            ViewBag.CountryId = CustomerIPCountry;
            return View("~/Views/tv-schedule/TvSchedule.cshtml");
        }

        //[HttpGet]
        [Route("cinema")]
		[MetadataActionFilter(MetaDataFriendlyName = "Rewind")]
        public ActionResult Cinema()
		{            
            return View("~/Views/Rewind/Cinema.cshtml");
        }

		[Route("TvSchedule")]
		public ActionResult TVScheduleRedirect()
		{
			return RedirectPermanent("/tv-schedule");
		}
	}
}
