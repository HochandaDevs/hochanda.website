﻿
function LoadOwlCarousel(elementToUse) {

    var owl = $('.product_carousel');

    if (elementToUse != null) {
        owl = elementToUse;
    }

    owl.hide();

    owl.owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        dots: true,
        responsive: {
            0: {
                items: 2,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            768: {
                margin: 20,
                nav: false
            },
            1000: {
                items: 4,
                margin: 30,
                nav: true
            },
            1200: {
                items: 5,
                margin: 30
            },
            1280: {
                items: 6,
                margin: 30
            },
            1500: {
                items: 7,
                margin: 30
            }
        },
        onDrag: function (event) {
            HidePop();
        }
    });

    setTimeout(function () {
        owl.show();
    }, 2000);
}

function LoadImageCarousel(elementToUse) {

    var owl = $('.product_carousel');

    if (elementToUse != null) {
        owl = elementToUse;
    }

    owl.hide();

    owl.owlCarousel({
        loop: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 5000,
        speed: 3000,
        margin: 10,
        responsiveClass: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            740: {
                margin: 20,
                items: 2,
                nav: true
            },
            1200: {
                margin: 20,
                items: 3,
                nav: true
            }
        }
    });

    setTimeout(function () {
        owl.show();
    }, 2000);
}

function LoadProductVideoCarousel() {
    // product page video carousel
    $(".product_video_carousel").hide().owlCarousel({
        loop: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 5000,
        speed: 3000,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            520: {
                margin: 20,
                items: 1
            },
            768: {
                margin: 20,
                items: 1
            },
            1024: {
                margin: 20,
                items: 2
            }
        }


    }).show();
}


// product page single image carousel
function LoadSingleOwl(elementToUse) {
    var owl = $('.singleProductOwl');


    if (elementToUse != null) {
        owl = elementToUse;
    }

    owl.owlCarousel({
        loop: false,
        dots: false,
        nav: true,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 5000,
        speed: 3000,
        responsive: {
            0: {
                items: 1
            }
        }
    });
}

//Straight away load the carousel on any page.
LoadOwlCarousel();

// Add to basket button pop under
function HidePop() {
    $('.pop').hide();
}

function ShowPop() {
    $('.pop').show();
}

//Show and hide the 'what is flexi-order basket' on the basket page
//todo: full in information from the TC page
function FlexiOrderTCToggle() {
    $("#flexiOrderTC").slideToggle();
}
function AddAddressToggle() {
    $("#AddAddressToggle").slideToggle();
}
function DeliveryToggle(toShow) {
    var deliveryShow = $(toShow).parent().find(".deliveryInfo");

    if (deliveryShow.is(":hidden")) {
        $(".deliveryInfo").slideUp();
        deliveryShow.slideDown();
    }
}

// Homepage grid items the same height and width
$(document).ready(function () {
    setTimeout(function () {
        $(".featured__cat .featured__item").css("height", $(".featured__cat .featured__item").width() + "px");
    }, 500);
});
$(window).resize(function () {
    $(".featured__cat .featured__item").css("height", $(".featured__cat .featured__item").width() + "px");
});

//MENU 2018
$(".header__main__menu").click(function () {

    if ($("#mySidenav").is(":hidden")) {
        $("#mySidenav").show(0, function () {
            ShowOrHideMenu();
        });
    }
    else {
        ShowOrHideMenu();
    }
});

function ShowOrHideMenu() {
    $(".h2_top_search_largescreen .mn2_search_toggle_btn").removeClass("hideSearch");
    $(".header__main__menu").toggleClass("open");
    $("#mySidenav").toggleClass("open");
    $(".mn2_toggle_btn").removeClass("open");
    $("#myCanvasNavTopMenu").removeClass("active");
    $(".mn2_toggle_box").hide();
    $("#myCanvasNavSearch").removeClass("active");

    if ($("#myCanvasNav").hasClass("active")) {

        $("#myCanvasNav").addClass("out");
        $("#myCanvasNav").removeClass("active");

        setTimeout(function () {
            $("#myCanvasNav").removeClass("out");
            $("#mySidenav").hide();

        }, 500);
        $("html").removeClass("frozen");

    }
    else {

        $("#myCanvasNav").addClass("active");
        $("html").addClass("frozen");
    }
}

$(".closebtn, #myCanvasNav, #myCanvasNavSearch").click(function () {

    $(".header__main__menu").removeClass("open");
    $("#mySidenav").removeClass("open");
    $("#fullscreenSearch").hide();
    $(".h2_top_search_largescreen .mn2_search_toggle_btn").removeClass("hideSearch");


    $("#myCanvasNav").addClass("out");
    $("#myCanvasNav").removeClass("active");
    $("#myCanvasNavSearch").removeClass("active");
    $("html").removeClass("frozen");

    setTimeout(function () {
        $("#myCanvasNav").removeClass("out");
        $("#mySidenav").hide();

    }, 500);


}
);

// DELIVERY | CURENCY | LANGAUGE - DROPDOWN

$(function () {
    $("a.preference_selector__toggle").click(function () {
        $(".preference_selector__box").slideToggle();
        $(".preference_selector__toggle").toggleClass("open");
        return false;
    });
});



//BRANDS

$(".header__secondary__brands_toggle").click(function () {
    $("#categoryDropdownMenu").hide();
    $("#brandsDropdownMenu").toggle();
});

//CATEGORIES

$(".header__secondary__category_toggle,.header__secondary__category_staff").click(function () {
    $("#brandsDropdownMenu").hide();
    $("#categoryDropdownMenu").toggle();
});


//Product page filter - toggle switches
//$(function () {
//    $('.toggle').on('click', function (event) {
//        event.preventDefault();
//        $(this).toggleClass('active_toggle');
//    });
//});

function ShowHideElement(forceHide, element, scrollToElement) {

    if (!(element instanceof jQuery)) {
        element = $(element);
    }

    if (!(scrollToElement instanceof jQuery)) {
        scrollToElement = $(scrollToElement);
    }

    if (forceHide) {
        element.slideUp();
    }
    else {
        element.slideToggle();

        if (element.is(":visible")) {
            ScrollToElement(scrollToElement);
        }
    }
}

function ShowHidePageNumbers(forceHide) {
    ShowHideElement(forceHide, $("#pageNumberFilter"), $("#pageNumberFilterButton"));
}

function ShowHideSortBy(forceHide) {
    ShowHideElement(forceHide, $("#sortByFilter"), $("#sortByFilterButton"));
}

function ShowHideCategoryFilter(forceHide) {
    ShowHideElement(forceHide, $("#categoryFilter"), $("#categoryFilterButton"));
}
function ShowHideBrandFilter(forceHide) {
    ShowHideElement(forceHide, $("#brandFilter"), $("#brandFilterButton"));
}

$(document).mouseup(function (e) {

    //Sort By
    var container = $("#sortByFilter");
    var containerButton = $("#sortByFilterButton")
    // if the target of the click isn't the container nor a descendant of the container
    if ((!container.is(e.target) && container.has(e.target).length === 0) && (!containerButton.is(e.target) && containerButton.has(e.target).length === 0)) {
        container.slideUp();
    }

    //Page Numbers
    container = $("#pageNumberFilter");
    containerButton = $("#pageNumberFilterButton")
    if ((!container.is(e.target) && container.has(e.target).length === 0) && (!containerButton.is(e.target) && containerButton.has(e.target).length === 0)) {
        container.slideUp();
    }

    //Categories
    container = $("#categoryFilter");
    containerButton = $("#categoryFilterButton")
    if ((!container.is(e.target) && container.has(e.target).length === 0) && (!containerButton.is(e.target) && containerButton.has(e.target).length === 0)) {
        container.slideUp();
    }

    //Brands
    container = $("#brandFilter");
    containerButton = $("#brandFilterButton")
    if ((!container.is(e.target) && container.has(e.target).length === 0) && (!containerButton.is(e.target) && containerButton.has(e.target).length === 0)) {
        container.slideUp();
    }

});


//ACCORDION MENU CONTROL

$(document).on('click', '.hello', function () {
    NestedAccordianMenu(this);
});

function NestedAccordianMenu(element) {

    var elementPressed = $(element);

    //If it's parent has class means that it is embeded and we need to do extra logic. (is level 2 and below)
    if ((elementPressed.parents().hasClass("voices"))) {

        //Now find any of the next UL children which has the class, remove it and hide it.
        elementPressed.next().children("li").find(".voices").hide().removeClass("voices");

        //find and remove the new class for the toggle button on anything next
        elementPressed.next().children("li").find(".is_it_me_youre_looking_for").removeClass("is_it_me_youre_looking_for");

        //Now toggle the class for the image on the elementPressed
        elementPressed.toggleClass("is_it_me_youre_looking_for");

        //Slide out the LI and add class
        elementPressed.next().slideToggle(350).toggleClass("voices");
    } else if (($(".voices").first()[0] == elementPressed.next()[0])) {
        //Default (is closing level 1)
        //We pressing same element which is open so remove the open stuff

        elementPressed.parent().find("ul").hide().removeClass("voices");
        elementPressed.parent().find("span").removeClass("is_it_me_youre_looking_for");
        elementPressed.removeClass("is_it_me_youre_looking_for");
    } else {
        if ($(".voices").first()[0] != undefined) {
            var liOpen = $(".voices").first()[0];
            $(liOpen).parent().find("ul").hide().removeClass("voices");
            $(liOpen).parent().find("span").removeClass("is_it_me_youre_looking_for");
            $(liOpen).removeClass("is_it_me_youre_looking_for");

            elementPressed.next().slideToggle(350).toggleClass("voices");
            elementPressed.toggleClass("is_it_me_youre_looking_for");
        } else {
            //Default (is opening level 1)

            //Now toggle the next UL and add the toggle image.
            elementPressed.next().slideToggle(350).toggleClass("voices");
            elementPressed.toggleClass("is_it_me_youre_looking_for");
        }
    }

}


var originalPosition = 0;

// Cache selectors outside callback for performance. 

//$(window).scroll(function () {
//    if (originalPosition == 0) {
//        originalPosition = $('#stickyBar').offset().top;
//    }

//    var stickyEl = $('#stickyBar');
//    var elTop = stickyEl.offset().top - $(window).scrollTop();

//    if ($(window).scrollTop() > elTop) {
//        if (!stickyEl.hasClass("sticky")) {
//            stickyEl.addClass("sticky");
//        }
//    }
//    else if ($(window).scrollTop() < originalPosition) {
//        if (stickyEl.hasClass("sticky")) {
//            stickyEl.removeClass("sticky");
//        }
//    }

//});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 600) {
        $("#stickyHide").addClass("transparent");
        $("#stickyBar").addClass("sticky");
    }
    else {
        $("#stickyHide").removeClass("transparent");
        $("#stickyBar").removeClass("sticky");
    }
});

// Product page filter - mobile toggle filter button 
function FilterToggle() {
    $('.filter__toggle').off('click').on('click', function (event) {
        event.preventDefault();
        $(".filter__container").toggleClass('filter_min');
    });
}

//FilterToggle();



// Prompt to download the APP
let deferredPrompt;

window.addEventListener('beforeinstallprompt', function(e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Update UI notify the user they can add to home screen
    //btnAdd.style.display = 'block';
});

//btnAdd.addEventListener('click', (e) => {
//    // hide our user interface that shows our A2HS button
//    btnAdd.style.display = 'none';
//    // Show the prompt
//    deferredPrompt.prompt();
//    // Wait for the user to respond to the prompt
//    deferredPrompt.userChoice
//      .then((choiceResult) => {
//          if (choiceResult.outcome === 'accepted') {
//              console.log('User accepted the A2HS prompt');
//          } else {
//              console.log('User dismissed the A2HS prompt');
//          }
//          deferredPrompt = null;
//      });
//});