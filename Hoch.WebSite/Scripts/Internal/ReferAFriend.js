﻿

app.controller('RAFWelcomeViewCtrl', ['$scope', '$http', '$rootScope', 'CommonCode', function ($scope, $http, $rootScope, CommonCode) {
    $rootScope.GetMetadata(false, false, false, 'Refer a Friend - Welcome Landing Page', 0);

    $scope.voucherCode = "";
    $scope.ReferName = "";
    $scope.discountVal = 0;

    //CommonCode.getBaseCookieSettings();
    

    $scope.init = function (pVoucherCode) {
        if (pVoucherCode != "") {
            $scope.voucherCode = pVoucherCode;
            var rafInfo = { pVCode: pVoucherCode };
            $http({
                method: 'POST',
                url: baseUrl + "ReferAFriend/Welcome",
                contentType: 'application/json',
                params: rafInfo
            }).then(function (result) {
                if (result.data !== undefined && result.data !== null) {
                   
                    $scope.ReferName = result.data.firstname;
                    $scope.discountVal = result.data.discountvalue;
                    if (result.data.fixedorPercent == 'fixed') {
                        $scope.fixedorPercent=CommonCode.CurrencySymbol;
                    }
                    if (result.data.fixedorPercent == 'percent') {
                        $scope.fixedorPercent = '%';
                    }
                    //$scope.fixedorPercent = result.data.fixedorPercent;

                }
            });
        }
    };
}]);



app.controller('RAFDetailViewCtrl', ['$scope', '$http', function ($scope, $http) {


    $scope.CreditUpdateOn = "";
    $scope.CreditOffValue = "";
    $scope.CreditCurrencyID = "";
    $scope.CreditStatus = "";
    $scope.VochureCode = "";
    $scope.YouReceiveOffer = "";
    $scope.YouReceivePercentOrAmount = "";
    $scope.TheyReceiveOffer = "";
    $scope.TheyReceivePercentOrAmount = "";
    $scope.ReferreName = "";
    $scope.CreditCurrency = "";
    $scope.NoOfPendingReference = "";
    $scope.NoOfSuccessfulReference = "";
    $scope.NoOfTotalReferrals = "";
    $scope.ActiveCredit = "";

    $scope.init = function (pCustId) {
        if (pCustId != "") {
            var rafInfo = { pCustId: pCustId };
            $http({
                method: 'POST',
                url: baseUrl + "ReferAFriend/RAFDetails",
                contentType: 'application/json',
                params: rafInfo
            }).then(function (result) {
                if (result.data !== undefined) {
                    $scope.CreditUpdateOn = result.data.CreditUpdateOn;
                    $scope.CreditOffValue = result.data.CreditOffValue;
                    $scope.CreditCurrencyID =result.data.CreditCurrencyID;
                    $scope.CreditStatus = result.data.CreditStatus;
                    $scope.VochureCode = result.data.VochureCode;
                    $scope.YouReceiveOffer = result.data.YouReceiveOffer;
                    $scope.YouReceivePercentOrAmount = result.data.YouReceivePercentOrAmount;
                    $scope.TheyReceiveOffer = result.data.TheyReceiveOffer;
                    $scope.TheyReceivePercentOrAmount = result.data.TheyReceivePercentOrAmount;
                    $scope.ReferreName = result.data.ReferreName;
                    $scope.CreditCurrency = result.data.CreditCurrency;
                    $scope.NoOfPendingReference = result.data.NoOfPendingReference;
                    $scope.NoOfSuccessfulReference = result.data.NoOfSuccessfulReference;
                    $scope.NoOfTotalReferrals = result.data.NoOfTotalReferrals;
                    $scope.ActiveCredit = result.data.ActiveCredit;

                }
            });
        }
    };
}]);


app.controller('RAFController', ['$scope', '$cookies', '$http', '$rootScope', 'CommonCode', function ($scope, $cookies, $http, $rootScope,CommonCode) {
    $rootScope.GetMetadata(false, false, false, 'Refer a Friend - Share Page', 0);

    $scope.setPartialName = function (name, partialCheck, id, ifFavLogin) {
        $scope.Validation = "";
        $scope.rafEmailAddress = "";
        $rootScope.partialName = name;
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }
    $scope.Validation = "";
    $scope.baseCurrency = CommonCode.CurrencySymbol;
    $scope.IsLoggedIn = 0;

    var signinData = $cookies.get("keepMeSignedIn");
    if (signinData != null) {
        $scope.IsLoggedIn = 1;
        signinData = JSON.parse(decodeURIComponent(signinData));
        if (signinData.Email != "" && signinData.CustomerLookupId != "") {
            $scope.signinData = signinData;

            $http({
                method: 'POST',
                url: baseUrl + "ReferAFriend/RAFGetDetailsForCustomer",
                contentType: 'application/json',
                data: JSON.stringify({ CustomerlookupId: $scope.signinData.CustomerLookupId, Email: $scope.signinData.Email })
            }).then(function (result) {
                if (result.data !== undefined && result.data != null) {
                    $scope.youReceiveOffer = result.data.youReceiveOfferWithCurrency;
                    $scope.theyReceiveOffer = result.data.theyReceiveOffer;
                    $scope.theyReceivePercentOrAmount=result.data.theyReceivePercentOrAmount;
                    $scope.RAFGetDetails = result.data;
                }
            });
        }
    }
    else {
        if (CommonCode.getBaseCookieSettings != null && CommonCode.getBaseCookieSettings != undefined)
        $http({
            method: 'POST',
            url: baseUrl + "ReferAFriend/RAFGetConfigDetails",
            contentType: 'application/json',
            data: JSON.stringify({ CurrencyID: CommonCode.CurrencyId })
        }).then(function (result) {
            if (result.data !== undefined && result.data != null) {
                $scope.youReceiveOffer = result.data.CreditOffValueWithCurrency;
            }
        });
    }
    

   

    $scope.SendEmail = function () {
        if ($scope.signinData != undefined && $scope.signinData != null) {
            //$scope.signinData = JSON.parse(decodeURIComponent($scope.signinData));
            $scope.Validation = "";

           
            if ($scope.RAFGetDetails !== undefined && $scope.RAFGetDetails != null) {
                    
                    var email = $scope.rafEmailAddress;
                    if (email != null && email != undefined && email != "undefined") {
                        if (email != "" && email != undefined && email != "undefined" && email != null) {
                            var re = /(\w+)\@(\w+)\.[a-zA-Z]/g;
                            var validEmail = re.test(email);
                            if (!validEmail) {
                                $scope.Validation = "Please enter a valid email address";
                            }
                            else {
                                $http({
                                    method: 'POST',
                                    url: "/ReferAFriend/SendShareFriendEmail",
                                    contentType: 'application/json',
                                    data: JSON.stringify(
                                        {
                                            vouchercode: $scope.RAFGetDetails.voucherCode,
                                            emailAddress: email,
                                            firstname: $scope.signinData.Firstname,
                                            lastname: $scope.signinData.Lastname,
                                            offer: $scope.RAFGetDetails.theirOffer,
                                            //url: 'w3.hochanda.com/refer-a-friend-welcome?voucher=' + $scope.RAFGetDetails.voucherCode //localhost
                                            url: 'www.hochanda.com/refer-a-friend-welcome?voucher=' + $scope.RAFGetDetails.voucherCode
                                        })
                                }).then(function (result) {
                                    if (result.data == true) {
                                        $scope.Validation = "Email has been sent";
                                    }
                                    else {
                                        $scope.Validation = "Email failed";
                                    }
                                });
                            }
                        }                        
                    }
                }
            //});            
        }
    }   
}
]);

