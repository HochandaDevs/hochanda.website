﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class JavascriptExtension
	{
		//TODO: Replace this class with this structure.
		//<script src = "~/Scripts/Internal/Account.js?v=@ConfigurationManager.AppSettings["VersionNo"]"></script>



        public static MvcHtmlString IncludeVersionedJs(this HtmlHelper helper, string filename)
        {
            string version = GetVersion(helper, filename);
            return MvcHtmlString.Create("<script type='text/javascript' src='" + filename + version + "'></script>");
        }

        public static MvcHtmlString IncludeVersionedCss(this HtmlHelper helper, string filename)
        {
            string version = GetVersion(helper, filename);
            return MvcHtmlString.Create("<link href='" + filename + version + "' rel='stylesheet' />");
        }

        private static string GetVersion(this HtmlHelper helper, string filename)
        {
            var version = "?v=" + ConfigurationManager.AppSettings.Get("VersionNo");
            return version;
            /*var context = helper.ViewContext.RequestContext.HttpContext;

            if (context.Cache[filename] == null)
            {
                var physicalPath = context.Server.MapPath(filename);
                //var version = $"?v={new System.IO.FileInfo(physicalPath).LastWriteTime.ToString("MMddHHmmss")}";
                var version = "?v=" + ConfigurationManager.AppSettings.Get("VersionNo");
                context.Cache.Add(filename, version, null,
                  DateTime.Now.AddMinutes(5), TimeSpan.Zero,
                  CacheItemPriority.Normal, null);
                return version;
            }
            else
            {
                return context.Cache[filename] as string;
            }*/
        }
    }
}