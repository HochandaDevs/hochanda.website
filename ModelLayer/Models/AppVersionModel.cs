﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
	public class AppVersionModel
	{
		public string CurrentAppVersion { get; set; }
		public string MinimumSupportedAppVersion { get; set; }
	}
}
