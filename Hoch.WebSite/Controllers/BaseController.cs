﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Hoch.Website.Models;
using ModelLayer.Models.Settings;
using System.Text;
using ModelLayer.Helpers;
using MaxMind.GeoIP2;

namespace Hoch.Website.Controllers
{
    public class BaseController : Controller
    {
        public int CustomerIPCountry { get; private set; }
        public int BaseCurrencyID { get; private set; }
        public int BaseLanguageID { get; private set; }
		public int BaseChannelId { get; private set; }
		public int BaseCompanyId { get; private set; }
		public string CustomerCountryCode { get; private set; }
        public string CurrencySymbol { get; private set; }
        public string DefaultStreamId { get; private set; }
        public string SimpleStreamRewindDataStream { get; private set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            GetBaseSettings(requestContext);
		}

        protected void GetBaseSettings(RequestContext requestContext)
        {
            //if (Request.Cookies["CountryIDCookie"] == null || Request.Cookies["CurrencyIDCookie"] == null || Request.Cookies["LanguageIDCookie"] == null)
            if (Request.Cookies["BaseCookieSettings"] == null)
            {
                SetBaseCookie(requestContext);
            }
            else
            {
                GetBaseCookie();

				//To fix a bug with America, where languageId was in place of the company id, we need to remove the old cookie and replace it with the new one. This will only affect Germany, Netherlands and US.
				if (BaseChannelId != 1 && BaseChannelId != 9)
				{
					SetBaseCookie(requestContext);
				}
            }
        }

        private void GetBaseCookie()
        {
            string basecookie = Request.Cookies["BaseCookieSettings"].Value;
            List<string> cookieValues = basecookie.Split('&').ToList();
            if (cookieValues != null)
            {
                for (int i = 0; i < cookieValues.Count; i++)
                {
                    string cookieValue = cookieValues[i];
                    string preferenceCookie = cookieValue.Split('=').First();

					//Changed all the ifs to elseif for performance. (Wont be much but good practice) - Sam

                    if (preferenceCookie.Contains("CountryIDCookie"))
                    {
                        CustomerIPCountry = Convert.ToInt32(cookieValue.Split('=').Last());
                    }
                    else if (preferenceCookie.Contains("CurrencyIDCookie"))
                    {
                        BaseCurrencyID = Convert.ToInt32(cookieValue.Split('=').Last());
                    }
					else if (preferenceCookie.Contains("LanguageIDCookie"))
                    {
                        BaseLanguageID = Convert.ToInt32(cookieValue.Split('=').Last());
                    }
					else if (preferenceCookie.Contains("ChannelIDCookie"))
					{
						BaseChannelId = Convert.ToInt32(cookieValue.Split('=').Last());
					}
					else if (preferenceCookie.Contains("CustomerCountryCodeCookie"))
                    {
                        CustomerCountryCode = cookieValue.Split('=').Last();
                    }
					else if (preferenceCookie.Contains("CurrencySymbolCookie"))
                    {
                        CurrencySymbol = GenericHelper.Instance.ConvertToUFT8(cookieValue.Split('=').Last());
                    }
					else if (preferenceCookie.Contains("DefaultStreamIdCookie"))
                    {
                        DefaultStreamId = cookieValue.Split('=').Last();
                    }
					else if (preferenceCookie.Contains("SimpleStreamRewindDataStreamCookie"))
                    {
                        SimpleStreamRewindDataStream = cookieValue.Split('=').Last();
                    }
					else if (preferenceCookie.Contains("BaseCompanyIdCookie"))
                    {
                        BaseCompanyId = Convert.ToInt32(cookieValue.Split('=').Last());
                    }
                }
            }
            else
            {
				BaseChannelId = 1;
                CustomerIPCountry = 1;
                BaseCurrencyID = 1;
                BaseLanguageID = 1;
                CustomerCountryCode = String.Empty;
                CurrencySymbol = String.Empty;
                DefaultStreamId = String.Empty;
                SimpleStreamRewindDataStream = String.Empty;
                BaseCompanyId = 1;
            }
        }

		protected void SetBaseCookieCallCentreCookie(int countryId, int languageId, int currencyId)
		{
			HttpCookie cookie = Request.Cookies["BaseCookieSettings"];

			if (cookie != null)
			{
				cookie = new HttpCookie("BaseCookieSettings");
			}

			cookie.Values.Add("CountryIDCookie", countryId.ToString());
			cookie.Values.Add("CurrencyIDCookie", currencyId.ToString());
			cookie.Values.Add("LanguageIDCookie", languageId.ToString());
			cookie.Values.Add("ChannelIDCookie", "1");
			cookie.Values.Add("CustomerCountryCodeCookie", "GB");
			cookie.Values.Add("CurrencySymbolCookie", "£");
			cookie.Values.Add("DefaultStreamIdCookie", "hochandauk");
			cookie.Values.Add("SimpleStreamRewindDataStreamCookie", "578");
			cookie.Values.Add("BaseCompanyIdCookie", "1");

			cookie.Expires = DateTime.Now.AddYears(5);
			Response.Cookies.Add(cookie);
		}

        private void SetBaseCookie(RequestContext requestContext)
        {
            GetClientLocation(requestContext);
            Selectors sel = new Models.Selectors();
            CurrencyCountryLanguageSelectorModel cclResultset = sel.PopulateMenuData();
            if (cclResultset != null)
            {
                //Get data for country
                try
                {
                    var countriesResult = cclResultset.Countries.FirstOrDefault(i => i.Country2DigitISOCode == CustomerCountryCode);

                    //If we dont have that country default to UK
                    if (countriesResult == null)
                    {
                        countriesResult = cclResultset.Countries.FirstOrDefault(i => i.CountryId == 1);
                    }

					var currency = cclResultset.Currencies.FirstOrDefault(i => i.CurrencyId == countriesResult.DefaultCurrencyId);

					if (currency == null)
					{
						currency = cclResultset.Currencies.FirstOrDefault(i => i.CurrencyId == 1);
					}

                    CustomerIPCountry = countriesResult.CountryId;
                    BaseCurrencyID = countriesResult.DefaultCurrencyId;
                    BaseLanguageID = countriesResult.DefaultLanguageId;
                    BaseChannelId = currency.DefaultChannelId;
                    CurrencySymbol = currency.CurrencySymbol;
                    DefaultStreamId = countriesResult.DefaultStreamId;
                    SimpleStreamRewindDataStream = currency.SimpleStreamRewindDataStream;
                    BaseCompanyId = countriesResult.CompanyId;

                    HttpCookie myCookie = new HttpCookie("BaseCookieSettings");
                    myCookie.Values.Add("CountryIDCookie", CustomerIPCountry.ToString());
                    myCookie.Values.Add("CurrencyIDCookie", BaseCurrencyID.ToString());
                    myCookie.Values.Add("LanguageIDCookie", BaseLanguageID.ToString());
                    myCookie.Values.Add("ChannelIDCookie", BaseChannelId.ToString());
					myCookie.Values.Add("CustomerCountryCodeCookie", CustomerCountryCode);
                    myCookie.Values.Add("CurrencySymbolCookie", CurrencySymbol);
                    myCookie.Values.Add("DefaultStreamIdCookie", DefaultStreamId);
                    myCookie.Values.Add("SimpleStreamRewindDataStreamCookie", SimpleStreamRewindDataStream);
                    myCookie.Values.Add("BaseCompanyIdCookie", BaseCompanyId.ToString());

                    myCookie.Expires = DateTime.Now.AddYears(5);
                    Response.Cookies.Add(myCookie);
                }
                catch (Exception ex)
                {
                    Response.Write("Exception on Base page" + ex.InnerException);
                    throw;
                }
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("BaseCookieSettings");
                myCookie.Values.Add("CountryIDCookie", "1");
                myCookie.Values.Add("CurrencyIDCookie", "1");
                myCookie.Values.Add("LanguageIDCookie", "1");
                myCookie.Values.Add("ChannelIDCookie", "1");
                myCookie.Values.Add("CustomerCountryCodeCookie", "GB");
                myCookie.Values.Add("CurrencySymbolCookie", "£");
                myCookie.Values.Add("DefaultStreamIdCookie", "hochandauk");
                myCookie.Values.Add("SimpleStreamRewindDataStreamCookie", "578");
                myCookie.Values.Add("BaseCompanyIdCookie", "1");
                myCookie.Expires = DateTime.Now.AddYears(5);
                Response.Cookies.Add(myCookie);
            }
        }

        public string GetClientLocation(RequestContext requestContext)
        {
            // string countryCode = "GB";
            string password = ConfigurationManager.AppSettings["GeoIPPassword"].ToString();
            int username = Convert.ToInt32(ConfigurationManager.AppSettings["GeoIPUsername"]);

            try
            {
                using (var client = new WebServiceClient(username, password))
                {
                    string userIpAddress = requestContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    if (String.IsNullOrEmpty(userIpAddress))
                    {
                        userIpAddress = requestContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
                    }

                    if (userIpAddress.Trim() == "::1" || userIpAddress.Trim() == "127.0.0.1")
                    {
                        //Little cheat for localhost
                        userIpAddress = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                        userIpAddress = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                                     .Matches(userIpAddress)[0].ToString();
                    }

                    if (userIpAddress.Trim() == "::1" || userIpAddress.Trim() == "127.0.0.1") //when run locally
                    {
                        userIpAddress = "104.20.48.249"; //get the clouflare IP
                    }
                    if (userIpAddress.Contains(",")) //if more than one IP
                    {
                        userIpAddress = userIpAddress.Split(',').First().Trim();
                    }

                    var response = client.Country(userIpAddress);
                    CustomerCountryCode = response.Country.IsoCode;
                }
            }
            catch (Exception ex)
            {
            }

            return CustomerCountryCode;
        }
    }
}