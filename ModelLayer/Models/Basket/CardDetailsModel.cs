﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
    public class CardDetailsModel
    {
        public int iBLookUP { get; set; }
        public int ChaseCustomerReferenceNumber { get; set; }
        public string Last4Digits { get; set; }
        public string Last4DigitsToDisplay { get; set; }
		public int ChaseMerchantId { get; set; }
        public int AddressId { get; set; }
        public bool SelectedCard { get; set; }
        public bool CustomersDefaultCard { get; set; }
    }
}
