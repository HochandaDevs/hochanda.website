﻿/// <reference path="../angular.min.js" />

app.controller('ProductController', ['$scope', '$http', 'CommonCode', "$window", "$sanitize", "$compile", "$timeout", "$rootScope", '$sce', "$filter", function ($scope, $http, CommonCode, $window, $sanitize, $compile, $timeout, $rootScope, $sce, $filter) {


    //Keep it null for now and change when it needs to.
    $scope.ShowPickAndMix = false;
    $scope.ShowProduct = false;
    $scope.ShowProductInformation = false;

    $scope.SelectedPickAndMixValue = 0;

    $scope.TimeLeftInReservationMinutes = 0;
    $scope.TimeLeftInReservationSeconds = 0;

    $scope.ProductImages = [];
    $scope.IsLoggedIn = 0;
    var keepMeSignedIn = getCookie("keepMeSignedIn");
    if (keepMeSignedIn != null) {
        $scope.IsLoggedIn = 1;
    }

    var canClick = true;

    $scope.IsAdding = false;
    $scope.IsAdded = false;
    $scope.IsFailed = false;

    $scope.AddPickAndMixToBasket = function () {

        if (canClick) {
            canClick = false;
            var basketExpiredDate = new Date(localStorage.getItem("basketExpiry"));

            if (basketExpiredDate == null || basketExpiredDate < new Date()) {
                ClearBasket();

                $scope.SelectedPickAndMixValue = 0;
                angular.forEach($scope.pickAndMixVariations, function (value, index) {
                    value.SelectedCount = 0;
                });
            }

            //Only add if we have the exact amount.
            if ($scope.SelectedPickAndMix() != undefined && $scope.SelectedPickAndMix() == $scope.productInformation.PickAndMixQuantity) {
                $scope.IsAdding = true;

                var basketId = localStorage.getItem("basketId") == undefined ? null : localStorage.getItem("basketId");

                var dataToSend = {
                    ParentProductId: $scope.productInformation.ParentProductId,
                    VariationId: $scope.productInformation.DefaultVariationId,
                    Quantity: 1,
                    LanguageId: CommonCode.LanguageId,
                    CurrencyId: CommonCode.CurrencyId,
                    CountryId: CommonCode.CountryId,
                    BasketId: localStorage.getItem("basketId") == undefined ? null : localStorage.getItem("basketId"),
                    PickAndMixIdentifier: $scope.PickAndMixIdentifier
                }



                $http({
                    method: 'POST',
                    url: baseUrl + 'reservations/UpdateItemOnReservation',
                    data: dataToSend,
                    contentType: 'application/json',
                }).then(function (result) {

                    if (result.data != null) {

                        if (result.data.HasReservationExpired) {
                            ClearBasket();

                            $scope.SelectedPickAndMixValue = 0;
                            $scope.PickAndMixIdentifier = 0;
                            angular.forEach($scope.pickAndMixVariations, function (value, index) {
                                value.SelectedCount = 0;
                            });

                            return;
                        }

                        $scope.TimeLeftInReservationMinutes = parseInt(result.data.TimeLeftInReservation.split(":")[1]);
                        $scope.TimeLeftInReservationSeconds = parseInt(result.data.TimeLeftInReservation.split(":")[2]);

                        //Get datetime of expiry based on minutes left in reservation. ExpiryDate does not work ot use as it will be GMT time and other timezones can mess that up.
                        var reservationExpiry = new Date();
                        reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(result.data.TimeLeftInReservation.split(":")[0]));
                        reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(result.data.TimeLeftInReservation.split(":")[1]));
                        reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(result.data.TimeLeftInReservation.split(":")[2]));
                        localStorage.setItem("basketExpiry", reservationExpiry);
                        localStorage.setItem("basketCount", result.data.BasketCount);

                        if (result.data.HasProductBeenAdded) {
                            setTimeout(function () {
                                $scope.IsAdding = false;
                                $scope.IsAdded = true;

                                //Reset everything back
                                $scope.SelectedPickAndMixValue = 0;
                                $scope.PickAndMixIdentifier = 0;
                                angular.forEach($scope.pickAndMixVariations, function (value, index) {
                                    value.SelectedCount = 0;
                                });

                                HidePop();

                                //Gets the left and top offset ($.offset == position on page; $.position == relative position) 
                                var leftPosition = $(".addToBasketPickAndMixButton").offset().left;

                                var topPosition = $(".addToBasketPickAndMixButton").offset().top;

                                //Width of pop under.
                                var buttonWidth = $(".addToBasketPickAndMixButton").outerWidth();
                                $(".pop").width(buttonWidth);

                                $(".pop").css("left", leftPosition);
                                $(".pop").css("top", topPosition + 50);

                                ShowPop();

                                setTimeout(function () {
                                    //fadeout after 5 seconds to look awesome...
                                    $('.pop').fadeOut(500);
                                }, 5000);

                                $scope.$applyAsync();
                            }, 500);


                        }
                        else {
                            $scope.IsAdding = false;
                            $scope.IsAdded = false;
                            $scope.IsFailed = true;
                        }
                    }
                    else {
                        $scope.IsAdding = false;
                        $scope.IsAdded = false;
                        $scope.IsFailed = true;
                    }

                    setTimeout(function () {
                        $scope.IsAdding = false;
                        $scope.IsAdded = false;
                        $scope.IsFailed = false;
                    }, 500);

                    canClick = true;

                }, function (response) {
                    //errored but can try again
                    canClick = true;
                    $scope.IsFailed = true;

                    setTimeout(function () {
                        $scope.IsAdding = false;
                        $scope.IsAdded = false;
                        $scope.IsFailed = false;
                    });
                });
            } else {
                canClick = true;
            }
        }
    }

    $scope.VariationSelectorImages = function () {
        $scope.ChangeImages($scope.ProductImageVariationId);
    }

    $scope.ChangeImages = function (variationId) {
        $http({
            method: 'GET',
            url: '/product/GetImageCarousel',
            contentType: 'application/json',
        }).then(function (result) {
            if (result.data != undefined) {
                var imagesToUse = $scope.productInformation.ProductImages.filter(function (image) {
                    return image.VariationId == variationId
                });

                if (imagesToUse.length == 0) {
                    imagesToUse = $scope.productInformation.ProductImages;
                }

                //Reset the array.
                $scope.ProductImages = [];
                if (imagesToUse == null || imagesToUse.length == 0) {
                    imagesToUse.push({
                        "Filename": $scope.productInformation.DefaultThumbnailFilename,
                        "ImageAltText": $scope.productInformation.DefaultImageAltText,
                        "VariationId": null,
                        "DisplayOrder": 1
                    });
                }

                $scope.ProductImages = imagesToUse;

                var compiledHtml = $compile(result.data);
                var element = compiledHtml($scope);

                //Clear it.
                document.getElementById("productImages").innerHTML = "";

                //Append It.
                $("#productImages").append(element[0]);
            }
        });
    }


    $scope.SelectedPickAndMix = function () {
        if ($scope.productInformation != null && $scope.productInformation.PickAndMixQuantity != null) {

            if ($scope.SelectedPickAndMixValue > $scope.productInformation.PickAndMixQuantity) {
                $scope.SelectedPickAndMixValue = $scope.productInformation.PickAndMixQuantity;
            }
            else if ($scope.SelectedPickAndMixValue < 0) {
                $scope.SelectedPickAndMixValue = 0;
                $scope.PickAndMixIdentifier = 0;

            }
        }
        return $scope.SelectedPickAndMixValue;
    }

    $scope.SelectedPickAndMixPercent = function () {
        if ($scope.productInformation == null || $scope.productInformation.PickAndMixQuantity == null) {
            return 0;
        }

        var percentValue = ($scope.SelectedPickAndMixValue / $scope.productInformation.PickAndMixQuantity) * 100;
        return $scope.SelectedPickAndMixValue == 0 ? 0 : percentValue > 100 ? 100 : percentValue;
    }

    $scope.ShowPreviousPrice = function () {
        if ($scope.productInformation != null) {
            if ($scope.productInformation.UsualPrice > $scope.productInformation.Price) {
                return true;
            }
        }

        return false;
    }

    $scope.PickAndMixIdentifier = 0;

    $scope.AddItemToPickAndMix = function (parentProductId, variationId) {

        if (canClick) {
            canClick = false;
            //Only try and add if we are not complete
            if ($scope.SelectedPickAndMix() != undefined && $scope.SelectedPickAndMix() < $scope.productInformation.PickAndMixQuantity) {

                var basketExpiredDate = new Date(localStorage.getItem("basketExpiry"));

                if (basketExpiredDate == null || basketExpiredDate < new Date()) {
                    ClearBasket();

                    $scope.SelectedPickAndMixValue = 0;
                    $scope.PickAndMixIdentifier = 0;

                    angular.forEach($scope.pickAndMixVariations, function (value, index) {
                        value.SelectedCount = 0;
                    });
                }

                var selectedPickAndMix = $scope.pickAndMixVariations.find(function (i) {
                    return i.VariationId == variationId;
                });

                if (selectedPickAndMix != null) {
                    var dataToSend = {
                        ParentProductId: selectedPickAndMix.ParentProductId,
                        VariationId: selectedPickAndMix.VariationId,
                        Quantity: 1,
                        LanguageId: CommonCode.LanguageId,
                        CurrencyId: CommonCode.CurrencyId,
                        CountryId: CommonCode.CountryId,
                        BasketId: localStorage.getItem("basketId") == undefined ? null : localStorage.getItem("basketId"),
                        PickAndMixIdentifier: $scope.PickAndMixIdentifier
                    }

                    $http({
                        method: 'POST',
                        url: baseUrl + 'reservations/UpdatePickAndMixSelectionItems',
                        data: dataToSend,
                        contentType: 'application/json',
                    }).then(function (result) {
                        if (result != null) {

                            if (!result.data.ReservationValid) {
                                ClearBasket();

                                $scope.SelectedPickAndMixValue = 0;
                                $scope.PickAndMixIdentifier = 0;

                                angular.forEach($scope.pickAndMixVariations, function (value, index) {
                                    value.SelectedCount = 0;
                                });

                                return;
                            }

                            var selectedPickAndMix = $scope.pickAndMixVariations.find(function (i) {
                                return i.VariationId == variationId;
                            });

                            if (result.data.ItemAdded) {
                                $scope.PickAndMixIdentifier = result.data.PickAndMixIdentifier;
                                localStorage.setItem("basketId", result.data.BasketId);

                                //Get datetime of expiry based on minutes left in reservation. ExpiryDate does not work ot use as it will be GMT time and other timezones can mess that up.
                                var reservationExpiry = new Date();
                                reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(result.data.TimeLeftInReservation.split(":")[0]));
                                reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(result.data.TimeLeftInReservation.split(":")[1]));
                                reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(result.data.TimeLeftInReservation.split(":")[2]));
                                localStorage.setItem("basketExpiry", reservationExpiry);


                                if (selectedPickAndMix.SelectedCount < $scope.productInformation.PickAndMixQuantity) {
                                    $scope.SelectedPickAndMixValue++;
                                    selectedPickAndMix.SelectedCount++;
                                }
                            }

                            if (result.data.IsOutOfStock) {
                                selectedPickAndMix.OutOfStock = true;
                            }
                        }

                        canClick = true;

                    }, function (response) {
                        //errored but can try again
                        canClick = true;
                    });
                } else {
                    canClick = true;
                }
            } else {
                canClick = true;
            }
        }
    }

    $scope.FeefoPageNumber = 0;

    $scope.PaginationNumbers = [1, 2, 3, 4, 5]

    $scope.PaginationShow = function (numberToShow) {

        var canShow = $scope.PaginationNumbers.find(function (element) {
            return element == numberToShow;
        });

        if (canShow != null) {
            return true;
        }
        else {
            return false;
        }
    }

    $scope.GetFeefoPageNumber = function (forward, numberToGoTo) {
        var originalNumber = $scope.FeefoPageNumber;

        if (numberToGoTo != null) {
            $scope.FeefoPageNumber = numberToGoTo;
        }
        else {
            if (forward) {
                $scope.FeefoPageNumber++;
            }
            else {
                $scope.FeefoPageNumber--;
            }
        }
        if ($scope.FeefoProductResultData != null && $scope.FeefoPageNumber > $scope.FeefoProductResultData.summary.meta.pages) {
            $scope.FeefoPageNumber = $scope.FeefoProductResultData.summary.meta.pages;
        }

        if ($scope.FeefoPageNumber <= 0) {
            $scope.FeefoPageNumber = 1;
        }

        if (originalNumber != $scope.FeefoPageNumber) {

            $http({
                method: 'GET',
                url: "https://api.feefo.com/api/10/reviews/product?merchant_identifier=hochanda&product_sku=*" + $scope.productInformation.ParentProductSKU + "*&rating=4,5&page=" + $scope.FeefoPageNumber + "&since_period=all",
                contentType: 'application/json',
            }).then(function (feefoResult) {
                //if (feefoResult.data != null && feefoResult.data.summary.meta.count > 0) {
                if (feefoResult.data != null && feefoResult.data.reviews.length > 0) {
                    $scope.FeefoProductResultData = feefoResult.data;

                    $scope.PaginationNumbers = [];
                    var startingNumber = $scope.GetStartingNumber($scope.FeefoPageNumber, $scope.FeefoProductResultData.summary.meta.pages);
                    var endNumber = $scope.GetEndNumber(startingNumber, $scope.FeefoProductResultData.summary.meta.pages);

                    for (var i = startingNumber; i <= endNumber; i++) {
                        $scope.PaginationNumbers.push(i);
                    }
                }
            });
        }
    }

    $scope.GetStartingNumber = function (startingNumber, maxNumber) {
        startingNumber = startingNumber - 2;
        if (startingNumber <= 0) {
            return 1;
        }
        else if (maxNumber - startingNumber < 4) {
            return maxNumber - 4;
        }
        else {
            return startingNumber;
        }
    }

    $scope.GetEndNumber = function (startingNumber, maxNumber) {

        var endNumber = startingNumber + 4;

        if (endNumber > maxNumber) {
            endNumber = maxNumber;
        }


        return endNumber;
    }

    $scope.GetBreadcrumbURL = function (prefix, id, name) {
        if ($scope.productInformation != null) {
            if (prefix != null && id != null && name != null) {
                return "/" + prefix + "/" + id + "/" + GetWebSafeURL(name);
            }
        }
    }

    $scope.ShowReviewTab = function () {
        return $scope.FeefoProductResultData != null
    }
    $scope.setReviewTab = function () {
        var ReviewsTab = angular.element(document.querySelector('#ReviewsTab'));
        var ReviewsTitle = angular.element(document.querySelector('#ReviewsTitle'));
        var DescTab = angular.element(document.querySelector('#DescriptionTab'));
        var DescTitle = angular.element(document.querySelector('#DescTitle'));
        var DeliveryTab = angular.element(document.querySelector('#DeliveryTab'));
        var DeliveryTitle = angular.element(document.querySelector('#DeliveryTitle'));
        DescTab.removeClass("active");
        DescTab.removeClass("show");
        DescTitle.removeClass("active");
        DescTitle.removeClass("show");

        DeliveryTab.removeClass("active");
        DeliveryTab.removeClass("show");
        DeliveryTitle.removeClass("active");
        DeliveryTitle.removeClass("show");

        ReviewsTab.addClass("active");
        ReviewsTab.addClass("show");
        ReviewsTitle.addClass("active");
        ReviewsTitle.addClass("show");

    }
    $scope.GetFeefoData = function () {
        $http({
            method: 'GET',
            url: "https://api.feefo.com/api/10/reviews/summary/product?merchant_identifier=hochanda&product_sku=*" + $scope.productInformation.ParentProductSKU + "*&since_period=all",
            contentType: 'application/json',
        }).then(function (feefoResult) {
            if (feefoResult.data != null && feefoResult.data.rating.product.count > 0) {

                $scope.FeefoSummaryResultData = feefoResult.data;

                $scope.GetFeefoPageNumber(true);

            }
        });
    }


    $scope.RemoveItemFromPickAndMix = function (parentProductId, variationId) {
        if (canClick) {
            canClick = false;
            //Only try and add if we are not complete
            if ($scope.SelectedPickAndMix() != undefined && $scope.SelectedPickAndMix() > 0) {

                var basketExpiredDate = new Date(localStorage.getItem("basketExpiry"));

                if (basketExpiredDate == null || basketExpiredDate < new Date()) {
                    ClearBasket();

                    $scope.SelectedPickAndMixValue = 0;
                    $scope.PickAndMixIdentifier = 0;

                    angular.forEach($scope.pickAndMixVariations, function (value, index) {
                        value.SelectedCount = 0;
                    });
                }

                var selectedPickAndMix = $scope.pickAndMixVariations.find(function (i) {
                    return i.VariationId == variationId;
                });

                //We only want to try and remove if we are not at 0.
                if (selectedPickAndMix.SelectedCount > 0) {

                    //Call wiki
                    var dataToSend = {
                        ParentProductId: selectedPickAndMix.ParentProductId,
                        VariationId: selectedPickAndMix.VariationId,
                        Quantity: -1,
                        LanguageId: CommonCode.LanguageId,
                        CurrencyId: CommonCode.CurrencyId,
                        CountryId: CommonCode.CountryId,
                        BasketId: localStorage.getItem("basketId") == undefined ? null : localStorage.getItem("basketId"),
                        PickAndMixIdentifier: $scope.PickAndMixIdentifier
                    }

                    $http({
                        method: 'POST',
                        url: baseUrl + 'reservations/UpdatePickAndMixSelectionItems',
                        data: dataToSend,
                        contentType: 'application/json',
                    }).then(function (result) {

                        if (result.data != null) {
                            if (!result.data.ReservationValid) {
                                ClearBasket();

                                $scope.SelectedPickAndMixValue = 0;
                                $scope.PickAndMixIdentifier = 0;

                                angular.forEach($scope.pickAndMixVariations, function (value, index) {
                                    value.SelectedCount = 0;
                                });

                                return;
                            }
                        }

                        if (result.data != null && result.data.ItemRemoved) {

                            //Get datetime of expiry based on minutes left in reservation. ExpiryDate does not work ot use as it will be GMT time and other timezones can mess that up.
                            var reservationExpiry = new Date();
                            reservationExpiry.setHours(reservationExpiry.getHours() + parseInt(result.data.TimeLeftInReservation.split(":")[0]));
                            reservationExpiry.setMinutes(reservationExpiry.getMinutes() + parseInt(result.data.TimeLeftInReservation.split(":")[1]));
                            reservationExpiry.setSeconds(reservationExpiry.getSeconds() + parseInt(result.data.TimeLeftInReservation.split(":")[2]));
                            localStorage.setItem("basketExpiry", reservationExpiry);


                            result.data.PickAndMixIdentifier;

                            $scope.SelectedPickAndMixValue--;
                            selectedPickAndMix.SelectedCount--;
                            selectedPickAndMix.OutOfStock = false;
                        }
                        canClick = true;

                    }, function (response) {
                        //errored but can try again
                        canClick = true;
                    });
                }
                else {
                    canClick = true;
                }
            } else {
                canClick = true;
            }
        }
    }

    CommonCode.getBaseCookieSettings();

    $scope.setPartialName = function (name, partialCheck, ppid, ifFavLogin) {
        $rootScope.partialName = name;
        if (ifFavLogin == 1) {
            CommonCode.favBaseUrl = window.location.href;
        }
    }

    var parentProductId = 0;

    var path = $window.location.pathname.split('/');
    var parentProductIdInURL = path[2]; //example;

    try {
        parentProductId = parseInt(parentProductIdInURL);

    }
    catch (error) {
        //Do nothing it has failed
    }

    if (parentProductId != 0) {
        //product metadata
        $rootScope.GetMetadata(false, false, true, '', parentProductId);
        $scope.videos = [];
        $scope.RewindStream = null;
        $http({
            method: 'GET',
            url: baseUrl + 'products/GetProductInformation?parentProductId=' + parentProductId + '&languageId=' + CommonCode.LanguageId + '&currencyId='
                + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
            contentType: 'application/json',
        }).then(function (result) {
            if (result.data != null) {

                $rootScope.AddRecentlyViewed(result.data.ParentProductId);

                document.title = result.data.TVDescription;

                $scope.productInformation = result.data;

                //display rewind video on carousel_small
                $http({
                    method: 'GET',
                    url: baseUrl + 'products/GetProductVideos?parentProductId=' + $scope.productInformation.ParentProductId + '&channelId=' + CommonCode.ChannelId,
                    contentType: 'application/json'
                }).then(function (resultVideo) {
                    if (resultVideo.data != null) {
                        $scope.productVideos = resultVideo.data;

                        if ($scope.productVideos != null && $scope.productVideos.length > 0 && $scope.productVideos[0].ShowDateUTC != "") {

                            var dateToUse;

                            if (isBrowserSafari) {
                                dateToUse = new Date($scope.productVideos[0].ShowDateUTC.replace(new RegExp('-', 'g'), '/'));
                            }
                            else if (detectIE()) {
                                var newDateString = $scope.productVideos[0].ShowDateUTC.replace(new RegExp('-', 'g'), '/');
                                dateToUse = new Date(newDateString.substring(0, newDateString.length - 3));
                            }
                            else {
                                dateToUse = new Date($scope.productVideos[0].ShowDateUTC);
                            }

                            var dateFormatted = $filter('date')(dateToUse, "yyyy-MM-dd", "UTC"); //TvScheduleInfo.TVShowDateUKToUse | date: 'yyyy-MM-dd' : 'UTC'}}
                            var timeFormatted = $filter('date')(dateToUse, "HH:mm:ss", "UTC"); //{{TvScheduleInfo.TVShowDateUKToUse | date: 'HH:mm:ss' : 'UTC'}}


                            $scope.simpleStreamRewindDataStream = CommonCode.SimpleStreamRewindDataStream;

                            $scope.RewindStream = "<div id=\"WatchVideo\" data-client=\"hochanda\" data-type=\"vod\" data-stream=\"" + $scope.simpleStreamRewindDataStream + "\" data-date=\"" + dateFormatted + "\" data-start=\"" + timeFormatted + "\"> <script src=\"https://dbxm993i42r09.cloudfront.net/ss.js\"></script></div>";
                        }
                    }
                });

                $scope.productInformation.IsFav = 0;
                var checkFavTrue;
                var favourites = JSON.parse(localStorage.getItem("userFavourites"));
                if (favourites != null) {
                    $scope.favouritesProducts = favourites.products;
                    for (var i = 0; i < $scope.favouritesProducts.length; i++) {
                        if ($scope.favouritesProducts[i].ParentProductId == $scope.productInformation.ParentProductId) {
                            $scope.productInformation.IsFav = 1;
                            //Changed to break as return would end the function completely
                            break;
                        }
                    }
                }
                var images = [];

                $scope.GetFeefoData();

                angular.forEach($scope.productInformation.ProductImages, function (value, key) {
                    images.push(value.Filename);
                });

                var reviewCount = $scope.productInformation.FeefoTotalReviews != null ? $scope.productInformation.FeefoTotalReviews : 1;
                var ratingValue = $scope.productInformation.FeefoAverageRating != null ? $scope.productInformation.FeefoAverageRating : 1;
                var availability = $scope.productInformation.StockLevel > 0 ? 'InStock' : 'Discontinued';

                var schema = {
                    "@context": "http://schema.org/",
                    "@type": "Product",
                    "name": $scope.productInformation.TVDescription,
                    "image": images,
                    "description": $scope.productInformation.TVDescription,
                    "logo": $scope.productInformation.DefaultThumbnailFilename,
                    "sku": $scope.productInformation.ParentProductSKU,
                    "brand": {
                        "@type": "Thing",
                        "name": $scope.productInformation.BrandName,
                    },
                    "offers": {
                        "@type": "Offer",
                        "priceCurrency": "GBP",
                        "price": $scope.productInformation.Price,
                        "availability": availability,
                        "url": $window.location.href,
                        "seller": {
                            "@type": "Organization",
                            "name": "Hochanda Ltd"
                        }
                    }
                };


                //Add reviews if we have any
                if ($scope.productInformation.FeefoAverageRating != null) {
                    schema.aggregateRating = {
                        "ratingValue": $scope.productInformation.FeefoAverageRating,
                        "bestRating": 5,
                        "worstRating": 1,
                        "reviewCount": $scope.productInformation.FeefoTotalReviews
                    }
                }

                $scope.ld = schema;

                //Now do breadcrumb
                var breadcrumb = {
                    "@context": "http://schema.org/",
                    "@type": "BreadcrumbList",
                    "itemListElement": [{
                        "@type": "ListItem",
                        "position": 1,
                        "name": $scope.productInformation.BrandName,
                        "item": $window.location.origin + "brand/" + $scope.productInformation.BrandId + "/" + GetWebSafeURL($scope.productInformation.BrandName)
                    }]
                };

                if ($scope.productInformation.CategoryLevel1Id != 20) {
                    breadcrumb.itemListElement.push({
                        "@type": "ListItem",
                        "position": 2,
                        "name": $scope.productInformation.CategoryLevel1Name,
                        "item": $window.location.origin + "category/" + $scope.productInformation.CategoryLevel1Id + "/" + GetWebSafeURL($scope.productInformation.CategoryLevel1Name)
                    });
                }
                if ($scope.productInformation.CategoryLevel2Id != 20) {
                    breadcrumb.itemListElement.push({
                        "@type": "ListItem",
                        "position": 3,
                        "name": $scope.productInformation.CategoryLevel2Name,
                        "item": $window.location.origin + "category/" + $scope.productInformation.CategoryLevel2Id + "/" + GetWebSafeURL($scope.productInformation.CategoryLevel2Name)
                    });
                }
                if ($scope.productInformation.CategoryLevel3Id != 20) {
                    breadcrumb.itemListElement.push({
                        "@type": "ListItem",
                        "position": 4,
                        "name": $scope.productInformation.CategoryLevel3Name,
                        "item": $window.location.origin + "category/" + $scope.productInformation.CategoryLevel3Id + "/" + GetWebSafeURL($scope.productInformation.CategoryLevel3Name)
                    });
                }
                if ($scope.productInformation.CategoryLevel4Id != 20) {
                    breadcrumb.itemListElement.push({
                        "@type": "ListItem",
                        "position": 5,
                        "name": $scope.productInformation.CategoryLevel4Name,
                        "item": $window.location.origin + "category/" + $scope.productInformation.CategoryLevel4Id + "/" + GetWebSafeURL($scope.productInformation.CategoryLevel4Name)
                    });
                }
                if ($scope.productInformation.CategoryLevel5Id != 20) {
                    breadcrumb.itemListElement.push({
                        "@type": "ListItem",
                        "position": 6,
                        "name": $scope.productInformation.CategoryLevel5Name,
                        "item": $window.location.origin + "category/" + $scope.productInformation.CategoryLevel5Id + "/" + GetWebSafeURL($scope.productInformation.CategoryLevel5Name)
                    });
                }

                $scope.ldExtra = breadcrumb;

                //Replace the \r\n (new line) with <p>
                $scope.productInformation.WebText = $scope.productInformation.WebText.replace(/(?:\\[rn]|[\r\n]+)+/g, "</p><p>");
                //Also Bought
                $scope.productsAlsoBought = [];
                $scope.relatedItems = [];
                $http({
                    method: 'GET',
                    url: baseUrl + 'Recommendations/GetProductsAlsoBought?productId=' + $scope.productInformation.ParentProductId + '&languageId=' + CommonCode.LanguageId + '&currencyId='
                + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
                    contentType: 'application/json',
                }).then(function (result) {

                    if (result.data != undefined) {
                        $scope.productsAlsoBought = result.data;

                        $http({
                            method: "GET",
                            url: '/Product/LoadCarousel?productScopeName=productsAlsoBought',
                            contentType: "text/plain",
                            dataType: "html"
                        }).then(function (data) {
                            var myEl = angular.element(document.querySelector('#ProductsAlsoBought'));
                            myEl.empty();
                            var partial = $("#ProductsAlsoBought");
                            var htmlelement = angular.element(data.data);
                            var compileAppendedArticleHTML = $compile(htmlelement[0]);
                            var element = compileAppendedArticleHTML($scope);
                            partial.append(element[0]);
                        });
                    }
                });

                //Related products
                $http({
                    method: 'GET',
                    url: baseUrl + 'Recommendations/GetRelatedItems?productId=' + $scope.productInformation.ParentProductId + '&languageId=' + CommonCode.LanguageId + '&currencyId='
                + CommonCode.CurrencyId + '&countryId=' + CommonCode.CountryId,
                    contentType: 'application/json',
                }).then(function (result) {
                    if (result.data != undefined) {
                        $scope.relatedItems = result.data;
                        $http({
                            method: "GET",
                            url: '/Product/LoadCarousel?productScopeName=relatedItems',
                            contentType: "text/plain",
                            dataType: "html"
                        }).then(function (data) {
                            var myEl = angular.element(document.querySelector('#RelatedItems'));
                            myEl.empty();
                            var partial = $("#RelatedItems");
                            var htmlelement = angular.element(data.data);
                            var compileAppendedArticleHTML = $compile(htmlelement[0]);
                            var element = compileAppendedArticleHTML($scope);
                            partial.append(element[0]);
                        });
                    }
                });
                if (result.data.IsPickAndMix) {
                    $scope.ShowPickAndMix = true;
                    //Get The grid
                    $http({
                        method: 'GET',
                        url: '/product/LoadGrid',
                        contentType: 'application/json',
                    }).then(function (result) {
                        if (result.data != undefined) {

                            var grid = result.data;

                            $scope.pickAndMixVariations = [];

                            //Get variation Pickers
                            angular.forEach($scope.productInformation.ProductVariations, function (value, index) {
                                var imageResult = $scope.productInformation.ProductImages.find(function (i) {
                                    return i.VariationId == value.VariationId;
                                });

                                if (imageResult == null) {
                                    imageResult = {};
                                    imageResult.ImageAltText = '';
                                    imageResult.Filename = '';
                                }

                                var variation = {
                                    ParentProductId: $scope.productInformation.ParentProductId,
                                    VariationId: value.VariationId,
                                    VariationName: value.VariationName,
                                    ImageAltText: imageResult.ImageAltText,
                                    ImageFilename: imageResult.Filename,
                                    SelectedCount: 0,
                                    OutOfStock: false
                                }

                                $scope.pickAndMixVariations.push(variation);
                            });

                            //Get the picker
                            $http({
                                method: 'GET',
                                url: '/product/GetPickAndMixVariation',
                                contentType: 'application/json',
                            }).then(function (result) {

                                var htmlElement = angular.element(grid);
                                $(htmlElement[0]).append(angular.element(result.data)[0]);

                                var compileAppendedArticleHTML = $compile(htmlElement[0]);

                                var element = compileAppendedArticleHTML($scope);

                                $("#pickAndMixVariations").append(element[0]);
                            });
                        }
                    });

                }
                else {
                    $scope.ShowProduct = true;

                    var parentProductId = $scope.productInformation.ParentProductId;
                    var variationId = $scope.productInformation.ProductVariations != null && $scope.productInformation.ProductVariations.length > 0 ? $scope.productInformation.ProductVariations[0].VariationId : $scope.productInformation.DefaultVariationId;

                    var variationData = {
                        ParentProductId: parentProductId,
                        VariationId: variationId
                    }

                    $scope.ProductImageVariationId = $scope.productInformation.DefaultVariationId;

                    $scope.ChangeImages(variationId);

                    $http({
                        method: 'POST',
                        url: '/product/GetAddToBasketButton',
                        data: JSON.stringify(variationData),
                        contentType: 'application/json',
                    }).then(function (result) {
                        if (result.data != null) {
                            var htmlElement = angular.element(result.data);
                            var compileAppendedArticleHTML = $compile(htmlElement[0]);
                            var element = compileAppendedArticleHTML($scope);

                            $("#addToBasketContainer").append(element[0]);
                        }
                    });
                }

                $scope.ShowProductInformation = true;

            }
            else {
                $window.location.href = '/';
            }
        }, function () {
            //Error
            $window.location.href = '/errors/generic';
        });
    }
    else {
        $window.location.href = '/errors/generic';
    }

    $scope.getYoutubeVideo = function (url) {
        return $sce.trustAsResourceUrl(url);
    }
}]);

function RemoveSingleOwlComments() {
    var numberOfComments = 0;
    $('data-single-owl').contents().each(function () {
        if (this.nodeType === Node.COMMENT_NODE) {
            if (numberOfComments > 1) {
                $(this).remove();
            }
            numberOfComments++;
        }
    });
}