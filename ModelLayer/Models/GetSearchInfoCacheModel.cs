﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetSearchInfoCacheModel
    {
        public int searchId { get; set; }
        public string searchProductSKU { get; set; }
        public Dictionary<int, string> searchTranslation { get; set; }
        public string searchCategory { get; set; }
        public string searchThumbnail { get; set; }
        public int IsPickAndMixItem { get; set; }
        public int searchBrandId { get; set; }
        public List<int> AllLanguages { get; set; }
        public List<int> AllCountries { get; set; }
    }
}
