﻿using Hoch.Website.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hoch.Website.Models
{
    public static class ViewContextExtension
    {
        public static BaseController BaseController(this ViewContext view)
        {
            BaseController baseController = (BaseController)view.Controller;
            return baseController;
        }
    }
}