﻿using Hoch.Website.Helpers;
using ModelLayer.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Hoch.Website.Controllers.Product_List
{
	public class ProductListingController : BaseController
	{
		[Route("category/{categoryId}/{categoryName}")]
		[MetadataActionFilter(IsCategory = true, IdParameterName = "categoryId")]
		public ActionResult CategoryListing(int categoryId, string categoryName)
		{
			return View("~/Views/product-list/ProductList.cshtml");
		}

		[Route("brand/{brandId}/{brandName}")]
		[MetadataActionFilter(IsBrand = true, IdParameterName = "brandId")]
		public ActionResult BrandListing(string brandId, string brandName)
		{
			return View("~/Views/product-list/ProductList.cshtml");
		}

		[Route("productslisting-brand/{brandId}/{brandname}")]
		public ActionResult ProductListingBrand(int brandId, string brandName)
		{
			return RedirectToRoutePermanent("BrandListing", new { brandId = brandId, brandName = brandName });
		}

		[Route("productslisting-category/{categoryId}/{categoryName}")]
		public ActionResult ProductListingCategory(int categoryId, string categoryName)
		{
			return RedirectToRoutePermanent("CategoryListing", new { categoryId = categoryId, categoryName = categoryName });
		}

		[Route("productslisting-freedom/freedomoffers")]
		public ActionResult ProductListingCategory()
		{
			return RedirectToActionPermanent("PageBuilder", "Sales", new { urlToUse = "freedom-offers" });
		}
	}
}