﻿using ModelLayer.Interfaces;
using ModelLayer.Interfaces.ReferAFriend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.ReferAFriend
{
    public class ReferAFriendWelcomeModel: IReferAFriendWelcomeModel,IModel
    {

        public string firstname { get; set; }
        public Nullable<int> discountvalue { get; set; }
        public string fixedorPercent { get; set; }

    }
}
