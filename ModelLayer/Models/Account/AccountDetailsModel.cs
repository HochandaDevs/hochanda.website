﻿using ModelLayer.Interfaces;
using ModelLayer.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Account
{
    public class AccountDetailsModel: IAccountDetailsModel,IModel
    {
        public int customerid { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string dob { get; set; }
        public int anonymous { get; set; }
        public string phonenumber1 { get; set; }
        public string phonenumber2 { get; set; }
        public string emailaddress { get; set; }
        public Nullable<int> phone1type { get; set; }
        public Nullable<int> phone2type { get; set; }

    }
}
