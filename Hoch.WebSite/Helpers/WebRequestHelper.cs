﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace Hoch.Website.Helpers
{
	public class WebRequestHelper
	{
		public string MyResponse { get; set; }

		public string WebClientRequest(string baseURL)
		{
			try
			{
				using (WebClient client = new WebClient())
				{
					client.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
					client.Headers.Add(HttpRequestHeader.ContentType, "application/json; charset=utf-8");

					MyResponse = client.DownloadString(baseURL);
				}
			}
			catch
			{

			}


			return MyResponse;
		}

		public string GetWebClientRequest(string baseURL)
		{
			using (var myClient = new WebClient())
			{
				using (Stream response = myClient.OpenRead(baseURL))
				{
					// The stream data is used here. 
					MyResponse = response.ToString();
					response.Close();
				}
			}
			return MyResponse;
		}

		public string WebRequestCall(string baseURL)
		{
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(baseURL);
			httpWebRequest.Method = WebRequestMethods.Http.Post;
			httpWebRequest.Accept = "application/json";
			httpWebRequest.KeepAlive = false;


			var response = (HttpWebResponse)httpWebRequest.GetResponse();
			MyResponse = response.ToString();
			return MyResponse;
		}

		public async Task<T> GetItem<T>(string url)
		{
			var uri = new Uri(url);

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.BadRequest);

			try
			{
				using (var client = new HttpClient())
				{
					using (response = await client.GetAsync(uri))
					{
						if (response.IsSuccessStatusCode)
						{
							var content = await response.Content.ReadAsStringAsync();
							var Item = JsonConvert.DeserializeObject<T>(content);
							return Item;
						}
						else
						{
							//Returns the default value for a generic
							return default(T);
						}

					}
				}
			}
			catch
			{
				//Do nothing as the response will still be a failure.
			}

			return default(T);

		}

		public async Task<string> GetItemString(string url)
		{
			var uri = new Uri(url);

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.BadRequest);

			using (var client = new HttpClient())
			{
				using (response = await client.GetAsync(uri))
				{
					if (response.IsSuccessStatusCode)
					{
						var content = await response.Content.ReadAsStringAsync();
						return content;
					}
				}
			}

			return string.Empty;
		}

		public async Task<List<T>> GetListItems<T>(string url)
		{
			var uri = new Uri(url);

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.BadRequest);

			using (var client = new HttpClient())
			{
				using (response = await client.GetAsync(uri))
				{
					if (response.IsSuccessStatusCode)
					{
						var content = await response.Content.ReadAsStringAsync();
						var Items = JsonConvert.DeserializeObject<List<T>>(content);
						return Items;
					}
				}
			}

			throw new Exception(response.ReasonPhrase);
		}

		/// <summary>
		/// Does a HTTP Post with data. The data needs to be transformed into a list of KeyValuePair. new list ==> new KeyValuePair Key,Value = BasketId, 1
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="uri"></param>
		/// <returns></returns>
		public async Task<OutputClass> PostRequest<OutputClass>(Uri uri, IEnumerable<KeyValuePair<string, string>> postData, string username = "", string password = "", string token = "")
		{
			OutputClass getResponse = default(OutputClass);
			//ServicePointManager.Expect100Continue = true;
			try
			{
				using (var client = new HttpClient())
				{
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    {
                        var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password));
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(token))
                        {
                            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                        }
                    }
                    //HTTP Post
                    using (var response = await client.PostAsync(uri, new FormUrlEncodedContent(postData)))
					{
						if (response.IsSuccessStatusCode)
						{
							var responseData = await response.Content.ReadAsStringAsync();

							if (!string.IsNullOrEmpty(responseData))
							{
								getResponse = JsonConvert.DeserializeObject<OutputClass>(responseData);
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				//Do nothing
			}
			return getResponse;
		}

        /// <summary>
        /// Does a HTTP Post with data. The data needs to be transformed into a list of KeyValuePair. new list ==> new KeyValuePair Key,Value = BasketId, 1
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<string> PostRequestString(Uri uri, IEnumerable<KeyValuePair<string, string>> postData)
		{
			string getResponse = string.Empty;
			//ServicePointManager.Expect100Continue = true;
			try
			{
				using (var client = new HttpClient())
				{
					//HTTP Post
					using (var response = await client.PostAsync(uri, new FormUrlEncodedContent(postData)))
					{
						if (response.IsSuccessStatusCode)
						{
							var responseData = await response.Content.ReadAsStringAsync();

							if (!string.IsNullOrEmpty(responseData))
							{
								getResponse = responseData;
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				//Do nothing
			}
			return getResponse;
		}

		public async Task<int> InsertErrorToPostgres(string description, string messagetype, string exception = "", int notification = 0, Exception ex = null)
		{
			int res = -1;
			try
			{
				WebRequestHelper wh = new WebRequestHelper();
				Uri uri = new Uri(ConfigurationManager.AppSettings.Get("WebAPIUrl") + "ErrorLog/CreateErrorLog");

				var postData = new List<KeyValuePair<string, string>>();

				postData.Add(new KeyValuePair<string, string>("Description", description));
				postData.Add(new KeyValuePair<string, string>("Exception", exception));
				postData.Add(new KeyValuePair<string, string>("MessageType", messagetype));
				postData.Add(new KeyValuePair<string, string>("AppId", "1"));
				postData.Add(new KeyValuePair<string, string>("IsDev", ConfigurationManager.AppSettings.Get("IsDev")));
				postData.Add(new KeyValuePair<string, string>("Notification", notification.ToString()));

				string resString = await wh.PostRequestString(uri, postData);

				//TODO once postgres has 100% been fixed
				try
				{
					var message = $"An error has occured. The details are: {description}. {messagetype} ";

					if(!string.IsNullOrEmpty(exception))
					{
						message += exception;
					}

					if (ex != null)
					{
						message += ex.Message;
						message += " " + ex.InnerException.ToString() ;
					}

					Utility.WriteLogMonitor(message);
				}
				catch
				{
					//Do nothing
				}

			}
			catch (Exception)
			{
				res = 0;
			}
			return res;
		}
	}
}
