﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Basket
{
	public class PickAndMixSelectedItemModel
	{
		public int ParentProductId { get; set; }
		public int VariationId { get; set; }
		public int Quantity { get; set; }
		public string VariationProductSKU { get; set; }
		public string VariationName { get; set; }
		public int PickAndMixBuildTableId { get; set; }
	}
}
