﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class DisplayAndUpdateCustomerInfo
    {
        public int AddressId { get; set; }
        public string CompanyName { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNumber { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string StreetAddress3 { get; set; }
        public string StreetAddress4 { get; set; }
        public string StreetAddress5 { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public int CountryId { get; set; }
        public int SelectedBillingAddress { get; set; }
        public int SelectedShippingAddress { get; set; }

        

    }
}
