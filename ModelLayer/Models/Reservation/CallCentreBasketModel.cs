﻿using ModelLayer.Helpers;
using ModelLayer.Models.Account;
using ModelLayer.Models.Basket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Reservation
{
	public class CallCentreBasketModel
	{
		public CallCentreBasketModel()
		{
			CardDetails = new List<CardDetailsModel>();
		}

		/// <summary>
		/// As we have a json ignore on the basketIdToUse, we need to set the value directly.
		/// </summary>
		private string _TempBasketId = string.Empty;

		public string BasketId
		{
			get
			{
				string basketId = string.Empty;

				if (string.IsNullOrEmpty(_TempBasketId))
				{

					if (BasketIdToUse.HasValue)
					{
						basketId = GenericHelper.Instance.ProtectBasketId(BasketIdToUse.Value);
					}
				}
				else
				{
					return _TempBasketId;
				}

				return basketId;
			}
			set
			{
				_TempBasketId = value;
			}

		}

		[JsonIgnore]
		public long? BasketIdToUse { get; set; }

		public int CurrencyId { get; set; }
		public int CountryId { get; set; }
		public int LanguageId { get; set; }
		public string SubTotalWithCurrency { get; set; }
		public decimal? SubTotal { get; set; }
		public string DeliveryWithCurrency { get; set; }
		public decimal? Delivery { get; set; }
		public string TotalTaxWithCurrency { get; set; }
		public decimal? TotalTax { get; set; }
		/// <summary>
		/// TODO: Get tax details back. Speak to Marco
		/// </summary>
		public Dictionary<string,string> TaxDetails { get; set; }
		public string TotalToPayWithCurrency { get; set; }
		public decimal? TotalToPay { get; set; }
		public Guid? CustomerReference { get; set; }
		public string CustomerEmail { get; set; }

		public List<CardDetailsModel> CardDetails { get; set; }

		public int ShippingAddressId { get; set; }
		public int BillingAddressId { get; set; }
		public int SelectedCardIBLookup { get; set; }

		public AccountDetailsModel CustomerAccountDetails { get; set; }

		public DisplayAndUpdateCustomerInfo AddressDetails { get; set; }

		public string NewCardUrl { get; set; }
	}

	public class CallCentreRequestBasketModel
	{
		public Guid ReservationId { get; set; }
		public int ShippingAddressId { get; set; }
		public int BillingAddressId { get; set; }

	}
}
