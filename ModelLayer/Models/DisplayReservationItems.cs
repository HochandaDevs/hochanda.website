﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public partial class DisplayReservationItems
    {
        //public int Id { get; set; }
        public  int ReservationItemId { get; set; }
        public int ParentProductID { get; set; }
        public int VariationID { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public int FlexiBuyItem { get; set; }
        public decimal ItemTotal { get; set; }
        public string TvDescription { get; set; }
        public string VariationName { get; set; }
        public string VariationProductSku { get; set; }
        public string ImageName { get; set; }
        public int FlexiBuy { get; set; }
        public int FlexiBuyInstallments { get; set; }
        public int ServiceID { get; set; }
        public int ServiceID_Default { get; set; }
        public int PandpruleApplied { get; set; }
        public int FreeItem { get; set; }
        public int PickNMixItem { get; set; }
        public int AuctionItem { get; set; }
        public int PendingStatus { get; set; }
    }
}
