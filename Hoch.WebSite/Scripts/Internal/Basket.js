﻿app.controller("BasketController", function ($scope, $http, $compile, $rootScope, $filter, CommonCode, $cookies) {
    //var baseCookie = $cookies.get('BaseCookieSettings');
    var basketId = localStorage.getItem("basketId");
    var isFlexiVal = -1;
    var reservItemIDVal = 0;
    var isItemFlexiVal = -1;
    var checked = null;
    var performCalculation = true;


    $scope.IsLoggedIn = 0;

    var keepMeSignedIn = $cookies.get('keepMeSignedIn');

    if (keepMeSignedIn != null) {
        $scope.IsLoggedIn = 1;
    }

    CommonCode.getBaseCookieSettings();

    $scope.UpdateCustomerAndItems = function () {
        //Add anything here we need for after logged in.
        UpdateCustomer();
    }

    function UpdateCustomer() {

        SetLogin();

        if ($scope.signinData != null && $scope.signinData.CustomerLookupId != null && $scope.signinData.CustomerLookupId != "") {
            var addressParams = {
                BasketId: basketId,
                CustomerRef: $scope.signinData.CustomerLookupId,
                ShippingID: 0,
                BillingID: 0,
                CardLookupID: 0
            };
            $http({
                method: 'POST',
                url: baseUrl + "Reservations/UpdateReservationByCustomer",
                headers: {
                    "Content-Type": 'application/json'
                },
                data: JSON.stringify(addressParams),
            }).then(function (result) {
                if (result.data != undefined) {
                    performCalculation = true;
                    DisplayBasketItems(performCalculation)
                }

            });
        }
        else {
            DisplayBasketItems(performCalculation)
        }
    }

    $scope.ShowExpired = function () {

        var expired = GetParameterByName("expiredBasket") == "true";

        if (expired) {
            return true;
        }

        return false;
    }

    function SetLogin() {
        var signinData = $cookies.get("keepMeSignedIn");

        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }
    }

    SetLogin();

    $scope.baseCurrency = CommonCode.CurrencySymbol;
    $scope.flexiMinAmount = 60;
    $rootScope.buttonCheck = "-1";
    $scope.redirectUrl = "";

    UpdateCustomer();


    $rootScope.$on('modalVoucher', function (event, value) {
        var signinData = $cookies.get("keepMeSignedIn");
        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }//modal might not get cookie value 
        $rootScope.voucherApplied = value.VoucherApplied;
        $rootScope.voucherDiscount = value.AmountSaved;
        $rootScope.vouchercode = value.VoucherCode;
        $rootScope.displayDiscount = value.AmountSavedWithCurrency;

        performCalculation = true;
        DisplayBasketItems(performCalculation);


    });


    if (window.location.href.indexOf('basket') != -1) {
        $rootScope.basketPaginationCheck = 1;
        $rootScope.UpsellClick = 0;
    }

    //Retain the checkbox checked value after page refresh
    $scope.flexiChecked = function flexiChecked(event) {
        checked = event.target.checked;
        if (checked == true) {
            $("#FlexiTable").slideToggle("slow");
            isFlexiVal = 1;
        }
        else {
            $("#FlexiTable").slideToggle("slow");
            isFlexiVal = 0;
        }

        isItemFlexiVal = 0;
        reservItemIDVal = 0;

        DisplayBasketItems(true);
    }

    $scope.FlexiItemChecked = function (event) {
        var checkBoxID = event.toElement.id;
        if (event.target.checked == true) {
            isItemFlexiVal = 1;

        }
        else {
            if (checked == true) {
                reservItemIDVal = 0;
            }

            isItemFlexiVal = 0;

        }
        reservItemIDVal = checkBoxID.split('_')[1];
        performCalculation = true;
        if (basketId != null && basketId != undefined) {
            DisplayBasketItems(performCalculation);
        }

    }

    $scope.UpdateItem = function (productId, variationID, quantity, type, reservationItemId) {
        //add null checks
        var parametersToUpdate = {
            ParentProductId: productId,
            VariationId: variationID,
            Quantity: type == 1 ? 1 : type == -1 ? "-" + quantity : "-" + 1,//for adding 1 removing 1 and removing entire qty
            LanguageId: CommonCode.LanguageId,
            CurrencyId: CommonCode.CurrencyId,
            CountryId: CommonCode.CountryId,
            BasketId: basketId,
            PickAndMixIdentifier: 0,
            ReservationItemId: reservationItemId
        }

        $http({
            method: 'POST',
            url: baseUrl + 'reservations/UpdateItemOnReservation',
            data: parametersToUpdate,
            headers: {
                "Content-Type": 'application/json'
            },
        }).then(function (result) {
            if (result.data !== undefined && result.data !== null) {

                var basketData = result.data;

                localStorage.setItem("basketCount", basketData.BasketCount);

                performCalculation = true;

                if (basketData.BasketCount == 0) {
                    ClearBasket();
                    location.reload();
                }
                else {
                    DisplayBasketItems(performCalculation);
                }
            }

        });


    }

    function DisplayBasketItems(performCalculation) {

        var basketParameters = {
            BasketID: basketId,
            PerformCalculation: performCalculation,
            CountryID: CommonCode.CountryId,
            CurrencyID: CommonCode.CurrencyId,
            LanguageID: CommonCode.LanguageId,
            IsFlexi: isFlexiVal,
            ReservItemID: reservItemIDVal,
            IsItemFlexi: isItemFlexiVal
        };

        $http({
            method: 'POST',
            url: baseUrl + "/Reservations/GetBasketDisplay",
            headers: {
                "Content-Type": 'application/json'
            },
            data: JSON.stringify(basketParameters),
        }).then(function (result) {

            isFlexiVal = -1;
            if (result.data !== undefined && result.data !== null) {
                var basketData = result.data;
                $scope.reservationData = basketData.displayCheckout.reservationModel[0];
                if ($scope.reservationData != undefined && $scope.reservationData != null) {


                    $scope.flexiBasketAvailable = $scope.reservationData.FlexiBasketAvailable;
                    $scope.numberofInstalments = $scope.reservationData.FlexiBasketInstallments;
                    $scope.toPay = $scope.reservationData.ToPayTodayWithCurrency;
                    $scope.subTotalAmount = $scope.reservationData.SubTotalWithCurrency;
                    $scope.deliveryAmount = $scope.reservationData.DeliveryWithCurrency;
                    $scope.nextInstalmentAmount = $scope.reservationData.FBPreviewWithCurrency;
                    $scope.flexiNnextInstalment = $scope.reservationData.ToPayFlexiBuyWithCurrency;
                    $scope.FlexiBasketSingleInstalment = $scope.reservationData.ToPayFlexiBuySingleWithCurrency;
                    $scope.reservationItemData = basketData.displayCheckout.reservationItemsModel;
                    $scope.filterReservationITemData = $filter('filter')($scope.reservationItemData, function (d) { return d.ParentProductID != 23 && d.ParentProductID != 22 && d.ParentProductID != 19 && d.Quantity != 0 }).length
                    $scope.basketDataLength = $scope.reservationItemData.length;
                    $scope.checkboxChecked = $scope.reservationData.FlexiBasketSelected;
                    $scope.errorMessage = $scope.reservationData.MessageToDisplay;//for now only message to display later we need to consider message code and display accordingly

                    if ($scope.reservationData.VoucherUsed) {
                        $rootScope.voucherApplied = $scope.reservationData.VoucherUsed;
                        $rootScope.vouchercode = $scope.reservationData.VoucherCode;
                        $rootScope.voucherDiscount = $scope.reservationData.VoucherDiscount;
                        $rootScope.displayDiscount = $scope.reservationData.VoucherDiscountString;
                    }


                    getFlexiOrderData(checked);
                    //showFlexiDiv();
                    CommonCode.GetMerchantAccount(CommonCode.CurrencyId).then(function (merchant) {
                        if (merchant.data != null) {
                            var items = $scope.reservationItemData.filter(function (item) {
                                return ((item.ParentProductID != 22 && item.Quantity > 0) && (item.ParentProductID != 23 && item.ParentProductID != 19 && item.Quantity > 0));
                            });
                            var list = $rootScope.FbPixelScript(items, merchant.data.lettercode3, 2);
                            angular.element("head").append(list[0]);
                        }
                    });
                }
            }
            else {
                $scope.errorMessage = "1";
            }

        },

        function errorCallback(result) {

            $scope.errorMessage = result.data;
        });


    }

    function getFlexiOrderData(checked) {
        if (checked != null || $scope.checkboxChecked == true) {


            $scope.FlexiOrderData = [];
            var FlexiDate = '';
            var keyValuePair = '';

            for (var i = 0; i < $scope.numberofInstalments; i++) {
                var CurrentInstallment;
                FlexiDate = new Date().setMonth(new Date().getMonth() + i);
                if (i == 0) {
                    CurrentInstallment = $scope.toPay;
                    FlexiDate = 'Today (including Delivery)'
                }
                else {
                    if ($scope.checkboxChecked == true) {
                        CurrentInstallment = $scope.FlexiBasketSingleInstalment;
                    }
                    else {
                        CurrentInstallment = $scope.nextInstalmentAmount;
                    }

                }

                $scope.FlexiOrderData.push({ "FlexiDate": FlexiDate, "CurrentInstallment": CurrentInstallment });

            }
        }

    }


    $scope.setPartialName = function (name) {
        $rootScope.partialName = name;
        var signinData = $cookies.get("keepMeSignedIn");
        if (signinData != null) {
            signinData = JSON.parse(decodeURIComponent(signinData));
            if (signinData.Email != "" && signinData.CustomerLookupId != "") {
                $scope.signinData = signinData;
            }
        }//added this for voucher
        if ($scope.signinData != null || $scope.signinData != undefined) {
            $rootScope.partialCheck = 1

            $rootScope.partialName = "voucher_template";
        }
        else {
            $rootScope.partialCheck = 0;
            $rootScope.partialName = "login_template";
            $rootScope.SetLoginRedirect(false, $scope.UpdateCustomerAndItems);
        }


    }



    $scope.updateTc = function (event) {


        $rootScope.buttonCheck = event.target.checked == true ? "1" : "";



    }

    $scope.ProceedtoCheckout = function ProceedtoCheckout() {

        if (isExternalVariable == null) {
            isExternalVariable = false;
        }

        if ($rootScope.buttonCheck == "1") {

            var location = "/checkout";

            if (isExternalVariable) {
                location = "/checkout?isexternal=true";
            }

            window.location.href = location;
        }
        else {
            $rootScope.buttonCheck = "0";
        }

    }

});