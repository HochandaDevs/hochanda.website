﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models.Recommendations
{
    public class RecommendationsResponseModel
    {
        public int CountOfParentProductId { get; set; }
        public int ParentProductId { get; set; }
    }
}
