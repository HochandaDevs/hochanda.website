﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Models
{
    public class GetCustomerCreditRequestModel
    {
        public Guid CustomerLookupId { get; set; }
        public int CurrencyId { get; set; }
        public string CustomerEmail { get; set; }
    }
}
